function ret = evalfirstreturnlast( varargin );
    assert( numel(varargin)>=1, 'evalfirstreturnlast:nargin', 'Function needs at least one input argument' );
    ret = varargin{end};
    try;
        evalin('caller',[varargin{1} ';']);
    catch me;
        if( isempty(me.identifier) );
            id = 'TTEST:evalat:throw'; 
        else;
            id = me.identifier; end;
        msg = ['A TTEST-evalat command threw an error.' newline 'Message:' newline me.message];
        warning( id, '\n%s', msg ); end;
end

function dummy;  %#ok<DEFNU> %Generates an error, if the 'end' of a function is missing. 
    assert( false, 'We never want to get here. This is a programming error.' );
end 