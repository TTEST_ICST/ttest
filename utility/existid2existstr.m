function [ str ] = existid2existstr( id );
    switch( id );
        case -1; str = 'any';

        case  0; str = 'does not exist';

        case  1; str = 'var';
        case  2; str = 'file';
        case  5; str = 'builtin';
        case  8; str = 'class';
        case  7; str = 'dir';
        otherwise; error( 'TTEST:EXIST', 'Wrong number for type given.' ); end;
end