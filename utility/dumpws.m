function st = dumpws( overwriteall )
% dumps (saves) the current workspace
% [] = dumpws( [overwriteflag] ) 
% dumps the current workspace to base. 
% if flag is set, variables are silently overwritten
%
% [ st ] = dumpws( ) 
% dumps the current workspace to the struct st
%
% E.g.: a = 1; st = dumpws;
%


    ws = evalin( 'caller', 'whos()' );
    if( nargin<=0 || isempty(overwriteall) );
        overwriteall = false; end;
    for i = 1:numel( ws );
        continueflag = false;
        val = evalin( 'caller', ws(i).name );
        if( nargout==0 );
            f = evalin ( 'base', ['exist( ''' ws(i).name ''', ''var'' );'] );
            if( f~=0 && ~overwriteall );
                fprintf( 'Do you want to overwrite the variable: %s\n',  ws(i).name );
                while( true );
                    in = input( '(y)es/(n)o/a(p)pend number/(a)ll overwrite/(d)isplay content/(s)top dumping: ', 's' ); 
                    switch in;
                        case 'n'; continueflag = true; break;
                        case 'y'; break;
                        case 'd'; disp( val );
                        case 'a'; overwriteall = true; break;
                        case 'p'; 
                            n = 0;
                            while( true );
                                n = n + 1;
                                newname = [ws(n).name '_' num2str(n)];
                                if( ~evalin( 'base', ['exist( ''' newname ''' );'] ) );
                                    break; end; end;
                            fprintf( 'Old variable name: %s, new variable name %s\n', ws(i).name, newname );
                            ws(i).name = newname;
                            break;
                        case 's'; return;
                        otherwise; fprintf( 'Wrong input\n'); end; end; 
                if( continueflag );
                    continue; end; end; 
            assignin( 'base', ws(i).name, val ); 
            fprintf( 'Dumped variable to base workspace: %s\n', ws(i).name );
        else;
            st.(ws(i).name) = val; end; end;

end

function dummy; end %#ok<DEFNU> %Generates an error, if the 'end' of a function is missing.   

% This Source Code Form is subject to the terms of the Mozilla Public
% License, v. 2.0. If a copy of the MPL was not distributed with this
% file, You can obtain one at https://mozilla.org/MPL/2.0/.
