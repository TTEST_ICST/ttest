function [ x ] = limit( varargin );
% Returns interesting values for types
% [ x ] = limit( 'like', var1, ..., varn );
% [ x ] = limit( type1, ..., typen );
%
% Input:
%	vari   any variable, whos type is used
%   typen   string, name of the type
% 
% Output:
%	x      cell array, interesting values
%
% Notes:
%	The following types are possible:
%     1. double, single, int8, int16, int32, int64, uint8, uint16, uint32, uint64
%     2. cdouble, csingle denoting complex variables
%
% 

% XX move into ttest namespace, maybe even better, make to strategy

 %#ok<*AGROW>
    if( isequal(varargin{1},'like') );
        varargin = varargin(2:end);
        for i = 1:numel( varargin );
            if( ~isreal(varargin{i}) );
                varargin{i} = ['c' class( varargin{i} )];
            else;
                varargin{i} = class( varargin{i} ); end; end; end;
    
    for i = numel(varargin):-1:1;
        switch varargin{i};
            case 'float';
                varargin{i} = 'single';
            case 'cfloat';
                varargin{i} = 'csingle';
            case 'floatingpoint';
                varargin(i) = [];
                varargin = [varargin 'single' 'double'];
            case 'cfloatingpoint';
                varargin(i) = [];
                varargin = [varargin 'csingle' 'cdouble'];                
            case 'int';
                varargin(i) = [];
                varargin = [varargin 'int8','int16','int32','int64']; 
            case 'uint';
                varargin(i) = [];
                varargin = [varargin 'uint8','uint16','uint32','uint64'];                 
            otherwise;
                end;  % do nothing
                
    end
    
    x = {};
    for i = 1:numel( varargin );
        if( varargin{i}(1)=='c' );
            complexflag = true; 
            varargin{i}(1) = [];
        else;
            complexflag = false; end;
        switch varargin{i};
            case {'double'};
                y = {0 1/-inf 1 -1 2 ...
                     eps flintmax flintmax-1 flintmax+2 realmax realmin ...
                     2.2250738585072009e-308 4.9406564584124654e-324 ...
                     inf -inf nan };
            case 'single';
                y = {single(0) single(1/-inf) single(1) single(-1) single(2) ...
                     eps('single') flintmax('single') flintmax('single')-1 flintmax('single')+2 realmax('single') realmin('single') ...
                     single(1.17549421e-38) single(1.40129846e-45) ...
                     single(inf) single(-inf) single(nan) };
            case {'int8','int16','int32','int64','uint8','uint16','uint32','uint64'};
                y = {cast(0,varargin{i}) cast(1,varargin{i}) cast(-1,varargin{i}) cast(2,varargin{i}) ...
                     intmin(varargin{i}) intmin(varargin{i})+1 intmax(varargin{i}) intmax(varargin{i})-1};
            otherwise;
                warning( 'limit:type', 'Unsupported type given. Type: %s', varargin{i} );
                end; 
        if( complexflag );
            y = ttest.allcomb( y, y ); 
            for n = 1:size( y, 1 );
                y{n,1} = y{n,1} + 1i*y{n,2}; end;
            y(:,2) = []; end;
        x = [x y{:}]; end; %do nothing
    
       
end

function dummy; end %#ok<DEFNU> %Generates an error, if the 'end' of a function is missing.   
