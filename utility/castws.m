function [ nerr, firstws_, lastws_ ] = castws( varargin );
% (experimental) casts all variables in the workspace to the given type
% [ nerr, firstws, lastws ] = castws( 'like',p, [options] )
% [ nerr, firswst, lastws ] = castws( str, [options] )
%
% Input:
%   p       variable, target type
%   str     string, name of target type
%
% Options:
%   'first'             loads the first saved workspace (i.e. the workspace before at the moment of the first call of castws)
%   'last'              loads the last saved workspace (i.e. the workspace before the last call of castws)
%   'clear'             clears the first workspace
%   'verbose',val       integer, default=1, verbose level
%   'skip',val          string or cell array of strings, default = 'ttest_', defines pattern to exclude variables
%                       if a variable contains one
%
% Output:
%   nerr                integer, number of variables which could not totally casted. Is zero if everything worked out.
%   firstws             struct, the saved 'first' workspace
%   lastws              struct, the saved 'last' workspace
%   
% Notes:
%   - By default, variables whose name start with 'ttest_' are not casted
%     The variable ans is never casted on Octave
%   - Function works also in static workspaces
%   - If some variables could not be casted, a message is printed after the cast
%   - Variables must be castable using cast( variable, classname ) OR classname(variable)
%   - Variables in caller workspace are overwritten using assignin.
%
% E.g.: st.c = 4; castws( 'single' );
%

persistent firstws;
persistent lastws;
    
    [verbose,varargin] = ttest.parsem( {'verbose','v'}, varargin, 1 );
    [skipvar,varargin] = ttest.parsem( {'skip'}, varargin, {'ttest_'} );
    [clearflag,varargin] = ttest.parsem( 'clear', varargin );
    [firstflat,varargin] = ttest.parsem( 'first', varargin );
    [lastflag,varargin] = ttest.parsem( 'last', varargin );
    [dryrun,varargin] = ttest.parsem( 'dryrun', varargin );
    if( ~iscell(skipvar) );
        skipvar = {skipvar}; end;

    if( clearflag );
        firstws = [];
        
    elseif( firstflat );
        fn = fieldnames( firstws );
        if( isempty(fn) );
            warning( 'castws:empty', 'First workspace is not set yet. Cannot be loaded.' ); 
            return; end;
        for i = 1:numel( fn );
            if( ~dryrun );
                assignin( 'caller', fn{i}, firstws.(fn{i}) ); end; end;
        
    elseif( lastflag );
        fn = fieldnames( lastws );
        if( isempty(fn) );
            warning( 'castws:empty', 'Last workspace is not set yet. Cannot be loaded.' ); 
            return; end;
        for i = 1:numel( fn );
            if( ~dryrun );
                assignin( 'caller', fn{i}, lastws.(fn{i}) ); end; end;
        
    else;
        if( isempty(lastws) );
            lastws = struct; end;
        ws = evalin( 'caller', 'whos()' );

        if( numel(varargin)==2 && strcmp(varargin{1},'like') );
            type = class( varargin{2} );
        elseif( numel(varargin)==1 && (isstring(varargin{1})||ischar(varargin{1})) )
            type = varargin{1}; 
        else;
            error( 'castws:input', 'Wrong input given' ); end;

        failedvar = {};
        for i = 1:numel( ws );
            if( contains_t(ws(i).name,skipvar) || ...
                    isoctave && strcmp(ws(i).name,'ans') );
                continue; end;
            
            var = evalin( 'caller', ws(i).name );
            lastws.(ws(i).name) = var;
            [var,successflag] = cast_preworker( var, type );
            if( successflag );
                if( ~dryrun );
                    assignin( 'caller', ws(i).name, var ); end;
            else;
                failedvar{end+1} = ws(i).name; end; end; %#ok<AGROW>

        nerr = numel( failedvar );
        if( verbose>=1 && nerr>0 );
            fprintf( 'Failed to cast the following variables:' );
            disp( failedvar ); end; 
        
        if( isempty(firstws) );
            firstws = lastws; end; end;
    
    firstws_ = firstws;
    lastws_ = lastws;    

end

function [ var, successflag ] = cast_preworker( var, type );
    if( isstruct(var) );
        [var,successflag] = cast_struct( var, type );
    elseif( iscell(var) );
        [var,successflag] = cast_cell( var, type );
    elseif( isnumeric(var) || islogical(var) || ischar(var) || isa(var,'sym') );
        [var,successflag] = cast_numeric( var, type );
    elseif( ismatlab && istable(var) );
        [var,successflag] = cast_table( var, type );
    else
        successflag = false; end;
end

function [ var, successflag ] = cast_struct( var, type );
    successflag = true;
    fn = fieldnames( var );
    for k = 1:numel( fn )
        [var.(fn{k}),fl] = cast_preworker( var.(fn{k}), type );
        successflag = successflag && fl; end;
end

function [ var, successflag ] = cast_cell( var, type );
    successflag = true;
    for k = 1:numel( var )
        [var{k},fl] = cast_preworker( var{k}, type );
        successflag = successflag && fl; end;
end

function [ var, successflag ] = cast_table( var, type );
    successflag = true;
    tn = var.Properties.VariableNames;
    for k = 1:numel( var )
        try;
            var = convertvars( var, tn{k}, type );
        catch;
            successflag = false; end; end;
end


function [ var, successflag ] = cast_numeric( var, type );
    successflag = true;
    try;
        var = cast( var, type );        
    catch me1;
        % XX check me
        try;
            var = eval( [type '(var);'] );
        catch me2;
            try;
                var1 = double( var );
                cast( var1, type );
                warning( 'castws:doublecast', 'Direct cast failed. Cast was done via intermediate cast over ''double''.' );
            catch;
                try;
                    var1 = eval( 'double( var );' );
                    var = eval( [type '(var1);'] );
                    warning( 'castws:doublecast', 'Direct cast failed. Cast was done via intermediate cast over ''double''.' );
                catch;
                    fprintf( 2, 'Cast failed:\n  %s\n  %s\n', me1.message, me2.message );
                    successflag = false;
                    end; end; end; end;
end

function ret = contains_t( str, pattern );
    str = char( str );
    ret = true;
    for j = 1:numel( pattern );
        if( contains(str,char(pattern{j})) );
            return; end; end;
    ret = false;
end
    
    
function dummy; end %#ok<DEFNU> %Generates an error, if the 'end' of a function is missing. 

% This Source Code Form is subject to the terms of the Mozilla Public
% License, v. 2.0. If a copy of the MPL was not distributed with this
% file, You can obtain one at https://mozilla.org/MPL/2.0/.
