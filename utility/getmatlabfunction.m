function [ h ] = getmatlabfunction( name, pattern )
% Returns a handle to the original function in case there exists an overload
% [ h ] = getmatlabfunction( name, [pattern] )
%
% On Matlab:
% returns a handle to the original matlab function, even when it is overloaded
% if there are more candidates, a function is chosen in whose path the pattern is contained
% pattern defaults to ''
%
% On Octave:
% This function only works on Octave 6.3 and only is able to return handles to the functions:
%    addpath, dbclear, dbstop, path, restoredefaultpath, rmpath

if( nargin<=1 );
    pattern = ''; end;

if( exist( 'OCTAVE_VERSION', 'builtin' ) )
    h = getmatlabfunction_octave( name, pattern );
else;
    h = getmatlabfunction_matlab( name, pattern ); end;

end

function [ h ] = getmatlabfunction_matlab( name, pattern )

    n = which( name, '-all' );
    m = matlabroot;
    for i = numel( n ):-1:1;
        if( ~isempty(pattern) && ~any(strfind(n{i},pattern)) );
            n(i) = [];
        elseif( any(strfind(n{i},'built-in')) );
            h = @(varargin) builtin( name, varargin{:} );
            return;
        elseif( any(n{i}=='@') );
            n(i) = [];
        elseif( any(strfind(n{i},m)) );
            continue;
        else;
            n(i) = []; end; end;

    length = cellfun( 'length', n );
    [~,idx] = min( length );
    folder = fileparts( n{idx} );
    olddir = cd( folder );
    cleandir = onCleanup( @() cd(olddir) );
    h = eval( ['@' name] );

end

function [ h ] = getmatlabfunction_octave( name, pattern )
    assert( isempty(pattern), 'TTEST:getmatlabfunction', 'On Octave no ''pattern'' can be passed.' );
    switch name;
        case 'addpath';
            h = @(varargin) builtin( 'addpath', varargin{:} );
        case 'dbclear';
            h = @(varargin) builtin( 'dbclear', varargin{:} );
        case 'dbstop';
            h = @(varargin) builtin( 'dbstop', varargin{:} );
        case 'path';
            h = @(varargin) builtin( 'path', varargin{:} );
        case 'restoredefaultpath';
            h = @(varargin) builtin( 'restoredefaultpath', varargin{:} );
        case 'rmpath'; 
            h = @(varargin) builtin( 'rmpath', varargin{:} );
        otherwise;
            error( 'TTEST:getmatlabfunction', 'Unsupported function name passed. This restriction applies to Octave only.' ); end;
    
end

function dummy; end %#ok<DEFNU> %Generates an error, if the 'end' of a function is missing.
