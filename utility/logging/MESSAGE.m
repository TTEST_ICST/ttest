function [] = MESSAGE( varargin );

    shift = 0;
    
    % get test_id
    %%%%%%%%%%%%%%
    if( numel(varargin)>=1 && isa(varargin{1},'ttest_id_c') );
        id = varargin{1};
        varargin(1) = [];
        shift = shift + 1;
    elseif( evalin( 'caller', 'exist(''ttest_id'',''var'');' ) );  % id not given
        id = evalin( 'caller', 'ttest_id;' );
    else
        id = ttest_id_c( 0 ); end;
    
    maxdisp = TTEST( id, 'maxdisp' );

    % parse input
    %%%%%%%%%%%%%%
    if( numel(varargin)==0 || strcmpi(varargin{1},'ttestclear') || strcmpi(varargin{1},'ttest_clear') );
        type = 'x';  % clear
    elseif( strcmpi(varargin{1},'ttestprintf') || strcmpi(varargin{1},'ttest_printf') || strcmpi(varargin{1},'ttestfprintf') || strcmpi(varargin{1},'ttest_fprintf') );
        shift = shift + 1;
        varargin(1) = [];
        type = 'f';  % type = 'fprintf'
    elseif( strcmpi(varargin{1},'ttestcapture') || strcmpi(varargin{1},'ttest_capture') );
        shift = shift + 1;
        varargin(1) = [];
        type = 'c';  % type = 'capture'
    elseif( ~isstring(varargin{1}) && ~ischar(varargin{1}) )
        type = 'c'; 
    else;
        flag = true;
        for i = 1:numel( varargin );
            if( isempty(inputname(i+shift)) );
                flag = false;
                break; end; end;
        if( flag );
            type = 'c';
        else;
            type = 'f'; end; end;

    % print out
    %%%%%%%%%%%%%%%%
    
    str = '';
    if( type=='x' );
        %do nothing
    elseif( type=='c' );
        for i = 1:numel( varargin );
            name = inputname( i + shift );
            if( ~isempty(name) );
                str = [str sprintf( '%s = %s\n', name, TTEST_DISP( maxdisp, varargin{i} ) )];  %#ok<AGROW>
            else;
                str = [str sprintf( '%s\n', TTEST_DISP( maxdisp, varargin{i} ) )]; end; end; %#ok<AGROW>
    else;
        [str,errmsg] = sprintf( varargin{:} ); 
        if( ~isempty(errmsg) );
            error( 'TTEST:message', '%s', errmsg ); end; end;
    
    if( ~isempty(str) );
        assignin( 'caller', 'ttest_message_error', str );
    else;
        evalin( 'caller', 'clear ttest_message_error' ); end;
end

function dummy; end %#ok<DEFNU> %Generates an error, if the 'end' of a function is missing. 

% This Source Code Form is subject to the terms of the Mozilla Public
% License, v. 2.0. If a copy of the MPL was not distributed with this
% file, You can obtain one at https://mozilla.org/MPL/2.0/.
