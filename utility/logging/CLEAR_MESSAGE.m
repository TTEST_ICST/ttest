function [] = CLEAR_MESSAGE;    
    evalin( 'caller', 'clear ttest_message_error' );
end

function TTEST_dummy; end %#ok<DEFNU> %Generates an error, if the 'end' of a function is missing. 

% This Source Code Form is subject to the terms of the Mozilla Public
% License, v. 2.0. If a copy of the MPL was not distributed with this
% file, You can obtain one at https://mozilla.org/MPL/2.0/.
