function [] = TRACE( varargin );

    if( isoctave );
        warning( 'TTEST:TRACE', 'The TRACE function does not work on Octave.' ); end;

    shift = 1:numel( varargin );
    % get function handles
    %%%%%%%%%%%%%%%%%%%%%%%%
    idx = cellfun( 'isclass', varargin, 'function_handle' );
    if( any(idx) );
        shift(idx) = [];
        handle = varargin(idx); 
        varargin(idx) = []; end;
    assert( ~isempty(handle), 'ttest:trace', 'No function handle given, but at least one must be given.' );

    % get test_id
    %%%%%%%%%%%%%%%
    if( numel(varargin)>=2 && isa(varargin{1},'ttest_id_c') );
        id = varargin{1};
        varargin(1) = [];
    elseif( evalin( 'caller', 'exist(''ttest_id'',''var'');' ) );  % id not given
        id = evalin( 'caller', 'ttest_id;' );
    else
        id = ttest_id_c( 0 ); end;
    
    maxdisp = TTEST( id, 'maxdisp' );
    
    
    % check if we have message given, or if we take it from caller
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    if( isempty(varargin) );
        try;
            str = evalin( 'caller', 'ttest_message_error' );
        catch;
            error( 'TRACE:nomessage', 'No message set.' ); end;
    else;
        % parse capture-style
        %%%%%%%%%%%%%%%%%%%%%%%%
        type = 'f';
        if( numel(varargin)==0 );
            return; end;
        if( strcmpi(varargin{1},'ttestprintf') || strcmpi(varargin{1},'ttest_printf') || strcmpi(varargin{1},'ttestfprintf') || strcmpi(varargin{1},'ttest_fprintf') );
            shift(1) = [];
            varargin(1) = [];
            type = 'f';  % type = 'fprintf'
        elseif( strcmpi(varargin{1},'ttestcapture') || strcmpi(varargin{1},'ttest_capture') );
            shift(1) = [];
            varargin(1) = [];
            type = 'c';  % type = 'capture'
        elseif( ~isstring(varargin{1}) && ~ischar(varargin{1}) )
            type = 'c';
        else;
            flag = true;
            for i = 1:numel( varargin );
                if( isempty(inputname(shift(i))) );
                    flag = false;
                    break; end; end;
            if( flag );
                type = 'c'; end; end;    

        % make message string
        %%%%%%%%%%%%%%%%%%%%%%%
        str = ['Trace:' newline];
        if( type=='c' );
            for i = 1:numel( varargin );
                name = inputname( shift(i) );
                if( ~isempty(name) );
                    str = [str sprintf( '  %s : %s\n', name, TTEST_DISP( maxdisp, varargin{i} ) )];  %#ok<AGROW>
                else;
                    str = [str sprintf( '  %s\n', TTEST_DISP( maxdisp, varargin{i} ) )]; end; end;  %#ok<AGROW>
        else;
            [str,errmsg] = sprintf( varargin{:} ); 
            if( ~isempty(errmsg) );
                error( 'TTEST:TRACE', errmsg ); end; end; end;
        
        TTEST( id, 'message', str ); 
    
    
    % call function
    %%%%%%%%%%%%%%%%%
    for i = 1:numel( handle );
        h = ['@() { ''' id() ''', handle{' num2str(i) '}() }']; 
        %h = ['@(' id() ') handle{' num2str(i) '}()']; 
        feval( eval(h) ); end;
    
    
    
end

function TTEST_dummy; end  %#ok<DEFNU> %Generates an error, if the 'end' of a function is missing. 

% This Source Code Form is subject to the terms of the Mozilla Public
% License, v. 2.0. If a copy of the MPL was not distributed with this
% file, You can obtain one at https://mozilla.org/MPL/2.0/.
