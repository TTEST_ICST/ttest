function [ ret ] = errorat( funname, numline )
% This function tries to generate an error in the function funname at line numline 
% [] = errorat( funname )
% [] = errorat( funname, numline )
%
% Input: 
%   funname     char-array, the m-function in which to generate an error
%   numline     integer, the line where one shall try to inject the error
%               if not given, the "injected error" is deleted
%
% Note:
%   This function is just a proof-of-concept.
%   Do not call this function without arguments, since it will clear all variables in your workspace
%
% Example:
%   errorat( 'spy', 40 );  %inject error in 'spy.m' at line 40
%   spy; %run spy and see it fail somewhere after line 40
%   errorat( 'spy' ); %remove injected error
%
% Credits to:  Per Isakson’s tracer4m for the idea

%TT EXPECT_NTHROW( @() errorat('spy',40) );
%TT a = 2;
%TT EXPECT_EQ( a, 2 );

    if( nargin==0 );
        evalin( 'caller', 'clear' );
        ret = false;
    elseif( nargin==1 );
        dbclear( 'in',funname );
    elseif( nargin==2 );
        dbstop( 'in',funname, 'at', num2str(numline), 'if','errorat' ); end; 
    
end
