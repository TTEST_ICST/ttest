function [ ret ] = onpath( folder );
% checks whether a folder is on the matlab path
    p1 = path;
    p2 = strsplit( p1, ';' );
    if( ispc );
        ret = any( strcmpi(folder,p2) );
    else;
        ret = any( strcmp(folder,p2) ); end;
end

function TTEST_dummy; end  %#ok<DEFNU> %Generates an error, if the 'end' of a function is missing. 
