function [ str ] = complexity( varargin );
% (experimental) Guesses to wich complexity class an algorithm belongs
% [ str ] = complexity( xy, [options] );
% [ str ] = complexity( [x], y, [options] );
%
% Input:
%   xy      2xN arary, first row: Input size, second row: Measured time
%   x       1xN array, default: 1:numel(y) Input size
%   y       1xN array, Measured time  
%
% Options:
%   'filter',bool   logical, default=true, filters input values
%   'verbose',val   integer, verbose level
%
% Output
%   str     string which describes complexity
%               1, logn, n, nlogn, n2, n3, n4, 2n, n!

%#ok<*AGROW>

% parse input
%%%%%%%%%%%%%%%%%
    
    [verbose,varargin] = ttest.parsem( {'verbose','v'}, varargin, 1, 'expect',@isnumeric ); %Verbose level
    [filter,varargin] = ttest.parsem( {'filter','f'}, varargin, 1 );
    
    if( numel(varargin)==2 );
        x = varargin{1};
        y = varargin{2};
        varargin(1:2) = [];
    elseif( numel(varargin)==1 && size(varargin{1},1)==1 );
        y = varargin{1};
        x = 1:numel( y );
        varargin(1) = [];
    elseif( numel(varargin)==1 && size(varargin{1},1)==2 );
        x = varargin{1}(1,:);
        y = varargin{1}(2,:);
        varargin(1) = [];
    else;
        error( 'complexity:input', 'Input given in wrong format.' ); end;
    
    ttest.parsem( varargin, 'test' );
    
    
% pre-processing
%%%%%%%%%%%%%%%%%%%%%%
y_orig = y;
[~,x_idx] = sort( x );
x = x(x_idx);
y = y(x_idx);
if( filter );
    %y = movmedian( y, 3 );
    y(end:-1:1) = cummin( y(end:-1:1) ); 
    y = y-min(y); end;

% set-up functions
%%%%%%%%%%%%%%%%%%%%%
    
    i = 0;
    while( true );
        i = i + 1;
        correction{i} = @(x) x; %default correction function
        switch i;
            case 1; % 1
                name{i} = 'O( 1 )     '; %all names must have the same length
                shortname{i} = '1';
                fun{i} = @(b,x) mean(y);
                correction{i} = @(x) norm( y_orig-mean(y_orig) )*sqrt(numel(y_orig))/sum(abs(y_orig)); %this function must work on the unfiltered input data, thus we compute the rel_err by hand
            case 2; % logn
                name{i} = 'O( logn )  ';
                shortname{i} = 'logn';
                fun{i} = @(b,x) b*(log( x + 1 ));
            case 3; % n
                name{i} = 'O( n )     ';
                shortname{i} = 'n';
                fun{i} = @(b,x) b*(x);
            case 4; % nlogn
                name{i} = 'O( n logn )';
                shortname{i} = 'nlogn';
                fun{i} = @(b,x) b*((x+1).*log(x+1));
            case 5; % n^2
                name{i} = 'O( n^2 )   ';
                shortname{i} = 'n2';
                fun{i} = @(b,x) b*(x.^2);
            case 6; % n^3
                name{i} = 'O( n^3 )   ';
                shortname{i} = 'n3';
                fun{i} = @(b,x) b*(x.^3);
            case 7; % n^4
                name{i} = 'O( n^4 )   ';
                shortname{i} = 'n4';
                fun{i} = @(b,x) b*(x.^4);
            case 8; % exp(n)
                name{i} = 'O( 2^n )   ';
                shortname{i} = '2n';
                fun{i} = @(b,x) b*(2.^x - 1);
            case 9; %n!
                name{i} = 'O( n! )    ';
                shortname{i} = 'n!';
                fun{i} = @(b,x) b*(gamma( x + 1 ) - 1);
                correction{i} = @(x) abs( x-3.2 ); %heuristically correction factor
            otherwise;
                break;  end;%leave loop
                
        val = y./fun{i}(1,x);
        val(isnan(val)) = 1;
        nanidx = isnan(val) | isnan(x);
        b(i) = mean( val(~nanidx).*x(~nanidx) ) / mean( x(~nanidx) ); %use x values as weight
        err(i) = sqrt( sum( abs( y - fun{i}(b(i),x) ).^2 ) ); 
        err(i) = correction{i}( err(i) ); end;

       
    rel_err = sqrt( numel(y) ) * err / sum( abs(y) );
    rel_err(isnan(rel_err)) = 0;
    for i = 1:numel( b );
        rel_err(i) = correction{i}( rel_err(i) ); end;
    [~,err_idx] = sort( rel_err ); %sorted index
    
    
    if( verbose>=1 );
        fprintf( '|  coefficient   |   err%%  | complexity  |\n' );
        fprintf( '|---------------:|--------:|-------------|\n' );
        for i = 1:numel( err_idx );
            rel_err_i = rel_err(err_idx(i));
            if( rel_err_i<1 && ~isnan(b(err_idx(i))) || verbose>=3 );
                fprintf( '|  %+13.6e | %6.2f  | %s |\n', b(err_idx(i)), 100*rel_err_i, name{err_idx(i)} ); end; end; end;
        
    if( verbose>=2 );
        clf; hold on;
        plot( x, y, 'k.' );
        for i = 1:numel( b );
            rel_err_i = rel_err(err_idx(i));
            if( rel_err_i<1 && ~isnan(b(err_idx(i))) || verbose>=3 );
                if( i==1 );
                    plot( x, fun{err_idx(i)}(b(err_idx(i)),x), 'r-' );
                elseif( i==2 );
                    plot( x, fun{err_idx(i)}(b(err_idx(i)),x), 'r-.' );
                else;
                    plot( x, fun{err_idx(i)}(b(err_idx(i)),x), 'b:' ); end; end; end;
        switch shortname{err_idx(1)};
            case {'n!','2n'}
                set(gca, 'YScale', 'log');
            otherwise;
                end; %do nothing
        axis( [min(x) max(x) min(y) max(y)] ); end;
    
    str = shortname{err_idx(1)};

end

function dummy; end %#ok<DEFNU> %Generates an error, if the 'end' of a function is missing. 

