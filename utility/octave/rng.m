function state = rng( seed );

    state = rand( 'state' );
    state = randn( 'state' );

    if( nargin>=1 );
	    randn( 'state', seed );
	    rand( 'state', seed ); end;
	
end

function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing. 