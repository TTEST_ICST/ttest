function throw( in );

	error( in.identifier, in.message );
	
end

function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing. 