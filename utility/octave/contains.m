function ret = contains( str, pat ); %needed for Octave compatibilty
    if( isempty(pat) );
        ret = true;
    else;
        ret = any( strfind(str,pat) ); end;
end

function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing. 