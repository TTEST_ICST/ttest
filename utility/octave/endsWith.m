function ret = endsWith( str, pat ); %needed for Octave compatibilty
    if( isempty(pat) );
        ret = true;
    else;
        idx = strfind( str, pat );
	    ret = ~isempty(idx) && idx(end)==numel(str)-numel(pat)+1; end;
end

function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing. 