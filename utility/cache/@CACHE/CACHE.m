% Class which represents the ttest_id-s

classdef CACHE < handle
    
	properties ( Access = public )
        opt;  % options structs
        filename;  % filename of mat file
        varname;  % variable name in mat file
        var;  % content of the variable
    end
    
%    properties ( GetAccess = public, SetAccess = private )
%          hash;  % hash of .var        
%    end
    
    properties ( Access = protected )  % private makes problems with Octave
        timer;  % timer used to check state of .var
        changed_flag;  % flag whether unsaved changes are present
        constructed = false;  % flag whether object is constructed or not
        parg;  % construction arguments
    end
    
    methods
        
        function obj = CACHE( varargin ); %Constructor %t: variable concering the construction of tensor valued sequence
            
            obj.constructed = false;
            obj.parg = varargin;
            
            % parse input
            %%%%%%%%%%%%%%
            [obj.opt.overwrite,varargin] = ttest.parsem( {'overwrite'}, varargin, 1 );
            [obj.opt.verbose,varargin] = ttest.parsem( {'verbose','v'}, varargin, 1 );
            [obj.opt.private,varargin] = ttest.parsem( {'private'}, varargin );
            % [obj.opt.shared,varargin] = ttest.parsem( {'shared'}, varargin, 0 );
            [obj.varname,varargin] = ttest.parsem( {'varname'}, varargin, '' );      % variable name in mat file
            [defaultvalue,varargin] = ttest.parsem( {'varvalue','value'}, varargin, ttest_empty_cache_object );  % initial value
            [obj.filename,varargin] = ttest.parsem( {'filename','file'}, varargin, '' );   % file name of mat file
            
            try;  % there seems to be a Matlab R2018a bug, and the call to inputname errors when the return value should be empty
                inputname1 = inputname( 1 );
            catch me;  %#ok<NASGU>
                inputname1 = '';  end;
                
                
            if( numel(varargin)==2 );
                if( obj.opt.verbose>0 && ~isempty(obj.filename) );
                    warning( 'CACHE:filename', 'name given both as a positional argument and a name-value pair. The positional argument is value is used.' ); end;
                obj.filename = varargin{2}; 
                if( isempty(obj.varname) );
                    obj.varname = inputname1; end;
                defaultvalue = varargin{1};
                varargin(1:2) = [];
            elseif( numel(varargin)==1 && ~isempty(inputname1) );
                assert( ~isempty(obj.filename), 'CACHE:filename', 'Name must be given as a positional argument or as a name-value pair.' ); 
                obj.varname = inputname1(1);
                defaultvalue = varargin{1};
                varargin(1) = [];
            elseif( numel(varargin)==1 );
                if( ~isempty(obj.filename) );
                    warning( 'CACHE:filename', 'name given both as a positional argument and a name-value pair. The positional argument is value is used.' ); end;
                obj.filename = varargin{1}; 
                varargin(1) = []; end;
            assert( isempty(varargin), 'CACHE:options', 'Unknown options given.' );
            defaultvarname = 'ttest_cache';
            
            % make filename
            %%%%%%%%%%%%%%%%%
            [dd,ff,ee] = fileparts( obj.filename );
            assert( ~isempty(ff), 'No name given (but must be given). Either as first argument or as name-value pair with name ''filename''.' );
            if( isempty(dd) );
                dd = pwd; end;
            
            if( isempty(ee) );
                ee = '.mat'; end;
            obj.filename = fullfile( dd, [ff ee] );
            
            % map file to variable
            %%%%%%%%%%%%%%%%%%%%%%%%
            if( obj.opt.verbose>=1 && ~exist([ff '.mat'],'file') );
                fprintf( 'CACHE: New mapping generated.\n  m-file: %s\n', obj.filename ); end;            
            
            do_exist_part = exist( obj.filename, 'file' );
            if( do_exist_part ); for dummy = 1;  % for loop is a hack so we can use `break`
                if( isempty(obj.varname) );
                    try;
                        st = load( obj.filename );
                    catch me;
                        if( obj.opt.overwrite );
                            do_exist_part = false;
                            break;
                        else;
                            fprintf( 2, 'Could not load existing cache file.\n  %s\n', me2str(me) ); end; end;
                    fieldn = fieldnames( st );
                    assert( numel(fieldn)==1, 'CACHE:mat', 'Too many variables in mat file. If there is more than one variable in mat file, one has to specify its name via option ''varname'' or by passing a variable as first positional argument.' );
                    obj.varname = fieldn{1};
                    obj.var = st.(fieldn{1}); 
                else;
                    if( ismatlab );
                        matObj = matfile( obj.filename );  % Octave cannot do this
                        fieldn = who( matObj );
                    else;
                        matObj = load( obj.filename );
                        fieldn = fieldnames( matObj ); end;
                    idx = strcmp( fieldn, obj.varname );
                    if( nnz(idx)==0 && numel(fieldn)==1 );
                        if( obj.opt.verbose>0 );
                            warning( 'CACHE:varname', 'Could not find variable with name %s in specified file. Load variable %s instead.', obj.varname, fieldn{1} ); end;
                        obj.varname = fieldn{1};
                    elseif( nnz(idx)==0 && numel(field)~=1 );
                        error( 'CACHE:varname', 'Could not find variable with name %s in specified file.', fieldn{1} );
                    elseif( nnz(idx)>1 );
                        error( 'CACHE:varname', 'Multiple variables with name %s in specified file. .', fieldn{1} );
                    else; 
                        end; % everything ok
                    st = load( obj.filename, obj.varname );
                    obj.var = st.(obj.varname); end; end; end;
        
            if( ~do_exist_part );
                if( ( ismatlab() && isequal(defaultvalue,ttest_info_c('empty')) || ...
                      isoctave() && isequal(defaultvalue,[]) ) && ...
                     ~isempty(obj.varname) );
                    defaultvalue = evalin( 'caller', [obj.varname ';'] ); end;
                if( isempty(obj.varname) );
                    obj.varname = defaultvarname; end;
                obj.savevariable_to_file_shdjfklhasdf8748thjklahg87947tzuiahsdjgk78( obj.varname, obj.filename, defaultvalue );
                obj.var = defaultvalue; end;
            
            % post processing
            %%%%%%%%%%%%%%%%%%%
%             obj.hash = ttest.datahash( obj.var );
            obj.changed_flag = false;
            
            obj.constructed = true;

        end
            
        function savevariable_to_file_shdjfklhasdf8748thjklahg87947tzuiahsdjgk78( ...
                      ~, ...  % is necessary for octave. There are problems with static/local functions
                      variablename_shdjfklhasdf8748thjklahg87947tzuiahsdjgk78, ...
                      filename_shdjfklhasdf8748thjklahg87947tzuiahsdjgk78, ...
                      value_shdjfklhasdf8748thjklahg87947tzuiahsdjgk78 ...
                      );  %#ok<INUSD>
            eval( [variablename_shdjfklhasdf8748thjklahg87947tzuiahsdjgk78 ' = value_shdjfklhasdf8748thjklahg87947tzuiahsdjgk78;'] );
            save( filename_shdjfklhasdf8748thjklahg87947tzuiahsdjgk78, variablename_shdjfklhasdf8748thjklahg87947tzuiahsdjgk78 );         
        end


        function setvar( obj, value );
            %#ok<*MCSUP>
            if( obj.constructed );
                obj.var = value;
%                 obj.hash = ttest.datahash( obj.var );
                obj.changed_flag = true;
                if( ~obj.opt.private ); 
                    savemat( obj ); end;
            else;
                obj.var = value; end;
        end
        
        function set.var( obj, value );
            %#ok<*MCSUP>
            if( obj.constructed );
                obj.var = value;
%                 obj.hash = ttest.datahash( obj.var );
                obj.changed_flag = true;
                if( ~obj.opt.private ); 
                    savemat( obj ); end;
            else;
                obj.var = value; end;
        end
        
        function savemat( obj );
            assign( obj.varname, obj.var );
            save( obj.filename, obj.varname, '-append' );
            obj.changed_flag = false;
        end
        
        function remove( obj );
            fn = obj.filename;
            delete( obj );
            delete( fn );
        end
        
    end
end

function dummy; end %#ok<DEFNU> %Generates an error, if the 'end' of a function is missing.   

% This Source Code Form is subject to the terms of the Mozilla Public
% License, v. 2.0. If a copy of the MPL was not distributed with this
% file, You can obtain one at https://mozilla.org/MPL/2.0/.
