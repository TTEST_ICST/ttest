function [ ret ] = ttest_empty_cache_object()
    
    persistent x;
    if( isempty (x) );
        x = ~exist( 'OCTAVE_VERSION', 'builtin' ); end;
        
    if( x ) 
        ret = ttest_info_c( 'empty' );  % octave cannot save classes
    else;
        ret = []; end;
end

function dummy; end %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.

% This Source Code Form is subject to the terms of the Mozilla Public
% License, v. 2.0. If a copy of the MPL was not distributed with this
% file, You can obtain one at https://mozilla.org/MPL/2.0/.
