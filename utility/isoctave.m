function ret = isoctave ()
    % returns true when run on octave
    persistent x;
    if( isempty (x) );
        x = logical( exist ('OCTAVE_VERSION', 'builtin') ); end;
    ret = x;
end

function dummy; end  %#ok<DEFNU> %Generates an error, if the 'end' of a function is missing. 