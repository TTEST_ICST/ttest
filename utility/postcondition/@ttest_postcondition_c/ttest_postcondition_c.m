classdef ttest_postcondition_c < handle
% Specify postcondiitons to be done on function completion or in case of error
% ttest_postcondition_c is a class and POSTCONDITION constructs an instance of that class

    properties ( SetAccess = 'private', GetAccess = 'public' )
        ds = struct; %dbstack
        ws = [];
        task = '';
    end

    methods
        function h = ttest_postcondition_c( dbstack_, task_, ws_ )
            % ttest_postcondition_c - Create a ttest_postcondition_c object
            h.ds = dbstack_;
            if( isstring(task_) || ischar(task_) );
                h.task = char( task_ ); 
            else;
                h.task = task_;
            end;
            
            if( nargin==2 );
                h.ws = [];
            else;
                h.ws = ws_; end;
        end
        
        function delete( h );
            
            if( ischar(h.task) );
                % get variables from caller workspace
                name = evalin( 'caller', 'whos()' );
                for i = 1:numel( name );
                    if( ~isfield(h.ws.name(i),name) );
                        h.ws.(name) = evalin( 'caller', name(i).name ); end; end;
                ret = evaluate( h );
                
            else;
                [ varname, allvarname ] = variablename( h.task );
                varname = varname{1};

                % get variables from caller workspace
                h.ws = struct;
                for i = 1:numel( allvarname );
                    try;
                        h.ws.(allvarname{i}) =  evalin( 'caller', [allvarname{i} ';'] ); 
                    catch;
                        error( 'evallazy:missing', 'Variable for lazy evaluation missing. Variable name: %s', varname{i} ); end; end;

                name = fieldnames( h.ws );
                arg = cell( 1, numel(varname) );

                for i = 1:numel( arg );
                    found = false;
                    for j = 1:numel( name );
                        if( strcmp(varname{i},name{j}) );
                            arg{i} = h.ws.(name{j});
                            name(j) = [];
                            found = true;
                            break; end; end; 
                        assert( found, 'evallazy:missing', 'Variable for lazy evaluation missing. Variable name: %s', varname{i} ); end;
                ret = feval( h.task, arg{:} ); end;  % we cannot use try/catch here, since non-present variables are initialized as []
            
            if( ~ret );
                disp( h.ds(2) );
                warning( 'Postcondition failed.' ); end;
        end
    end
    
end

function ret_hdhsfjkalz723fghzgt678qg24zuig89gq4hzufv = evaluate( h_hdhsfjkalz723fghzgt678qg24zuig89gq4hzufv );  % this is in a subfunction in order to reduce chance of duplicate variable names
    % unpack workspace
    for i = 1:numel( h_hdhsfjkalz723fghzgt678qg24zuig89gq4hzufv.ws );
        eval( [h_hdhsfjkalz723fghzgt678qg24zuig89gq4hzufv.ws(i).name '= h.ws(i).name;'] ); end;
    ret_hdhsfjkalz723fghzgt678qg24zuig89gq4hzufv = eval( h_hdhsfjkalz723fghzgt678qg24zuig89gq4hzufv.task );

end

function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.   

% This Source Code Form is subject to the terms of the Mozilla Public
% License, v. 2.0. If a copy of the MPL was not distributed with this
% file, You can obtain one at https://mozilla.org/MPL/2.0/.
