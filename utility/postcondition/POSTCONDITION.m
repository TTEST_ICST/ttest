function [ varargout ] = POSTCONDITION( varargin )

    % check if POSTCONDITION is disabled/enabled
    idx = find( cellfun( 'isclass', varargin, 'ttest_errorlvl_c' ) );
    if( any(idx) );
        try;
            if( strcmp(varargin{idx(1)}(),'dis') );
                return; end;
        catch me; % XX Check which is the correct error to catch if DISABLED is not installed
            2; end; end;

    h = varargin{1};
    varargin(1) = [];

    % find free variable name in caller workspace
    persistent ctr;
    if( isempty(ctr) );
        ctr = 0; end;
    ctr = ctr + 1;
    name = ['ttest_postcondition_' num2str(ctr)];
    while( true );
        flag = evalin( 'caller', ['exist( ''' name ''', ''var'');'] );
        if( ~flag );
            break; end;
        ctr = ctr + 1; end;

    % parse input
    if( isstring(h) || ischar(h) );  % string version
        h = char( h );
        % get variables to be captured
        ws = struct;
        for i = 1:numel( varargin );
            ws.(varargin{i}) = evalin( 'caller', varargin{i} ); end;
        if( nargout==0 );
            assignin( 'caller', name, ttest_postcondition_c(dbstack,h,ws) );
        else; 
            varargout{1} = ttest_postcondition_c(dbstack,h,ws); end;
    else;  % function handle version
        if( nargout==0 );
            assignin( 'caller', name, ttest_postcondition_c(dbstack,h) ); 
        else;
            varargout{1} = ttest_postcondition_c(dbstack,h); end; end;
        
        
    
end

function dummy; end %#ok<DEFNU> %Generates an error, if the 'end' of a function is missing.   

% This Source Code Form is subject to the terms of the Mozilla Public
% License, v. 2.0. If a copy of the MPL was not distributed with this
% file, You can obtain one at https://mozilla.org/MPL/2.0/.
