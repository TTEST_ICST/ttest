function [t] = secloop( sec ); 
%is a loop which runs for sec seconds
% [] = secloop( sec )

%etime cannot be used in thread-based pool
%   start = cputime;
%   while( cputime - start<=sec ); 
%       pause( .1 ); end;
%   t = cputime - start;
    
    tic;
    while( toc<=sec ); 
        pause( .1 ); end;
    t = toc;
end

function dummy; end %#ok<DEFNU> %Generates an error, if the 'end' of a function is missing.