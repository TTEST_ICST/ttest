function [ varargout ] = terror( varargin );  %#ok<STOUT>
% Wrapper for error() accepting any number of input and any number of output
% [ varargout ] = terror( varargin );
    if( numel(varargin)==0 );
        varargin{1} = 'terror:noid';
        varargin{2} = 'terror:noid';
    elseif( numel(varargin)==1 && contains(varargin{1},':') );
        varargin{2} = varargin{1}; end;
    error( varargin{:} );
    %we do not need to set values for varargout, since an error is thrown
end