function ret = captureat( varargin );
% Captures variables in other files
% captureat( [ttest_id], 'in', n1, ..., nn, 'at', l1, ..., ll, 'capture', var1,... , varv, ['group',name] )
% ret = captureat( [ttest_id] );
%
% Input:
%   'in', n1, ..., nn                       strings, function names where to inject the assign statement
%   'at', l1, ..., ln                       strings or integers, locations where to inject the assign statements
%                                               if string li is given, then on each line in the source code which contains substring str
%                                               if integer li is given, on that line (or the next executable line)
%                                           an assign statement is injected
%   'capture', var1, ..., varv,             strings, name of variable to capture. If not given, the whole workspace is captured
%   'group',name                            string, optionally, name of substruct where variables are captured, 
%                                               can be used if variable with the same name shall be captured multiple times
%                                           OR
%                                           the value 0 (zero), in which case each variable is captured in new struct-array
%
% Output:
%   ret             struct (maybe with substructs), captured variables,
%                   also clears all variables captured so far
%



    % get test_id
    %%%%%%%%%%%%%%%
    if( numel(varargin)>=1 && isa(varargin{1},'ttest_id_c') );
        ttest_id = varargin{1};
        varargin(1) = [];
    elseif( evalin( 'caller', 'exist( ''ttest_id'', ''var'' );' ) ); %id not given
        ttest_id = evalin( 'caller', 'ttest_id;' );
    else;
        ttest_id = ttest_id_c( 0 ); end;
    
    if( numel(varargin)==0 );
        try;
            ret = TTEST( ttest_id, 'capture','ttest_getcapture' ); 
        catch me;
            if( strcmp(me.identifier,'MATLAB:nonExistentField') || ...
                    contains(me.message,'structure has no member ''capture''') );
                ret = [];
                warning( 'captureat:nothing', 'Nothing captured yet.' );
            else
                throw( me ); end; end;
        return; end;
    
    data = ttest_at_parse_input( [1 1 0 0 0 1], varargin{:} ); % parse input
    assert( numel(data.group)<=1, 'captureat:group', 'Only one group name can be given.' );

    for i = 1:numel( data.in ); %loop through files
        
        [fid,clean_fid] = ttest_at_open_file( data.in{i} ); %#ok<ASGLU>
        if( fid==-1 );
            continue; end;
        
        data.at = ttest_at_get_linenumber( fid, data.at ); %get line numbers
        
        % make command
        %%%%%%%%%%%%%%%%
        cmd = ['TTEST( ''' ttest_id() ''', ''capture''' ];
        if( ~isempty(data.group) );
            cmd = [cmd ', ''group'', ''' data.group{1} '''']; end; %#ok<AGROW>
        for c = 1:numel( data.capture );
            cmd = [cmd ', ''' data.capture{c} '''' ]; end; %#ok<AGROW>
        cmd = [cmd ')']; %#ok<AGROW>
        
        
        
        ttest_at_set_breakpoint( data.in{i}, data.at, cmd );
        end;
    

end

function dummy; end %#ok<DEFNU> %Generates an error, if the 'end' of a function is missing.

% This Source Code Form is subject to the terms of the Mozilla Public
% License, v. 2.0. If a copy of the MPL was not distributed with this
% file, You can obtain one at https://mozilla.org/MPL/2.0/.
