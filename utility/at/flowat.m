function ret = flowat( varargin );


    % get test_id
    %%%%%%%%%%%%%%%
    if( numel(varargin)>=1 && isa(varargin{1},'ttest_id_c') );
        ttest_id = varargin{1};
        varargin(1) = [];
    elseif( evalin( 'caller', 'exist( ''ttest_id'', ''var'' );' ) ); %id not given
        ttest_id = evalin( 'caller', 'ttest_id;' );
    else;
        ttest_id = ttest_id_c( 0 ); end;
    
    % return stored flow if no arguments are given
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    if( numel(varargin)==0 );
        try;
            ret = TTEST( ttest_id, 'flow','ttest_getflow' ); 
            fn = fieldnames( ret );
            if( numel(fn)==1 && strcmp(fn{1},'null') );
                ret = ret.null; end;
        catch me;
            if( strcmp(me.identifier,'MATLAB:nonExistentField') || ...
                    contains(me.message,'structure has no member ''flow''') );
                warning( 'flowat:nothing', 'Nothing flown yet.' );
            else
                throw( me ); end; end;
        return; end;

    % parse input
    %%%%%%%%%%%%%%%
    data = ttest_at_parse_input( [1 1 0 0 0 0 1 1], varargin{:} ); % parse input

    for i = 1:numel( data.in ); %loop through files
        
        [fid,clean_fid] = ttest_at_open_file( data.in{i} ); %#ok<ASGLU>
        if( fid==-1 );
            continue; end;
        
        data.at = ttest_at_get_linenumber( fid, data.at ); %get line numbers
        
        % generate id for flowpoint
        data.flowpoint{1} = TTEST('id');
        
        % save expected flow points in TTEST
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        if( strcmp(data.type{1},'expect') );
            if( isempty(data.group) );
                TTEST( ttest_id, 'flow','expected', data.flowpoint{1}() );
            else;
                TTEST( ttest_id, 'flow','expected', 'group',data.group{1}, data.flowpoint{1}() ); end; end;
        
        % make command
        %%%%%%%%%%%%%%%%
        cmd = ['TTEST( ''' ttest_id() ''', ''flow''' ...
               ttest.tifh(isempty(data.group),@()'',@()[', ''group'', ''' data.group{1} '''']) ...
               ', ''' data.type{1} ''', ''' data.flowpoint{1}() ''' );'
               ];
        
        ttest_at_set_breakpoint( data.in{i}, data.at, cmd );
        end;
    

end

function dummy; end %#ok<DEFNU> %Generates an error, if the 'end' of a function is missing.

% This Source Code Form is subject to the terms of the Mozilla Public
% License, v. 2.0. If a copy of the MPL was not distributed with this
% file, You can obtain one at https://mozilla.org/MPL/2.0/.
