function inputat( varargin );
% Injects a string into the next input(), thus making it non-blocking
% inputat( 'in', n1, ..., nn, 'at', l1, ..., ll, 'input', str, ['type',type] )
% inputat( 'in', [n1, ..., nn], 'at', l1, ..., ll, 'input', str, 'type','temporal' )
%
% Input:
%   'in', n1, ..., nn       strings, function names where to inject the assign statement
%   'at', l1, ..., ln       strings or integers, locations where to inject the assign statements
%                               if string li is given, then on each line in the source code which contains substring str
%                               if integer li is given, on that line (or the next executable line) an assign statement is injected
%   'input', str1, ,str2    strings, keyboard input for the next input() command
%   'type', type            string 'spatial' or 'temporal'
%                               If type=='spatial', then the following call to input in the same function will get input injected
%                               If type=='temporal', then the next call to input (from anywhere) will get input injected. 
%                                   Note that: In this case, the given function name does not matterno function name may be given, but the option 'in' mu
%                               If no functionnames are given, type 'temporal' is always chosen.
%   
% Notes:
%   - This function only work when to TTEST overload of input() is on the path.
%     This overload behaves exactly like Matlabs input. If for some reason, 
%     one cannot use the TTEST-input, one may use the function inputemu in ttest.*
%     
%   - The assign is executed as first statement on the line (similar for evalat, inputat, etc...)
%
% E.g.:
%   assignat( 'in','eval2nd', 'at',1, 'assign', 'second',@() 10 );
%   eval2nd( @()20,@()20 ) %yields 10
%   clearat( 'in','eval2nd' ); %deletes injected code
%   
%

    data = ttest_at_parse_input( [1 1 0 0 1 0 0 1], varargin{:} ); % parse input
    assert( numel(data.input)==1, 'inputat:input', 'Exactly one input must be given.' );
    assert( numel(data.type)<=1, 'inputat:type', 'Multiple types given, but only one type is allowed.' );
    
    if( isempty(data.in) );
        input( ttest_input_c(data.input{1}) );
    else;
        for i = 1:numel( data.in ); %loop through files
            [fid,clean_fid] = ttest_at_open_file( data.in{i} ); %#ok<ASGLU>
            if( fid==-1 );
                continue; end;
            data.at = ttest_at_get_linenumber( fid, data.at ); %get line numbers
            if( isempty(data.type) || ~isempty(data.type{1}) && any(strcmp(data.type{1},{'spatial','local'})) );
                cmd = ['return2nd( { ''ttest'', TTEST( ''input'', ''' data.input{1} ''' ) }, false )'];
            elseif( any(strcmp(data.type{1},{'temporal','global'})) );
                cmd = ['return2nd( { ''ttest'', input( ttest_input_c( ''' data.input{1} ''' ) ) }, false )'];
            else;
                error( 'inputat:type', 'wrong type given' ); end;
                

            ttest_at_set_breakpoint( data.in{i}, data.at, cmd ); end;
end;


end

function dummy; end %#ok<DEFNU> %Generates an error, if the 'end' of a function is missing.

% This Source Code Form is subject to the terms of the Mozilla Public
% License, v. 2.0. If a copy of the MPL was not distributed with this
% file, You can obtain one at https://mozilla.org/MPL/2.0/.
