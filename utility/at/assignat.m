function assignat( varargin );
% Injects assign statements (i.e. assign values to variables) into other files
% assignat( [ttest_id], 'in', n1, ..., nn, 'at', l1, ..., ll, 'assign', var1,val1, ..., varv,valv )
%
% Input:
%   'in', n1, ..., nn                       strings, function names where to inject the assign statement
%   'at', l1, ..., ln                       strings or integers, locations where to inject the assign statements
%                                               if string li is given, then on each line in the source code which contains substring str
%                                               if integer li is given, on that line (or the next executable line)
%                                           an assign statement is injected
%   'assign', var1,val1, ..., varv,valv     pairs of string,value, variable name and corresponding value OR
%                                           struct, representing the workspace to be assigned
%   
% Notes:
%   - If it happens that some inputfile is named 'at' (or similiar collisions of option-names and variable/value names,
%     one can prefix the 'in' options with any string. The same prefix must then be used for all other options too.
%     E.g., the above command is equivalent to the following command
%     assignat( [ttest_id], 'ABCin', n1, ..., nn, 'ABCat', l1, ..., ll, 'ABCassign', var1,val1, ..., varv,valv )
%   - The assign is executed as first statement on the line (similar for evalat, inputat, etc...)
%
% E.g.:
%   assignat( 'in','eval2nd', 'at',1, 'assign', 'second',@() 10 );
%   eval2nd( @()20,@()20 ) %yields 10
%   clearat( 'in','eval2nd' ); %deletes injected code
%   


    % get test_id
    %%%%%%%%%%%%%%%
    if( numel(varargin)>=1 && isa(varargin{1},'ttest_id_c') );
        ttest_id = varargin{1};
        varargin(1) = [];
    elseif( evalin( 'caller', 'exist( ''ttest_id'', ''var'' );' ) ); %id not given
        ttest_id = evalin( 'caller', 'ttest_id;' );
    else;
        ttest_id = ttest_id_c( 0 ); end;

    data = ttest_at_parse_input( [1 1 1 0], varargin{:} ); % parse input
    assert( numel(data.varname)==numel(data.varvalue) || ...
            numel(data.varname)==1 && numel(data.varvalue)==0 && isstruct(data.varname{1}), ...
            'ttest_at:assign', 'Wrong input. Either name-value pairs of variables or exactly one struct representing the workspace must be given.' );

    for i = 1:numel( data.in ); %loop through files
        
        [fid,clean_fid] = ttest_at_open_file( data.in{i} ); %#ok<ASGLU>
        if( fid==-1 );
            continue; end;
        
        data.at = ttest_at_get_linenumber( fid, data.at ); %get line numbers
        
        % store variables in TTEST
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%
        as_id = TTEST( 'id' ); %get id for message
        for v = 1:numel( data.varname );
            TTEST( ttest_id, 'assign', as_id(), data.varname{v}, data.varvalue{v} ); end;
        
        % make command
        %%%%%%%%%%%%%%%%
        cmd = 'return2nd( { ''ttest'', ';
        for v = 1:numel( data.varname );
            cmd = [cmd 'assign( ''' data.varname{v} ''', TTEST( ''' ttest_id() ''', ''assign'', ''' as_id() ''', ''' data.varname{v} ''') ), ']; end; %#ok<AGROW>
        cmd = [cmd '}, false )']; %#ok<AGROW>
        
        
        ttest_at_set_breakpoint( data.in{i}, data.at, cmd );
        end;
    

end

function dummy; end %#ok<DEFNU> %Generates an error, if the 'end' of a function is missing.

% This Source Code Form is subject to the terms of the Mozilla Public
% License, v. 2.0. If a copy of the MPL was not distributed with this
% file, You can obtain one at https://mozilla.org/MPL/2.0/.
