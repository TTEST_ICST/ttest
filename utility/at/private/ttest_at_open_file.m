function [ fid, clean_fid ] = ttest_at_open_file( name );        

assert( isstring(name) || ischar(name), 'ttest_at:input', 'filename must be a string or a character vector.' );
fid = fopen( which(name) ); 
if( fid==-1 );
    warning( 'evalat:file', 'Could not open ''%s''.', name ); 
    clean_fid = -1;
else;
    clean_fid = onCleanup( @()fclose(fid) ); end;  % Make sure we free the file, onCleanup is also executed if we overwrite the variable

end


function dummy; end %#ok<DEFNU> %Generates an error, if the 'end' of a function is missing.

% This Source Code Form is subject to the terms of the Mozilla Public
% License, v. 2.0. If a copy of the MPL was not distributed with this
% file, You can obtain one at https://mozilla.org/MPL/2.0/.
