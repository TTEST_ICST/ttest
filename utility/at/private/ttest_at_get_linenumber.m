function [ at ] = ttest_at_get_linenumber( fid, at );        

    at_idx = find( cellfun( 'isclass', at, 'string' ) | cellfun( 'isclass', at, 'char' ) );
    if( any(at_idx) );
        txt = {};
        while( true );
            txt{end+1} = fgetl( fid );  %#ok<AGROW>
            if( ~ischar(txt{end}) );
                txt(end) =  [];
                break; end; end;
        for a = at_idx;
            for t = 1:numel( txt );
                if( any(strfind(txt{t},at{a})) );
                    at{end+1} = t; end; end; end;  %#ok<AGROW>
        at(at_idx) = []; end; 
    
    at = cell2mat( at );
    at = unique( at );
end

function dummy; end %#ok<DEFNU> %Generates an error, if the 'end' of a function is missing.

% This Source Code Form is subject to the terms of the Mozilla Public
% License, v. 2.0. If a copy of the MPL was not distributed with this
% file, You can obtain one at https://mozilla.org/MPL/2.0/.
