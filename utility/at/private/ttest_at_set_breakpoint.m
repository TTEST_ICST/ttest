function ttest_at_set_breakpoint( in, at, cmd )
    
    clearat( 'save', in );
    for i = 1:numel( at );
        % get true line where breakpoint is set
        db_old = dbstatus( in );  % save original breakpoints
        dbclear( 'in',in );      % delete all breakpoints
        dbstop( 'in',in, 'at',num2str(at(i)), 'if',cmd );  % set new breakpoints
        db_set = dbstatus( in );  % check at which line breakpoint is set
        at_i = db_set.line;
        dbstop( db_old );         % restore original breakpoints
        
        % check whether true line already has conditional breakpoint
        cmd_i = cmd;
        if( ~isempty(db_old) );
            idx = [db_old.line]==at_i;
            assert( nnz(idx)<=1, 'ttest:at:fatal', 'Fatal programming error' );
            if( nnz(idx)==1 );
                if( ismatlab );
                    val = [db_old.anonymous];
                    if( val(idx)~=0 );
                        warning( 'ttest:at:anonymousfunction', 'Breakpoint at anonymous function at line question already present. Command cannot be injected.' );
                        continue; end; 
                else;
                    end; % octave has no notion of breakpoints at anonymous functions yet
                if( ismatlab )
                    val = db_old.expression{idx};
                else;
                    val = db_old.cond; end;
                if( ~isempty(val) );
                    cmd_i = [cmd '||' val]; end; end; end;
        dbstop( 'in',in, 'at',num2str(at_i), 'if',cmd_i ); 
    end; 
    
end
        
function dummy; end %#ok<DEFNU> %Generates an error, if the 'end' of a function is missing. 

% This Source Code Form is subject to the terms of the Mozilla Public
% License, v. 2.0. If a copy of the MPL was not distributed with this
% file, You can obtain one at https://mozilla.org/MPL/2.0/.
