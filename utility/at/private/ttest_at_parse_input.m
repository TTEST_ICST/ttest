function [ data ] = ttest_at_parse_input( in_at_as_ev_inp_capt_grp_type, varargin )

 %#ok<*AGROW>
    [data.in,data.at,data.varname,data.varvalue,data.evalcmd,data.input,data.capture,data.group,data.type] = deal( {} );
    if( numel(varargin)==0 );
        return; end;
    dash = varargin{1}(1:end-2);
    assert( in_at_as_ev_inp_capt_grp_type(1)>1 || ...
            numel(varargin{1})>=2 && strcmpi(varargin{1}(end-1:end),'in'), ...
            'ttest_at:input', 'First input must be ''.....in''.' );
    if( in_at_as_ev_inp_capt_grp_type(1)>1 );
        opt = 'in'; end;
    
    in_at_as_ev_inp_capt_grp_type = logical( in_at_as_ev_inp_capt_grp_type );
    i = 1;
    while( i<=numel(varargin) );
        if( isstring(varargin{i}) );  % cast string to char
            varargin{i} = char( varargin{i} ); end;
        if    ( numel(in_at_as_ev_inp_capt_grp_type)>=1 && in_at_as_ev_inp_capt_grp_type(1) && strcmp(varargin{i},[dash 'in']) );
            opt = 'in';
        elseif( numel(in_at_as_ev_inp_capt_grp_type)>=2 && in_at_as_ev_inp_capt_grp_type(2) && strcmp(varargin{i},[dash 'at']) );
            opt = 'at';
        elseif( numel(in_at_as_ev_inp_capt_grp_type)>=3 && in_at_as_ev_inp_capt_grp_type(3) && strcmp(varargin{i},[dash 'assign']) );
            opt = 'assign';
        elseif( numel(in_at_as_ev_inp_capt_grp_type)>=4 && in_at_as_ev_inp_capt_grp_type(4) && strcmp(varargin{i},[dash 'eval']) );
            opt = 'eval';
        elseif( numel(in_at_as_ev_inp_capt_grp_type)>=5 && in_at_as_ev_inp_capt_grp_type(5) && strcmp(varargin{i},[dash 'input']) );
            opt = 'input';
        elseif( numel(in_at_as_ev_inp_capt_grp_type)>=6 && in_at_as_ev_inp_capt_grp_type(6) && strcmp(varargin{i},[dash 'capture']) );
            opt = 'capture';
        elseif( numel(in_at_as_ev_inp_capt_grp_type)>=6 && in_at_as_ev_inp_capt_grp_type(6) && strcmp(varargin{i},[dash 'group']) );
            opt = 'group';
        elseif( numel(in_at_as_ev_inp_capt_grp_type)>=7 && in_at_as_ev_inp_capt_grp_type(7) && strcmp(varargin{i},[dash 'type']) );
            opt = 'type_flow';
        elseif( numel(in_at_as_ev_inp_capt_grp_type)>=8 && in_at_as_ev_inp_capt_grp_type(8) && strcmp(varargin{i},[dash 'type']) );
            opt = 'type_input';            
        else;
            switch opt;
                case 'in';
                    assert( ischar(varargin{i}), 'ttest_at:in', 'Filename must be a string or character vector.' );
                    data.in{end+1} = varargin{i};
                case 'at'; 
                    if( isnumeric(varargin{i}) );
                        data.at = [data.at num2cell(varargin{i})];
                    elseif( ischar(varargin{i}) );
                        data.at{end+1} = varargin{i};
                    else;
                        error( 'ttest_at:at', 'Wrong format for line numbers. Must be either a string or number(s)' ); end;
                case 'assign'; 
                    if( isstruct(varargin{i}) );
                        error( 'ttest_at:fatal', 'not yet implemented' );
                        data.varname{end+1} = varargin{i}; %#ok<UNRCH>
                    else;
                        assert( numel(varargin)>=i+1, 'ttest_at:assign', 'Input missing. Arguments must be given as pairs: varname/varvalue.' ); 
                        assert( isvarname(varargin{i}), 'ttest_at:varname', 'Variable name is not a proper name.' );
                        data.varname{end+1} = varargin{i};
                        data.varvalue{end+1} = varargin{i+1}; end;
                    i = i + 1; 
                case 'eval';
                    if( ischar(varargin{i}) || isa(varargin{i},'function_handle') );
                        data.evalcmd{end+1} = varargin{i};
                    else;
                        error( 'ttest_at:eval', 'Only strings and functon handles are possible.' ); end;
                case 'input';
                    if( ischar(varargin{i}) );
                        data.input{end+1} = varargin{i}; 
                    else;
                        error( 'ttest_at:string', 'Only strings are possible.' ); end;;
                case 'capture';
                    if( isequal(varargin{i},0) || isequal(varargin{i},'0') );
                        % do nothing == capture whole workspace
                    elseif( ischar(varargin{i}) );
                        assert( isvarname(varargin{i}), 'ttest_at:varname', 'Variable name is not a proper name.' );
                        data.capture{end+1} = varargin{i}; 
                    else;
                        error( 'ttest_at:string', 'Only strings or the value 0 are possible.' ); end;
                case 'group';
                    if( isequal(varargin{i},0) || isequal(varargin{i},'0') );
                        data.group{end+1} = '0'; 
                    elseif(ischar(varargin{i}) );
                        assert( isvarname(varargin{i}), 'ttest_at:varname', 'Group name is not a proper name.' );
                        data.group{end+1} = varargin{i}; 
                    else;
                        error( 'ttest_at:string', 'Only strings or the value 0 are possible.' ); end;
                case 'type_flow';
                    switch varargin{i};
                        case 'abort'; data.type{end+1} = 'abort';
                        case 'failure'; data.type{end+1} = 'failure';
                        case 'expect'; data.type{end+1} = 'expect';
                        otherwise; error( 'ttest_at:flow', 'Wrong type' ); end;
                case 'type_input';
                    switch varargin{i};
                        case {'t','temporal','g','global'}; data.type{end+1} = 'global';
                        case {'s','spatial','l','local'};  data.type{end+1} = 'local';
                        otherwise; error( 'ttest_at:input', 'Wrong type' ); end;
            end; end; 
        i = i + 1; end; 
    
    if( isempty(data.at) );
        data.at = {1}; end;
    
end

function dummy; end %#ok<DEFNU> %Generates an error, if the 'end' of a function is missing. 

% This Source Code Form is subject to the terms of the Mozilla Public
% License, v. 2.0. If a copy of the MPL was not distributed with this
% file, You can obtain one at https://mozilla.org/MPL/2.0/.
