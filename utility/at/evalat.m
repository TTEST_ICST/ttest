function [ handle ] = evalat( varargin );
% (experimental) Injects code into other files
% evalat( 'in', n1, ..., nn, 'at', l1, ..., ll, 'eval', cmd1, ..., cmdc )
%
% Input:
%   'in', n1, ..., nn                       strings, function names where to inject the assign statement
%   'at', l1, ..., ln                       strings or integers, locations where to inject the assign statements
%                                               if string li is given, then on each line in the source code which contains substring str
%                                               if integer li is given, on that line (or the next executable line)
%                                           an assign statement is injected
%   'cmd', cmd1, ..., cmdc                  strings, eval-able commands. 
%                                           The commands evalin('caller',...) and assignin('caller',...) do not work.
%
%
% Notes: For more information see assignat
%
% E.g.: 
%   evalat( 'in','evalat', 'at','%<LABEL>', 'eval','fprintf(''Hello World\n'')' ); 
%   evalat();  %prints out "Hello World"
%   clearat( 'in','evalat' ); %clear injected code
%
% See also: assignat, evalat, captureat, inputat, flowat, clearat
%


    persistent handlecontainer;
    if( ~iscell(handlecontainer) );
        handlecontainer = {}; end;
    
    if( isa(varargin{1},'ttest_info_c') && isequal(varargin{1}.info,'handle') );
        handle = handlecontainer{varargin{2}};
        return; end;
    
    data = ttest_at_parse_input( [1 1 0 1], varargin{:} );  % parse input %<LABEL>
    
    
    for i = 1:numel( data.in );  % loop through files

        [fid,clean_fid] = ttest_at_open_file( data.in{i} );  %#ok<ASGLU>
        if( fid==-1 );
            continue; end;
        data.at = ttest_at_get_linenumber( fid, data.at );  % get line numbers

        cmd = 'false ';
        for e = 1:numel( data.evalcmd );
            if( isempty(data.evalcmd{e}) );
                continue;
            elseif( isa(data.evalcmd{e},'char') );
                data.evalcmd{e} = strrep( data.evalcmd{e}, '''', '''''' ); 
                cmd = [ cmd '|| evalfirstreturnlast(''' data.evalcmd{e} ''',''ttest'',false)'];  %#ok<AGROW>                                
            elseif( isa(data.evalcmd{e},'function_handle') );
                idx = numel( handlecontainer ) + 1;
                handlecontainer{idx} = data.evalcmd{e};
                val = ['ttest_evalat_handle_' num2str(idx)];
                cmd = [ cmd '|| ' ...
                       'returnfalse( assign( ''' val ''', evalat(ttest_info_c(''handle''), ' num2str(idx) ' ) ) ) || ' ...
                       'returnfalse( ' val '() )'  ...
                       ];  %#ok<AGROW>
            else;
                error( 'ttest:evalat', 'fatal failure' ); end; end; 

        ttest_at_set_breakpoint( data.in{i}, data.at, cmd );
end;
    

end

function dummy; end %#ok<DEFNU> %Generates an error, if the 'end' of a function is missing.

% This Source Code Form is subject to the terms of the Mozilla Public
% License, v. 2.0. If a copy of the MPL was not distributed with this
% file, You can obtain one at https://mozilla.org/MPL/2.0/.
