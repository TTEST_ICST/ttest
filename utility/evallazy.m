function [ ret ] = evallazy( h, ws );
% Evaluates a function handle using the variables currently in the workspace
% [ ret ] = evallazy( h, [ws] );
%
% Input:
%   h       function_handle
%   ws      struct containing the workspace. If not given, the function handle is evaluated in the callers workspace
%
% Output:
%   ret     the return value of h()
%
% Example:
%   a = 1; h = @(b) a+b; a = 0; b = 1; evallazy( h ) %yields 2
%


[ varname, allvarname ] = variablename( h );
varname = varname{1};

%get variables from caller workspace/struct ws
if( nargin==1 );
    ws = struct;
    for i = 1:numel( allvarname );
        try;
            ws.(allvarname{i}) =  evalin( 'caller', [allvarname{i} ';'] ); 
        catch;
            error( 'evallazy:missing', 'Variable for lazy evaluation missing. Variable name: %s', varname{i} ); end; end; end;

name = fieldnames( ws );
arg = cell( 1, numel(varname) );

for i = 1:numel( arg );
    found = false;
	for j = 1:numel( name );
        if( strcmp(varname{i},name{j}) );
            arg{i} = ws.(name{j});
            name(j) = [];
            found = true;
            break; end; end; 
        assert( found, 'evallazy:missing', 'Variable for lazy evaluation missing. Variable name: %s', varname{i} ); end;
ret = feval( h, arg{:} ); %we cannot use try/catch here, since non-present variables are initialized as []

end

function dummy; end %#ok<DEFNU> %Generates an error, if the 'end' of a function is missing.

% This Source Code Form is subject to the terms of the Mozilla Public
% License, v. 2.0. If a copy of the MPL was not distributed with this
% file, You can obtain one at https://mozilla.org/MPL/2.0/.
