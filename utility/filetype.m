function [ t, numargin, numargout ] = filetype( filename )
% written by: Loren Shure
% https://blogs.mathworks.com/loren/2013/08/26/what-kind-of-matlab-file-is-this/
%
% See also: [Function definition meta-info](https://undocumentedmatlab.com/articles/function-definition-meta-info)
% Takes a filename and returns 'script', 'function' or 'class'

    try;
        [dir,fn,~] = fileparts( filename );
        if( ~isempty(dir) && ~strcmp(pwd,dir) );
            olddir = cd( dir );
            dirclean = onCleanup( @() cd(olddir) ); end;
        if( isoctave );
          try;
              numargin = nargin( fn );
              numargout = nargout( fn);
          catch me;
              if( any(strfind( me.message, 'script' )) );
                  numargin = 0;
                  numargout = 0;
                  t = 'script';
                  return;
              else;
                  error( 'TTEST:filetype', 'This type of file is not yet identifiable by this function. It must be added!' ); end; end;
        else;
            numargin = nargin( filename );
            numargout = nargout( filename ); end;
        if( isoctave );
            t = 'function';
        else;
          val = exist( filename, 'class' );
          if( isequal(val,8) );
              t = 'class';
          else;
              t = 'function'; end; end;
    catch me;
        if( strcmp(me.identifier, 'MATLAB:nargin:isScript') );
            numargin = 0;
            numargout = 0;
            t = 'script';
        else;
            rethrow( me ); end; end;
end

function TTEST_dummy; end %#ok<DEFNU> %Generates an error, if the 'end' of a function is missing. 
