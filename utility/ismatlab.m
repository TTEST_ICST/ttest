function ret = ismatlab ()
    % returns true when run on m
    persistent x;
    if( isempty (x) );
        x = ~exist( 'OCTAVE_VERSION', 'builtin' ); end;
    ret = x;
end

function dummy; end  %#ok<DEFNU> %Generates an error, if the 'end' of a function is missing. 