function [opt] = parseinputs( varargin )
% runs tests
% [ opt ] = parseInputs( ['recursive'], ['doctest' | 'test'], ['filename_1', ..., 'filename_n'], [options] )
%
% Input:
%   filename    one or more char-arrays, the files to be executed,
%               can be passed with or without trailing '.m'
%               in the event of more than one char-array being passed to 'testrunner',
%               the type of execution ('test' or 'doctest') will be determined by the
%               name of the first file - if tests should be executed, the first file must be named 'test'
%   flag        char-array, can be either 'recursive' or 'doctest', or both, order-agnostic
%               the char-array 'test' is given implicitly as default when a file starting with
%               'test' is passed to the testrunner
%
% Output:
%    opt.              option struct with fields
%       .searchTerm    cell array of strings, default = {'*test','test*'}, the search terms determing which test file shall be executed
%       .recursive     boolean, defines whether the subfolders should be searched for the files or not
%       .doctest       boolean, defines whether the files are executed or parsed and searched

% parse options
%%%%%%%%%%%%%%%%%%%%%

idx = false( 1, numel(varargin) );;
for i = 2:numel( varargin );
    if( islogical(varargin{i}) || isnumeric(varargin{i}) );
        idx(i) = true; 
        idx(i-1) = true; end; end;
option_arg = varargin( idx );
varargin = varargin( ~idx );

[opt.verbose,option_arg] = ttest.parsem( {'verbose','v'}, option_arg, 1 );
[opt.recursive,option_arg] = ttest.parsem( {'recursive','r'}, option_arg, 0 );
[opt.doctest,option_arg] = ttest.parsem( {'doctest','d'}, option_arg, 0 );
assert( isempty(option_arg), 'runttests:option', 'Unknown option(s) given.' );


% options 'recursive', 'doctest', 'test' can be given also the very beginning
if( numel(varargin)>=1 && strcmpi(varargin{1},'recursive') );
    opt.recursive = 1;
    varargin(1) = []; end;

if( numel(varargin)>=1 );
    switch lower(varargin{1});
        case {'test','unittest'};
            opt.doctest = 0;
            varargin(1) = [];
        case 'doctest';
            opt.doctest = 1;
            varargin(1) = [];
        otherwise;
            end; end;  % do nothing;

% parse searchterm
if( numel(varargin)==0 );
    if( opt.doctest );
        opt.searchterm = {'*'};
    else;
        opt.searchterm = {'test*','*test'}; end;
else;
    opt.searchterm = cell( 1, numel(varargin) );
    for i = 1:numel( varargin )
        assert( ischar(varargin{i}) || isstring(varargin{i}), 'runttests:searchterm', 'Only strings can be given as searchterms.' );
        opt.searchterm{i} = char( varargin{i} ); end; end;

% postprocess searchterms
for i = 1:numel( opt.searchterm );
    assert( ischar(opt.searchterm{i}) || isstring(opt.searchterm{i}), 'runttests:options', 'All arguments must be strings.' );
    opt.searchterm{i} = char( opt.searchterm{i} );
    n = numel( opt.searchterm{i} );
    if( n>=1 && opt.searchterm{i}(end)=='*' );
        continue; end;
    if( n>=2 && strcmp(opt.searchterm{i}(end-1:end),'.m') );
        continue; end;
    opt.searchterm{i} = [opt.searchterm{i} '.m']; end;
    
        
end

function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.   

% This Source Code Form is subject to the terms of the Mozilla Public
% License, v. 2.0. If a copy of the MPL was not distributed with this
% file, You can obtain one at https://mozilla.org/MPL/2.0/.
