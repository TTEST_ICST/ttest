function [filename_out,testlist] = parselines( filelist, magiccommentstring )
% [ outList ] = parselines( fileList, searchTerm )
%
% Input:    fileList...a struct containing char arrays with the path to all the files found based on the searchTerms
%           searchTerm...a char array, indicating the prefix of the lines to be parsed (e.g. '%TT')
%
% Output:   outList...a char array assembling all the files in fileList with those lines preceded 
%                     by the searchTerm uncommented and all other lines commented
%
%    outList = parselines(fileList, '%TT')
%    each file in fileList is parsed and those lines starting with %TT are uncommented
%    while all other lines are prefixed by '%'; the files are assembled into one char array
%    'outList', whereby line breaks are maintained

testlist = [];
funcnamelist = [];
basepath = which( 'TTEST.m' );
basepath = basepath(1:end-numel('TTEST.m'));
doctestdir = fullfile( basepath, 'tmp', 'tr' );

for ii = 1:numel( filelist );
    
    try;
        S = fileread( filelist{ii} );
    catch me;
        warning( 'parselines:ioerror', 'Could not read file %s.\n  Error id: %s\n  Error msg: %s', filelist{ii}, me.identifier, me.message ); 
        continue; end;
    [~,funcname,~] = fileparts( filelist{ii} );
    funcnamelist = [funcnamelist, {funcname}];  %#ok<AGROW>
    S = strrep( S, '\r\n', '\n' );
    S = strsplit( S, '\n' );
    S = switchcomment( S, magiccommentstring );
    testlist = [testlist, {S}]; end;  %#ok<AGROW>

basepath = pwd;
cleandir = onCleanup( @() cd(basepath) );
cd( doctestdir );

filename_out = {};
for ii = 1:numel( testlist );
    % create an .m-file in that directory
    
    filename_candidate = ['doctest_', funcnamelist{ii}];
    fn = [freename( doctestdir, filename_candidate, 8 ), '.m'];
    fid = fopen( fn, 'w' );
    if( fid~=-1 );  % write all the tests in there and close the file
        fileCleanup = onCleanup( @() fclose(fid) );
        fprintf( fid, '%s\n', testlist{ii} );
        filename_out{end+1} = fn;  %#ok<AGROW>
    else;
        warning( 'parselines:ioerror', 'Could not write to file %s\n, which is the doctest file for %s\n', fn, filelist{ii} ); end; end;

end

function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.

% This Source Code Form is subject to the terms of the Mozilla Public
% License, v. 2.0. If a copy of the MPL was not distributed with this
% file, You can obtain one at https://mozilla.org/MPL/2.0/.
