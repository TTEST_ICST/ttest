function code_out = switchcomment( code_in, searchTerm );
% switches comments
% [ code_out ] = switchcomment( code_in, searchTerm );
% uncomments lines starting with <searchterm> and comments out all other lines
    code_out = cell( 1, numel(code_in) ); %pre allocate
    numline = numel( code_in );
    for ii = 1:numline;
        testStr = code_in{ii};
        if( numel(testStr) > numel(searchTerm) && strcmp(testStr(1:numel(searchTerm)), searchTerm) );
            code_out{ii} = [testStr(numel( searchTerm )+1:end) newline];
        else;
            code_out{ii} = ['%' testStr newline]; end; end;
    code_out = [code_out{:}];
end

function dummy; end %#ok<DEFNU> %Generates an error, if the 'end' of a function is missing.

% This Source Code Form is subject to the terms of the Mozilla Public
% License, v. 2.0. If a copy of the MPL was not distributed with this
% file, You can obtain one at https://mozilla.org/MPL/2.0/.
