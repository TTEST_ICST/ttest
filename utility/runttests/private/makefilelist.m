function [ filelist, folderlist ] = makefilelist( searchTerm, recursive )
% fileList = makefilelist( [searchTerm], [recursive] )
%
% Input:    
%    searchTerm     cell array of strings, default = {'test*','*test.m'}, containing char arrays to be searched for
%    recursive      flag (boolean), default = false, determines if recursive search should be performed
%
% Output:   
%   fileList        struct containing char arrays with the path to all the files found based on the searchTerms
%   folderlist      corresponding folders where the files reside


if( nargin<=0 || isempty(searchTerm) );
    searchglobaly = false;
    searchTerm = {'test*','*test.m'}; 
else;
    searchglobaly = true; end;
if( nargin<=1 );
    recursive = false; end;
if( ~iscell(searchTerm) );
    searchTerm = {searchTerm}; end;

% find files
files = [];
if( recursive );
    for kk = 1:numel( searchTerm );
        files = [files; recursivesearch( fullfile(pwd, searchTerm{kk}) )]; end;  %#ok<AGROW>
else;
    for kk = 1:numel( searchTerm )
        files = [files; dir( searchTerm{kk} )]; end; end;  %#ok<AGROW>

if( isempty(files) && searchglobaly );
    for i = 1:numel( searchTerm );
        x = which( searchTerm{i} );
        if( ~isempty(x) );
            files = [files; dir(x)]; end; end; end;  %#ok<AGROW>

% post process and filter filess
filelist = [];
folderlist = [];
for ii = 1:numel( files )
    if( strcmp(files(ii).name,'.') || strcmp(files(ii).name,'..') );
        continue; end;
    if( recursive );
        [folder, name, ext] = fileparts( files(ii).name );
    else;
        folder = files(ii).folder;
        name = files(ii).name;
        ext = name(end-1:end);
        name = name(1:end-2); end;
    if( numel(name)>=1 && strcmp(ext, '.m') && ~strcmp([name, ext], 'runttests.m') && isvarname(name) );
        oldfolder = cd( files(ii).folder );
        cd( oldfolder );
        folderlist{end+1} = folder;  %#ok<AGROW>
        filelist{end+1} = fullfile( folder, [name ext] ); end; end;  %#ok<AGROW>

[filelist,idx] = unique( filelist );
folderlist = folderlist(idx);
	

end

function dummy; end %#ok<DEFNU> %Generates an error, if the 'end' of a function is missing.

