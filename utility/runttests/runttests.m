function successflag = runttests( varargin )  
% [ successflag ] = runttests( ['recursive'], ['test' | 'doctest'], [filename1, ..., filenamen] )
% This function collects and executes tests.
%
% Input:
%   filenamei       strings, default = test*, *test,
%                   one or more strings, the files to be executed,
%                   can be passed with or without trailing '.m'
%                   accepts wildcards
%   'recursive'     optional, if given, then also all subdirectories are scanned for tests
%   'test'          optional, (default behaviour) if given, then all files starting or ending with filenamei are executed
%   'doctest'       optional, if given, then only code in comment-lines starting with %TT are executed
%
% Output:
%   successflag     (experimental) true when all tests succeeded, false when one test did not succeed or when no tests were executed at all
%
% Example:
%   runttests( );              % execute all files in current directory that start or end with 'test'
%   runttests( 'recursive' );  % execute all files in current and subdirectories that start with 'test'
%   runttests( 'testxy' );     % execute script 'testxy.m'
%   runttests( 'testabc', 'testdef', 'testxyz' );            % execute scipts 'testabc.m', 'testdef.m', and 'testxyz.m'
%   runttests( 'recursive', '*xyz*',  );  % Search for files containing the pattern 'xyz' in current and subdirectories
%   runttests( 'doctest', '*xyz*',  );    % Search for files containing the pattern 'xyz', and execute only those starting with %TT

% XX add multiline support for doctests

successflag_ = false;
olddir = pwd;
dirCleanup = onCleanup( @() cd(olddir) );

bp = which( 'TTEST.m' );
bp = bp(1:end-numel( 'TTEST.m' ));
doctestdir = fullfile( bp, 'tmp', 'tr' );

% parse input
%%%%%%%%%%%%%%%
opt = parseinputs( varargin{:} );
[filelist,folderlist] = makefilelist( opt.searchterm, opt.recursive );

if( isempty(filelist) );
    warning( 'runttests:notests', 'No tests found.');
    return; end;

% handle doctest comments
%%%%%%%%%%%%%%%%%%%%%%%%%%%
filelist_original = filelist;
if( opt.doctest );
    % run the files in 'doctestdir'
    filename = parselines( filelist, '%TT' );
    filelist = fullfile( doctestdir, filename ); 
    rehash; end;

% run tests
%%%%%%%%%%%%%
if( opt.verbose>=1 );
    starttime_test = tic; end;
failurelist = [];
successlist = [];
lastfolder = '';
for jj = 1:numel( filelist );
    if( opt.verbose>=1 );
        starttime_subtest = tic; end;
    try;
        if( opt.verbose>=1 ); 
            fprintf( 'Running %s', filelist_original{jj} ); 
            if( ~strcmp(filelist_original{jj},filelist{jj}) );
                fprintf( '  ( using temporary file: %s )', filelist{jj} ); end;
            fprintf( '\n' ); end;
        if( opt.verbose>=10 );
            fprintf( 'Press any key to continue\n' ); 
            pause; end;
        
        [ ty, ~, nargo ] = filetype( filelist{jj} );
        
        if( ~isequal(lastfolder,folderlist{jj}) );
            clear cleanpath;
            oldpath = addpath( folderlist{jj} );
            cleanpath = onCleanup( @() path(oldpath) );
            lastfolder = folderlist{jj}; end;
        
        switch ty;
            case {'s','script'};
                ttest_run_script_in_workspace_jaskldlhajskld7897925hjlhjsfk23_( filelist{jj} ); 
            case {'f','function'};
                if( nargo==0 );
                    if( isoctave );
                        [~,file_] = fileparts( filelist{jj} ); 
                    else
                        file_ = filelist{jj}; end;
                    dbstop( 'in', file_, 'if', 'returnfalse( cellfun( @(x) ttest.void( @() x() ), localfunctions ), ''ttest'');' );
                    ttest_run_function_in_workspace_jaskldlhajskld7897_( filelist{jj} );
                    clearat( 'in', file_ );
                else;
                    warning( 'ttest:function', 'Function for unit tests must have no output arguments in order to use it as a unit test function. File skipped: %s', filelist{jj} ); end;
            case {'c','class'};
                error( 'ttest:fatal', 'Class based tests are not supported by TTESTs.' ); 
            otherwise;
                error( 'ttest:fatal', 'Programming error' ); end;
        ttest_last_id = TTEST( 'initlast' );
        if( TTEST( ttest_last_id, 'errorflag' ) );
            failurelist = [failurelist newline filelist_original{jj}];  %#ok<AGROW>
            if( opt.verbose>=1 );
                fprintf( 'Failure in test %s', filelist_original{jj} ); end;
        else;
            successlist = [successlist newline filelist_original{jj}];  %#ok<AGROW>
            if( opt.verbose>=1 );
                fprintf( '\nDone    %s', filelist_original{jj} ); end; end;
	catch me;
        failurelist = [failurelist newline filelist_original{jj}];  %#ok<AGROW>
        warning( 'runttests:runtime', 'Error while executing %s (%s)\n  Id:      %s\n  Message: %s\n  Full error description:\n%s', ...
                 filelist_original{jj}, filelist{jj}, me.identifier, me.message, me2str(me,opt.verbose) ); end; 
    if( opt.verbose>=1 );
        duration_subtest = toc( starttime_subtest );
        fprintf( ' (Duration: %f sec)\n__________\n', duration_subtest ); end; end;

% print out summary
%%%%%%%%%%%%%%%%%%%%%

if( ~isempty(successlist) );
    successflag_ = true;
    if( opt.verbose>=1 );
        fprintf( 1, '\nSuccessfull tests:\n%s\n\n', successlist ); end; end;

if( ~isempty(failurelist) );
    successflag_ = false;
    if( opt.verbose>=0 );
        fprintf( 2, '\nFailed tests:\n%s\n\n', failurelist ); end; end;

if( opt.verbose>=1 );
    duration_test = toc( starttime_test );
    fprintf( '  (Duration: %f sec)\n__________\n', duration_test ); end;
     
if( nargout==1 );
    successflag = successflag_; end;
    
% delete created doctest files
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if( opt.doctest );
    for i = 1:numel( filelist );
        delete( filelist{i} ); end; end;

end

function ttest_run_function_in_workspace_jaskldlhajskld7897_( ttest_name_jaskldlhajskld7897925hjlhjsfk_ );
    [~,ttest_file_jaskldlhajskld7897925hjlhjsfk_,~] = fileparts( ttest_name_jaskldlhajskld7897925hjlhjsfk_ );
    eval( ttest_file_jaskldlhajskld7897925hjlhjsfk_ );
end

function ttest_run_script_in_workspace_jaskldlhajskld7897925hjlhjsfk23_( ttest_name_jaskldlhajskld7897925hjlhjsfk23 );

        run( ttest_name_jaskldlhajskld7897925hjlhjsfk23 );
    
end

function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.   

% This Source Code Form is subject to the terms of the Mozilla Public
% License, v. 2.0. If a copy of the MPL was not distributed with this
% file, You can obtain one at https://mozilla.org/MPL/2.0/.
