function [sec,subsec] = sectionname( stackidx );

% get string of section name

ds = dbstack();

execution_file = ds(stackidx+1).file;
execution_line = ds(stackidx+1).line;

fid = fopen( execution_file );
sec = '';
subsec = '';
current_line = 1;
if( ~isequal(fid,-1) );
    while( true)
        tline = fgetl( fid );
        if( isa(tline,'double') && isequal(tline,-1) );
            break; end;
        tline = strtrim( tline );
        if( numel(tline>=19) && isequal(tline,'%TTEST_AUTOGENERATE') || ~ischar(tline) );
            break; end;
        if( startsWith(tline,'%%%') )
            subsec = tline; 
        elseif( startsWith(tline,'%%') );
            subsec = '';
            sec = tline; end;
        if( execution_line <= current_line )
            break; end;
        current_line = current_line + 1; end;

    fclose( fid );
    if( numel(subsec)>=2 );
        subsec = subsec(2:end); end;
    if( numel(sec)>=3 );
        sec = sec(3:end); end; end;
end
