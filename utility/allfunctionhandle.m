function [ ret, ftype_s ] = allfunctionhandle( funname, varargin );
% This function retrieves handles to local functions in a file
% [ handle, ftype ] = allfunctionhandle( filename, [funnameout], [options] );
%
% Input:
%   filename            string, function name
%   funnameout          string or cell array of strings, default = [], name of local function to be returned. 
%   flag                string, optional, if given, it must be equal to 'assignin'
% 
% Options:
%   'assignin'          If given, then assignin is used to create variables of the function handles in the callers workspace 
%   'nostatic'          this option does not work in static workspaces, but better in non-static workspaces
%   'linenumber',arr    vector of integers, default = 1:inf. If given, then only at given linenumbers will debugcode be injected. 
%                       Usually can be set to [], in which case this function is much faster
%   'nested'            (experimental) if given, function handles to nested functions are also retrieved.
%                       Evaluating handles to nested functions is usually not possible, since some variables are missing
%
% Output:
%   handle  cell array of handles
%           If funnameout is given, only the handles to funnameout are returned (if this local function exists).
%           If funnameout is a string, than handle is function_handle and not a cell array
%           If flag=='isequal', then assignin is used to create variables of the function handles in the callers workspace 
%   ftype   type of functions of handle. Can be 'b'(uiltin), 'l'(ocal), 'n'(ested), 'p'(rivate)
%           So far, this function cannot retrieve handles to nested and builtin functions.
%
% Example:
%   h = allfunctionhandle( 'spy' );
%   imagesc( h{1}() );
%
% Note:
%   The called function must be sufficiently well behaved in order that this function works.
%     The default options of this function are such that it will work in most times.
%     This means, it must not call itself before accesses any variable
%     Setting the right options, is very important for this function.
%


% XX add options 'private', 'subfunction', 'nested' -> only return those functions
% XX local/nested functions in private folder not working yet

    persistent handle;
    persistent db;
    persistent nested;
    
    % logic for injected functions
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    if( isequal(funname,'--clear') );
        % this is called by the injected function
        evalin( 'caller', 'clear' );
        ret = false;
        return;
    elseif( iscell(funname) );
        % this is called by the injected function
        % here we get the local functions
        evalin( 'caller', 'clear; ' );
        if( nargin>=2 );
            %overwrite some often used functions with variable names
            evalin( 'caller', ['clear; ' varargin{1} ' = {}; nargin = {}; nargout = {}; narginchk = {};'] ); end;
        if( iscolumn(funname) );
            funname = funname'; end;
        handle = funname;
        ret = false; 
        return; 
    elseif( isa(funname,'function_handle') );
        % this is called by the injected function
        % here we get the nested functions
        nested{end+1} = funname;
        ret = false; 
        return; end;

    % parse input
    %%%%%%%%%%%%%%%%%
    assert( isa(funname,'char') || isa(funname,'string'), 'allfunctionhandle:input', 'First input must be the name of the function.' );
    %funnameout = parsem( 'funnameout', varargin, [] );
    [verbose,varargin] = ttest.parsem( {'verbose','v'}, varargin, 1 );
    [nonstaticflag,varargin] = ttest.parsem( 'nonstatic', varargin );
    [assigninflag,varargin] = ttest.parsem( {'assignin','assign'}, varargin );
    [linenum,varargin] = ttest.parsem( {'linenum','linenumber'}, varargin, nan );
    [nestedflag,varargin] = ttest.parsem( 'nested', varargin );
    [privateflag,varargin] = ttest.parsem( 'private', varargin, true );
    if( nestedflag );
        if( isoctave );
            warning( 'TTEST:nested', 'Nested functions can only be returned in Matlab.' );
            nestedflag = 0;
        else;
            warning( 'TTEST:nested', 'Returning function handles from nested functions is experimental.\nIt may work, but the function handle is most likely of no use.' );  end; end;
    if( numel(varargin)>=1 );
        funnameout = varargin{1}; 
    else;
        funnameout = []; end;
   % assert( ~(~isempty(funnameout) && assigninflag), 'allfunctionhandle:opt', 'Option ''assign'' is not possible together with the specification of ''namefunout''.' );

    % pre processing
    %%%%%%%%%%%%%%%%%
    ftype_s = '';
    handle = [];
    nested = {};
    nestedname = {};
    funname = char( funname );  % cast filename to char
    [~,filename,filedir,ftype,numargin,numargout] = parsename( funname );

    if( verbose>=1 );
        fprintf( 'Retrieve handles to local functions.\n' ); end;
    switch ftype(1);  % identify type of file
        case 'b';
            if( verbose>=1 );
                fprintf( '    Retrieving of handles to local functions of built-ins is not yet implemented.\n' ); end;
            
        case {'s','f'};
            if( ismatlab && nestedflag );  % get nested functions
                if( verbose>=0 );
                    fprintf( 'Retrieve handles to nested functions. This is experimental.\n' ); end;
                ci = getcallinfo( filename );
                for i = 1:numel(ci);
                    if( strcmp(ci(i).type,'nested-function') );
                        nestedname{end+1} = ci(i).name; end; end; end;  %#ok<AGROW>
            
            
            if( strcmp(funname,'allfunctionhandle') );
                % workaround so that we can retrieve local functions of this function too.
                handle = localfunctions;
            else;
                db = dbstatus( funname ); % get breakpoints
                dbclean = onCleanup( @() cleanbreakpoint(funname,db) );
                dbclear( funname ); % delete breakpoints
                inject( funname, funname, nonstaticflag, linenum, nestedname, verbose );
                execute( funname, numargin, numargout );
                ftype_s = [ftype_s repmat( 'l', [1 numel(handle)] ) repmat( 'n', [1 numel(nested)] )]; end;

        case 'c';
            fprintf( '    Retrieving of handles to local functions of classes is not yet implemented.\n' );

        otherwise;
            error( 'allfunctionhandle:filetype', 'Wrong file type' ); end;
        
        

    %get functions in private folder
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    if( privateflag );
        if( verbose>=1 );
            fprintf( 'Retrieve handles to private functions.\n' ); end;
        privatehandle = getprivate( filedir );
    else;
        privatehandle = []; end;

    handle = [handle nested privatehandle];
    ftype_s = [ftype_s repmat( 'p', [1 numel(privatehandle)] )];

    % post processing
    %%%%%%%%%%%%%%%%%%%
    ret = selectret( filename, funnameout, handle ); %only return function handle to functions in funname (when given)
    if( assigninflag ); % assign in callers workspace if option is given
        for i = 1:numel( ret );
            name_i = strrep( func2str(ret{i}), '/', '_');
            assignin( 'caller', name_i, ret{i} ); end; 
    else;
        if( ~isempty(funnameout) && ~iscell(funnameout) );
            ret = ret{1}; end; end;
    
end

function cleanbreakpoint( funname, db )
    if( isoctave() );
        maxlinenum = ttest.countline( funname );
        linenum = 0;
        while( true && linenum<=maxlinenum );
            linenum = linenum + 1;
            dbclear( 'in', funname, 'at', num2str(linenum) ); end;
    else;
        dbclear( funname );
        dbstop( db ); end;
end


function inject( filename, name, nonstaticflag, linenum, nestedname, verbose );
% injects command into the function

    starttime = clock;
    
    evalstringpre = 'return2nd( {';

    if( nonstaticflag );
        % for non-static workspaces
        evalstring10 = ['allfunctionhandle( localfunctions, ''''' filename '''''  )'];
    else;
        % for static workspaces
        evalstring10 = ['allfunctionhandle( localfunctions )']; end;
    
    evalstring20 = cell( 1, numel(nestedname) );
        for i = numel(nestedname):-1:1;
            evalstring20{i} = ['allfunctionhandle(@' nestedname{i} '), ']; end;
        evalstring30 = '[]';  % otherwise we have a trailing comma
        evalstringpost = '}, false);';

    evalstring = [evalstringpre evalstring10 ', ' evalstring20{:} evalstring30 evalstringpost];
    dbstop( 'in', name, 'if', evalstring );
    
    % for all remaining lines
    try;
        if( isoctave() );
            maxlinenum = ttest.countline( name );
        else;
            maxlinenum = inf; end;
        if( any(isnan(linenum)) );
            linenum_ = dbstatus( name );
            linenum = linenum_.line;
            while( true && linenum<=maxlinenum );
                linenum = linenum + 1;
                dbstop( 'in', name, 'at', num2str(linenum), 'if', 'allfunctionhandle( ''--clear'' )' ); end;
        else;
            for i = 1:numel( linenum );
                 dbstop( 'in', name, 'at', num2str(linenum(i)), 'if', 'allfunctionhandle( ''--clear'' )' ); end; end;
    catch me;
        if( ~strcmp(me.identifier,'MATLAB:lineBeyondFileEnd') );
            rethrow( me ); end; end;
    
    if( etime(clock,starttime)>5 && verbose>=1 );
        fprintf( 'You may want to consider the option ''linenumber'' to speed up this function.\n' ); end;
    
end

function execute( name, numargin, numargout );
% executes the injected function

    starttime = clock;
    w = warning( 'error', 'MATLAB:Debugger:dbstopForNonMfunction' );  %#ok<CTPCT>
    cleanWarning = onCleanup( @() warning(w) );
    argin = repmat( {[]}, [1 abs(numargin)] );
    argout = repmat( {[]}, [1 abs(numargout)] );
    try;
        % if we test scripts, the variables in the callers workspace are also cleared, since the workspaces are the same
        % thus we feval the function in a subfunction where we do not need variables
        try;
            execute_worker( name, argin, argout )
        catch me;
            switch me.identifier
                case {'MATLAB:needMoreRhsOutputs'};
                    execute_worker_nolhs( name, argin );
                otherwise;
                    if( strfind(me.message,'undefined in return list') );  % octave error
                    else;
                        rethrow( me ); end; end; end;
    catch me;
        switch me.identifier;
            case 'Octave:undefined-function';
            case 'MATLAB:refClearedVar';
            case 'MATLAB:Debugger:dbstopForNonMfunction';
            case 'MATLAB:unassignedOutputs';
            otherwise;
                if( ismatlab );
                    rethrow( me );
                else;
                    throw( me ); end; end; end;
    if( etime(clock,starttime)>5 );
        fprintf( 'You may want to consider the option ''nonstatic'' to speed up this function.\n' ); end;
end

function execute_worker( name, argin, argout )  %#ok<INUSL>
    [~,argout{:}] = evalc( 'feval( name, argin{:} );' );  %#ok<NASGU>    
end

function execute_worker_nolhs( name, argin )
% if we test scripts, the variables in the callers workspace are also cleared, 
% since the workspaces are the same
    feval( name, argin{:} );
end

function handle = getprivate( fundir );
% retrieves function handles from functions in private folder

    try;
        olddir = cd( fundir );
        cleandir = onCleanup( @() cd(olddir) );
        cd private;
        list_m = dir( '*.m' );
        list_p = dir( '*.p' );
        list2_m = cell( 1, numel(list_m) );
        for i = 1:numel( list_m );
            list2_m{i} = str2func( strrep(list_m(i).name, '.m', '' ) ); end;
        list2_p = cell( 1, numel(list_p) );
        for i = 1:numel( list_p );
            list2_p{i} = str2func( strrep(list_p(i).name, '.p', '' ) ); end;
        handle = [list2_m list2_p];
    catch me;
        handle = {};
        if( strcmp(me.identifier,'MATLAB:cd:NonExistentFolder') );
        elseif( strfind(me.message,'private: No such file or directory') );  % Octave error
        else;
            if( ismatlab );
                rethrow( me );
            else;
                throw( me ); end; end; end;
end

function ret = selectret( filename, funnameout, handle )
    % only return function handle to functions in funname (when given)
    
    ret = handle;
    if( nargin>=2 && ~isempty(funnameout) );
        if( ~iscell(funnameout) );
            funnameout = {funnameout}; end;
        retnew = {};
        for i = 1:numel( funnameout );
            retname = cellfun( @func2str, ret, 'UniformOutput',false );
            idx = cellfun( @(x) isequal(x,funnameout{i}), retname );
            if( ~any(idx) );
                warning( 'allfunctionhandle:name', 'Function with name ''%s'' not found in file ''%s''.', funnameout{i}, filename ); 
            else;
                retnew{end+1} = ret{idx}; end; end;  %#ok<AGROW>
        ret = retnew; end;
end

function [ longname, filename, filedir, type, numargin, numargout ] = parsename( filename )
    type = '';
    longname = which( filename );
    idx1 = find( longname=='(', 1 );
    idx2 = find( longname==')', 1, 'last' );
    
    if( any(strfind(longname,'built-in')) || any(strfind(longname,'builtin')) );
        type = 'b'; end;
    if( ~isempty(idx2) );
        longname( idx2:end ) = []; end;    
    if( ~isempty(idx1) );
        longname( 1:idx1 ) = []; end;
    
    
    [type_,numargin,numargout] = filetype( filename );
    if( isempty(type) );
        type = type_; end;
    
    [filedir,name_,ext_] = fileparts( longname );
    filename = [name_ ext_];
end

% function ret = container( what, value );
% % this function is a safeguard against the called function 
% % maybe the callee wants to mess with the caller
%     persistent c_
%     if( isempty(c_) );
%         c_ = struct; end;
%     try;
%         ret = c_.(what);
%     catch;
%         ret = [];
%         c_.(what) = []; end;
%     
%     if( nargin==2 );
%         ret = c_.(what);
%         c_.(what) = value; end;
% end

function dummy; end  % Generates an error, if the 'end' of a function is missing.  

% This Source Code Form is subject to the terms of the Mozilla Public
% License, v. 2.0. If a copy of the MPL was not distributed with this
% file, You can obtain one at https://mozilla.org/MPL/2.0/.
