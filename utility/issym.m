function flag = issym(x)
% [ flag ] = issym( x )
% Tests if an object is symbolic, including vpa.
% flag = logical(strcmp(class(x),'sym'));
%
% E.g.: issym( [sym('23.2') 0] )
%
% Depends on: //
%
% See also: isvpa
%


    
    flag = logical( strcmp(class(x),'sym') ); %#ok<STISA>
end