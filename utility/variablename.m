function [ varname, allvarname, varidx ] = variablename( varargin )
% Returns the variable names in a function handle
% [ name ] = variablename( h1, ..., hn)
% Input:
%   hi      function_handles or anything else
%           anything else will be converted to a nullary function for processing
%   
% Output:
%   varname         cell array of cell array of char-arrays
%   allvarname      all varnames without duplicates and sorted
%   idx             cell array of vectors of integers, where the integers represent the varnames
%
% Ex.: variablename( @(x,y) x+y, @(y,z) ); %yields varname=={{'x','y'},{'y','z'}}; allvarname=={'x','y'}; idx=={[1,2],[2,3]};
%


    varname = cell( 1, numel(varargin) );
    [varname{:}] = deal( {} );
    for i = 1:numel( varargin );
        if( ~isa(varargin{i},'function_handle') );  % make everything to function handles
            varargin{i} = @() varargin{i}; end;
        var = func2str( varargin{i} );
        var = [',' var(3:find(var==')',1) - 1) ','];  % remove '@('  and ')', add ',' so that it is easier to parse
        idx = find( var == ',' );  % find ','
        for j = 1:numel( idx ) - 1;
            val = var(idx(j)+1:idx(j+1)-1);
            if( ~isempty(val) );
                varname{i}{end+1} = val; end; end; end;
    [allvarname,~,varidx] = unique( [varname{:}] );  % sort variable names and give them integer values
    if( isempty(varidx) );
        varidx = {cell( 1, 0 )};
    else;
        varidx = mat2cell( varidx.', 1, cellfun('prodofsize',varname) ); end;
end

% This Source Code Form is subject to the terms of the Mozilla Public
% License, v. 2.0. If a copy of the MPL was not distributed with this
% file, You can obtain one at https://mozilla.org/MPL/2.0/.
