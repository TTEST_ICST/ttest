function [ ret ] = eval2nd( first, second );  %#ok<INUSL>
% evalutes the second argument
% [ ret ] = eval2nd( first, second ); 
ret = second(); 
end