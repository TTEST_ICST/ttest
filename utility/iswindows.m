function ret = iswindows ()
    % returns true when run on windows
    ret = ispc;
end

function dummy; end  %#ok<DEFNU> %Generates an error, if the 'end' of a function is missing. 