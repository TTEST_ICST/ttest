function [ ret, st ] = isequalproperty( varargin )
% Returns false whenever the underlying properties of all inputs are not the same
% [ ret, st ] = isequalproperty( C1, C2, ... )
%
% Input:
%   Ci                  anything
%
% Output:
%   ret                 bool, true when the underyling types of call Ci are the same
%                       tested is: class, sparsity, size, 
%   st                  struct, having the fields .class, .sparse, .size
%
% E.g.: [ret,st] = isequalproperty( [2 3],single([10 30]) );
% 

    ret_class = true;
    ret_size = true; 
    ret_sparse = true;
    for i = 1:numel( varargin ) - 1;
        if( isa(varargin{i},'double') && isa(varargin{i+1},'logical') && ...
            isequal(double(logical(varargin{i})),varargin{i}) ...
          );
            % do nothing
        elseif( isa(varargin{i},'logical') && isa(varargin{i+1},'double') && ...
                isequal(double(logical(varargin{i+1})),varargin{i+1}) ...
              );
            % do nothing
        elseif( ~isequal(class(varargin{i}),class(varargin{i+1})) );
            ret_class = false; end;
        if( isbuiltin(varargin{i})>=1 );
            if( ~isequal(size(varargin{i}),size(varargin{i+1})) );
                ret_size = false; end;
            if( ~isequal(issparse(varargin{i}),issparse(varargin{i+1})) );
                ret_sparse = false; end; end;
        
        if( nargin==1 );
            ret = ret_class && ret_size && ret_sparse; 
            if( ~ret ); 
                break; end; end; end;

    ret = ret_class && ret_size && ret_sparse; 
    if( nargout==2 );
        st.class = ret_class;
        st.size = ret_size;
        st.sparse = ret_sparse; end;

end

function dummy; end %#ok<DEFNU> %Generates an error, if the 'end' of a function is missing.   
