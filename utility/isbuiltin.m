function [ ret ] = isbuiltin( x );
% checks if variable is a builtin or fundamental type
% [ ret ] = isbuiltin( x );
%
% Input:
%   x       thing to check
%
% Output:
%   ret     2  ...  fundamental type
%           1  ...  any other matlab type
%           0  ...  anything else (user defined type)
% Example:
%   isbuiltin( int8(2) )
%


    switch class( x )
        case {'double','single','int8','uint8','int16','uint16','int32','uint32','int64','uint64', ...
              'char','string','logical','function_handle','table','timetable','struct','cell' ...
             }; 
            ret = 2;
        case {'datetime'};
            ret = 1;
        otherwise;
            ret = 0; end;
end

function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
