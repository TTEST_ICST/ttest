function [ ret, Loca, Locb ] = subset( A, B );
% checks if A is a subset of B
% [ ret, Loca, Locb ] = subset( A, B );
%
% Input:
%   A,B     cell arrays OR matrices OR vectors
%
% Output:
%   ret         true is A \subset B
%   Loca,Locb   equivalent output of ismember( A, B )
%
% Note:
%   If A,B are row-vectors then the elements of the vectors
%              matrices (including column-vectors) then, the column vectors of both matrices
%              cell arrays, then the elements of the cell arrays
%   are assumed to be the elements of sets
%
%   If cell arrays are given, nans are treated differently, in particular: 
%       subset( {nan}, {nan} )  % returns true
%       subset( nan, nan )  % returns false
%
% E.g.: subset( [1 2], [1 2 3] );  % returns true
%       subset( [1 1;1 2].', [1 1; 1 2; 2 3].' );  % returns true
%       subset( {[1],[1 2]}, {[1],[1 2],[1;2]} );  % returns true
%


    if( ~iscell(A) && isrow(A) );
        [Loca,Locb] = ismember( A, B );
    elseif( ~iscell(A) );
        [Loca,Locb] = ismember( A.', B.', 'rows' );
        Loca = Loca.';
        Locb = Locb.';
    else;
        Loca = false( 1, numel(A) );
        Locb = zeros( 1, numel(A) );
        for i = 1:numel( A );
            for j = 1:numel( B );
                if( isequal(A{i},B{j}) || isequaln(A{i},B{j}) );
                    Loca(i) = true;
                    Locb(i) = j;
                    break; end; end; end; end;

    ret = all( Loca );
end

function dummy; end %#ok<DEFNU> %Generates an error, if the 'end' of a function is missing.