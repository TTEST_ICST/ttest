
function st = namevalue( varargin );
% namevalue( name, value, [options] );
% returns a list of N name-value pairs, where N is between minlen and maxlen
% Input:
%   minlen      integer, default: minlen=0, maxlen=numel(name)
%   maxlen      integer or inf, default: maxlen=minlen, if maxlen>numel(name), then maxlen=numel(name)
%               if maxlen is given, minlen must be given too
%   name        cell array of strings, the 'name's
%   value       cell array of functions returning, the 'values's
%
% Output:
%   x           cell array of name value pairs
% 
% E.g.: namevalue( 1, 2, {'opt1','opt2'}, {{nan, 0, -1}, @randn} )

    if( isequal(varargin,{'register'}) );
        st = [];  % cannot be registered automatically
        return; end;    
    
    opt = struct;
	[ opt, varargin ] = ttest_strategy_opt_parsem( opt, varargin{:} );
	[ opt, varargin ] = ttest_drawer_opt_parsem( opt, varargin{:} );
    [ opt.allowmultiple, varargin ] = ttest.parsem( {'allowmultiple'}, varargin );

    
    assert( numel(varargin)==2 && iscell(varargin{1}) && iscell(varargin{2}), ...
        'TTEST:NAMEVALUE', 'Wrong arguments given. First two arguments must be cell arrays consisting of the allowed names and values, respectively.' );
    assert( numel(varargin{1}) == numel(varargin{2}), ...
         'TTEST:NAMEVALUE', 'Cell arrays of names and values must have the same number of elements.' );
    

    % determine number of name value pairs
    name = varargin{1};
    value = varargin{2};
    
    if( isempty(opt.minsze) );
        opt.minlen = 0; end;
    if( isempty(opt.maxsze) );
        opt.maxlen = inf; end;
        
    drawer = @( state ) ttest.namevalue_drawer( state, opt, name, value );

    st = ttest_strategy_c( drawer, [], [], opt );
            
end

function dummy; end %#ok<DEFNU> %Generates an error, if the 'end' of a function is missing.   

% This Source Code Form is subject to the terms of the Mozilla Public
% License, v. 2.0. If a copy of the MPL was not distributed with this
% file, You can obtain one at https://mozilla.org/MPL/2.0/.
