function [ ex_state, raw ] = sqmatrix_drawer( state, opt );

	%XX needs to draw nan values as well
    
    ex_state = ttest_state_c( 'raw', [] );

    num_draw = state.num_draw;
    % get size
    while( true );
        if( isempty(opt.minsze) && isempty(opt.maxsze) );  % both minsz and maxsz not given
            opt.minsze = floor( sqrt(num_draw) );
            opt.maxsze = ceil( 2^(sqrt(num_draw)) );
        elseif( ~isempty(opt.minsze) && isempty(opt.maxsze) ) ;  % minsz given
            opt.maxsze = opt.minsze;
        elseif( ~isempty(opt.maxsze) && isempty(opt.minsze) );  % maxsz given
            opt.minsze = floor( sqrt(num_draw) );
        else;
            end;  % do nothing
        assert( numel(opt.minsze)==1 && numel(opt.maxsze)==1, 'sqm_drawer:input', '''minsze'' and ''maxsze'' must be given as scalars.' );
        opt.maxsze = max( opt.minsze, opt.maxsze );

        sz = ttest.randcauchy( 1, opt.minsze, min(opt.maxsze,2^24), num_draw/30 );
        
        if( prod(sz)<1e3 );
            break; end; end;
    % degenerate cases
    if( isequal(sz,0) );
        raw = zeros( 0, 0 );
        return; end;
    
    type = mod( round( state.lvl*1000 ), 42 ) + 1;
    switch lower( type ); 
        case {1,'zeros'};
            raw = zeros( sz );
        case {2,'ones'};
            raw = ones( sz );
        case {3,'randn'};
            raw = randn( sz );
        case {4,'matrix'};
            raw = randn( sz );
            % raw = matrix_drawer( sze, lvl, opt );  % XX missing
        case {5,6,'array'};
            raw = randn( sz );
            % raw = array_drawer( state, opt );  % XX missing
        case {7,'compan'};
            raw = compan( ttest.randx(1,sz(1)+1) );
        case {8,'hadamard'};
            raw = [];
            while( isempty(raw) );
                try; raw = hadamard( sz(1) ); 
                catch; sz(1) = sz(1)+1; end; end;
            if( sz(1)<opt.minsze || sz(1)>opt.maxsze );
                raw = ttest.randx( sz(1) ); end;
        case {9,'hilb','hilbert'};
            raw = hilb( sz(1) );
        case {10,'invhilb','invhilbert','inversehilbert'};
            raw = invhilb( sz(1) );
        case {11,'magic'};
            raw = magic( sz(1) );
        case {12,'pascal'};
            switch randi(3);
                case 1; raw = pascal( sz(1) );
                case 2; raw = pascal( sz(1), 1 );
                case 3; raw = pascal( sz(1), 2 ); end;
        case {13,'rosser','wilkinson'};
            if( sz(1)==8 );
                raw = rosser;
            else;
                raw = wilkinson( sz(1) ); end;
    case {14,'binomial'};
        if( ismatlab );
            raw = gallery( 'binomial', sz(1) );
        else;
            raw = randn( sz ); end;
        
        case {15,'chebspec'};
            raw = gallery( 'chebspec', sz(1), randi(2)-1 );
        case {16,'chebvand'};
            raw = gallery( 'chebvand', sz(1) );
        
        case {17,'clement'};
            raw = gallery( 'clement', sz(1), randi(2)-1);
        
        % 'condex' skipped
        case {18,'condex'};
            switch sz(1);
                case {1,2}; raw = ttest.randx;
                case 3; raw = gallery( 'condex', 3, 2, abs(ttest.randx) );
                case 4; raw = gallery( 'condex', 4, 1, abs(ttest.randx) );
                otherwise; raw = gallery( 'condex', sz(1), 4, abs(ttest.randx) ); end;
        
        case {19,'dramadah'};
            raw = gallery( 'dramadah', sz(1), randi(3) );
        
        case {20,'frank'};
            raw = gallery( 'frank', sz(1), randi(2)-1 );
        case {21,'gcdmat'};
            raw = gallery( 'gcdmat', sz(1) );
        case {22,'gearmat'};
            raw = gallery( 'gearmat', sz(1) );
        case {23,'grcar'};
		    switch sz(1);  % fix for octave
			    case 0; raw = [];
				case 1; raw = [1];
				case 2; raw = [1 1; -1 1];
				otherwise; raw = gallery( 'grcar', sz(1) ); end;
        case {24,'invol'};
            raw = gallery( 'invol', sz(1) );
        case {25,'ipjfact'};
            if( sz(1)==1 );
                raw = 2;
            else;
                raw = gallery( 'ipjfact', sz(1) ); end;
        case {26,'kahan'};
            raw = gallery( 'kahan', sz(1) );
        case {27,'lauchli'};
            raw = randn( sz );
            % raw = gallery( 'lauchli', sz(1) );
            % XX move lauchli to matrix, lauchli is a (n+1) x n matrix
        case {28,'lehmer','totallynonnegative'};
            raw = gallery( 'lehmer', sz(1) );
        case {29,'lesp'};
            if( sz(1)==1 );
                raw = -5;
            else;
                raw = gallery( 'lesp', sz(1) ); end;
        case {30,'lotkin'};
            raw = gallery( 'lotkin', sz(1) );
        case {31,'minij'};
            raw = gallery( 'minij', sz(1) );
        case {32,'neumann'};
            sz = round(sqrt(sz(1)))^2;
            if( sz>=opt.minsze && sz<=opt.maxsze && sz>1 );
                raw = full( gallery( 'neumann', sz(1) ) );
            else;
                raw = ttest.randx( sz(1) ); end;
        case {33,'orthog'};
            if( ismatlab );
                raw = real( gallery( 'orthog', sz(1), randi(6) ) );
            else;
                raw = real( gallery( 'orthog', sz(1), randi(5) ) ); end;
        case {34,35,'parter'};
            raw = gallery( 'parter', sz(1) );
        % case {35,'poisson'};  % change value of 'parter' if this gets readded
        %    raw = gallery( 'poisson', sz(1) );
        case {36,'redheff','redheffer'};
            raw = gallery( 'redheff', sz(1) );
        case {37,'riemann'};
            raw = gallery( 'riemann', sz(1) );
        case {38,'ris'};
            raw = gallery( 'ris', sz(1) );
        case {39,'smoke'};
            raw = real( gallery( 'smoke', sz(1) ) );
        case {40,'toeppen'};
            raw = full( gallery( 'toeppen', sz(1) ) );
        case {41,'tridiag'};
            raw = full( gallery( 'tridiag', sz(1) ) );
        case {42,'triw'};
            raw = full( gallery( 'tridiag', sz(1) ) );
        % case {43,44,45,46,'tgallery'};
        %    raw = tgallery( ttest.randcauchy(1,1,inf), sz(1), 1, ttest.randx, 'nocell', 'verbose',-1 );
        %    sz1 = size( raw, 1 );
        %    sz2 = size( raw, 2 );
        %    if( sz1<sz2 ); 
        %        raw = raw(1:sz1,1:sz1);
        %    elseif( sz2<sz1 ); 
        %        raw = raw(1:sz2,1:sz2); end;
        %    if( sz1<opt.minsze ); 
        %        raw(opt.minsze,opt.minsze) = 0;
        %    elseif( sz2>opt.maxsze ); 
        %        raw = raw(1:opt.maxsze,1:opt.maxsze); end;
        otherwise  % 43 to 46
            error( 'TTEST:fatal', 'Wrong input for sqmatrix_drawer. Input: %s', num2str(lower(type)) ); end;
        
        
	raw = full( raw );
        
    if( opt.invertible );
        idx = ~isfinite( raw );
        raw(idx) = opt.invertible .* sign(raw(idx));
       
        [U,S,V] = svd( raw );
        if( ~isvector(S) );
            S = diag( S ); end;
        idx = abs( S )<=opt.invertible;
        S(idx) = opt.invertible .* sign(S(idx));
        S = diag( S );
        raw = U*S*V';
   
    elseif( opt.hermitian );
        raw = (raw + raw')./2;
   
    elseif( opt.antihermitian);
        raw = (raw - raw')./2;
        
    % further transformations
    elseif( type>3 );
        if( randi(4)==1 );
            raw = fliplr( raw ); end;        
        if( randi(4)==1 );
            raw = flipud( raw ); end;                
        if( randi(4)==1 );
            raw = raw.'; end;
        if( randi(6)==1 );
            raw = round( raw*randi( 10 ) )/randi( 10 ); end;
        if( randi(20)==1 && all(isfinite(raw(:))) && ~issparse(raw) && sz(1)<100 );
            raw = pinv( raw ); end; end;
    
    if( isequal(opt.allowinf,true) && state.lvl>.9 && rand>.7 );
        raw( rand(size(raw))>state.lvl ) = (rand-.5)*inf; 
    else;
        raw( isinf(raw) ) = randi( 10 ); end;
    if( isequal(opt.allownan,true) && state.lvl>.9 && rand>.7 );
        raw( rand(size(raw))>state.lvl ) = nan; 
    else;
        raw( isnan(raw) ) = randi( 10 ); end;    
        
    % sparse is missing
    if( ~isempty(opt.allowsparse) && isequal(opt.allowsparse,false) );
        raw = double( raw ); end;
    if( islogical(raw) );
        raw = double( raw ); end;    
    while( true );
        if( ~isempty(opt.minnorm) && norm(raw)<opt.minnorm ); 
            raw = raw/norm(raw)*opt.minnorm; 
            continue; end;
        if( ~isempty(opt.maxnorm) && norm(raw)>opt.maxnorm ); 
            raw = raw/norm(raw)*opt.maxnorm;
            continue; end;
        if( ~isempty(opt.minval) && min(raw(:))<opt.minval );
            raw(raw<opt.minval) = opt.minval;
            continue; end;
        if( ~isempty(opt.maxval) && max(raw(:))>opt.minval );
            raw(raw>opt.maxval) = opt.maxval;continue; end;
        break; end;
    
    
end

function dummy; end %#ok<DEFNU> %Generates an error, if the 'end' of a function is missing.   

% This Source Code Form is subject to the terms of the Mozilla Public
% License, v. 2.0. If a copy of the MPL was not distributed with this
% file, You can obtain one at https://mozilla.org/MPL/2.0/.
