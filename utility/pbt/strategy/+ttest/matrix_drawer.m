function [ ex_state, raw ] = matrix_drawer( state, opt );

    ex_state = ttest_state_c( 'raw', [] );

    sz = ttest.randcauchy( [1,2], opt.minsze, opt.maxsze, state.num_draw/20 );
    sz = ceil( sz.^(1/2) );
    
    while( prod(sz)>2^24 );
        idx = find( sz>=2 );
        idx = idx(randi( numel(idx) ));
        sz(idx) = sz(idx) - 1; end;
    
    %degenerate cases
    if( prod(sz)==0 );
        raw = zeros( sz );
        return; end;
    
    %make random sequence
    if( ~isempty(opt.type) );
        type = opt.type;
    elseif( state.num_draw<=9 );
        type = state.num_draw;
    else;
        type = randi( 3 ) + 3; end;
    
    raw{2} = 0;
    for i = 1:opt.imax;
        while( true );    
            switch lower( type ); 
                case {1,'zeros'};
                    raw{i} = zeros( sz );
                case {2,'ones'};
                    raw{i} = ones( sz );
                case {3,'consecutive'};
                    raw{i} = reshape( 1:prod(sz), [sz 1 1] );
                case {4,'array'};
                    opt.mindim = 2;
                    opt.maxdim = 2;
                    [~,raw_] = ttest.array_drawer( state, opt );
                    raw{i} = raw_;
                case {5,'hankel'};
                    val1 = ttest.randx( 1, sz(1) );
                    if( randi(2)==1 );
                        val2 = ttest.randx( 1, sz(2) );
                    else;
                        val2 = zeros( 1, sz(2) ); end;
                    val2(1) = val1(end);
                    raw{i} = hankel( val1, val2 );
                case {6,'chebvand'};
                    raw{i} = gallery( 'chebvand', sz(1), sz(2) );            
                case {0,7,'toeplitz'};
                    if( randi(2)==1 );
                        val1 = ttest.randx( 1, max(sz) );
                        val2 = ttest.randx( 1, max(sz) );
                        val2(1) = val1(1);
                    else;
                        val1 = ttest.randx( 1, max(sz) );
                        val2 = val1; end;            
                    raw{i} = toeplitz( val1(1:sz(1)), val2(1:sz(2)) );
                case {8,'circul','circulant'};
                    raw{i} = gallery( 'circul', max(sz) );
                    raw{i} = raw{i}(1:sz(1),1:sz(2));
                case {9,'cycol'};
                    raw{i} = gallery( 'cycol', sz, round(rand*sz(1))+1 );
                otherwise;
                    error( 'ttest:fatal', 'programming error.' ); end;
                
                 %further transformations
        if( opt.type>3 );
            if( randi(4)==1 );
                raw{i} = fliplr( raw{i} ); end;        
            if( randi(4)==1 );
                raw{i} = flipud( raw{i} ); end;            
            if( randi(4)==1 );
                raw{i} = raw{i}.'; end;        
            if( randi(2)==1 );
                raw{i} = round( raw{i}*randi( 10 ) )/randi( 10 ); end;
            if( randi(2)==1 && issquare(x) );
                raw{i} = pinv( raw{i} ); end;
            if( randi(2)==1 && prod(sz)~=0 );
                val = limit( 'double' );
                raw{i}(randperm( prod(sz), randi(prod(sz)) )) = val{randi(numel(val))}; end; end;
        
        raw{i} = fixraw( raw{i}, opt );
        if( checkraw(raw{i},opt) );
            break; end; end;

   
    end

    raw = raw{1} + 1i*raw{2};
    
end

function ret = dist( varargin );
    if( nargin==0 );
        x = rand;
        m = 0;
        M = inf;
    elseif( nargin==2 );
        x = rand;
        m = varargin{1};
        M = varargin{2};
    else;
        x = varargin{1};
        m = varargin{2};
        M = varargin{3}; end;
        
    %function which gives random integers between m and M, 0<=m<=M<=inf
    k = .5; %the higher k, the smaller the returned values
    e = .5; %offset is necessary so that function works for m=0 too
    ret = round((m+e)./(1 + (m+e)./M.*x.^k - x.^k)-e/2);
end

function [ raw ] = fixraw( raw, opt );

    sz = size( raw );
    assert( all(opt.minsze<=sz) && all(sz<=opt.maxsze) );
    
    if( ~isequal(opt.allownan,true) );
        idx = isnan( raw );
        if( any(idx(:)) );
            raw( idx ) = opt.minval; end; end;
    
    if( ~isequal(opt.allowinf,true) );
        idx = isinf( raw );
        if( any(idx(:)) );
            raw( idx ) = opt.minval; end; end;
    
    if( isequal(opt.allowneg,false) );
        idx = raw<0;
        if( any(idx(:)) );
            raw(idx) = -raw(idx); end; end;
    
    if( ~isempty(opt.minval) && isempty(opt.maxval) );
        idx = raw<opt.minval;
        if( any(idx(:)) );
            raw(idx) = raw(idx) + opt.minval; end; end;
    if( ~isempty(opt.maxval) && isempty(opt.minval) );
        idx = raw>opt.maxval;
        if( any(idx(:)) );
            raw(idx) = opt.maxval - raw(idx); end; end;
    if( ~isempty(opt.minval) && ~isempty(opt.maxval) );
        idx = raw<opt.minval;
        if( any(idx(:)) );
            raw(idx) = raw(idx) - floor( (raw(idx)-opt.minval) ./ (opt.maxval-opt.minval) ).*(opt.maxval-opt.minval); end;
        idx = raw>opt.maxval;
        if( any(idx(:)) );
            raw(idx) = raw(idx) + floor( (opt.maxval-raw(idx)) ./ (opt.maxval-opt.minval) ).*(opt.maxval-opt.minval); end; end;
    
        

end

function [ ret ] = checkraw( raw, opt );  %#ok<INUSD>
    ret = true;
end

function dummy; end %#ok<DEFNU> %Generates an error, if the 'end' of a function is missing.   

% This Source Code Form is subject to the terms of the Mozilla Public
% License, v. 2.0. If a copy of the MPL was not distributed with this
% file, You can obtain one at https://mozilla.org/MPL/2.0/.
