function [ st ] = matrix( varargin );

    if( isequal(varargin,{'register'}) );
        st = {ttest.matrix,'mat'}; 
        return; end;

    opt = struct;
	[ opt, varargin ] = ttest_strategy_opt_parsem( opt, varargin{:} );
	[ opt, varargin ] = ttest_drawer_opt_parsem( opt, varargin{:} );
    
    ttest.parsem( varargin, 'test' );
    
    if( isempty(opt.minsze) );
        opt.minsze = [1 1]; end;    
    if( isempty(opt.maxsze) );  % minsz given
        opt.maxsze = [inf inf]; end;
    if( numel(opt.minsze)==1 );
        opt.minsze = [opt.minsze opt.minsez]; end;
    if( numel(opt.maxsze)==1 );
        opt.maxsze = [opt.maxsze opt.maxsze]; end;
    
    if( isequal(opt.allowcp,true) ); 
        opt.imax = 2;        
    else;
        opt.imax = 1; end;
    
    assert( all(opt.minsze>=0) && all(opt.maxsze>0), 'ttest:matrix', 'All dimensions must be non-negative.' );
    assert( isequal(2,numel(opt.minsze),numel(opt.maxsze)), 'ttest:matrix', 'Wrong length of size vectors.' );
    assert( all(opt.minsze<=opt.maxsze), 'ttest:matrix', 'Minimum value larger than maximum value.' );
    
    
    drawer = @( state ) ttest.matrix_drawer( state, opt );
    
    st = ttest_strategy_c( drawer, [], [], opt );
  
end

function dummy; end %#ok<DEFNU> %Generates an error, if the 'end' of a function is missing.   

% This Source Code Form is subject to the terms of the Mozilla Public
% License, v. 2.0. If a copy of the MPL was not distributed with this
% file, You can obtain one at https://mozilla.org/MPL/2.0/.
