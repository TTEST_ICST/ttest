function st = array( varargin )

    if( isequal(varargin,{'register'}) );
        st = {ttest.array,'arr'};
        return; end;
		
    opt = struct;
	[ opt, varargin ] = ttest_strategy_opt_parsem( opt, varargin{:} );
	[ opt, varargin ] = ttest_drawer_opt_parsem( opt, varargin{:} );
    
    ttest.parsem( varargin, 'test' );
    
    if( isempty(opt.minsze) && isempty(opt.maxsze) );  % both minsz and maxsz not given
        opt.minsze = [1 -1];
        opt.maxsze = [inf -1];
    elseif( ~isempty(opt.minsze) && isempty(opt.maxsze) );  % minsz given
        opt.maxsze = opt.minsze;
    elseif( ~isempty(opt.maxsze) && isempty(opt.minsze) );  % maxsz given
        opt.minsze = ones( 1, numel(opt.maxsze) );
    else;  % both given
        end;  % do nothing    
        
    opt.mindim = min( nnz(opt.minsze~=-1), nnz(opt.maxsze~=-1) );
    opt.maxdim = max( nnz(opt.minsze~=-1), nnz(opt.maxsze~=-1) );
    
    if( isequal(opt.allowcp,true) ); 
        opt.imax = 2;        
    else;
        opt.imax = 1; end;
    
    drawer = @( state ) ttest.array_drawer( state, opt );
    
    st = ttest_strategy_c( drawer, [], [], opt );
    

end



function dummy; end %#ok<DEFNU> %Generates an error, if the 'end' of a function is missing.   

% This Source Code Form is subject to the terms of the Mozilla Public
% License, v. 2.0. If a copy of the MPL was not distributed with this
% file, You can obtain one at https://mozilla.org/MPL/2.0/.
