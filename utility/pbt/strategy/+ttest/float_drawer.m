function [ ex_state, raw ] = float_drawer( state, opt );
    
    ex_state = ttest_state_c( 'raw', [] );

    if( ~opt.allowsingle );
        target = 'double';
        numbyte = 8;
    elseif( ~opt.allowdouble );
        target = 'single';
        numbyte = 4;
    elseif( randi(2)==1 );
        target = 'double';
        numbyte = 8;
    else;
        target = 'single';
        numbyte = 4; end;
    
    for i = 1:opt.imax;
        if( i==2 && ~isequal(opt.allowcp,true) ); 
            raw(2) = cast( 0, class(raw(1)) ); end;
        for k = -1000:0;
            if( state.num_draw<numel( opt.start) );
                state.num_draw = state.num_draw + 1;  % to prevent an infinite loop
                raw(i) = opt.start{state.num_draw};
            else;
                raw(i) = typecast( uint8(randi(256,[1 numbyte])) - 1, target ); 
                if( ~opt.allowsingle );
                    % do nothing;
                elseif( ~opt.allowdouble );
                    raw(i) = single( raw(i) );
                elseif( randi(2)==1 );
                     % do nothing
                else;
                    raw(i) = single( raw(i) ); end;
            end;
            
            raw(i) = fixraw( raw(i), opt );
            if( checkraw(raw(i),opt) );
                break; end; end; 
        assert( k<0, 'TTEST:strategy', 'Failed to generate examples.' ); end;
    
    if( numel(raw)==2 );
        raw = raw(1) + 1i*raw(2); end;

end


function [ raw ] = fixraw( raw, opt );
    if( isequal(opt.allowneg,false) && raw<0 );
        raw = -raw; end;
    if( ~isempty(opt.minval) && isempty(opt.maxval) && raw<opt.minval );
        raw = raw + opt.minval; end;
    if( ~isempty(opt.maxval) && isempty(opt.minval) && raw>opt.maxval );
        raw = opt.maxval - raw; end;
    if( ~isempty(opt.minval) && ~isempty(opt.maxval) && raw<opt.minval );
        raw = raw + floor( (raw-opt.minval) / (opt.maxval-opt.minval) )*(opt.maxval-opt.minval); end;
    if( ~isempty(opt.minval) && ~isempty(opt.maxval) && raw>opt.minval );
        raw = raw + floor( (opt.maxval-raw) / (opt.maxval-opt.minval) )*(opt.maxval-opt.minval); end;    
        
end

function [ ret ] = checkraw( raw, opt );
    ret = true;
    
    if( isnan(raw) );
        ret = ~isequal( opt.allownan, false );
        return; end;
    
    if( isinf(raw) );
        ret = ret && ~isequal( opt.allowinf, false ); end;
    if( ~isempty(opt.minval) );
        ret = ret && raw>=opt.minval; end;
    if( ~isempty(opt.maxval) );
        ret = ret && raw<=opt.maxval; end;
    if( isequal(opt.allowneg,false) );
        ret = ret && raw>=0; end;
    
end

function dummy; end %#ok<DEFNU> %Generates an error, if the 'end' of a function is missing.   

% This Source Code Form is subject to the terms of the Mozilla Public
% License, v. 2.0. If a copy of the MPL was not distributed with this
% file, You can obtain one at https://mozilla.org/MPL/2.0/.

