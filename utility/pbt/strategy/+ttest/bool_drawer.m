function [ ex_state, raw ] = bool_drawer( state, opt );

    ex_state = ttest_state_c( 'raw', [] );

    %determine length of output sequence
    
    if( opt.minsze + state.num_draw < opt.maxsze );
        opt.minsze = opt.minsze + state.num_draw; end;
    opt.len = ttest.randcauchy( [1 1], opt.minsze, opt.maxsze, state.num_draw/20 );
    opt.len = min( opt.len, 2^32-1 );
    
    %degenerate cases
    if( opt.len==0 );
        raw = logical( [] );
        return; end;
    
    %make random sequence
    if( ~isempty(opt.type) );
        type = opt.type;
    elseif( state.num_draw<=9 );
        type = state.num_draw;
    else;
        type = randi( 7 ) + 2; end;
    
    switch lower( type ); 
        case {1,'false'};
            x = false( 1, opt.len );
        case {2,3,'true'};
            x = true( 1, opt.len );
        case {0,4,'mix'};
            x = logical([]);
            while( numel(x)<opt.len );
                opt_ = opt;
                opt_.minsze = randi( opt.len );
                opt_.maxsze = opt_.minsze;
                opt_.type = randi( 9 );
                [~,x_] = ttest.bool_drawer( state, opt_ );
                x = [x x_]; end; %#ok<AGROW>
        case {5,'periodic','period'};
            if( opt.len>1 );
                periodlen = randi( ceil(opt.len/2) );
            else;
                periodlen = 1; end;
            period = logical( randi(2,1,periodlen)-1 );
            x = repmat( period, 1, floor(opt.len/periodlen) );
        case {6,'random'};
            x =  randi( 2, 1, opt.len ) - 1; 
        case {7,'log'};
            x = diff( floor(log(1:opt.len+1)) );
        case {8,'thuemorse','thue-morse'}; % Thue-Morse binary sequence
            % ben payne (2020). Aperiodic array generation (https://www.mathworks.com/matlabcentral/fileexchange/28474-aperiodic-array-generation), MATLAB Central File Exchange. Retrieved October 21, 2020. 
            l = max( ceil(log2(opt.len)), 1 );
            x = zeros( 1, l );
            x(1) = logical(randi(2)-1);
            for n = 1:l;
                x(2^(n-1)+1:2^n)= ~x(1,1:2^(n-1)); end;
            x(opt.len+1:end) = [];
        case {9,'fibonacci','fib'}; %Fibonacci word
            % ben payne (2020). Aperiodic array generation (https://www.mathworks.com/matlabcentral/fileexchange/28474-aperiodic-array-generation), MATLAB Central File Exchange. Retrieved October 21, 2020. 
            l = max( ceil(log2(opt.len)), 1 );
            initialAry = [0];
            nextAry = [0 1];
            if( l==1 );
              x = logical( nextAry );
            else;
                for gen = 2:l;
                  x = zeros( 1, size(initialAry,2)+size(nextAry,2) );
                  srcindx = 1;
                  for indx = 1:size( nextAry, 2 );
                    if( nextAry(indx)==0 );
                      x(1,srcindx:srcindx+1) = [0 1];
                      srcindx = srcindx + 2;
                    else
                      x(1,srcindx) = 0;
                      srcindx = srcindx + 1; end; end;
                  initialAry = nextAry;
                  nextAry = x; end; end;
        end;

        %further transformations
        if( opt.type>2 );
            if( randi(2)==1 );
                x = mod( cumsum(x), 2 ); end;   
            if( randi(2)==1 );
                x = flip( x ); end;
            if( randi(2)==1 );
                x = ~x; end; end;
                
            
        %check if length is in bounds minlen/maxlen
        if( numel(x)<opt.minsze );
            switch randi(3);
                case 1;
                    x(end+1:opt.minsze) = logical( randi(2,1,opt.minsze-numel(x))-1 );
                case 2;
                    x(end+1:opt.minsze) = true;
                case 3;
                    x(end+1:opt.minsze) = false; end;
        elseif( numel(x)>opt.maxsze );
            x(opt.maxsze+1:end) = []; end;
            
    raw = logical( x );
end

function dummy; end %#ok<DEFNU> %Generates an error, if the 'end' of a function is missing.   

% This Source Code Form is subject to the terms of the Mozilla Public
% License, v. 2.0. If a copy of the MPL was not distributed with this
% file, You can obtain one at https://mozilla.org/MPL/2.0/.
