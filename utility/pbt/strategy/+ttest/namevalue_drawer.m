function [ ex_state, raw ] = namevalue_drawer( state, opt, name, value );

    ex_state = ttest_state_c( 'raw', [] );

    len = ttest.randcauchy( [1 1], opt.minsze, opt.maxsze, state.num_draw );
    
    
    N = [];
    while( true );
        N = [N randperm( numel(name), min(len,numel(name)) )]; %#ok<AGROW>
            if( numel(N)>=len );
                break; end; end;
        
    if( ~opt.allowmultiple );
        N = unique( N ); 
        len = numel( N ); end;
    
    raw = cell( 1, 2*len );
    
    for i = 1:len;
        raw{2*i-1} = name{N(i)};
        if( isa(value{N(i)},'function_handle') );
            raw{2*i} = value{N(i)}();
        elseif( iscell(value{N(i)}) );
            raw{2*i} = value{N(i)}{randi(numel(value{N(i)}))};
        elseif( isnumeric(value{N(i)}) );
            raw{2*i} = value{N(i)}(randi(numel(value{N(i)})));
        else;
             error( 'TTEST:NAMEVALUE', ...
                 'Wrong type of ''value{ %i }''.\n  Allowed arguments are function_handles, cell-arrays or numeric values.\n  Given type is:', ...
                 i, class(value{N(i)}) ); end; end;
    
            
end

function dummy; end %#ok<DEFNU> %Generates an error, if the 'end' of a function is missing.   

% This Source Code Form is subject to the terms of the Mozilla Public
% License, v. 2.0. If a copy of the MPL was not distributed with this
% file, You can obtain one at https://mozilla.org/MPL/2.0/.

