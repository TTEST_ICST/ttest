function [ ex_state, raw ] = array_drawer( state, opt );
    
    ex_state = ttest_state_c( 'raw', [] );

    num_draw = state.num_draw;
    
	while( true );
        if( opt.minsze(end)==-1 && opt.maxsze(end)==-1 );
            opt.dim = ttest.randcauchy( 1, 0, inf, num_draw/20 );
        else;
            opt.dim = ttest.randcauchy( 1, opt.mindim, opt.maxdim, num_draw/20 ); end;

        opt.minsze(opt.minsze==-1) = [];
        opt.maxsze(opt.maxsze==-1) = [];
        opt.minsze(end+1:opt.dim) = opt.minsze(end);
        opt.maxsze(end+1:opt.dim) = opt.maxsze(end);
        opt.minsze = opt.minsze(1:opt.dim);
        opt.maxsze = opt.maxsze(1:opt.dim);
        idx = opt.minsze>opt.maxsze;
        opt.minsze(idx) = opt.maxsze(idx);
        assert( all(opt.minsze<=opt.maxsze), 'ttest:array', 'Minimum value larger than maximum value.' );

        sz = [ttest.randcauchy( [1 opt.dim], opt.minsze, opt.maxsze, num_draw/(3*opt.dim) ) 1 1]; 
        
        if( opt.dim==0 );
            sz = []; end;
        
        if( prod(sz)<2^(num_draw/4) + 1000 && prod(sz)<2^11 );
            break; end; 
        num_draw = num_draw - 1; end;
    

    
    
    % degenerate cases
    if( opt.dim==0 );
        raw = zeros( 0, 0 );
        return; end;
    
    % make random sequence
    type = mod( round( state.lvl*1000 ), 11 );
        
    raw{2} = 0;
    for i = 1:opt.imax;
        while( true );
            switch lower( type ); 
                case {0,1,'zeros'};
                    raw{i} = zeros( sz );
                case {2,5,'ones'};
                    raw{i} = ones( sz );
                case {3,6,'consecutive'};
                    raw{i} = reshape( 1:prod(sz), [sz 1 1] );
                case {4,7,'randn'};
                    raw{i} = randn( sz );
                case {8,'rand'};
                    raw{i} = rand( sz );
                case {9,'nrand'};
                    raw{i} = 2*rand( sz ) - 1;
                case {10,'irdand'};
                    mv = ttest.randcauchy + 1;
                    raw{i} = randi( mv, sz );
                case {11,'cyclic'};
                    idx = randi( numel(sz) );
                    if( randi(2)==1 );
                        val = 1:prod(sz(1:idx));
                    else;
                        val = randi( dist(rand,1,inf), 1, prod(sz(1:idx)) ); end;
                    raw{i} = repmat( val, [ones(1,randi(numel(sz))) prod(sz(idx+1:end))] );
                    raw{i} = reshape( raw{i}, [sz 1 1] );
                otherwise;
                    error( 'ttest:fatal', 'programming error.' ); end; 
            raw{i} = fixraw( raw{i}, opt );
            if( checkraw(raw{i},opt) );
                break; end; end; end;
        
    raw = raw{1} + 1i*raw{2};
        
end

function [ raw ] = fixraw( raw, opt );

    sz = size( raw );
    if( numel(sz)<opt.dim );
        sz( numel(sz):opt.dim ) = 1; end;
    while( numel(sz)>=2 );
        if( sz(end)==1 );
            sz(end) = []; 
        else;
            break; end; end;
    assert( all(opt.minsze(1:numel(sz))<=sz) && all(sz<=opt.maxsze(1:numel(sz))) );
    
    
    if( ~isequal(opt.allownan,true) );
        idx = isnan( raw );
        if( any(idx(:)) );
            raw( idx ) = opt.minval; end; end;
    
    if( ~isequal(opt.allowinf,true) );
        idx = isinf( raw );
        if( any(idx(:)) );
            raw( idx ) = opt.minval; end; end;
    
    if( isequal(opt.allowneg,false) );
        idx = raw<0;
        if( any(idx(:)) );
            raw(idx) = -raw(idx); end; end;
    
    if( ~isempty(opt.minval) && isempty(opt.maxval) );
        idx = raw<opt.minval;
        if( any(idx(:)) );
            raw(idx) = raw(idx) + opt.minval; end; end;
    if( ~isempty(opt.maxval) && isempty(opt.minval) );
        idx = raw>opt.maxval;
        if( any(idx(:)) );
            raw(idx) = opt.maxval - raw(idx); end; end;
    if( ~isempty(opt.minval) && ~isempty(opt.maxval) );
        idx = raw<opt.minval;
        if( any(idx(:)) );
            raw(idx) = raw(idx) - floor( (raw(idx)-opt.minval) ./ (opt.maxval-opt.minval) ).*(opt.maxval-opt.minval); end;
        idx = raw>opt.maxval;
        if( any(idx(:)) );
            raw(idx) = raw(idx) + floor( (opt.maxval-raw(idx)) ./ (opt.maxval-opt.minval) ).*(opt.maxval-opt.minval); end; end;
    
        

end

function [ ret ] = checkraw( raw, opt );  %#ok<INUSD>
    ret = true;
end

function dummy; end %#ok<DEFNU> %Generates an error, if the 'end' of a function is missing.   

% This Source Code Form is subject to the terms of the Mozilla Public
% License, v. 2.0. If a copy of the MPL was not distributed with this
% file, You can obtain one at https://mozilla.org/MPL/2.0/.
