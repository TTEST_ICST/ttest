function st = sqmatrix( varargin )

    if( isequal(varargin,{'register'}) );
        st = {ttest.sqmatrix,'sqm'}; 
        return; end;    
    
    opt = struct;
	[ opt, varargin ] = ttest_strategy_opt_parsem( opt, varargin{:} );
	[ opt, varargin ] = ttest_drawer_opt_parsem( opt, varargin{:} );
    [ opt.invertible, varargin ] = ttest.parsem( {'inv','invertible'}, varargin );
    [ opt.hermitian, varargin ] = ttest.parsem( {'symmetric','hermitian'}, varargin );
    [ opt.antihermitian, varargin ] = ttest.parsem( {'antisymmetric','antihermitian'}, varargin );
	
    ttest.parsem( varargin, 'test' );
    drawer = @( state ) ttest.sqmatrix_drawer( state, opt );
    
    st = ttest_strategy_c( drawer, [], [], opt );
    

end

function dummy; end %#ok<DEFNU> %Generates an error, if the 'end' of a function is missing.   

% This Source Code Form is subject to the terms of the Mozilla Public
% License, v. 2.0. If a copy of the MPL was not distributed with this
% file, You can obtain one at https://mozilla.org/MPL/2.0/.
