function st = float( varargin )

    if( isequal(varargin,{'register'}) );
        st = {ttest.float,'flt'}; 
        return; end;

    opt = struct;
	[ opt, varargin ] = ttest_strategy_opt_parsem( opt, varargin{:} );
	[ opt, varargin ] = ttest_drawer_opt_parsem( opt, varargin{:} );
	[ opt.allowsingle, varargin ] = ttest.parsem( {'allowsingle','allow_single','single',}, varargin, false );
	[ opt.allowdouble, varargin ] = ttest.parsem( {'allowdouble','allow_double','double'}, varargin, true );
    
    if( ~opt.allowdouble );
        opt.target = {'single'};
        opt.allowsingle = true;
    elseif( ~opt.allowsingle );
        opt.target = {'double'};
        opt.allowdouble = true;
    else;
        opt.target = {'single','double'}; end;
    
    lim = limit( opt.target{:} );
    len = numel( lim );
    perm = randperm( len );
    opt.start = lim( perm );
    
    if( isequal(opt.allowcp,true) ); 
        opt.imax = 2;        
    else;
        opt.imax = 1; end;
    
    
    
    assert( isempty(opt.minsze) || isequal(opt.minsze,1), 'TTEST:strategy', '''minsze'' is not a valid option for float strategy.' );
    assert( isempty(opt.maxsze) || isequal(opt.maxsze,1), 'TTEST:strategy', '''maxsize'' is not a valid option for float strategy.' );
    assert( isempty(opt.minnorm), 'TTEST:strategy', '''minnorm'' is not a valid option for float strategy.' );
    assert( isempty(opt.maxnorm), 'TTEST:strategy', '''maxnorm'' is not a valid option for float strategy.' );
    
    ttest.parsem( varargin, 'test' );
    drawer = @( state ) ttest.float_drawer( state, opt );
    
    st = ttest_strategy_c( drawer, [], [], opt );
    

end



function dummy; end %#ok<DEFNU> %Generates an error, if the 'end' of a function is missing.   

% This Source Code Form is subject to the terms of the Mozilla Public
% License, v. 2.0. If a copy of the MPL was not distributed with this
% file, You can obtain one at https://mozilla.org/MPL/2.0/.
