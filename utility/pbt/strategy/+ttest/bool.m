function st = bool( varargin );

    if( isequal(varargin,{'register'}) );
        st = {ttest.bool,'bool'};
        return; end;
    
    opt = struct;
	[ opt, varargin ] = ttest_strategy_opt_parsem( opt, varargin{:} );
	[ opt, varargin ] = ttest_drawer_opt_parsem( opt, varargin{:} );
    
    if( isempty(opt.minsze) );
        opt.minsze = 0; end;
    if( isempty(opt.maxsze) );
        opt.maxsze = inf; end;
    assert( opt.minsze<=opt.maxsze );
    
    ttest.parsem( varargin, 'test' );
    drawer = @( state ) ttest.bool_drawer( state, opt );
    
    st = ttest_strategy_c( drawer, [], [], opt );

end

function dummy; end %#ok<DEFNU> %Generates an error, if the 'end' of a function is missing.   

% This Source Code Form is subject to the terms of the Mozilla Public
% License, v. 2.0. If a copy of the MPL was not distributed with this
% file, You can obtain one at https://mozilla.org/MPL/2.0/.
