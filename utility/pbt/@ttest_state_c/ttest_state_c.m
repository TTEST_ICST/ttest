classdef ttest_state_c
    
    properties ( Access = public )
        type;      % type of saved state. Admissible values: 'raw'
        state;     % topology depends on `type`
		           %       'raw': always empty
    end
    
    methods
	
		%% constructors
        
        function [ st ] = ttest_state_c( type_, state_ );
            
            assert( isequal(type_,'raw'), 'ttest_state_c:type', 'Wrong type given.' );
			st.type = type_;
			st.state = [];
           
        end
        
    end
    
end

function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.   

% This Source Code Form is subject to the terms of the Mozilla Public
% License, v. 2.0. If a copy of the MPL was not distributed with this
% file, You can obtain one at https://mozilla.org/MPL/2.0/.
