function st = filter( varargin )

    opt = struct;
    [opt, varargin] = ttest_strategy_opt_parsem( opt, varargin{:} );
    [opt.num_try, varargin] = ttest.parsem( {'num_try','numtry'}, varargin, 20 );

    if( numel(varargin)==0 );
        st = @(f,st) ttest_strategy_c( @(state) filter_drawer(state,opt,f,st), [], [], opt );
        return; end;
    
    f = varargin{1};
    st = varargin{2};
    
    drawer = @( state ) ttest.filter_drawer( state, opt, f, st );
    
    st = ttest_strategy_c( drawer, [], [], opt );
    
end

function dummy; end %#ok<DEFNU> %Generates an error, if the 'end' of a function is missing.   

% This Source Code Form is subject to the terms of the Mozilla Public
% License, v. 2.0. If a copy of the MPL was not distributed with this
% file, You can obtain one at https://mozilla.org/MPL/2.0/.
