function st = tuple( varargin )

    opt = struct;
    [opt, varargin] = ttest_strategy_opt_parsem( opt, varargin{:} );
    
    if( numel(varargin)==0 );
        st = @( varargin ) tuple(varargin{:}); 
        return; end;
    
    drawer = @( state ) ttest.tuple_drawer( state, opt, varargin{:} );
    
    st = ttest_strategy_c( drawer, [], [], opt );
    
end

function dummy; end %#ok<DEFNU> %Generates an error, if the 'end' of a function is missing.   

% This Source Code Form is subject to the terms of the Mozilla Public
% License, v. 2.0. If a copy of the MPL was not distributed with this
% file, You can obtain one at https://mozilla.org/MPL/2.0/.
