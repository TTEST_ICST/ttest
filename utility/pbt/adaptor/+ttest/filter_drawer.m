function [ ex_state, raw ] = filter_drawer( state, opt, f, st );  %#ok<INUSL>
    ex_state = ttest_state_c( 'raw', [] );
    raw = st.example();  % we draw the first example from st so that the internal state is updated.
    st_ = deepcopy( st );  % subsequent draws are from a copy
    n = 0;
    while( n<opt.num_try && ~f(raw) );
        n = n + 1;
        raw = st_.example(); end;
    assert( n<opt.num_try, 'TTEST:filter:nummax', 'Filter condition too strong. Could not find valid example.' );

end

function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.   

% This Source Code Form is subject to the terms of the Mozilla Public
% License, v. 2.0. If a copy of the MPL was not distributed with this
% file, You can obtain one at https://mozilla.org/MPL/2.0/.
