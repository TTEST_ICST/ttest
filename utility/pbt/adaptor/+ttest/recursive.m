function [ st ] = recursive( varargin )
% This returns a generator whos values may be drawn from base, or from adpt(base)
% [ st ] = recursive( base, adpt, [options] )
%
% E.g.: recursive( bool, pair )
%


    opt = struct;
    [opt, varargin] = ttest_strategy_opt_parsem( opt, varargin{:} );
    
    if( numel(varargin)==0 );
        st = @(base,adpt) ttest_strategy_c( @(state) recursive_drawer(state,opt,base,adpt), [], [], opt );
        return; end;
    
    base = varargin{1};
    adpt = varargin{2};
    
    assert( isa(base,'ttest_strategy_c'), 'map:arg', 'First positional argument must be a strategy.' );
    assert( isa(adpt,'function_handle'), 'map:arg', 'Second positional argument must be a strategy.' );    
    
    drawer = @( state ) ttest.recursive_drawer( state, opt, base, adpt );
    
    st = ttest_strategy_c( drawer, [], [], opt );

end

function dummy; end %#ok<DEFNU> %Generates an error, if the 'end' of a function is missing.   

% This Source Code Form is subject to the terms of the Mozilla Public
% License, v. 2.0. If a copy of the MPL was not distributed with this
% file, You can obtain one at https://mozilla.org/MPL/2.0/.
