function [ ex_state, raw ] = map_drawer( state, opt, m, varargin );  %#ok<INUSL>
    ex_state = ttest_state_c( 'raw', [] );
    ex = cell( 1, numel(varargin) );
    for i = 1:numel( ex );
        ex{i} = varargin{i}.example; end;
    raw = m( ex{:} );
end

function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.   

% This Source Code Form is subject to the terms of the Mozilla Public
% License, v. 2.0. If a copy of the MPL was not distributed with this
% file, You can obtain one at https://mozilla.org/MPL/2.0/.
