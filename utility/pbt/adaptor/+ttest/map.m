function st = map( varargin )

    opt = struct;
    [opt, varargin] = ttest_strategy_opt_parsem( opt, varargin{:} );
	
	if( numel(varargin)==0 );
		st = @(m, varargin) ttest_strategy_c( @(state) map_drawer(state,opt,m,varargin{:}), [], [], opt ); 
        return; end;	
	
    m = varargin{1};
	st = varargin(2:end);
	
	drawer = @( state ) ttest.map_drawer( state, opt, m, st{:} );
      
    assert( isa(m,'function_handle'), 'map:arg', 'First positional argument must be a function_handle.' );
    assert( all( cellfun( 'isclass', st, 'ttest_strategy_c' ) ), 'map:arg', 'Second to last positional arguments must be strategies (ttest_strategy_c).' );
    
    st = ttest_strategy_c( drawer, [], [], opt );
end

function dummy; end %#ok<DEFNU> %Generates an error, if the 'end' of a function is missing.   

% This Source Code Form is subject to the terms of the Mozilla Public
% License, v. 2.0. If a copy of the MPL was not distributed with this
% file, You can obtain one at https://mozilla.org/MPL/2.0/.
