function [ ex_state, raw ] = pair_drawer( state, opt, base ); %#ok<INUSL>
    %copy the state of the global random number stream
    
    ex_state = ttest_state_c( 'raw', [] );
    
    raw1 = deepcopy( base ).example();
    raw2 = base.example();
    raw = {raw1, raw2};
end

function dummy; end %#ok<DEFNU> %Generates an error, if the 'end' of a function is missing.   

% This Source Code Form is subject to the terms of the Mozilla Public
% License, v. 2.0. If a copy of the MPL was not distributed with this
% file, You can obtain one at https://mozilla.org/MPL/2.0/.
