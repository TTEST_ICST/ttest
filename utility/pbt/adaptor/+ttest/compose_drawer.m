function [ ex_state, raw ] = compose_drawer( state, opt, f );  %#ok<INUSL>
        
    ex_state = ttest_state_c( 'raw', [] );
    
    raw = f( state );
    
end

function dummy; end  %#ok<DEFNU>  %Generates an error, if the 'end' of a function is missing.   

% This Source Code Form is subject to the terms of the Mozilla Public
% License, v. 2.0. If a copy of the MPL was not distributed with this
% file, You can obtain one at https://mozilla.org/MPL/2.0/.
