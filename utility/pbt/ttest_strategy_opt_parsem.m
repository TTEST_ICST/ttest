function [ opt, varargin ] = ttest_strategy_opt_parsem( opt, varargin )
    
    [opt.verbose,varargin] = ttest.parsem( {'verbose','v'}, varargin, 1, 'expect', @isnumeric );
    [opt.name,varargin]    = ttest.parsem( {'name'}, varargin, [], 'expecte', @(x) isa(x,'string') || isa(x,'char') );
    % [opt.filter,varargin]  = ttest.parsem( {'filter'}, varargin, [], 'expecte', @(x) isa(x,'function_handle') );
    % [opt.map,varargin]     = ttest.parsem( {'map'}, varargin, [], 'expecte', @(x) isa(x,'function_handle') );

    % derandomize
    
    
end

function dummy; end %#ok<DEFNU> %Generates an error, if the 'end' of a function is missing. 

% This Source Code Form is subject to the terms of the Mozilla Public
% License, v. 2.0. If a copy of the MPL was not distributed with this
% file, You can obtain one at https://mozilla.org/MPL/2.0/.
