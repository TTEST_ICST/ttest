function [ ret ] = GIVEN( varargin );

%% %%%%%%%%%%%
% get test_id
%%%%%%%%%%%%%%
if( numel(varargin)>=1 && isa(varargin{1},'ttest_id_c') );
    id = varargin{1};
    varargin(1) = [];
elseif( evalin( 'caller', 'exist(''ttest_id'',''var'');' ) ); %id not given
    id = evalin( 'caller', 'ttest_id;' );
else;
    id = ttest_id_c( 0 ); end;
% enableinjection = TTEST( id, 'inject', false ); %disable injection and get current state
% cleanup_injection = onCleanup( @() TTEST( id, 'inject', enableinjection ) );

%% %%%%%%%%%%%
% parse input
%%%%%%%%%%%%%%%
%%% parse options
[ opt.name,varargin ]        = ttest.parsem( {'name'}, varargin, '' );  % prefix of name where to store failed examples
if( isempty(opt.name) );
    opt.name = generate_name( varargin{:} ); end;
[ opt.maxtime, varargin ]    = ttest.parsem( {'maxtime','timeout'}, varargin, 30 );  % approximate maximum execution time
[ opt.maxexample, varargin ] = ttest.parsem( {'maxnumexample','maxexamples','maxexample','maxtest','max_examples'}, varargin, 200 );
[ opt.minexample, varargin ] = ttest.parsem( {'minnumexample','minexamples','minexample','mintest','min_satisfying_examples'}, varargin, 5 );
[ opt.rng, varargin ]        = ttest.parsem( {'rng','derandomize'}, varargin, [] );
if( ~isempty(opt.rng) );
    rng( opt.rng ); end;
opt.verbose = TTEST( id, 'verbose' );

for i = 1:numel( varargin );
    if( isa(varargin{i},'string') );
        varargin{i} = char( varargin{i} ); end; end;

ret = true;
str = '';

%% rerun old tests
[ret_, str_] = rerun_old_tests( opt );
ret = ret && ret_;
str = [str str_];

%% run new tests
if( isa(varargin{end},'char') );  
    %% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % everything given as string
    % %%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % cannot be put into subfunction due to use of evalin/assignin
    func = varargin{end};
    stname = varargin(1:end-1);
    
    st = cell( size(stname) );
    for i = 1:numel( st );
        st_lookup = stname{i};
        if( ischar(st_lookup) );
            st_lookup = stname{i};
            while( st_lookup(end)=='_' || st_lookup(end)>='0' && st_lookup(end)<='9' );
                st_lookup(end) = []; end;
            st{i} = TTEST( 'global', 'pbt', st_lookup );
        elseif( isa(st_lookup,'ttest_strategy_c') );
            error( 'TTEST:GIVEN', 'This mixed form where the strategies are given as variables and the command is given as string is not possible.' );
        else;
            error( 'TTEST:GIVEN', 'Wrong arguments given,' ); end; end;
    
    assert( numel(unique(stname))==numel(stname), 'TTEST:GIVEN', 'Strategies with the same name must not be used multiple times. Append to the strategies numbers or underscores if necessary.' );
    
    wh = evalin( 'caller', 'whos;' );
    names = cell( 1, numel(wh) );
    [names{:}] = wh.name;
    for i = 1:numel( stname );
        if( any(strcmp(names,stname{i})) );
            fprintf( 2, 'Variable present in workspace which has the same name as an argument.\n  Variable name: %s\n. Type ''D'' to delete the variable or anything else to abort the test: ', stname{i} ); 
            ask = input( '' ,'s');
            if( strcmp(ask,'D') );
                % do nothing. Variable is deleted afterwards
            else;
                error( 'TTEST:GIVEN', 'Test aborted.' ); end;
            
        end; end;
    clearstr = ['clear ' sprintf( '%s ', stname{:} )];
    
    timings = 0;  % we cannot start with empty array, since then timings(end) throws an error in the first round
    ret = true;
    try;
        exlist = {};
        for numtest = 1:opt.maxexample;
            if( sum(timings) + median(timings)>=opt.maxtime );
                break; end;
            starttic = tic;
            str = GIVEN_sprintf_example_beforeloop( str, opt );
            ex = cell( 1, numel(st) );
            
            try;            
                for j = 1:MAXNUM_TRY_DRAW;
                    for i = 1:numel( st );
                        ex{i} = st{i}.example();
                        assignin( 'caller', stname{i}, ex{i} ); end;
                    exhash = thash( ex );
                    if( ~any(strcmp(exlist, exhash)) );
                        exlist = [exlist exhash];  %#ok<AGROW>
                        break; end; end;
                if( j == MAXNUM_TRY_DRAW );
                    break; end;
                str = GIVEN_sprintf_example_afterloop( str, opt, ex );

                rngstate = rng();

                ret = evalin( 'caller', func );
                if( ~ret );
                    str = GIVEN_sprintf_failedtest( str, opt, ex );
                    h = evalin( 'caller', ['@() ' func ';' ] );
                    str_ = saveexample_wrapper_stringstyle( opt.name, h, ex, rngstate );
                    str = [str str_];  %#ok<AGROW>
                    break; end;
            catch me;
                ret = 0;
                str = [str 'Error thrown. Test failed.' newline me2str(me)];  %#ok<AGROW>
                break; end;

            timings(numtest) = toc( starttic ); end;  %#ok<AGROW>
           
    catch me;  %#ok<NASGU>
        end;  % do nothing. this is just for cleanup of assignin-ed variables.
    
    if( ~isempty(stname) );
        evalin( 'caller', clearstr ); end;
    
    
elseif( isa(varargin{end},'function_handle') );
    if( numel(varargin)==1 );
        %% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        % implicit form, only function handle given
        % %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        [ret_,numtest,str_,opt] = GIVEN_implicit( varargin{1}, opt );
        ret = ret && ret_;
        str = [str str_];
        
    elseif( all(cellfun('isclass',varargin(1:end-1),'ttest_strategy_c') | cellfun('isclass',varargin(1:end-1),'char')) );
        %% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        % implicit form, function handle and strategies given
        % %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        [ret_,numtest,str_,opt] = GIVEN_explicit( varargin{end}, opt, varargin{1:end-1} );
        ret = ret && ret_;
        str = [str str_];
        
    else;
        error( 'TTEST:GIVEN', 'Wrong arguments given.' ); end;
else;
    error( 'TTEST:GIVEN', 'Wrong arguments given.' ); end;



if( opt.verbose>=1 );
    fprintf( 2, '%s', str ); end;
if( ~ret );
    EXPECT_FAIL( id, 'Parametrized test failed.' ); end;
if( ret && numtest<opt.minexample );
    ret = false;
    EXPECT_FAIL( id, 'Too less examples generated. Test failed.' ); end;

end


function [ ret, numtest, str, opt ] = GIVEN_implicit( func, opt );
    % get variable names
    [ varname, ~, ~ ] = variablename( func );
    varname = varname{1};
    st = cell( 1, numel(varname) );
    for i = 1:numel( varname );
        lookupname = strtrim( varname{i} );
        while( true );
            if( lookupname(end)=='_' );
                lookupname(end) = []; 
                continue; end;
            if( lookupname(end)>='0' && lookupname(end)<='9' );
                lookupname(end) = []; 
                continue; end;
            break; end;
        st{i} = deepcopy( TTEST( 'global', 'pbt', lookupname ) ); end;
    
    [ret,numtest,str,opt] = GIVEN_explicit( func, opt, st{:} );
    
end

function [ ret, numtest, str, opt ] = GIVEN_explicit( func, opt, varargin );

    str = '';
    ret = true;

    %% parse input
    st = varargin;
    
    for i = 1:numel( st );
        if( isa(st{i},'char') );
            st{i} = deepcopy( TTEST( 'global', 'pbt', st{i} ) ); end; end;
    
    % get variable names
    [ ~, ~, varidx ] = variablename( func );
    varidx = varidx{1};
    try;
        if( isempty(varidx) && nargin(func)==1 );  % case of function handle (i.e. not anonymous function, e.g. @isinf instead of @(x) isinf(x) )
            varidx = 1; end;
    catch me;  % octave cannot use nargin for builtins
        if( any(strfind(me.message,'built-in')) );
            varidx = 1;
        else;
            error( 'TTEST:fatal', 'Some internals of Octave or Matlab seems to have changed and the code needs to be adapted.' ); end; end;
    
    ex = cell( 1, numel(st) );
    timings = 0;  % we cannot start with empty array, since then timings(end) throws an error in the first round
    exlist = {};
    
    assert( numel(varidx)==numel(st), 'TTEST:GIVEN', 'Number of strategies given is different from number of input arguments of function handle.' );
    
    for numtest = 1:opt.maxexample;
        if( sum(timings) + timings(end)>=opt.maxtime );
            break; end;
        starttic = tic;
        str = GIVEN_sprintf_example_beforeloop( str, opt );
        try;
            for j = 1:MAXNUM_TRY_DRAW;
                for i = 1:numel( st );
                    ex{i} = st{varidx(i)}.example(); end;
                exhash = thash( ex );
                if( ~any(strcmp(exlist, exhash)) );
                    exlist = [exlist exhash];  %#ok<AGROW>
                    break; end; end;
            if( j == MAXNUM_TRY_DRAW );
                break; end;
            str = GIVEN_sprintf_example_afterloop( str, opt, ex );

            rngstate = rng();

            ret = feval( func, ex{:} );
            if( ~ret );
                str = GIVEN_sprintf_failedtest( str, opt, ex );
                str_ = saveexample_wrapper_functionstyle( opt.name, func, ex, rngstate );
                str = [str str_];  %#ok<AGROW>
                break; end;
        catch me;
            ret = 0;
            str = [str 'Error thrown. Test failed.' newline me2str(me)];  %#ok<AGROW>
            break; end;
        
        timings(numtest) = toc( starttic ); end; %#ok<AGROW>
 
    

    
end

%% helper functions

function [ ret, str ] = rerun_old_tests( opt );
    str = '';
    ret = true;
    basepath = fileparts( which( 'TTEST' ) );
    path = fullfile( basepath, 'tmp', 'fail', opt.name );
    lst = dir( [path '*.mat'] );
    for i = 1:numel( lst );
        ct = load( fullfile( lst(i).folder, lst(i).name ) );
        ct = ct.ct;
        [str_, ret] = evalc( 'ct.run()' );
        str = [str str_];  %#ok<AGROW>
        if( ~ret );
            str = [str 'Old example failed' newline];  %#ok<AGROW>
            str = GIVEN_sprintf_failedtest( str, opt, ct.ex );
            ct.numsucceed = 0;
        else;
            ct.numsucceed = ct.numsucceed + 1; end;
        if( ct.numsucceed==3 );
            delete( fullfile( lst(i).folder, lst(i).name ) );
        else;
            save( fullfile( lst(i).folder, lst(i).name ), 'ct' ); end; end;
        
end

function name = generate_name( varargin );
    
    if( ismatlab );
        name1 = thash( varargin );
    else;
        name1 = hash( 'MD5', TTEST_DISP( inf, varargin{:} ) ); end;
    name1 = name1(1:8);
    
    ds = dbstack;
    name2 = '00000000';
    if( numel(ds)>=3 );
        ds = dbstack( '-completenames' );
        fid = fopen( ds(3).file );
        if( fid>=0 );
            closefile = onCleanup( @() fclose(fid) ); 
            for i = 1:ds(2).line - 1
                fgetl( fid ); end; 
            name2_ = fgetl( fid );
            if( ischar(name2_) );
                name2 = strtrim( name2_ );
                name2(name2==' ') = [];
                name2 = thash( name2 );
                name2 = name2(1:8); end; end; end;
    name = ['ttest_' name1 '_' name2];
end

%% print functions

function [ str ] = GIVEN_sprintf_example_beforeloop( str, opt );
    if( opt.verbose>=2 );
        fprintf( 'Generated example with arguments:\n' ); end;
end

function [ str ] = GIVEN_sprintf_example_afterloop( str, opt, ex );
    if( opt.verbose>=2 );
        for i = 1:numel( ex );
            fprintf( '%s\n', TTEST_DISP( [], ex{i} )); end; end;
end

function [ str ] = GIVEN_sprintf_failedtest( str, opt, expandedarg_ );
    if( opt.verbose>=1 );
        str = [str 'Test failed with example:' newline];
        str = [str TTEST_DISP( [], expandedarg_{:} ) newline]; end; 
end

%% save example

function [ str ] = saveexample_wrapper_functionstyle( name, func, ex, rngstate );
    ct.test = @() feval( func, ex{:} );
    ct.rngstate = rngstate;
    ct.ex = ex;
    str = saveexample( name, ct );
end

function [ str ] = saveexample_wrapper_stringstyle( name, handle, ex, rngstate );
    ct.test = handle;
    ct.rngstate = rngstate;
    ct.ex = ex;
    str = saveexample( name, ct );
end

function [ str ] = saveexample( prefix, ct );
    ct.run = @() testhandle( ct.test, ct.rngstate );
    ct.numsucceed = 0;

    str = '';

    savefail = 'file';  % TTEST( id, 'savefail' );
   
    name_time = datestr( datetime('now'), 'yymmdd_HHMMSSFFF' );

    varname = [prefix '_' name_time];

    switch savefail;
        case {'no','none',false,'false'};
            %do nothing
        case {'file','mat'};
            filename = fullfile( fileparts(which('TTEST')), 'tmp', 'fail', varname );  % directory where to save failed example
            save( [filename '.mat'], 'ct' );
            if( ismatlab() );
                str = ['<a href="matlab:' varname ' = load( ''' filename '.mat'' ); ' varname '.ct.run()">!! Rerun failed test. !!</a>']; end;
         case {'base'};
             assignin( 'base', varname, handle );
             if( ismatlab() );
                 str = ['<a href="matlab:evalin(''base'', ''' varname '.ct.run()'' )">Rerun failed test.</a>']; end;
        otherwise;
            error( 'TTEST_SAVEEXAMPLE:TTEST_SAVEFAIL', 'Wrong value for ''TTEST_SAVEFAIL''.' ); end;
      
end

function ret = testhandle( h, rngstate );
    rng( rngstate );
    ret = h();
end

%% Constants
function [ ret ] = MAXNUM_TRY_DRAW; ret = 30; end

function dummy; end %#ok<DEFNU> %Generates an error, if the 'end' of a function is missing. 

% This Source Code Form is subject to the terms of the Mozilla Public
% License, v. 2.0. If a copy of the MPL was not distributed with this
% file, You can obtain one at https://mozilla.org/MPL/2.0/.
