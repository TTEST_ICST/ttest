function st = range( varargin );

    opt = struct;
	[ opt, varargin ] = ttest_strategy_opt_parsem( opt, varargin{:} );
    
    assert( numel(varargin)>=2, 'TTEST:RANGE', 'At least two arguments must be given defining the start end endpoint.' );
    
    a = varargin{1};
    b = varargin{2};
    if( numel(varargin)==2 );
        s = cast( 1, 'like', a );  
    else; 
        s = varargin{3}; end;
    
    assert( isequal(class(a),class(b),class(s)), 'TTEST:series:types', 'Types of ''a'', ''b'' and ''s'' must be equal.' );
    set = {};
    v = a;
    if( isAlways(all(s<0)) );
        while( isAlways(all(v>b)) );
            set{end+1} = v; %#ok<AGROW>
            v = v + s; end;
    else;
        while( isAlways(all(v<b)) );
            set{end+1} = v; %#ok<AGROW>
            v = v + s; end; end;
    
    drawer = @( state ) ttest.value_drawer( state, opt, set );
    
    st = ttest_strategy_c( drawer, [], [], opt );
    
end

function dummy; end %#ok<DEFNU> %Generates an error, if the 'end' of a function is missing.   

% This Source Code Form is subject to the terms of the Mozilla Public
% License, v. 2.0. If a copy of the MPL was not distributed with this
% file, You can obtain one at https://mozilla.org/MPL/2.0/.
