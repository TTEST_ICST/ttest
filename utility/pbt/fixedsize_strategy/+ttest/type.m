function st = type( varargin );

%#ok<*AGROW>

    opt = struct;
	[ opt, varargin ] = ttest_strategy_opt_parsem( opt, varargin{:} );
    
    assert( numel(varargin)>=1, 'TTEST:arg', 'TTEST:type neads at least one additional argument defining the class to which the argument shall be casted.' );
    
    a = varargin{1};
    varargin(1) = [];
    

    
    if( ~iscell(a) );
        a = {a}; end;
    
    i = 1;
    while( i<=numel(varargin) );
        j = 0;
        val = '';
        switch varargin{i};
            case 'int';
                val = {'int8','int16','int32','int64','uint8','uint16','uint32','uint64'};
                j = 7;
            case 'sint';
                val = {'int8','int16','int32','int64'};
                j = 3;
            case 'uint';
                val = {'uint8','uint16','uint32','uint64'};                
                j = 4;
            case 'float';
                val = {'single','double'};
                j = 1;
            case 'numeric';
                val = {'single','double','int8','int16','int32','int64','uint8','uint16','uint32','uint64'};                
                j = 9;
            case {'charstr','strchar','charstring','stringchar','text'};
                val = {'char','string'};
                j = 1;
            case {'symvpa','vpasym'};
                val = {'double','sym','vpa'};
                j = 2;
            otherwise;
                end; %do nothing       
       if( ~isempty(val) );
           varargin = [varargin{1:i-1} val varargin{i+1:end}]; end; 
       i = i+1+j; end;
    
    set = {};
    for i = 1:numel( varargin );
        t = varargin{i};
        switch t;
            case 'vpa';
                set = [set cellfun( @vpa, a, 'UniformOutput', false )];
            case 'sym';
                set = [set cellfun( @sym, a, 'UniformOutput', false )];
            otherwise;
                try;
                    set = [set cellfun( @(y) cast(y,t), a, 'UniformOutput', false) ];
                catch;
                    try;
                        set = [set cellfun( @(y) feval(t,y), a, 'UniformOutput', false) ];
                    catch;
                        error( 'TTEST:type:cast', 'Cannot cast to %s.', t ); end; end; end; end;
        
    drawer = @( state ) ttest.value_drawer( state, opt, set );
    
    st = ttest_strategy_c( drawer, [], [], opt );        
        
end

% This Source Code Form is subject to the terms of the Mozilla Public
% License, v. 2.0. If a copy of the MPL was not distributed with this
% file, You can obtain one at https://mozilla.org/MPL/2.0/.
