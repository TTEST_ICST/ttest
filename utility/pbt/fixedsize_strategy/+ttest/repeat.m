function st = repeat( varargin );

    opt = struct;
	[ opt, varargin ] = ttest_strategy_opt_parsem( opt, varargin{:} );
    [ opt.evaluate, varargin ] = ttest.parsem( {'eval','evaluate'}, varargin );
    
    p = varargin{1};
    n = varargin{2};
    assert( numel(varargin)==2 );
    
    if( opt.evaluate )
        set = {};
        if( isa(p,'ttest_strategy_c') );
            for i = 1:n;
                set = [set p.example()]; end;  %#ok<AGROW>
        else;
            for i = 1:n
                set = [set p()]; end; end; %#ok<AGROW>
    else;
        set = repmat( {p}, [1 n] ); end;
    
    drawer = @( state ) ttest.value_drawer( state, opt, set );
    
    st = ttest_strategy_c( drawer, [], [], opt );
    
end

function dummy; end %#ok<DEFNU> %Generates an error, if the 'end' of a function is missing.   

% This Source Code Form is subject to the terms of the Mozilla Public
% License, v. 2.0. If a copy of the MPL was not distributed with this
% file, You can obtain one at https://mozilla.org/MPL/2.0/.
