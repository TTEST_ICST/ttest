classdef ttest_strategy_c < handle
    
    properties ( Access = public )
        drawer;    % handle to the drawer function
%         shrinker;  % handle to the shrinker function
%         hasher;    % handle to the hasher function
		opt        % options
        state      % internal state of the strategy
    end
    
    properties ( SetAccess = private, GetAccess = public );
        shorthand  % registered shorthand name, may be empty
        id         % unique id accross all strategies, defined at construction time
    end
	
        
    
    methods
	
        %% constructors
        
        function [ st ] = ttest_strategy_c( varargin );
        % constructs a strategy
        % [ st ] = ttest_strategy_c();  % constructs an empty strategy
        %
            
            st.id = ttest_id_c();
        
            if( numel(varargin)==0 );
                return; end;
            
            [st.drawer,varargin]    = ttest.parsem( {'drawer','d'},   varargin, [],                      'asserte', @(x) isa(x,'function_handle') );
%             [st.shrinker,varargin] = ttest.parsem( {'shrinker','s'}, varargin, @ttest.default_shrinker, 'asserte', @(x) isa(x,'function_handle') );
%             [st.hasher,varargin]   = ttest.parsem( {'hasher','h'},   varargin, @ttest.default_hasher,   'asserte', @(x) isa(x,'function_handle') );
            [st.opt,varargin]       = ttest.parsem( {'opt'},          varargin, [],                      'asserte', @isstruct );
            
            st.state = struct;
            st.state.num_draw = 0;
            
            if( numel(varargin)>=1 );
			    assert( isempty(st.drawer), 'ttest_strategy_c:options', 'If drawer is given with option <''drawer''> it must not be given as positional argument.' );
			    assert( isa(varargin{1},'function_handle'), 'ttest_strategy_c:options', 'A drawer (function handle) must be given either with option <''drawer'',h> or as first positional argument.' );
                st.drawer = varargin{1}; 
                varargin(1) = []; end;
            if( numel(varargin)>=1 );  % currently, shrinker and hasher are not used
                varargin(1) = []; end;
            if( numel(varargin)>=1 );
                varargin(1) = []; end;            
%             if( numel(varargin)>=1 );
% 				assert( isempty(st.drawer), 'ttest_strategy_c:options', 'If shrinker is given with option <''shrinker''> it must not be given as positional argument.' );
% 				assert( isa(varargin{1},'function_handle'), 'ttest_strategy_c:options', 'A shrinker (function handle) may be given either with option <''shrinker'',h> or as second positional argument.' );
%                 st.shrinker = varargin{1}; 
%                 varargin(1) = []; end;
%             if( numel(varargin)>=1 );
% 				assert( isempty(st.drawer), 'ttest_strategy_c:options', 'If hasher is given with option <''hasher''> it must not be given as positional argument.' );
% 				assert( isa(varargin{1},'function_handle'), 'ttest_strategy_c:options', 'A hasher (function handle) may be given either with option <''hasher'',h> or as third positional argument.' );
%                 st.hasher = varargin{1}; 
%                 varargin(1) = []; end;
            if( numel(varargin)>=1 );
				assert( isempty(st.opt), 'ttest_strategy_c:options', 'If options are given with option <''opt''> they must not be given as positional argument.' );
				assert( isa(varargin{1},'struct'), 'ttest_strategy_c:options', 'Options may be given either with option <''opt'',struct> or as fourth positional argument.' );
                st.opt = varargin{1}; 
                varargin(1) = []; end;            
            
            ttest.parsem( varargin, 'test' );
            assert( ~isempty(st.drawer) && isa(st.drawer,'function_handle'), 'ttest_strategy_c:drawer', 'No drawer given, or no function handle given.' );
%           assert( ~isempty(st.shrinker) && isa(st.shrinker,'function_handle'), 'ttest_strategy_c:shrinker', 'No shrinker given, or no function handle given.' );
%           assert( ~isempty(st.hasher) && isa(st.hasher,'function_handle'), 'ttest_strategy_c:hasher', 'No hasher given, or no function handle given.' );
            assert( isstruct(st.opt), 'ttest_strategy_c:opt', 'No options given.' );
            
            if( ~isempty(st.opt.name) );
                % register strategy
                st.shorthand = st.opt.name;
                TTEST( 'global', 'pbt', st.opt.name, st ); end;
            
        end
        
        function [ out ] = deepcopy( in, stateflag )
            % returns a copy of a strategy.
            % does not copy state if stateflag is given and false
            out = ttest_strategy_c();
            out.drawer = in.drawer;
            out.opt = in.opt;
            out.state = in.state;
            if( nargin==2 && stateflag==false );
                out.reset(); end;
        end
		
        %% main functions
        
        function [ ex ] = draw( st, state );  % draws a ttest_instance_c
		    if( nargin==1 );
			    state = struct; end;
			state = condset( state, 'lvl', rand );
			state = condset( state, 'num_draw', st.state.num_draw );
			state = condset( state, 'opt', st.opt );
% 			state = condset( state, 'shrink', false );
				
            [ state, raw ] = st.drawer( state );
            assert( isa(state,'ttest_state_c'), 'ttest:strategy', 'Strategy did no return correct state.' );
            ex = ttest_instance_c( state, raw, st );
            st.state.num_draw = st.state.num_draw + 1;
        end
        
        function raw = example( st, state );  % returns an example of an instance
            if( nargin==2 );
                raw = st.draw( state ).raw;
            else;
                raw = st.draw().raw; end;
        end
        
        function name( st, nme ); %#ok<INUSD> %registers the strategy
            error( 'ttest:fatal', 'not implemented yet' );
        end
        
        function reset( st )
            st.state.num_draw = 0;
        end
        
        function disp( st );
            fprintf( '     drawer: %s\n', TTEST_DISP( [], st.drawer ) );
            fprintf( '  shorthand: %s\n', st.shorthand );
            fprintf( '         id: %s\n', st.id.id );
            fprintf( '        opt: \n%s\n', TTEST_DISP( [], st.opt ) );
            fprintf( '      state: \n%s\n', TTEST_DISP( [], st.state ) );
        end

    end
    
end

function [ s ] = condset( s, field, value );
    if( ~isfield(s,field) );
	    s.(field) = value; end;
end

function dummy; end %#ok<DEFNU> %Generates an error, if the 'end' of a function is missing.   

% This Source Code Form is subject to the terms of the Mozilla Public
% License, v. 2.0. If a copy of the MPL was not distributed with this
% file, You can obtain one at https://mozilla.org/MPL/2.0/.
