function [ opt, varargin ] = ttest_drawer_opt_parsem( opt, varargin )
    
    [opt.minnorm,varargin]    =   ttest.parsem( {'minnorm','minmag','min_magnitude'}, varargin,           [], 'asserte', @isnumeric );
    [opt.maxnorm,varargin]    =   ttest.parsem( {'maxnorm','maxmag','max_magnitude'}, varargin,          [], 'asserte', @isscalar );
    [normval,varargin] = ttest.parsem( {'norm'}, varargin,  [], 'asserte', @isnumeric );
    if( ~isempty(normval) );
        assert( isempty(opt.minnorm) && isempty(opt.maxnorm), 'GIVEN:opt', 'If ''norm'' is given, options  ''minnorm'' and ''maxnorm'' must not be given.' );
        opt.minnorm = normval;
        opt.maxnorm = normval; end;
    assert( all(opt.minnorm<opt.maxnorm), 'GIVEN:opt', '''minnorm'' must be strictly smaller than ''maxnorm''.' );
    
    [opt.minval,varargin]        =   ttest.parsem( {'min','minval','min_value'}, varargin,              [], 'asserte', @isnumeric );
    [opt.maxval,varargin]        =   ttest.parsem( {'max','maxval','max_value'}, varargin,              [], 'asserte', @isnumeric );
    [valval,varargin] = ttest.parsem( {'val'}, varargin,  [], 'asserte', @isnumeric );
    if( ~isempty(valval) );
        assert( isempty(opt.minval) && isempty(opt.maxval), 'dopt_parsem:val', 'If ''val'' is given, options  ''minval'' and ''maxval'' must not be given.' );
        opt.minval = valval;
        opt.maxval = valval; end;
    assert( all(opt.minval<opt.maxval), 'GIVEN:opt', '''minval'' must be strictly smaller than ''maxval''.' );
    
    [opt.minsze,varargin]     =   ttest.parsem( {'minsize','minsze','min_size'}, varargin,  [], 'asserte', @isnumeric );
    [opt.maxsze,varargin]     =   ttest.parsem( {'maxsize','maxsze','max_size'}, varargin,  [], 'asserte', @isnumeric );
    [szeval,varargin] = ttest.parsem( {'sze'}, varargin,  [], 'asserte', @isnumeric );
    if( ~isempty(szeval) );
        assert( isempty(opt.minsze) && isempty(opt.maxsze), 'dopt_parsem:sze', 'If ''sze'' is given, options  ''minsze'' and ''maxsze'' must not be given.' );
        opt.minsze = szeval;
        opt.maxsze = szeval; end;
    len = min( numel(opt.minsze), numel(opt.maxsze) );
    assert( all(opt.minsze(1:len)<=opt.maxsze(1:len)), 'GIVEN:opt', '''minsze'' must be smaller than ''maxsze''.' );
    
    [opt.allownan,varargin]   =   ttest.parsem( {'allownan','nan','allow_nan'}, varargin,   [], 'expecte', @(x) isnumeric(x) || islogical(x) );        
    [opt.allowinf,varargin]   =   ttest.parsem( {'allowinf','inf','allow_infinity'}, varargin,   [], 'expecte', @(x) isnumeric(x) || islogical(x) );        
    [opt.allowcp,varargin]    =   ttest.parsem( {'allowcomplex','allowcp','complex','cp'}, varargin, [], 'expecte', @(x) isnumeric(x) || islogical(x) );        
    [opt.allowneg,varargin]   =   ttest.parsem( {'allownegativ','allowneg','negative','neg'}, varargin, [], 'expecte', @(x) isnumeric(x) || islogical(x) );
    [opt.allowsparse,varargin] =  ttest.parsem( {'allowsparse','allowsp','sparse','sp'}, varargin, [], 'expecte', @(x) isnumeric(x) || islogical(x) );
    
    [opt.type,varargin]       =   ttest.parsem( {'type'}, varargin, [], 'expecte', @(x) ischar(x) || isstring(x) );

end

function dummy; end %#ok<DEFNU> %Generates an error, if the 'end' of a function is missing. 

% This Source Code Form is subject to the terms of the Mozilla Public
% License, v. 2.0. If a copy of the MPL was not distributed with this
% file, You can obtain one at https://mozilla.org/MPL/2.0/.
