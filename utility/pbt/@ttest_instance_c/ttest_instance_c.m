classdef ttest_instance_c
    
    properties (SetAccess = private, GetAccess = public)
        state; % stuff which can be used to recreate raw_
        raw;   % the example as used by the user (raw data)
        st;    % the strategy which constructed it
    end
    
    methods
	
	    %% constructors        
        function obj = ttest_instance_c( state_, raw_, st_ );
            obj.state = state_;
            obj.raw = raw_;
            obj.st = st_;
        end
		
		%% main functions        
%         function obj_out = shrink( obj_in );
%             persistent i;
%             if( isempty(i) );
%                 i = 0; end;
%             i = i + 1;
%             obj_out = obj_in.st.shrinker( obj_in, i );
%         end
        
%         function ret = hash( obj );
%             ret = obj.st.hasher( obj );
%         end
        
%         function obj = redraw( obj );
%             obj.st.drawer( obj.state );
%         end
		 
		%% Helper functions
        function disp( obj );
            fprintf( 'ttest_instance_c:\n' );
            disp( obj.raw );
        end	
        
        function raw = example( obj );
            raw = obj.raw;
        end;
        
        
    end
    
end

function dummy; end %#ok<DEFNU> %Generates an error, if the 'end' of a function is missing. 

% This Source Code Form is subject to the terms of the Mozilla Public
% License, v. 2.0. If a copy of the MPL was not distributed with this
% file, You can obtain one at https://mozilla.org/MPL/2.0/.
