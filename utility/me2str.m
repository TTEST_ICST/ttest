function [ str ] = me2str( me, verbose );
%#ok<*AGROW>

    if( nargin<=1 );
        verbose = 1; end;

    assert( isa(me,'struct') || isa(me,'MException'), 'TTEST:input', 'Input argument must be an exception.' );

    str = ['  Error id:   ' me.identifier newline '  Error msg:  ' me.message newline];
    if( isfield(me,'Correction') && ~isempty(me.Correction) );
        str = [str '  Correction: ' me.correction newline]; end;
    if( isfield(me,'Cause') && numel(me.Cause)>=0 );
        str = [str TTEST_DISP( [], me.Cause )]; end;
    
    
    % remove unwanted entries
    last_time_dots = false;
    replacement = cell( 1, numel(me.stack) );
    file = cell( 1, numel(me.stack) );
    for i = numel( me.stack ):-1:1;
        if( isoctave );
            [~,file{i},~] = fileparts( me.stack(i).file );
            file{i} = [file{i} '.m']; 
        else;
            file{i} = me.stack(i).file; end;
        
        if( verbose<=1 && ( ...
                 contains( lower(file{i}), 'expect_' ) || ...
                 contains( lower(file{i}), 'assert_' ) || ...
                 contains( lower(file{i}), 'todo_' ) || ...
                 contains( lower(file{i}), 'ttest_' ) || ...
                 contains( file{i}, 'runttests.m' ) || ...
                 contains( file{i}, 'GIVEN.m' ) || ...
                 contains( file{i}, which('run') ) || ...
                 contains( file{i}, 'CACHE.m' ) ...
                 ) ...
            );
            if( ~last_time_dots );
                last_time_dots = true;
                replacement{i} = [newline '  ...' newline];
            else;
                last_time_dots = false;
                replacement{i} = ''; end; end; end;
    
    for i = 1:numel( me.stack );
        if( ~isempty(replacement{i}) );
            str = [str replacement{i}];
        else;
            fullname = which( file{i} );
            funcname = me.stack(i).name;
            intxt = sprintf( '%s:%i', file{i}, me.stack(i).line );
            if( ismatlab() && numel(file{i})>=2 && ~strcmp(file{i}(end-1:end),'.p') );
                str = [str sprintf( '\n  In: %s\n      <a href="matlab: opentoline(''%s'',%i,0)"  style="font-weight:bold">%s</a>\n', funcname, fullname, me.stack(i).line, intxt )];
            else; 
                str = [str sprintf( '\n  In: %s\n      %s\n', funcname, intxt )]; end; end; end;
      
end

function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.  
