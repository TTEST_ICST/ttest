function last = returnlast( varargin );  %#ok<INUSL>
	% returns the last argument
	last = varargin{end};
end