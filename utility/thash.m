function h = thash( in, type );

	if( nargin==0 );
        error( 'thash:argin', 'At least one input argument, the thing to be hashed must be given.' );
    elseif( nargin==1 );
        if( ismatlab );
            type = 'jan';
        else;
            type = 'oct'; end; end;
    
    switch type
        case 'jan';
            h = ttest.datahash( in );
        case 'bad';
            h = badhash( in );
        case 'oct';
            assert( isoctave(), 'thash:type', 'Type ''oct'' only possible on Octave.' );
            h = octhash( in );
        otherwise;
            error( 'thash:type', 'wrong type given: possible types: ''md5'', ''bad'', ''oct''.' ); end;

end

function h = octhash( in );
%    persistent tmpdir;
%    if( isempty(tmpdir) );
%        bp = which( 'TTEST.m' );
%        bp = bp(1:end-numel( 'TTEST.m' ));
%        tmpdir = fullfile( bp, 'tmp', 'hash' ); 
%         end;
    
%    filename = fullfile( tmpdir, ['hashdummy_' num2str(randi(100000)) num2str(randi(100000)) num2str(randi(100000)) '.octave'] );
    try;
        txt = save( '-binary', '-z', '-', 'in' );
    catch
        warning( 'thash:octave', 'Could not serialize data. I use the display data instead to generate a hash.' );
        txt = TTEST_DISP( inf, in ); end;
    h = hash( 'MD5', txt );
%    delete( filename );
end

function h = badhash( in )
    outputlen = 32;
    in = getByteStreamFromArray( in );
    len = numel( in );
    if( mod(len,2) );
        in(end+1) = 0; end;
    while( len>=outputlen );
        len = len/2;
        if( mod(len,2) );
            len = len + 1; end;
        in(1:len/2) = bitxor( in(1:len/2), in(len/2+1:len) );
    end
    h = dec2hex( in(1:outputlen).' );
    h = h(:).';
end

function dummy; end %#ok<DEFNU> %Generates an error, if the 'end' of a function is missing.
