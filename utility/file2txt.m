function ret = file2txt( name, var, newlineflag )
% file2txt reads a text file and produces a text which matlab parses as the text file.
% 
% [ ret ] = file2txt( in, [out], [newlineflag] )
%
% Input:
%   in              string, filename or functionname whose content shall be converted to a matlab evalable string. 
%                       If the file cannot be found, `which(name)` is used to locate it
%   var             string, optional, default = [], if given, then 'var = ' is prepended to the output
%   newlineflag     bool, optional, default = false, if set, then newline characters are pre and appeneded to the first and last line of the output
%
% Output:
%   ret             character array
%
% 
% Example:
%   file2txt( 'pinv' )
%   file2txt( 'file2txt.m', 'varname', true )
% 


 %#ok<*AGROW>

    
    if( nargin<=2 || isempty(newlineflag) );
        newlineflag = false; end;
    
    if( newlineflag );
        ret = newline;
    else;
        ret = ''; end;
        
    if( nargin<=1 || isempty(var) );
        ret = [ret '[' newline]; 
    else;
        ret = [ret var ' = [' newline]; end;
    fid = fopen( name, 'r' ); % File to be read
    if( fid==-1 );
        name = which( name );
        idx = strfind( name, '%' );
        if( ~isempty(idx) );
            name(idx(1):end) = []; end;
        fid = fopen( which(name,'all'), 'r' ); end;
    assert( fid>=3, 'file2txt:cannotopenfile', 'Cannot open file %s', name );
    closefid = onCleanup( @() fclose( fid ) );
    
    while( ~feof(fid) );
        line = fgetl( fid );
        line = regexprep( line, '''', '''''' ); % Replace the ' with ''
        line = regexprep( line, '\t', '\\t');
        line = regexprep( line, '\r', '\\r');
        if( isempty(line) );
            ret = [ret 'newline ...' newline];
        else;
            ret = [ret '''' line ''' newline ...' newline]; end; end;
    ret = [ret  '];'];
    
    if( newlineflag );
        ret = [ret newline]; end;
    
end

function dummy; end %#ok<DEFNU> %Generates an error, if the 'end' of a function is missing.   