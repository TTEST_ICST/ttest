function [ varargout ] = noutput( n );
% Returns n output arguments, each with the value 1
% [ varargout ] = noutput( n );

	varargout = cell( 1, n );
	[varargout{:}] = deal( 1 );
end

function dummy; end %#ok<DEFNU> %Generates an error, if the 'end' of a function is missing.   
