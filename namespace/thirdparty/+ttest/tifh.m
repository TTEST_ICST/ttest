function out = tifh(truth,yesh,noh)
% Ternary if operator
% out = truth & yes : no ;
% Input:
%	yes, no 		function handles
%
% E.g.: tif(true, @()1, @()2)
    
if( truth );
    out = yesh(); 
else; 
    out = noh(); end;
    
end

function dummy; end %#ok<DEFNU> %Generates an error, if the 'end' of a function is missing.   