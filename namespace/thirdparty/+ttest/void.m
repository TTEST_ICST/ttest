
function out = void( f, out, capture )

    if( nargin <= 1 );
        out = 1; end;
    if( nargin <= 2 );
        capture = false; end;
    if( capture )
        try;
            f();
    catch me;
        if( isempty(me.identifier) );
            id = 'TTEST:void'; 
        else;
            id = me.identifier; end;
        msg = ['ttest.void captured an error and rethrows it as warning.' newline 'Id:' newline id newline 'Message:' newline me.message];
        warning( id, '\n%s', msg ); end;
    else;
        f(); end;
    
end
