function [ A ] = randncp( varargin );

A = randn( varargin{:} ) + 1i*randn( varargin{:} );

end