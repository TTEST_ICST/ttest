% Copyright (c) 2015, Matt J
% All rights reserved.
% 
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are
% met:
% 
%     * Redistributions of source code must retain the above copyright
%       notice, this list of conditions and the following disclaimer.
%     * Redistributions in binary form must reproduce the above copyright
%       notice, this list of conditions and the following disclaimer in
%       the documentation and/or other materials provided with the distribution
% 
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
% AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
% IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
% ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
% LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
% CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
% SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
% INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
% CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
% ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
% POSSIBILITY OF SUCH DAMAGE.


function [ fh_clean ] = afclean( fh )
% Tries to purge an anonymous function of all non-essential workspace data
% [ fh_clean ] = afclean( fh )
%
% Input:
%  fh           an anonymous funtion
%
% Output:
%  fh_clean     The same as fh, but with its workspace reduced to the essential variables.
%
% DISCLAIMERS: The cleaning process will break anonymous functions that call nested
% functions or subfunctions. Unfortunately also, the implementation of AFCLEAN relies
% on a certain amount of undocumented MATLAB, in particular the FUNCTIONS command whose
% behavior in future releases of MATLAB, The MathWorks doesn't guarantee. It also 
% makes heavy use of EVAL. When the tool works, it seems to work well, but use at your
% own risk!

    s = functions( fh ); 
    s = s.workspace{1};
    eval( structvars(s).' );
    clear s;
    fstr = func2str( fh );  
    fh_clean = eval( fstr );
    clear fstr fh

end


function [ assigns ] = structvars( varargin )
% print a set of assignment commands that, if executed, would assign fields of a structure 
% to individual variables of the same name (or vice versa).
% [ assigns ] = structvars( [nCols], InputStructure, [RHS] )
%

%
% Input:
%   InputStructure      struct
%   RHS                 bool, default=true, When true (default), dot indexing expressions will be on the right hand side.
%   nCols               If given, assignment strings will be split across nCols columns.
% 
% Output:
% 
%	assigns             string, a text containing the commands
%
%
% Examples: Given structure myStruct, with fields a,b,c, & d
%
% (1) structvars(myStruct)   %assign fields to variables
% 
%         ans =
% 
%         a = myStruct.a;     
%         b = myStruct.b;     
%         c = myStruct.c;     
%         d = myStruct.d;     
% 
% (2) structvars(3,myStruct)   %split the last result across 3 columns
% 
%         ans =
% 
%         a = myStruct.a;     c = myStruct.c;     d = myStruct.d;     
%         b = myStruct.b;                                             
% 
% (3) structvars(3,myStruct,0)  %assign variables to fields 
% 
%         ans =
% 
%         myStruct.a = a;    myStruct.c = c;    myStruct.d = d;    
%         myStruct.b = b; 
%
% The routine is useful when you want to pass many arguments to a function
% by packing them in a structure. The commands produced by structvars(...)
% can be conveniently copy/pasted into the file editor at the location in the file
% where the variables need to be unpacked.
%
% Copyright, Matt Jacobson, Xoran Technologies, Inc. 2009



    if( isnumeric(varargin{1}) ); 
        nCols = varargin{1};
        varargin(1) = [];
        idx = 2;
    else;
        nCols = 1;
        idx = 1; end;

    nn = length( varargin );
    S = varargin{1};
    if( nn<2 );
        RHS = true; 
    else;
        RHS = varargin{2}; end

    fields = fieldnames( S );

    sname = inputname( idx ); 
    if( isempty(sname) );
        sname = 'S'; end


    if( RHS );
        assigns = cellfun( @(f) [f ' = ' sname '.' f ';     '], fields, 'UniformOutput',0 );
    else; %LHS
        assigns = cellfun( @(f) [ sname '.' f ' = ' f ';    '], fields, 'UniformOutput',0 ); end;

    L0 = length( assigns );
    L = ceil( L0/nCols )*nCols;
    Template = false( nCols, L/nCols );
    Template(1:L0) = true;
    Template = Template.';

    Table = cell( size(Template) );
    Table(:) = {' '};
    Table(Template) = assigns;

    for ii = 1:nCols;
        TextCols{ii} = strvcat( Table(:,ii) ); end;

    assigns = [TextCols{:}];

end