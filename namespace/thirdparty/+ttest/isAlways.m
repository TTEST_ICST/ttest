%version: 2021-01-18
function ret = isAlways( varargin )
%this function exists to have a uniform interface for the function isAlways

try;
    ret = logical( varargin{1} );
catch;
    if( nargin==3 );
        switch varargin{3};
            case 'falseWithWarning'
                warning( 'sym:isAlways', 'Unable to prove ''%s''.', strtrim(evalc('disp(varargin{1})')) );
                ret = false;
            case 'trueWithWarning'
                warning( 'sym:isAlways', 'Unable to prove ''%s''.', strtrim(evalc('disp(varargin{1})')) );
                ret = true;                
            case 'false';
                ret = false;
            case 'true';
                ret = true;
            case 'err';
                error( 'sym:isAlways', 'Unable to prove ''%s''.', strtrim(evalc('disp(varargin{1})')) );
            otherwise;
                error( 'sym:isAlways', 'Wrong Argument.' ); end;
    else;   %'falseWithWarning';
        warning( 'sym:isAlways', 'Unable to prove ''%s''.', strtrim(evalc('disp(varargin{1})')) );
        ret = false; end; 
    
end