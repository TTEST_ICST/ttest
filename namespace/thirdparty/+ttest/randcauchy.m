function [ R ] = randcauchy( X, lb, ub, ga );
% Returns a positive integer between 0 and inf
% [ ret ] = randcauchy( [X, lb, ub, ga] );
% Input: 
%   X       vector of integers, size of the output array
%   lb/ub   positive numbers, lb<=ub, default:0 and inf, lower bound/upper bound of generated values
%   ga      positive number, default:1, controls the distribution. Larger ga yield larger returned values
%
% Output:
%   R       array of size X with values in [lb ub]
%
% E.g.: randcauchy
%       randcauchy( 0, 1, inf, 0 )
%
%
% See also: randx

    if( nargin<=0 || isempty(X) );
        X = 1; end;
    if( nargin<=1 || isempty(lb) );
        lb = 0; end;
    if( nargin<=2 || isempty(ub) );
        ub = inf; end;    
    if( nargin<=3 || isempty(ga) );
        ga = 0.5; end;        
    ga = 2^(-ga);
    
    B = rand( X );
    e = .5; %offset is necessary so that function works for m=0 too
    R = round((lb+e)./(1 + (lb+e)./ub.*B.^ga - B.^ga)-e/2);
    
end

function dummy; end %#ok<DEFNU> %Generates an error, if the 'end' of a function is missing.   

