function [value, args, retname] = parsem( varargin )
% [value, args, retname] =  parsem( name, args , [input3], [options], ['set' || 'condset'], ['help','helptext'] )
% [value, args, retname] =  parsem( args , 'test')
% Parses varargin and is easier to use than Matlabs parse.
%
% Input:
% ======
%   name                string or cell-array of strings. 
%                       The questionable argument. The function searches for string <name> in <args>. The return value depends on [input3] and on args.
%
%   args                string or cell array of strings
%         'parse_one'           If contained in args, then val = 1. Args is treated in the usual way. Thus the value in args can be different from the return value!!!
%         'parse_random'        If contained in args, then val = 0 or 1. Args is treated in the usual way. Thus the value in args can be different from the return value!!!
%         'help','helptext'     If contained in args, then an help is printed
%
%
%   [input3]            default value
%                           If input3 is not given: If the string can be found, and the entry in args behind the string is not a string, then this value is returned.
%                                                   Otherwise, If the string can be found the function returns 1, otherwise 0.
%                           If input3 is given:     If the string can be found, the function returns the value in <args> which is one position behind the last occurence of <name>.
%                                                   If that string cannot be found, the function returns the value which is defined input3.
%                       The last occurence of 'name' defines what happens
% Options:
% ========
%   'set'               All occurences of 'name' in args are removed and at the end 'name' (and if given input3) is appended.
%   'condset'           Only sets the value if 'name' is not found. The ordering of the element in args will be changed.
%   'help','helptext'   Adds some help-text to the help-output
%   'test'              Tests if args is empty. If not, a warning is printed.
%                       If 'test' is given, 'name' must be ommited and no other option must be given
%   'expect',val        expected values for the <value> one position behind the last occurence of <name>
%                       If <value> is not an expected value, a warning is given
%                       only allowed if input3 is given, 
%                       format of val:
%                           {'clop',[lb ub]}             lb <= value <  ub
%                           {'opop',[lb ub]}             lb <  value <  ub
%                           {'opcl',[lb ub]}             lb <  value <= ub
%                           {'clcl',[lb ub]}             lb <= value <= ub
%                           {e1, ..., en}                value must be equal to an element in {e1,...,en} (in particular e1 must be unequal to 'clop','opop','opcl' and 'clcl')
%                           f                            f(value) must return true, where f is a function handle
%                           'name'                       value must be equal to an element in <name>. If the option <name> is not given, thus a warning is printed. 
%                                                        Thus, 'expect','name' shall only be given for mandatory options
%   'expecte',val       The same as 'expect', but an empty matrix as argument (i.e. []) is always implicitely allowed
%                       
%
% Remark:
%   If an option shall be removed, call this function with the option to be removed and use the second return argument.
%
% Output:
%   value               The parsed value or the set value
%   args                input arguments without the parsed values (except 'set' or 'condset' is given).
%                       If the input argument is contained more than once, then all of those are removed in the return value.
%   retname             The parsed name. First entry  in 'name' if any of 'name' is not present in 'args'
%


% XX Add option for mandatory parameters - implement using 'expect' syntax ?

if( strcmp(varargin{2},'test') );
    if( ~isempty(varargin{1}) );
%         if( strcmp(varargin{1},{'help'}) );
%             error( 'parsem:help', 'Early termination due to option ''help''.'); end;
        [ST,~] = dbstack;
        warning( 'parsem:unkown', 'Error in %s()/parsem(): Unkown argument(s):', ST(end).name );
        try; 
            if( isempty(varargin{1}) );
                fprintf( 'Empty argument.\n' ); 
            else;
                disp(varargin{1}); end;
        catch; 
            fprintf( 'Failed to print arguments.\n' ); end;
        fprintf( '\n' ); end; %do not call vprintf, since vprintf calls parsem
    return; end;

name = varargin{1};
args = varargin{2};
if( ~iscell(args) ); 
    args = {args}; end;
if( ~iscell(name) ); 
    name = {name}; end;

idxa = strcmp(varargin,'assert'); %look, if 'assert' is set
idxae = strcmp(varargin,'asserte'); %look, if 'assert' is set
idxe = strcmp(varargin,'expect'); %look, if 'expect' is set
idxee = strcmp(varargin,'expecte'); %look, if 'expect' is set
if( any(idxae|idxee) );
    emptystring = '[] or '; %used in the help text
    assertexpectempty = 1;
else;
    emptystring = ''; %used in the help text
    assertexpectempty = 0; end;
idx = idxa|idxae|idxe|idxee;
if( any(idx) );
    if( any(idxe) );
        h_exception = @warning; %function handle to warning or error, depending on we have 'expect' or 'assert'
    else;
        h_exception = @error; end;
    idx = find( idx );
    assertexpect = varargin{idx(end)+1};
    varargin([idx idx+1]) = [];
else 
    assertexpect = 0; end;

idx = find( strcmp(varargin,'help') );
if( any(idx) );
    helptext = varargin{idx(end)+1};
    varargin([idx idx+1]) = [];
else;
    helptext = ''; end;

idx = find( strcmp(varargin,'set') ); %look, if 'set' is set
if( any(idx) ); 
    varargin(idx) = []; 
    set = 1; 
else; 
    set = 0; end;      

idx = find( strcmp(varargin,'condset') ); %look, if 'condset' is set
if( any(idx) ); 
    varargin(idx) = []; 
    condset = 1; 
else; 
    condset = 0; end;      

%Set default value, determine if strings are allowed for val
if( size(varargin,2)>=3 );
    input3 = varargin{3};
    
    default = input3;
    defaultgiven = 1;
else;
    default = 0;
    defaultgiven = 0; end;

%read value for name, remove everything which is read 

idx = [];
if( ~isempty(name) );
    retname = name{1}; 
else
    retname = []; end;
for i = 1:numel( name );
    value = find( strcmp(args,name{i}) );
    if( ~isempty(value) );
        retname = name{i}; end;
    idx = [idx value]; end; %#ok<AGROW>
szeargs = size(args,2); %number of elements in args
if( ~isempty(idx) );
    for i = 1:size( idx, 2 );
        k = idx(i)+1;
        if( szeargs>=k && (~ischar(args{k}) || defaultgiven) );
            idx(end+1) = k;  %#ok<AGROW> %save the index of val, so that it can get deleted afterwards
            value = args{k};
        elseif( defaultgiven )
            warning( 'parsem:missing', 'In ''args'' is probably a ''value'' for some ''name'' missing.' );
            value = 1;
        else
            value = 1; end; end;
    args(idx) = [];
else
    value = default; end;

%make everything for set and condset
if( set || (condset && isempty(idx)) ); 
    args{end+1} = name{1};
    value = default;
    args{end+1} = value;
elseif( condset );
    args{end+1} = name{1};
    args{end+1} = value; end;

parse_oneflag = 0;
parse_randomflag = 0;
helpflag = 0;
%if( any(find(strcmp(args,'parse_one'))) );
if( numel(args)>=1 );
    if( strcmp(args{1},'parse_one') );
        parse_oneflag = 1;
        value = 1; 
    elseif( strcmp(args{1},'parse_random') );
        parse_randomflag = 1;
        value = randi( 2 )-1;
    elseif( strcmp(args{1},'help') );
        helpflag = 1;
        fprintf( '       Name:   ' );
        disp( name );
        if( defaultgiven || nargout==3 )
            fprintf( '       Default: ' );
            if( nargout==3 );
               disp( name{1} );
               fprintf( '\n' );
            elseif( isempty(default) );
                if( iscell(default) );
                    fprintf( '   {}\n' );
                else;
                    fprintf( '   []\n' ); end; 
            else;
                if( isstring(default) || ischar(default) );
                    default = ['   ''' default '''']; end;
                disp( default ); end; end;

        if( ~isequal(assertexpect,0) && ~any(strcmp(assertexpect,'name')) );
            fprintf( '       Expect: ' );
            if( iscell(assertexpect) && ischar(assertexpect{1}) );
                fprintf( '    ' ); end;
            if( iscell(assertexpect) && numel(assertexpect)==2 )
                switch assertexpect{1}
                    case 'opop'; fprintf( '( %f, %f )\n', assertexpect{2}(1), assertexpect{2}(2) );
                    case 'opcl'; fprintf( '( %f, %f ]\n', assertexpect{2}(1), assertexpect{2}(2) );
                    case 'clop'; fprintf( '[ %f, %f )\n', assertexpect{2}(1), assertexpect{2}(2) );
                    case 'clcl'; fprintf( '[ %f, %f ]\n', assertexpect{2}(1), assertexpect{2}(2) );
                    otherwise;
                        disp( assertexpect{1}); 
                        fprintf( '\b / ');
                        disp( assertexpect{2} ); end;
            else
                disp(assertexpect); end; end;
        if( ~isempty(helptext) );
            fprintf( '       Help:       ' );
            disp( helptext ); end;

        fprintf( '\n' ); end; end;


if( ~isequal(assertexpect,0) && ~parse_oneflag && ~parse_randomflag && ~helpflag );
    
    if( assertexpectempty && isequal(value,[]) );
            %everything ok
        elseif( iscell(assertexpect) && strcmp(assertexpect{1},'clop') )
            if( ~isscalar(value) || value<assertexpect{2}(1) || value>=assertexpect{2}(2) );
                value = dispc( value );
                h_exception( 'parsem:expect','Unexpected value for %s.\n  Allowed values are: %s%f <= val < %f\n Given value is: %s\n', retname, emptystring, assertexpect{2}(1), assertexpect{2}(2), value ); end;
        elseif( iscell(assertexpect) && strcmp(assertexpect{1},'opop') )
            if( ~isscalar(value) || value<=assertexpect{2}(1) || value>= assertexpect{2}(2) );
                value = dispc( value );
                h_exception( 'parsem:expect','Unexpected value for %s.\n  Allowed values are: %s%f < val < %f\n Given value is: %s\n', retname, emptystring, assertexpect{2}(1), assertexpect{2}(2), value ); end;
        elseif( iscell(assertexpect) && strcmp(assertexpect{1},'opcl') )
            if( ~isscalar(value) || value<=assertexpect{2}(1) || value> assertexpect{2}(2) );
                value = dispc( value );
                h_exception( 'parsem:expect','Unexpected value for %s.\n  Allowed values are: %s%f < val <= %f\n Given value is: %s\n', retname, emptystring, assertexpect{2}(1), assertexpect{2}(2), value ); end;         
        elseif( iscell(assertexpect) && strcmp(assertexpect{1},'clcl') )
            if( ~isscalar(value) || value<assertexpect{2}(1) || value>assertexpect{2}(2) );
                value = dispc( value );
                h_exception( 'parsem:expect','Unexpected value for %s.\n  Allowed values are: %s%f <= val <= %f\n Given value is: %s\n', retname, emptystring, assertexpect{2}(1), assertexpect{2}(2), value ); end;
        elseif( iscell(assertexpect) && ~any(cellfun(@(x)isequal(x,value),assertexpect)) );
            s = dispc( assertexpect );
            value = dispc( value );
            h_exception( 'parsem:expect','Unexpected value for %s.\n  Allowed values are %s%s\n  Given value is: %s\n', retname, emptystring, s, value );
        elseif( isa(assertexpect,'function_handle') && ~assertexpect(value) ); % value is checked by function handle wheter it is allowed
            s = dispc( assertexpect );
            value = dispc( value );
            h_exception( 'parsem:expect','Unexpected value for %s.\n  Allowed values are %sdetermined by: %s\n  Given value is: %s \n', retname, emptystring, s, value ); 
        elseif( any(strcmp(assertexpect,'name')) && isequal(value,0) );
            s = dispc( name );
            h_exception( 'parsem:expect','Missing argument. One argument must have one of the values: %s%s\n', emptystring, s ); end;
        
    end;
   
end
        

function dummy; end %#ok<DEFNU> %Generates an error, if the 'end' of a function is missing.   

