% Copyright (c) 2015, Matt J
% All rights reserved.
% 
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are
% met:
% 
%     * Redistributions of source code must retain the above copyright
%       notice, this list of conditions and the following disclaimer.
%     * Redistributions in binary form must reproduce the above copyright
%       notice, this list of conditions and the following disclaimer in
%       the documentation and/or other materials provided with the distribution
% 
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
% AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
% IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
% ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
% LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
% CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
% SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
% INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
% CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
% ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
% POSSIBILITY OF SUCH DAMAGE.

function varargout= afslim(varargin)
% This is a tool for constructing an anonymous function which carries only external parameters specified by the user.
% [ anonFunc ] = afslim( fun, a, b, c, ... )
% [ anonFunc ] = afslim( func2str(fun), a, b, c, ... )
%
% fun() is an anonymous function and a,b,c,... are extra parameters that it needs to refer to. 
% It can also be specified in string form.
% 
% 
% E.g.: The following code (important - must be run as an mfile function, not from the command line!!) 
% generates two files containing an anonymous function with the same functional behavior. 
% However, tst1.mat consumes 259 MB whereas tst2.mat consumes only 1 KB.
% 
%    function test
% 
%         b=2;
% 
%          fun1=@(x)x+b; 
%          fun2=afslim(fun1,b);   %alternatively fun2=afslim('@(x)x+b', b)
% 
%         b=rand(6000); 
% 
%       save tst1 fun1 
%       save tst2 fun2 
% 
%    end  


    for iuo99753fqhewsgju__7921zqar = 2:length( varargin );          
        eval( [inputname(iuo99753fqhewsgju__7921zqar) ' = varargin{' num2str(iuo99753fqhewsgju__7921zqar) '};']  ); end

    clear iuo99753fqhewsgju__7921zqar
    varargin(2:end)=[]; 
    if( ~ischar(varargin{1}) );
        varargin{1} = func2str( varargin{1} ); end

    varargout{1}=eval( varargin{1} );
    clear varargin;
 
end

function dummy; end %#ok<DEFNU> %Generates an error, if the 'end' of a function is missing.   
