function [ A ] = randxcp( varargin );

A = ttest.randx( varargin{:} ) + 1i*ttest.randx( varargin{:} );

end