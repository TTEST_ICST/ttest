function [ R ] = randx( varargin );
% Returns a rational number between -inf and inf
% [ ret ] = randx( [X] );
% Input: 
%   X       vector of integers, size of the output array
%
% Output:
%   R       array of size X with rational values in (-inf inf)
%
% E.g.: randx
%
%
% See also: randp
    if( nargin==1 );
        X = varargin{1}; 
    else;
        X = [varargin{:}]; end;
    R = (ttest.randcauchy(X)-ttest.randcauchy(X))./(ttest.randcauchy(X)-ttest.randcauchy(X));
    idx = ~isfinite( R );
    R(idx) = ttest.randcauchy( [1,nnz(idx)] ) - ttest.randcauchy( [1,nnz(idx)] );
    
    
end