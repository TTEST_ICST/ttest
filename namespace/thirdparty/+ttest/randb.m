function [ R ] = randb( X, lvl );
% Returns random booleans
% [ ret ] = randb( X, [lvl] );
% Input: 
%   X       vector of integers, size of the output array
%   lvl     number between 0 and 1, chance for true
%
% Output:
%   R       logical array of size X
%
% E.g.: randb
%       randb( 0, 1, inf, 0 )
%


    
    if( nargin<=0 || isempty(X) );
        X = 1; end;
    if( nargin<=1 || isempty(lvl) );
        lvl = 0.5; end;
    
    R = rand( X )<lvl;
    
end