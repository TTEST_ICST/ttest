% test TTEST R2017b

% Tests which need at least R2017b to work

% preconditions   
TTEST init
TTEST v 0

%% test EQ
assert( EXPECT_EQ( 'a', 'a' ) );
assert( EXPECT_EQ( "a", "a" ) );
if( ismatlab );
    assert( ~EXPECT_EQ( 'a', "a" ) );
    assert( ~EXPECT_EQ( "a", 'a' ) );
end;

%% test NE
assert( ~EXPECT_NE('a','a') );
assert( ~EXPECT_NE("a","a") );
if( ismatlab );
    assert( EXPECT_NE('a',"a" ) );
    assert( EXPECT_NE("a",'a' ) );
end;

%% test STREQ
if( ismatlab );
    assert( EXPECT_STREQ( '23', "23" ) );
    assert( EXPECT_STREQ( 'ab', "ab" ) );
    assert( ~EXPECT_STREQ( 'ab', "aB" ) );
    assert( ~EXPECT_STREQ( 'ab', "ac" ) );
end;
assert( EXPECT_STREQ( "23", "23" ) );
assert( EXPECT_STREQ( "ab", "ab" ) );
assert( ~EXPECT_STREQ( "ab", "aB" ) );
assert( ~EXPECT_STREQ( "ab", "ac" ) );


%% test STRNEQ
if( ismatlab );
    assert( ~EXPECT_STRNE( '23', "23" ) );
    assert( ~EXPECT_STRNE( 'ab', "ab" ) );
    assert( EXPECT_STRNE( 'ab', "aB" ) );
    assert( EXPECT_STRNE( 'ab', "ac" ) );
end;    
assert( ~EXPECT_STRNE( "23", "23" ) );
assert( ~EXPECT_STRNE( "ab", "ab" ) );
assert( EXPECT_STRNE( "ab", "aB" ) );
assert( EXPECT_STRNE( "ab", "ac" ) );

%% test STRCASEEQ
if( ismatlab );
    assert( EXPECT_STRCASEEQ( '23', "23" ) );
    assert( EXPECT_STRCASEEQ( 'ab', "ab" ) );
    assert( EXPECT_STRCASEEQ( 'ab', "aB" ) );
    assert( ~EXPECT_STRCASEEQ( 'ab', "ac" ) );
end;    
assert( EXPECT_STRCASEEQ( "23", "23" ) );
assert( EXPECT_STRCASEEQ( "AB", "ab" ) );
assert( EXPECT_STRCASEEQ( "ab", "aB" ) );
assert( ~EXPECT_STRCASEEQ( "ab", "ac" ) );

%% test STRCASENEQ
if( ismatlab );
    assert( ~EXPECT_STRCASENE( '23', "23" ) );
    assert( ~EXPECT_STRCASENE( 'ab', "ab" ) );
    assert( ~EXPECT_STRCASENE( 'ab', "aB" ) );
    assert( EXPECT_STRCASENE( 'ab', "ac" ) );
end;
assert( ~EXPECT_STRCASENE( "23", "23" ) );
assert( ~EXPECT_STRCASENE( "AB", "ab" ) );
assert( ~EXPECT_STRCASENE( "ab", "aB" ) );
assert( EXPECT_STRCASENE( "ab", "ac" ) );

%% test NO_THROW
assert( EXPECT_NO_THROW( "2;" ) );
assert( ~EXPECT_NO_THROW( "adsasfasf a asdkasd;" ) );
assert( ~EXPECT_NO_THROW( "adsasfasf a asdkasd;", "asd:asd", "MATLAB:UndefinedFunctiom", "Octave:undefined-functiom" ) );
assert( EXPECT_NO_THROW( "adsasfasf a asdkasd;", "asd:asd", "MATLAB:UndefinedFunctio", "Octave:undefined-functio" ) );

assert( ~EXPECT_THROW( "0;") );
assert( EXPECT_THROW( "inv(0);", 'MATLAB:singularMatrix', 'Octave:singular-matrix' ) );
assert( ~EXPECT_THROW( "inv(0);", "asd:asd" ) );
assert( EXPECT_THROW( "inv(0);", "asd:asd", "MATLAB:singularMatrix", "Octave:singular-matrix" ) );

assert( ~EXPECT_THROW( "0;" ) );
assert( ~EXPECT_THROW( "adsasfasdasda ajkdwaklsd;" ) );
assert( ~EXPECT_THROW( "adsasfasdasda ajkdwaklsd;", "asd:asd" ) );
assert( EXPECT_THROW( "adsasfasdasda ajkdwaklsd;", "asd:asd", "MATLAB:UndefinedFunction", "Octave:undefined-function" ) );


%% postprocessing
% Set errorflag to true
TTEST errflag false
