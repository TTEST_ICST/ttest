% test TTEST

TTEST init

% preconditions 

%% FAIL
EXPECT_STRCONTAINS( @() ttest.expect_fail, 'Expect test failed.' ) ;

%% SUCCEED
ttest.expect_succeed;

%% TRUE
ttest.expect_true( 1 );
ttest.expect_true( 1, 1, 1 );
EXPECT_THROW( @() ttest.assert_true([0 1]), 'ttest:assert:true' );

%% FALSE
ttest.expect_false( 0 );

%% TRUTHY
ttest.expect_truthy( [1 2 3] );

%% FALSY
ttest.expect_falsy( [0 0 0] );

%% PRED
ttest.expect_pred( @isempty, [] );

%% NTHROW
ttest.expect_throw( @() sum, 'MATLAB:minrhs', 'Octave:invalid-fun-call', 'o','Texinfo ' );
