
TTEST init
fprintf( '============================\nTHIS TEST IS DISABLED\n============================\n' );
assert( false, 'This test is disabled' );

%% inject

% XX I have no idea what is tested here
INJECT( 'all', 1 );
TTEST( 'inject', true );
EXPECT_ERROR( @() all(2), 'TTEST:FAULT:all' );
INJECT( 'all' ); % remove injection
EXPECT_NO_THROW( @() all(2) );

INJECT( 'i', 0, 1, {{'fun = ''pi'''}} );
TTEST( 'inject',true );  % remove injection
EXPECT_NEAR( i, 3.141592, 1e-6 );
INJECT();
EXPECT_EQ( i, 1i );