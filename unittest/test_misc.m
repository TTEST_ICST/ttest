% preconditions

TTEST init

%% DISABLED
errflagold = TTEST('errflag',false);
TTEST v 0
assert( EXPECT_EQ( DISABLED, 2, 3 ) );
assert( EXPECT_PRED( @asgfj, DISABLED, 'ti' ) );
assert( EXPECT_THAT( DISABLED ) );
TTEST('errflag',errflagold);
TTEST v 1

%% SAVE DELETE
directory = pwd;
filename = fullfile( pwd, 'temp_file' );
TTEST_SAVE( '%MSG1', filename ); %do not make newline characters here, since they make problems
TTEST_SAVE( '%MSG2', filename, '%MSG1' );
text = fileread( filename );
EXPECT_STREQ( text, '%MSG2' );
TTEST_DELETE( filename, '%MSG2' );
fid = fopen( filename );
EXPECT_EQ( fid, -1 );
if( fid~=-1 );
    fclose( fid ); end;

%% DISP
TTEST( 'maxdisp',500 );
str = TTEST_DISP( TTEST('maxdisp'), 1:5000 );
EXPECT_LE( numel(str), 600 );
EXPECT_GE( numel(str), 400 );

str = TTEST_DISP( TTEST('maxdisp'), [1 4 5], 'a,b,c,d' );
EXPECT_THAT( str, ttest.AllOf(ttest.HasSubstr('1'),ttest.HasSubstr('4'),ttest.HasSubstr('5'),ttest.HasSubstr('a,b,c,d')) );
EXPECT_NTHROW( @() TTEST_DISP( [], ttest_id_c(0) ) );

%% FPRINTF
%%% untested

%% PARSE_PARAM
%%% XX missing

%% SAVE

%% SECTION NAME
%%% SUBNAME ABCDEF
x = TTEST( 'v',1 );
errflagold = TTEST('errflag',false);
str = evalc( 'EXPECT_FAIL;' );
TTEST( 'errflag', errflagold );
TTEST( 'v',x );
EXPECT_THAT( str, ttest.HasSubstr('SECTION NAME') );
EXPECT_THAT( str, ttest.HasSubstr('SUBNAME ABCDEF') );

%%% SUBNAME 123
x = TTEST( 'v',1 );
errflagold = TTEST('errflag',false);
str = evalc( 'EXPECT_FAIL;' );
TTEST('errflag',errflagold);
TTEST( 'v',x );
EXPECT_THAT( str, ttest.HasSubstr('SECTION NAME') );
EXPECT_THAT( str, ttest.HasSubstr('SUBNAME 123') );

%% TTEST_CHECK_ERROR_WARNING
TTEST v 0

w = warning( 'off', 'TTEST:EXCEPTION:NOTFULLYQUALIFIED' );
cleanwarning = onCleanup( @() warning(w) );

%%% no exceptions thrown

texc0 = {};
oexc0a = {'o', 'id:id0'};
oexc0b = {'o', 'id:id0','testTTEST0.m','testTTEST0','Message 0',[0]};
mexc0_X = {'m', 'id:id0'};
assert( TTEST_CHECK_ERROR_WARNING( ttest_id, '', texc0{:}, oexc0a{:} ) );
assert( TTEST_CHECK_ERROR_WARNING( ttest_id, '', texc0{:}, oexc0b{:} ) );
assert( TTEST_CHECK_ERROR_WARNING( ttest_id, '', texc0{:}, oexc0a{:}, oexc0b{:} ) );
assert( ~TTEST_CHECK_ERROR_WARNING( ttest_id, '', texc0{:}, mexc0_X{:} ) );

%%% mandatory exception thrown
texc1a = {'t', 'e', 'id:id1a', 'Message 1a', 10, 'testTTEST1a.m', 'testTTEST1a'};
texc1b = {'t', 'e', 'id:id1b', 'Message 1b', 10, 'testTTEST1b.m', 'testTTEST1b'};
mexc1a = {'m', 'id:id1a',[5 10 15],'ID:id1a','testTTEST1a.m'};
mexc1a2 = {'m', ':id1a'};
mexc1ab = {'m', ':id1a', 'Message 1b'};
mexc1_X = {'m', [1:20],'id:id1','testTTEST3'};
oexc1a = {};
assert( TTEST_CHECK_ERROR_WARNING( ttest_id, '', texc1a{:}, mexc1a{:} ) );
assert( TTEST_CHECK_ERROR_WARNING( ttest_id, '', texc1a{:}, mexc1a2{:} ) );
assert( ~TTEST_CHECK_ERROR_WARNING( ttest_id, '', texc1a{:}, mexc1_X{:} ) );
assert( ~TTEST_CHECK_ERROR_WARNING( ttest_id, '', texc1a{:} ) );
assert( ~TTEST_CHECK_ERROR_WARNING( ttest_id, '', mexc1a{:} ) );
assert( ~TTEST_CHECK_ERROR_WARNING( ttest_id, '', texc1a{:}, texc1b{:}, mexc1ab{:} ) );

texc2 = {'t', 'w', 'id:id2', 'Message 2', 20, 'testTTEST2.m', 'testTTEST2'};
oexc2a = {'o','id:id2',20};
oexc2b = {'o','id:id1',20};
oexc2c = {'o','id:id2',10};
oexc2id_X = {'o',':id20','id0:'};
oexc2msga_X = {'o','Message 20'};
oexc2msgb_X = {'o','Mes"$)�'};
oexc2line_X = {'o',[10 30]};
oexc2fun_X = {'o','testTTEST20','testTTEST200'};
oexc2file_X = {'o','testTTEST20.m','testTTEST200.m'};
assert( TTEST_CHECK_ERROR_WARNING( ttest_id, '', texc2{:}, oexc2a{:} ) );
assert( ~TTEST_CHECK_ERROR_WARNING( ttest_id, '', texc2{:} ) );
assert( ~TTEST_CHECK_ERROR_WARNING( ttest_id, '', texc2{:}, oexc2b{:}, oexc2c{:} ) );
assert( ~TTEST_CHECK_ERROR_WARNING( ttest_id, '', texc2{:}, oexc2id_X{:} ) );
assert( ~TTEST_CHECK_ERROR_WARNING( ttest_id, '', texc2{:}, oexc2msga_X{:} ) );
assert( ~TTEST_CHECK_ERROR_WARNING( ttest_id, '', texc2{:}, oexc2msgb_X{:} ) );
assert( ~TTEST_CHECK_ERROR_WARNING( ttest_id, '', texc2{:}, oexc2line_X{:} ) );
assert( ~TTEST_CHECK_ERROR_WARNING( ttest_id, '', texc2{:}, oexc2fun_X{:} ) );
assert( ~TTEST_CHECK_ERROR_WARNING( ttest_id, '', texc2{:}, oexc2file_X{:} ) );

texc3 = {'t', 'w', 'id:id2', 't', 'w', 'id:id3'};
oexc3 = {'o','id:'};
assert( TTEST_CHECK_ERROR_WARNING( ttest_id, '', texc3{:}, oexc3{:} ) );

TTEST v 1

%% TTEST_PARSE
if( ismatlab )
    TTEST v 1
    TTEST_PARSE = allfunctionhandle( 'TTEST_CHECK_ERROR_WARNING', 'TTEST_PARSE', 'v',0 );  % get function handle to local function
    texc1a = {'e', 'id:id1', 'Message 1', 'str1.m', 10, 'testTTEST1.m', 'testTTEST1'};
    t = TTEST_PARSE( 't', texc1a{:} );
    EXPECT_EQ( t, {'t', {'id:id1'}, {'Message 1'}, [10], {'testTTEST1'}, {'str1.m','testTTEST1.m'}, 'Error'} );

    EXPECT_THROW( @() TTEST_PARSE( 'w', 'testTTEST2' ), 'TTEST:EXCEPTION' );
    t = TTEST_PARSE( 't', 'w', 'testTTEST2' );
    EXPECT_EQ( t, {'t', {}, {}, [], {'testTTEST2'}, {}, 'Warning'} );

    t = TTEST_PARSE( 'o','o' );
end;

%% TTEST_IDENTIFY
if( ismatlab );
    h = allfunctionhandle( 'TTEST_CHECK_ERROR_WARNING', 'TTEST_IDENTIFY', 'v',0 );  % get function handle to local function
    %%% valid linenumbers
    EXPECT_EQ( h([1 2 3]), 'line' );
    EXPECT_EQ( h(0), 'line' );
    %%% invalid linenumbers
    EXPECT_EQ( h(-2), 'err' );
    EXPECT_EQ( h(0.3), 'err' );
    EXPECT_EQ( h(inf), 'err' );
    EXPECT_EQ( h(nan), 'err' );

    %%% valid ids
    EXPECT_NTHROW( '[type,arg] = h( ''ttest_id'' );', 'TTEST:EXCEPTION' );
    EXPECT_EQ( type, 'id' );
    EXPECT_EMPTY( arg, '' );
    EXPECT_NTHROW( '[type,arg] = h( ''ttest_id!"$%'' );', 'TTEST:EXCEPTION' );
    EXPECT_EQ( type, 'id' );
    EXPECT_STREQ( arg, '!"$%' );
    EXPECT_EQ( h(':'), 'id' );
    EXPECT_EQ( h('A:'), 'id' );
    EXPECT_EQ( h(':A:'), 'id' );
    EXPECT_EQ( h(':A:B:'), 'id' );
    EXPECT_EQ( h(':A:B'), 'id' );
    EXPECT_EQ( h(':id_'), 'id' );
    EXPECT_EQ( h(':id1'), 'id' );
    EXPECT_EQ( h(':id'), 'id' );
    EXPECT_EQ( h('id:id'), 'id' );

    %%% invalid ids
    EXPECT_EQ( h('id'), 'fun' );
    EXPECT_EQ( h('id::'), 'msg' );
    EXPECT_EQ( h('::id'), 'msg' );
    EXPECT_EQ( h('_:id'), 'msg' );
    EXPECT_EQ( h('id::id'), 'msg' );
    EXPECT_EQ( h("id"), 'fun' );
    EXPECT_EQ( h("id::"), 'msg' );
    EXPECT_EQ( h("::id"), 'msg' );
    EXPECT_EQ( h("_:id"), 'msg' );
    EXPECT_EQ( h("id::id"), 'msg' );

    %%% valid filenames
    EXPECT_EQ( h('ttest_file=%'), 'file' );
    EXPECT_EQ( h('C:\'), 'file' );
    EXPECT_EQ( h('/MATLAB'), 'file' );
    EXPECT_EQ( h('~/'), 'file' );
    EXPECT_EQ( h('file.m'), 'file' );
    EXPECT_EQ( h('file.p'), 'file' );
    %%% invalid filenames
    EXPECT_EQ( h('MATLAB'), 'fun' );

    %%% valid function names
    EXPECT_THROW( @() h('ttest_fun'), 'TTEST:EXCEPTION' );
    EXPECT_EQ( h('ttest_funa'), 'fun' );
    EXPECT_EQ( h('a'), 'fun' );
    EXPECT_EQ( h('a_'), 'fun' );
    EXPECT_EQ( h('a1'), 'fun' );
    %%% invalid function names
    EXPECT_EQ( h(''), 'msg' );
    EXPECT_EQ( h(' '), 'msg' );
    EXPECT_EQ( h('_a'), 'msg' );
    EXPECT_EQ( h('1'), 'msg' );

    %%% invalid stuff
    EXPECT_EQ( h(@sum), 'err' );
    EXPECT_EQ( h({}), 'err' );
end;



