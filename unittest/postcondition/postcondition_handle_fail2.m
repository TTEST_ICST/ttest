function postcondition_handle_fail2( );
    %#ok<*NASGU>
    a = 2;

    if( isoctave )
        pc = POSTCONDITION( @(a) a==3 );
    else;
        POSTCONDITION( @(a) a==3 ); end;    
    
    if( isoctave );
        clear pc; end;
end