function postcondition_handle_fail3( );
    %#ok<*NASGU>
    a = 2;
    b = 3;
    
    
    if( isoctave )
        pc = POSTCONDITION( @(a) a==b );
    else;
        POSTCONDITION( @(a) a==b ); end;    
    
    b = 2;
    a = 2;
    
    if( isoctave );
        clear pc; end;    
end