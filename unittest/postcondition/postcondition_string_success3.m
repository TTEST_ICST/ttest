function postcondition_string_success3( );
    %#ok<*NASGU>
    a = 2;
    b = 3;
    POSTCONDITION( 'a==b', 'b' );
    b = 3;
    a = 3;
end