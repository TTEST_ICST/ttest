function postcondition_handle_success3( );
    %#ok<*NASGU>
    a = 2;
    b = 3;
    
    if( isoctave )
        pc = POSTCONDITION( @(a) a==b );
    else;
        POSTCONDITION( @(a) a==b ); end;  
    
    b = 4;
    a = 3;
    
    if( isoctave );
        clear pc; end;    
end