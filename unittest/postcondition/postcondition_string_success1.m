function postcondition_string_success1( );
    %#ok<*NASGU>
    a = 2;
    POSTCONDITION( 'a==2', 'a' );
    a = 3;
end