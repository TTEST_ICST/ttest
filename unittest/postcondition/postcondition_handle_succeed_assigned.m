function postcondition_handle_succeed_assigned( );
    %#ok<*NASGU>
    a = 2;
    ps = POSTCONDITION( @(a) a==3 );
    a = 3;
    clear ps;
end