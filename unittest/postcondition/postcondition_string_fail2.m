function postcondition_string_fail2( );
    %#ok<*NASGU>
    a = 2;
    POSTCONDITION( 'a==3' );
end