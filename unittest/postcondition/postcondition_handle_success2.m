function postcondition_handle_success2( );
    %#ok<*NASGU>
    a = 2;
    
    if( isoctave )
        pc = POSTCONDITION( @(a) a==3 );
    else;
        POSTCONDITION( @(a) a==3 ); end;
        
    a = 3;
    
    if( isoctave );
        clear pc; end;
end