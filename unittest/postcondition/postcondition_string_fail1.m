function postcondition_string_fail1( );
    %#ok<*NASGU>
    a = 2;
    POSTCONDITION( 'a==3', 'a' );
    a = 3;
end