function postcondition_handle_fail1( );
    %#ok<*NASGU>
    a = 2;
    POSTCONDITION( @() a==3 );
    a = 3;
end