function postcondition_string_fail3( );
    %#ok<*NASGU>
    a = 2;
    b = 3;
    POSTCONDITION( 'a==b', 'b' );
    b = 2;
    a = 2;
end