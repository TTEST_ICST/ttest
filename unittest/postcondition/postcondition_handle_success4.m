function postcondition_handle_success4( );
    %#ok<*NASGU>
    a = 2;
    
    POSTCONDITION( @() a==5, DISABLED );
    
end