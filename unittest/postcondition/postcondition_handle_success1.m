function postcondition_handle_success1( );
    %#ok<*NASGU>
    a = 2;
    pc = POSTCONDITION( @() a==2 );
    a = 3;
end