% test TTEST OUTPUT
% These tests check whether the test macros return the correct output variables

TTEST init
TTEST v 0

% preconditions 

%% ASSERT
TODO_FAIL( 'NONAME tests missing' );
%[ret,a] = ASSERT( 2 );
%assert( EXPECT_TRUE( ret ) );
%assert( EXPECT_EQ(a,2) );

%% EXPECT
%%% TRUTHY
%[ret,a] = EXPECT( 2 );
%assert( EXPECT_TRUE( ret ) );
%assert( EXPECT_EQ( a, 2 ) );

%assert( EXPECT_THROW( '[ret,a,b] = EXPECT( 2 )', ':TooManyOutputs' ) );

%%% NTHROW
%[ret,a] = EXPECT( @() 2 );
%assert( EXPECT_TRUE( ret ) );
%assert( EXPECT_EQ( a, 2 ) );


%% TRUE
[ret,a] = EXPECT_TRUE( 1 ) ;

assert( EXPECT_TRUE( ret ) );
assert( EXPECT_EQ( a, 1 ) );

%% TRUTHY
[ret,a,b,c] = EXPECT_TRUTHY( 0, 2, 3 );
assert( EXPECT_FALSE( ret ) );
assert( EXPECT_EQ( a, 0 ) );
assert( EXPECT_EQ( b, 2 ) );
assert( EXPECT_EQ( c, 3 ) );

%% FALSY
[ret,a,b] = EXPECT_FALSY( [0 0 0], [1 0 0] );
assert( EXPECT_TRUE( ret ) );
assert( EXPECT_EQ( a, [0 0 0] ) );
assert( EXPECT_EQ( b, [1 0 0] ) );

%% PRED
[ret,a] = EXPECT_PRED( @sum, [2 3] );
assert( EXPECT_TRUE( ret ) );
assert( EXPECT_EQ( a, 5 ) );

%% EQ
[ret,a,b,c,d] = EXPECT_EQ( 2, 2, 3, 2 );
assert( EXPECT_FALSE( ret ) );
assert( EXPECT_EQ( a, b, d, 2 ) );
assert( EXPECT_EQ( c, 3 ) );

[ret,x,y] = EXPECT_EQ( 2, 2, 3, 2 );
assert( EXPECT_FALSE( ret ) );
assert( EXPECT_EQ( x, y, 2 ) );

%% ALMOST_EQ
[~,a] = EXPECT_ALMOST_EQ( 1 );
assert( EXPECT_EQ( a, 1 ) );

[ret,a,b,de] = EXPECT_ALMOST_EQ( 1, 1+eps );
assert( EXPECT_TRUE( ret ) );
assert( EXPECT_EQ( a, 1 ) );
assert( EXPECT_EQ( b, 1+eps ) );
assert( EXPECT_ALMOST_EQ( de, 4*eps ) );

%%% symbolics
EXPECT_THROW( @() EXPECT_ALMOST_EQ( sym(2), 2, 3 ), 'ttest:property' );


%% ALMOST_NE
[ret,~,~,de] = EXPECT_ALMOST_NE( 1, 16 );
assert( EXPECT_TRUE( ret ) );
assert( EXPECT_ALMOST_EQ( de, 4*eps(16) ) );

%% FILE_EQ
assert( TODO_FAIL( 'Tests for FILE_EQ missing,' ) );

%% THROW_NTHROW
[ret,a] = EXPECT_THROW( @terror, 'terror:noid' );
assert( EXPECT_TRUE( ret ) );
assert( EXPECT_EQ( a, [] ) );

[ret,a] = EXPECT_THROW( @() inv(0), ':singularMatrix' );
assert( EXPECT_TRUE( ret ) );
if( ismatlab() );
    assert( EXPECT_EQ( a, inf ) );
else;
    assert( EXPECT_EQ( a, 0 ) );
end;


%% postprocessing
% Set errorflag to true
TTEST errflag false

