% preconditions

TTEST init

%% init
oldid = ttest_id;
TTEST init
EXPECT_EQ( oldid, ttest_id );
TTEST init-hard
EXPECT_NE( oldid, ttest_id );
newid = TTEST('id',10);
TTEST init-last
EXPECT_EQ( newid, ttest_id );

%% verbose
TTEST v 1  % this command must not produce screen output
ASSERT_PRED( @(x) ~isempty(x), evalc('EXPECT_EQ(2,3);') );
TTEST v 0 
ASSERT_PRED( @(x) isempty(x), evalc('EXPECT_EQ(2,3);') );

EXPECT_NTHROW( @() TTEST('v') );
EXPECT_NTHROW( @() TTEST('v',1) );
EXPECT_EQ( 1, TTEST('v',0) );
EXPECT_EQ( 0, TTEST('v') );

TTEST clear
TTEST v 1

%% clear_all
TTEST ttest_id_89124190jaksdjk var variable10 10
TTEST global v 54321
TTEST todo ass
str = evalc( 'TTEST p' );
EXPECT_THAT( str, ttest.HasSubstr('TODO') );
EXPECT_THAT( str, ttest.HasSubstr('ASSERT') );
EXPECT_THAT( str, ttest.HasSubstr('54321') );
val = TTEST('todo');
EXPECT_THAT( char(val{1}), ttest.HasSubstr('ASSERT') ); 
TTEST clear all
EXPECT_EQ( TTEST('ttest_id_89124190jaksdjk','var','variable10',nan),[] );
EXPECT_EQ( TTEST('global','v'),1 );
val = TTEST('todo');
EXPECT_PRED( @(x) numel(x)==2, val );
EXPECT_THAT( char(val{1}), ttest.Not(ttest.HasSubstr('ASSERT')) );
TTEST clear all

%% var
%%% local
EXPECT_EQ( [], TTEST( 'var', 'var1', 1 ) );
EXPECT_EQ( 1, TTEST( 'var', 'var1', 2 ) );
EXPECT_NTHROW( 'TTEST( ''clearvar'', ''var1'' );' );
EXPECT_EQ( [], TTEST( 'var', 'var1', 1 ) );

%%% global
unusedvarname = ['ttest_ajkslf_jkal_' num2str(randi(100000000000000))];  % To make sure that this is not defined by accident
EXPECT_EQ( [], TTEST( 'global', 'var', unusedvarname, 1 ) );
EXPECT_EQ( 1, TTEST( 'global', 'var', unusedvarname, 2 ) );
EXPECT_NTHROW( @() TTEST( 'global', 'clearvar', unusedvarname ) );

%% wrong_input
%%% local
EXPECT_THROW( 'TTEST( ''wronginput'' )', 'TTEST:argin' );
EXPECT_NTHROW( 'TTEST( ''var'' )', 'TTEST:var' );
EXPECT_NTHROW( 'TTEST( ''exp'' )', 'TTEST:todoexpass' );

%%% global
EXPECT_THROW( @() TTEST( 'global', 'wronginput' ), 'TTEST:argin' );
EXPECT_NTHROW( @() TTEST( 'global','var' ), 'TTEST:var' ); 
EXPECT_NTHROW( @() TTEST( 'global', 'exp' ), 'TTEST:todoexpass' );

%% todoexpass
TTEST v 0
TTEST ass exp
EXPECT_NTHROW( 'ASSERT_EQ(2,3)' ); %Function handle style does not work here
EXPECT_NTHROW( @() ASSERT_EQ(ttest_id,2,3) ); %Function handle style does not work here
TTEST todo ass
EXPECT_THROW( 'TODO_EQ(2,3)' );
EXPECT_THROW( @() TODO_EQ(ttest_id,2,3) );
TTEST todo exp
TODO_EQ(2,2); %This must not produce screen output
%make standard values again
TTEST todo todo
TTEST exp exp
TTEST ass ass
TTEST v 1

TTEST('experr',@(a,b,c,d) fprintf('OK') );
str = evalc( 'EXPECT_FAIL;' );
EXPECT_EQ( str, 'OK' );

%% tellme
EXPECT_NTHROW( @() TTEST('tellme') );

%% init
clear ttest_id
TTEST init



