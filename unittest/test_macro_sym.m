% test macro sym

TTEST init
TTEST v 0
if( ismatlab );
    % XX check whether symbolic toolbox is installed
else;
    try;
        pkg load symbolic; 
    catch;
        error( 'TTEST:symbolic', 'Symbolic package does not seem to be installed.\n  Try executing:\n    pkg install -forge symbolic' ); end; end;


%check if we are in the right directiory
a = which( 'test_macro' );
assert( numel(a)>=14 && strcmpi(a(1:end-13),pwd), 'test_macro', 'This function must be called from the ./unittest folder in the TTEST folder.' );
% preconditions 

%% TRUTHY
assert( EXPECT_TRUTHY( sym(1) ) );
assert( EXPECT_TRUTHY( sym(inf) ) );

%% FALSY
EXPECT_THROW( @() EXPECT_FALSY(@sum), 'sym:isAlways' );

%%% ALMOST_EQ
assert( EXPECT_ALMOST_EQ( sym(2), 2 ) );

%% SUPERSET
syms xx;
assert( EXPECT_SUPERSET( {xx,xx+1},{xx,xx+1} ) );

%% postprocessing
% Set errorflag to false
TTEST errflag false
