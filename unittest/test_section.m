%% prerequisites


%% t1
clear;
TTEST hardinit test_section


a = 1;
x = 10;
TESTCASE( 't1' );
    EXPECT_EQ( a, 1 );
    b = 1;  %#ok<NASGU>
TESTCASE( 't2' );  % section 't1' is implicitely closed
    EXPECT_EQ( a, 1 );
    EXPECT_EXIST( 'b', 0 );
    b = 3; 
    SECTION( 't2' );  % section 's1' is nested in section 't1'
        EXPECT_EQ( a, 1 );
        EXPECT_EQ( b, 3 );
        b = 4; 
        c = 4;  %#ok<NASGU>
    SECTION( 't2' );  % unnamed section is implicitely closed
        EXPECT_EQ( a, 1 );
        EXPECT_EQ( b, 3 );
        EXPECT_EXIST( 'c', 0 );
        c = 5; d = 6;
        SUBSECTION( 's3' );
            EXPECT_EQ( a, 1 );
            EXPECT_EQ( b, 3 );
            EXPECT_EQ( c, 5 );
            EXPECT_EQ( d, 6 );
            d = 7;
        ENDSUBSECTION  % we are back in 's1' as it was before opening of 's3'
        EXPECT_EQ( c, 5 );
        EXPECT_EQ( d, 6 );
        % ENDSUBSECTION( 's3' );  % would throw an error, since no subsection is open
TESTCASE( 't3' );  % closes everything
    EXPECT_EQ( a, 1 );
    EXPECT_EXIST( 'b', 0 );
ENDTESTCASE;
EXPECT_EQ( a, 1 );
b = 2;  % we are outside of all sections here
TESTCASE( 't4' );
    EXPECT_EQ( a, 1 );
    EXPECT_EQ( b, 2 );

%% t2
clear;
TTEST hardinit test_section

a = 1;
SUBSECTION();
SUBSECTION();
    EXPECT_EQ( a, 1 );
    
%% t3
clear;
TTEST hardinit test_section

a = 1;
TESTCASE();
    b = 2;
    SUBSECTION();
        EXPECT_EQ( a, 1 );
        EXPECT_EQ( b, 2 );
    ENDSUBSECTION();
    EXPECT_EQ( a, 1 );
    EXPECT_EQ( b, 2 );
    
%% t4
clear;
TTEST hardinit test_section

a = 1;  %#ok<NASGU>
SUBSECTION();
    b = 2;  %#ok<NASGU>
    a = 10;
    EXPECT_EQ( a, 10 );
TESTCASE();
    EXPECT_EQ( a, 1 );
    EXPECT_EXIST( 'b', 0 );
    
%% t5
clear;
TTEST hardinit test_section 

SUBSECTION();
    c = 3;  %#ok<NASGU>
SECTION();
    b = 2;
TESTCASE()
    a = 1;
    EXPECT_EXIST( 'c', 0 );
    EXPECT_EXIST( 'b', 0 );
    EXPECT_EQ( a , 1 );
ENDTESTCASE();
EXPECT_EXIST( 'a', 0 );
EXPECT_EXIST( 'b', 0 );
EXPECT_EXIST( 'c', 0 );
    

%% t6
clear;
TTEST hardinit test_section 

a = 1;
SECTION();
    c = 3;  %#ok<NASGU>
ENDTESTCASE();

EXPECT_EQ( a, 1 );
EXPECT_EXIST( 'c', 0 );

%% t7
clear;
TTEST hardinit test_section 

a = 1;
SUBSECTION();
    c = 3;  %#ok<NASGU>
ENDTESTCASE();

EXPECT_EQ( a, 1 );
EXPECT_EXIST( 'c', 0 );

%% t8
clear;
TTEST hardinit test_section 

a = 1;
SUBSECTION();
    c = 3;  %#ok<NASGU>
ENDSECTION();

EXPECT_EQ( a, 1 );
EXPECT_EXIST( 'c', 0 );

%% t9
clear;
TTEST hardinit test_section 

a = 1;
SECTION();
    c = 3;  %#ok<NASGU>
ENDSECTION();

EXPECT_EQ( a, 1 );
EXPECT_EXIST( 'c', 0 );

%% t10
clear;
TTEST hardinit test_section 

a = 1;
SUBSECTION();
    c = 3;
ENDSUBSECTION();

EXPECT_EQ( a, 1 );
EXPECT_EXIST( 'c', 0 );

%% t11
clear;
TTEST hardinit test_section 

a = 1;
TESTCASE();
TESTCASE();
TESTCASE();
    EXPECT_EQ( a, 1 );
ENDTESTCASE();
EXPECT_EQ( a, 1 );

%% t12
clear;
TTEST hardinit test_section 

a = 1;
TESTCASE();
	b = 2;
    SUBSECTION()
        c = 3;
    SECTION();
        EXPECT_EQ( a, 1 );
        EXPECT_EQ( b, 2 );
        EXPECT_EXIST( 'c', 0 );
        
%% restore
clear
TTEST hardinit test_section

global ttest_a
ttest_a = 1; %#ok<NASGU>
TESTCASE( 'global' );
	SECTION( '1a' );
        ttest_a = 2;
    SECTION( '1b' );    
        EXPECT_EQ( ttest_a, 1 );
        
TESTCASE( 'path' );
    SECTION( '1a'   );
        addpath( 'at' )
    SECTION( '1b' );
        EXPECT_FALSE( onpath( fullfile('TTEST','unittest','at') ) );
        
TESTCASE( 'warning' );
    SECTION( '1a' );
        warning( 'off', 'TTEST:sectiontest' );
    SECTION( '1b' );    
        w = warning;
        idx = arrayfun( @(i) strcmp(w(i).identifier,'TTEST:sectiontest'), 1:numel(w) );
        EXPECT_TRUE( ~any(idx) );  
        
TESTCASE( 'pwd' );
    SECTION( '1a' );
        cd cache
    SECTION( '1b' );
        p = pwd;
        EXPECT_FALSE( numel(p)>= 5 && isequal(p(end-4:end),'cache') );  
        
TESTCASE( 'debug' );
    SECTION( '1a' );
        dbstop test_section 1;
    SECTION( '1b' );
        db = dbstatus;
        idx = arrayfun( @(i) strcmp(db(i).name,'test_section'), 1:numel(db) );
        EXPECT_TRUE( ~any(idx) );
    
TESTCASE( 'rng1' );
    SECTION( '1a' );
        TTEST( 'var', 'rand', randi(10000) );
    SECTION( '1b' );
        EXPECT_NE( randi(10000),  TTEST( 'var', 'rand' ) );

TESTCASE( 'base' );
    num = num2str( randi(1000000) );
    name = ['ttest_x_hajskdh7q2gha' num];
    SECTION( '1a' );
        assignin( 'base', name, 10 );
    SECTION( '1b' );
        wh = evalin( 'base', 'who' );
        idx = arrayfun( @(i) strcmp(wh{i},name), 1:numel(wh) );
        EXPECT_TRUE( any(idx) );
        evalin( 'base', ['clear ' name] );
        
ENDTESTCASE();



        
        
        
        