function allfunctionhandle_function;
%#ok<*DEFNU>
fprintf( 'allfunctionhandle_function\n' );
2;

    function nested1;
    fprintf( 'nested1\n' );
    
        function nested11;
        fprintf( 'nested11\n' );
        end
        
        function nested12;
        fprintf( 'nested12\n' );
        end
        
    end

    function nested2;
    fprintf( 'nested2\n' );
        
        function nested21;
        fprintf( 'nested21\n' );
        end
        
    end	

    function nested3;
    fprintf( 'nested3\n' );
    end	

end

function local1
fprintf( 'local1\n' );

    function localnested1
    fprintf( 'localnested1\n' );
    end
end
 
function local2
fprintf( 'local2\n' );
end

