function staticfunction
% function which is a static workspace 
%   - it includes tests for TTESTS
%   - is also used as target to test certain functions

    k = 1;
    k = k + 1;
    k = k + 1;
    function ret = nestedfunction3 %#ok<DEFNU>
        %fprintf( 'in nested function\n' );
        ret = 3;
    end

    2;
    k = 2;

    ttest_id = TTEST('init','staticfunction'); %#ok<NASGU>
    TTEST v 0
    EXPECT_FAIL( 'This message should not be printed.' );
    TTEST v 1
    
    eq5 = ttest.Matcher( @(x) x==5 );
    EXPECT_THAT( 5, eq5 );
    
    [~,a] = EXPECT_NTHROW( @() inv(0), ':singularMatrix' );
    if( ismatlab );
        EXPECT_EQ( a, inf );
    else;
        EXPECT_EQ( a, 0 ); end;  % WTF
    
    b = 0;
    EXPECT_NTHROW( 'b = inv(0)', ':singularMatrix' );
    if( ismatlab );
        EXPECT_EQ( b, inf );
    else;
        EXPECT_EQ( b, 0 ); end;
    
end

function ret = localfunction2 %#ok<DEFNU>
    %fprintf( 'in localfunction2' );
    ret = 2;
end

function dummy; end %#ok<DEFNU> %Generates an error, if the 'end' of a function is missing.