% test TTEST utility

TTEST init test_utility
if( ismatlab );
    % XX check whether symbolic toolbox is installed
else;
    try;
        pkg load symbolic; 
        sym( 1 );
    catch;
        error( 'TTEST:symbolic', 'Symbolic package does not seem to be installed.\n  Try executing:\n    pkg install -forge symbolic' ); end; end;

% preconditions 

%% castws
%%% stress test
TESTCASE( 'castws' );
    SECTION( 'stresstest' );
    for ttest_type = {'int8','uint16','single'};
        for var = {sym(3), struct('m',struct('m',1)), {int32([2 3 4]) {int32([3 2])}}, single( 3200 ), logical([10 10 10])};
            warning off castws:doublecast
            var = var{1};  %#ok<FXSET>
            nerr = castws( ttest_type{1} );
            EXPECT_EQ( nerr, 0 ); end; end;



%%% sym test
    SECTION( 'sym test' );
    var = struct( 'm', struct('m',1) );
    try;
        castws( 'sym' );
        EXPECT_ISA( var.m.m, 'sym' );

        var = @() true;
        nerr = castws( 'double', 'v',0 );
        EXPECT_ISA( var, 'function_handle' );
        EXPECT_EQ( nerr, 1 );

        nerr = castws( 'double', 'v',0, 'skip',{'ttest_','var'} );
        EXPECT_EQ( nerr, 0 );
    catch;
        EXPECT_FAIL( 'castws test failed.' ); end;
ENDTESTCASE();
%% allfunctionhandle
if( ismatlab )
TESTCASE( 'allfunctionhandle' );
    cd allfunctionhandle
    SECTION( 'static function' );    
        warning off TTEST:nested
        allfunctionhandle( 'staticfunction', 'nested',1, 'v',-1, 'assign' );
        EXPECT_NTHROW( @() assert(staticfunction_nestedfunction3()==3) );
        EXPECT_NTHROW( @() assert(localfunction2()==2) );
    
    SECTION( 'script function' );
        allfunctionhandle( 'scriptfunction', 'v',-1, 'assign' );
ENDTESTCASE();
else;
    TODO_FAIL( 'allfunctionhandle does not work on Octave yet.' );
end;

%% complexity
TESTCASE( 'complexity' );
    if( isoctave );
        warning( 'off', 'Octave:colon-nonscalar-argument' ); end;
    x = 1:100;
    EXPECT_EQ( '1', complexity( 5*ones([1 100])+0.001*randn(1,100), 'v',0 ) );
    EXPECT_EQ( '1', complexity( 10*ones([1 1000])+.1*randn(1,1000), 'v',0 ) );
    EXPECT_EQ( 'logn', complexity( x, log(x)+0.1*randn(size(x)), 'v',0 ) );
    EXPECT_EQ( 'n', complexity( 1:100+randn(1,100), 'v',0 ) );
    EXPECT_EQ( 'nlogn', complexity( x, x.*log(x)+randn(size(x)), 'v',0 ) );
    EXPECT_EQ( 'n2', complexity( [x;x.^2+100*randn(size(x))], 'v',0 ) );
    EXPECT_EQ( 'n3', complexity( [x;x.^3+1000*randn(size(x))], 'v',0 ) );
    EXPECT_EQ( 'n4', complexity( [x;x.^4+1000*randn(size(x))], 'v',0 ) );
    EXPECT_EQ( '2n', complexity( 2.^(x+.01*randn(size(x))), 'v',0 ) );
    EXPECT_EQ( 'n!', complexity( gamma(1:15 + abs(0.1*randn(1,15))), 'v',0 ) );
    if( ismatlab );
        for i = [1001 1003 1005 1007];
            rng( i );  % next test is flaky, until we have a 'flaky' option, we need to seed the rng here and check it only on Matlab
            EXPECT_EQ( 'n!', complexity( gamma(1:15) + 10*randn(1,15), 'v',0 ) ); end; end;
ENDTESTCASE();

%% isequalproperty
TESTCASE( 'isequalpropert' );
    warning( 'off', 'MATLAB:structOnObject' );
    clear x
    EXPECT_TRUE( isequalproperty([], []) );
    EXPECT_TRUE( isequalproperty(@sum, @prod) );
    EXPECT_TRUE( isequalproperty(sparse([1 2],[1 1],[-1 -2],2,2,10), sparse([1 2],[1 1],[-1 -2],2,2,20)) );
    EXPECT_TRUE( isequalproperty(ttest_id_c(1), ttest_id_c(2)) );

    EXPECT_FALSE( isequalproperty(zeros(1,0), zeros(0,1)) );
    EXPECT_FALSE( isequalproperty(zeros(1,0), zeros(0,1)) );
    EXPECT_FALSE( isequalproperty(single(2), double(2)) );
    EXPECT_FALSE( isequalproperty([], {}) );
    
    x.id = 'ttest_id_1';
    EXPECT_TRUE( isequalproperty(struct(ttest_id_c(1)),x) );
    EXPECT_FALSE( isequalproperty(ttest_id_c(1),x) );
ENDTESTCASE();


%% secloop
% XX These two tests currently fail for an unknown reason,
% XX but only when the whole test suite is executed with no debugger attached
% EXPECT_MAXTIME( DISABLED('onoctave'), @() secloop(2), 8 );
% EXPECT_MINTIME( DISABLED('onoctave'), @() secloop(10), 2 );

%% subset
EXPECT_TRUE( subset( [1 2], [1 2 3] ) );
EXPECT_TRUE( subset( [1 1;1 2].', [1 1;1 2;2 3].' ) );
EXPECT_TRUE( subset( {[1],[1 2]}, {[1],[1 2],[1;2]} ) );
EXPECT_FALSE( subset( [1 2 3], [1 2]  ) );
EXPECT_FALSE( subset( [1 1;1 2;2 3].', [1 2;2 3].' ) );
EXPECT_FALSE( subset( {[1],[1 2]}, {[1],[1;2]} ) );
if( ismatlab );
    EXPECT_THROW( @() subset([1 2]',[1 2 3]'), ':AandBColnumAgree' );
else;
    EXPECT_THROW( @() subset([1 2]',[1 2 3]'), 'ismember: number of columns in A and B must match' );
end;
EXPECT_TRUE( subset({1 2},{1 2 3}) );
EXPECT_TRUE( subset( {nan},{nan} ) );
if( ismatlab() )
    syms xx
    EXPECT_FALSE( subset( xx+[0:2], xx+[1:3] ) );
end

%% terror
EXPECT_THROW( @() terror('id:id'), 'id:id' );




%% evallazy
%%% variablename
if( ismatlab );
    allfunctionhandle( 'evallazy', 'assign', 'v',0 );
    EXPECT_EMPTY( variablename( ) );
    EXPECT_EQ( variablename( @()1 ), {{}} );
    EXPECT_EQ( variablename( @(x)x ), {{'x'}} );
    [vn,an,i] = variablename( @(x,y) x, @(z,x)z );
    EXPECT_EQ( vn, {{'x','y'},{'z','x'}} );
    EXPECT_EQ( an, {'x','y','z'} );
    EXPECT_EQ( i, {[1 2],[3 1]} );

    %%% evallazy_handle
    a = 1; h = @(b) a+b; a = 2; b = 4; %#ok<NASGU>
    EXPECT_EQ( evallazy( h ), 5 );

    a = 1; h = @(b,c) a+b; a = 2; b = 4; %#ok<NASGU>
    try;
        evallazy( h );
        assert( false );
    catch me;
        assert( strcmp(me.identifier,'evallazy:missing') ); end;

    a = 1; h = @(b) a+b; b = 10; a = 20; st.b = 0; %#ok<NASGU>
    EXPECT_EQ( evallazy( h, st ), 1 );


    a = 1; h = @(b,c) a+b; b = 10; a = 20; st.b = 0;
    try;
        evallazy( h, st );
        assert( false );
    catch me;
        assert( strcmp(me.identifier,'evallazy:missing') ); end;

    EXPECT_EQ( evallazy( @() 2 ), 2 );
end;
