% test severity

TTEST init

%% DISABLED
assert( EXPECT_THAT( evalc('EXPECT_EQ( 2, 3, DISABLED )'), ttest.HasSubstr('skipped') ) );
assert( EXPECT_THAT( evalc('EXPECT_EQ( 2, 2, DISABLED(0) )'), ttest.Not(ttest.HasSubstr('skipped')) ) );
if( ismatlab );
    assert( EXPECT_THAT( evalc('EXPECT_EQ( 2, 2, DISABLED(''ismatlab'') )'), ttest.HasSubstr('skipped') ) );
else;
    assert( EXPECT_THAT( evalc('EXPECT_EQ( 2, 2, DISABLED(''ismatlab'') )'), ttest.Not(ttest.HasSubstr('skipped')) ) ); end;

%% ENABLED
if( ismatlab );
    assert( EXPECT_THAT( evalc('EXPECT_EQ( 2, 2, ENABLED(ismatlab) )'), ttest.Not(ttest.HasSubstr('skipped')) ) );
else;
    assert( EXPECT_THAT( evalc('EXPECT_EQ( 2, 2, ENABLED(ismatlab) )'), ttest.HasSubstr('skipped') ) ); end;

%% T_O_D_O_E_D
if( ispc );
    assert( EXPECT_THAT( evalc('EXPECT_EQ( 2, 3, TODOED(@() ispc) )'), ttest.HasSubstr('TODO') ) );
    assert( EXPECT_THAT( evalc('EXPECT_EQ( 2, 3, TODOED(@() isunix) )'), ttest.Not(ttest.HasSubstr('TODO')) ) );
else;
    assert( EXPECT_THAT( evalc('EXPECT_EQ( 2, 3, TODOED(@() isunix) )'), ttest.HasSubstr('TODO') ) );
    assert( EXPECT_THAT( evalc('EXPECT_EQ( 2, 3, TODOED(@() ispc) )'), ttest.Not(ttest.HasSubstr('TODO')) ) ); end;

%% is
assert( EXPECT_EQ( ismatlab, ~isoctave ) );
assert( EXPECT_EQ( ispc, ~isunix, ENABLED( @()~ismac ) ) );

%% postprocessing
% Set errorflag to false
TTEST errflag false
