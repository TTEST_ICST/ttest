for n = 1:16
    fid = fopen( ['test' num2str(n) 'Test.m'], 'wt' );
    fprintf( fid, [
            'function test_suite = test' num2str(n) 'Test' newline ...
            '    test_suite = buildFunctionHandleTestSuite(localfunctions);' newline newline] );
    for k = 1:1024
        fprintf( fid, [
            'function test_2_2_' num2str(k) newline ...
            '    assertEqual( 2, 2 );' newline ...
            ] );
    end
    fclose( fid) ;
end

