clear all
%%
cd 16
tic;
runxunit;
T16 = toc % (5.6, 4.9), 4.9, 4.9, 7.0, 4.1
cd ..
%%
cd 1024
tic;
runxunit;
T1024 = toc % (7.2) 3.5, 2.7, 2.8, 3.1, 2.6, 2.6, 2.5
cd ..
%%
cd E
try;
TEx = timeit( @runxunit ) % 0.02
catch me;
    disp( me ); end;
cd ..
