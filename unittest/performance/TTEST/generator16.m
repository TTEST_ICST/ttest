for n = 1:16
    fid = fopen( ['test' num2str(n) 'Test.m'], 'wt' );
    fprintf( fid, 'TTEST init\n' );
    for k = 1 : 1024;
        fprintf( fid, [
                'TESTCASE( ''testpart' num2str(k) '''' ' )' newline ...
                '    EXPECT_EQ( 2, 2 );' newline ...
                ] );
    end;
    fclose(fid);
end;
