
for n = 1:1024
    fid = fopen( ['test' num2str(n) 'Test.m'], 'wt' );
    fprintf( fid, 'TTEST init\n' );
    fprintf( fid, [
			'TESTCASE( ''test' num2str(n) '''' ' )' newline ...
            '    EXPECT_EQ( 2, 2 );' newline ...
            ] );
    fclose(fid);
end
