TTEST clear all
cd 16
tic;
runttests;
T16 = toc;  % 24.2, 23.3
cd ..

%%
TTEST clear all
cd 1024
tic;
runttests;
T1024 = toc;  % 11.5, 9.5
cd ..

%%
TTEST clear all
cd E
timeit( @() runttests )
toc
cd ..

%%
TTEST clear all
cd 16
profile on
runttests;
profile off
profile viewer
cd ..

%%
TTEST clear all
cd 1024
profile on
runttests;
profile off
profile viewer
cd ..

%%
TTEST clear all
cd E
profile on
for i = 1:5
    runttests
end
profile off
profile viewer
cd ..
