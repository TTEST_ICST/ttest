clc

cd 16
tic;
runtests;
T16 = toc; % 
cd ..

cd 1024
tic;
runtests;
T1024 = toc; % 
cd ..

cd E
TEx = timeit( @runtests ); % 
cd ..

T16
T1024
TEx