for n = 1:16
    fid = fopen( ['test' num2str(n) 'Test.m'], 'wt' );
    for x = 1:1024
        fprintf( fid, [
            'function testPart' num2str(x) '( testCase );' newline ...
            '    EXPECT_EQ( 2, 2 );' newline ...
            'end' newline ...
            ] );
    end
    fclose(fid);
end
