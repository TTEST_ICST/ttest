cd 16
tic;
runtests;
T16 = toc; % 109, 102, 103
cd ..

cd 1024
tic;
runtests;
T1024 = toc; % 37, 31, 31
cd ..

cd E
TEx = timeit( @runtests ); % 0.135, 0.133
cd ..

T16
T1024
TEx