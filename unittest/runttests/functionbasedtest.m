function functionbasedtest
 %#ok<*DEFNU>
 
function test1
    %fprintf( 'In Test1\n' );
    TTEST init functionbasedtest_test1
    TTEST var functionbasedtest_test1 functionbasedtest_test1

function test2
    %fprintf( 'In Test2\n' );
    TTEST init functionbasedtest_test2
    TTEST var functionbasedtest_test2 functionbasedtest_test2
