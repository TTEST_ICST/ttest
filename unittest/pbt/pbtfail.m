function [ ret ] = pbtfail( what, varargin );
% for pbt tests which must fail
% Input:
%   what       string, one of the belo
%
% what:
%   'sort'         sorts the input, fails for arrays of length 10,20,30,40
%   'rounddown'    rounds down, fails for negative numbers where it round up
%   
%   

    switch what
        case 'sort';
            ret = sortfail( varargin{:} );
        case 'rounddown';
            ret = rounddownfail( varargin{:} );
    end;
end

function [ vec ] = sortfail( vec );
    if( mod(numel(vec),10) );
        vec = sort( vec ); end;
end

function [ x ] = rounddownfail( x );
    x = fix( x );
end

function dummy; end %#ok<DEFNU> %Generates an error, if the 'end' of a function is missing.   
