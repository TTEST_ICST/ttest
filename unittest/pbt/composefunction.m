function [ raw ] = composefunction( tt )
    %% function which tests compose adaptor
    fl = float( 'allownan',0, 'allowinf',0 );
    f1 = fl.example( tt );
    f2 = fl.example( tt );
    raw = sort( [f1 f2] );
    
end

function dummy; end %#ok<DEFNU> %Generates an error, if the 'end' of a function is missing.