% test TTEST GIVEN

TTEST init ttest_id_given

% preconditions 

%% basictest
a = 100; GIVEN( 'sqm1_', 'EXPECT_NE( sqm1_, a )' );

GIVEN( @(flt) EXPECT_SCALAR( flt ) );
GIVEN( ttest.float('nan',0), @(x) EXPECT_EQ( x, x.' ) );
flt = ttest.float( 'nan',0 );
GIVEN( flt, @(x) EXPECT_EQ( x, x ) );
GIVEN( 'flt', @(x) EXPECT_SCALAR( x ) );

GIVEN( 'sqm', 'arr', 'sqm', @(x,z,y) EXPECT_ISA( x, y, z, 'double' ) );

%% multiple
ttest.float( 'nan',0, 'name','fltr' );
GIVEN( 'fltr1_', 'fltr2_', 'EXPECT_TRUE( fltr1_<=fltr2_ || fltr1_>=fltr2_ )' );
GIVEN( 'fltr', 'fltr', @(x,y) EXPECT_TRUE( x>=y || x<=y ) );
GIVEN( @(fltr1,fltr2) EXPECT_TRUE( fltr1>=fltr2 || fltr1<=fltr2 ) );


%% rounddown
TESTCASE( 'roundown' );
    TTEST v 0
    cd pbt
    assert( ~GIVEN( @(flt) flt >= pbtfail('rounddown',flt) ) );
    st = ttest.float( 'allowneg',false, 'nan',0, 'name','fltp' );
    assert( GIVEN( @(fltp) fltp >= pbtfail('rounddown',fltp) ) );
    assert( GIVEN( st, @(x) x >= pbtfail('rounddown',x) ) );
    TTEST v 1
ENDTESTCASE();

%% function_handle
assert( GIVEN( ttest.float('min',inf,'nan',0),  @isinf, 'minnumexample',1 ) );

%% handle behaviour of class
% XX unit test missing

%% maxtime
TTEST v 0

t1 = tic; 
assert( ~GIVEN( @() ttest.void( @() pause(0.5), true ), 'maxtime',2 ) );
t2 = toc( t1 );
assert( t2<2.6 );

t1 = tic; 
assert( ~GIVEN( 'ttest.void( @() pause(0.5), true )', 'maxtime',2 ) );
t2 = toc( t1 );
assert( t2<2.6 );

TTEST v 1
%% maxexample
TTEST v 0

t1 = tic;
assert( ~GIVEN( @() ttest.void( @() pause(0.5), true ), 'maxexample',3 ) );
t2 = toc( t1 );
assert( t2<2 );

t1 = tic;
assert( ~GIVEN( 'ttest.void( @() pause(0.5), true )', 'maxexample',3 ) );
t2 = toc( t1 );
assert( t2<2 );

TTEST v 1

%% postprocessing
% Set errorflag to false
TTEST errflag false
