% test TTEST GIVEN

TTEST init ttest_id_strategy
TTEST v 0

% preconditions 

%% float
assert( ~GIVEN( ttest.float( 'max',-1e10, 'single',true, 'double',true ), @(x) x<=-1e10 ) );
assert( GIVEN( ttest.float( 'max',-1e10, 'single',true, 'double',true, 'nan',0  ), @(x) x<=-1e10 ) );
assert( ~GIVEN( ttest.float( 'min',inf, 'nan',0 ), @(x) isinf(x) ) );  % Too less examples generated
assert( GIVEN( ttest.float( 'min',100, 'max',300, 'nan',0, 'single',true, 'double',true ), @(x) x>=100 && x<=300 ) );
assert( GIVEN( ttest.float( 'allowneg',0, 'nan',0 ), @(x) x>=0 ) );
assert( GIVEN( ttest.float( 'double',0 ), @(x) EXPECT_ISA( x, 'single' ) ) );

%% array

assert( GIVEN( ttest.array('minsze',5,'maxsze',[5 2]), @(x) all( size(x)<=[5 2] ) ) );
assert( GIVEN( ttest.array('minval',5,'maxval',6), @(x) all( x(:)>=5 ) && all( x(:)<=6 ) ) );

%% postprocessing
% Set errorflag to false
TTEST errflag false