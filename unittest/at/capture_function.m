function capture_function( ); 
%#ok<*NASGU>
	a = 10; % <La10>
	a = 20; % <La20>
	a = 30; % <La30>
    b = 40; % <Lb40>
    c = 50; % <Lc50>
end % <Lend>