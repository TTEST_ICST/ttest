function [ret,flag] = input_function( sflag );

    flag = true;
    a = 2;          %#ok<NASGU> %<0>
    b = 3;          %#ok<NASGU> %<00>
	if( sflag==1 );
        [ret,f] = input( '', 's', 'debug' ); %<1>
        flag = flag && f;
    elseif( sflag==2 )
        [ret,f] = input( '', 'debug' );      %<2>
        flag = flag && f;
    elseif( sflag==3 );
        [ret,f] = sub_function;     %<3>
        flag = flag && f;
    elseif( sflag==4 );
        [ret,f] = sub_function;     %<3>
        flag = flag && f;
        [in3,f] = input( '', 's', 'debug' );
        flag = flag && f;
        ret = [ret in3];
    elseif( sflag==5 );
        [ret{1},f] = input( '', 'debug' );
        flag = flag && f;
        [ret{2},f] = input( '', 'debug' ); 
        flag = flag && f; end; 
    
end

function [ret,f] = sub_function()
    [ret,f] = input( '', 's', 'debug' );
end