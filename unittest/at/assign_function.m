function [in,a,b,c,d,e] = assign_function( in ); 
%#ok<*NASGU>
2;
    4;      % <4> % this line must be on line number 4
	a = 10; % <A>
	a = 20; % <B>
	a = 30; % <C>
    b = 40; % <D>
    c = 50; % <E>
end % <F>