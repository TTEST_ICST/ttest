function ret = inputemu_function( txt, layout, txt2 );
    %t2 = timer( 'Name','TTEST_INPUT2', 'StartDelay',1.5, 'TimerFcn', @(t,~) ttest.inputemu( 'key_normal', '\ENTER', 'layout',layout) );
    %start( t2 );
    if( nargin==2 );
        txt2 = txt; end;
    try;
        t1 = timer( 'Name','TTEST_INPUT1', 'StartDelay',.5, 'TimerFcn', @(t,~) ttest.inputemu( 'key_normal', [txt '\ENTER'], 'layout',layout) );
        start( t1 );
        ret = EXPECT_EQ( txt2, input('','s') );
    catch;   
        ret = EXPECT_FAIL( "Test failed" ); end;
    %pause( 1.5 );
end