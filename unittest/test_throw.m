% test TTEST THROW
% These tests check macro calls which must throw

TTEST init
TTEST v 0

% preconditions 

%% ASSERT
EXPECT_TRUE( function_throw() );