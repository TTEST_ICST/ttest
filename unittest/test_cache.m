% test severity

TTEST init
TTEST v 0

numdir_basefolder_before = dir();
olddir = cd( 'cache' );  % we switch folder since we create files
cleanDir = onCleanup( @() cd(olddir) );
numdir_cachefolder_before = dir();
ASSERT_EQ( numel(numdir_cachefolder_before), 3 );  % one file plus `..` and `.` and `.gitignore`
w = warning;
if( isoctave )
    warning( 'off', 'Octave:classdef-to-struct' );
    end;
cleanWarning = onCleanup( @() warning(w) );

%% name constructor
clear mmap1 mmap2 mmap3

mmap1 = CACHE( 'tname', 'v',0, 'overwrite',true );
assert( EXPECT_EQ( mmap1.var, ttest_info_c('empty'), ENABLED('onmatlab') ) );
assert( EXPECT_EQ( mmap1.var, [], ENABLED('onoctave') ) );
mmap1.var = 10;
assert( EXPECT_EQ( mmap1.var, 10 ) );
clear mmap1

mmap2 = CACHE( 'tname.mat', 'v',0 );
assert( EXPECT_EQ( mmap2.var, 10 ) );
clear mmap2

mmap3 = CACHE( fullfile(pwd,'tname.mat'), 'v',0 );
assert( EXPECT_EQ( mmap3.var, 10 ) );
mmap3.remove();
clear mmap3

%% value name constructor
clear mmap1 mmap2 mmap3 mmap4 mmap5 mmap6

a1 = 10;
mmap1 = CACHE( a1, 'tname1', 'v',0 );
assert( EXPECT_EQ( mmap1.var, 10 ) );
clear mmap1

a1 = 20;
mmap2 = CACHE( a1, 'tname1' );
assert( EXPECT_EQ( mmap2.var, 10 ) );
clear mmap2

a2 = 20;
[ret,mmap3] = EXPECT_THROW( @() CACHE( a2, 'tname1' ), ':varname' );
assert( ret );
assert( EXPECT_EQ( mmap3.var, 10 ) );
mmap3.remove();
clear mmap3

a1 = 100;  %#ok<NASGU>
mmap4 = CACHE( 'filename','tname2', 'varname','a1', 'v',0 );
assert( EXPECT_EQ( mmap4.var, 100 ) );
mmap4.remove();
clear mmap4

a1 = 1000;
mmap5 = CACHE( a1, 'filename','tname3', 'varname','a2', 'v',0 );
assert( EXPECT_EQ( mmap5.var, 1000 ) );
mmap5.remove();
clear mmap5

mmap6 = CACHE( 11, 'tname4', 'v',0 );
assert( EXPECT_EQ( mmap6.var, 11 ) );
mmap6.remove();
clear mmap6


%% assign to variable
clear gs
a = 1; a1 = 2; a2 = 3; 
save( 'gs.mat', 'a','a1','a2' );
mmap = CACHE( 'gs', 'varname','a' );
assert( EXPECT_EQ( mmap.var, 1 ) );

mmap.var = 10;
st = load( 'gs.mat' );
assert( EXPECT_EQ( st.a, 10 ) );

mmap.remove();
assert( EXPECT_EQ( exist('gs.mat','file'), 0 ) );

%% private
clear mmap1 mmap2 mmap3 mmap4

a1 = 1000;
mmap1 = CACHE( a1, 'tname1', 'private', 'v',0 );
assert( EXPECT_EQ( a1, 1000 ) );
mmap1.var = 2000;
assert( EXPECT_EQ( mmap1.var, 2000 ) );
clear mmap1

a1 = 3000;
mmap2 = CACHE( a1, 'tname1', 'private' );
assert( EXPECT_EQ( mmap2.var, 1000 ) );
mmap2.opt.private = 0;
mmap2.var = 4000;
assert( EXPECT_EQ( mmap2.var, 4000 ) );
clear mmap2

a1 = 5000;
mmap3 = CACHE( a1, 'tname1', 'private' );
assert( EXPECT_EQ( mmap3.var, 4000 ) );
mmap3.var = 6000;
mmap3.savemat();
clear mmap3

a1 = 7000;
mmap4 = CACHE( a1, 'tname1' );
assert( EXPECT_EQ( mmap4.var, 6000 ) );
mmap4.remove();
clear mmap4

%% test macro
assert( EXPECT_THROW( @() EXPECT_EQ( CACHE('tt1','v',0), CACHE('tt2','v',0), 30), ':CACHE' ) );
CACHE('tt1').remove();
CACHE('tt2').remove();

assert( EXPECT_EQ( CACHE('tt1','v',0), 30 ) );
assert( EXPECT_EQ( CACHE('tt1','v',0), 30 ) );
CACHE(3,'tt1').remove();

assert( EXPECT_EQ( CACHE(3,'tt2','v',0), 3 ) );
assert( EXPECT_EQ( CACHE(4,'tt2','v',0), 3 ) );
CACHE(3,'tt2').setvar(4);
assert( EXPECT_EQ( CACHE(4,'tt2','v',0), 4 ) );
CACHE(3,'tt2').remove();


%% last test
%%% check whether there are temporary files which did not get deleted

numdir_cachefolder_after = dir();
clear cleanDir
p = pwd;
if( any(strfind(pwd,'cache')) );  % This seems to be a Matlab bug. onCleanup task is not called in Matlab
    cd ..;
    end;
numdir_basefolder_after = dir();

assert( EXPECT_EQ( numel(numdir_cachefolder_before), numel(numdir_cachefolder_after) ) ); 
assert( EXPECT_EQ( numel(numdir_basefolder_before), numel(numdir_basefolder_after) ) );

%% reset errflg
TTEST errflg false



