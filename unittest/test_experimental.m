% test_experimental

TTEST hardinit test_experimental

   
TESTCASE( 'TIMETESTS' );
    warning( 'off', 'TTEST:Maxtime' );

    if( ismatlab );
        SECTION( 'MINTIME' );
            EXPECT_MINTIME( @() secloop(10), 2 );
            for i = 1:10;
                val = randi(20) + 3;
                EXPECT_MINTIME( @() secloop(val), val-2 );
            end        
    else;
        TODO_FAIL( 'Mintime tests currently do not work on Octave.' ); end;
            
    SECTION( 'MAXTIME' );    
        EXPECT_MAXTIME( @() secloop(2), 8 );
        EXPECT_MAXTIME( '1', 2 );
        EXPECT_MAXTIME( @() pause(5), 1, OPTION('not'), OPTION('verbose',0) );
        EXPECT_MAXTIME( @() pause(2), 10 );
        EXPECT_MAXTIME( 'badcall', 0.00001, OPTION('not'), OPTION('verbose',0) );
        EXPECT_MAXTIME( @() terror, 2 );  % throwing errors after function started counts as successful            

%     SECTION( 'DEFINED' );
%         assert( ~EXPECT_DEFINED( @() ajsd ) );
%         assert( EXPECT_DEFINED( @() 2 ) );
        
    if( ismatlab );
        SECTION( 'MINTIME' );
            EXPECT_MINTIME( @() secloop(10), 2 );
            for i = 1:100
                val = randi(20) + 3;
                EXPECT_MINTIME( @() secloop(val), val-2 );
            end                
    else;
        TODO_FAIL( 'Mintime tests currently do not work on Octave.' ); end;   
        
    SECTION( 'Timetests' );
        EXPECT_MAXTIME( @() secloop(1), .1, OPTION('not'), OPTION('verbose',0) );
        
        [ret,a,b] = EXPECT_MAXTIME( @() secloop(1), .1, OPTION('not'), OPTION('verbose',0) );

ENDTESTCASE();
