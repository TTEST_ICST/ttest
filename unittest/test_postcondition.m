% test TTEST experimental

TTEST init ttest_id_postcondition
TTEST clear all

TESTCASE( 'postcondition' );
cd postcondition

% preconditions 

%% POSTCONDITION_handle
EXPECT_NTHROW( @() postcondition_handle_success1() );
EXPECT_NTHROW( @() postcondition_handle_success2() );
EXPECT_NTHROW( @() postcondition_handle_success3() );
EXPECT_NTHROW( @() postcondition_handle_success4() );
EXPECT_THAT( evalc('postcondition_handle_fail1()'), ttest.HasSubstr('Postcondition failed') );
EXPECT_THAT( evalc('postcondition_handle_fail2()'), ttest.HasSubstr('Postcondition failed') );
EXPECT_THAT( evalc('postcondition_handle_fail3()'), ttest.HasSubstr('Postcondition failed') );

%% POSTCONDITION_string
% XX not implemented yet, will probably never be implemented
% postcondition_string_success1()
% postcondition_string_success2()
% postcondition_string_success3()
% postcondition_string_fail1()
% postcondition_string_fail2()
% postcondition_string_fail3()

%%

ENDTESTCASE();
