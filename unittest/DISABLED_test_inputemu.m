% test inputemu

TTEST init inputemu

warning( 'This test must be executed by hand currently.' );

%% keys_success
if( false );
    layout = 'de-at';  %#ok<UNRCH>
    EXPECT_TRUE( inputemu_function( '1234567890', layout ) );
    EXPECT_TRUE( inputemu_function( '!"�$%&/()=', layout ) );
    EXPECT_TRUE( inputemu_function( 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ', layout ) );
    EXPECT_TRUE( inputemu_function( ',.-;:_', layout ) );
    EXPECT_TRUE( inputemu_function( '+=', layout ) );
    EXPECT_TRUE( inputemu_function( '<>*', layout ) );
end
%% keys_failing
if( false );
    layout = 'de-at';  %#ok<UNRCH>
    EXPECT_TRUE( inputemu_function( '�', layout ) );
    EXPECT_TRUE( inputemu_function( '^', layout ) );
    EXPECT_TRUE( inputemu_function( '^^', layout ) );
    EXPECT_TRUE( inputemu_function( 'a^', layout ) );
    EXPECT_TRUE( inputemu_function( '�?', layout ) );
    EXPECT_TRUE( inputemu_function( '������', layout ) );
    EXPECT_TRUE( inputemu_function( '''', layout ) );
    EXPECT_TRUE( inputemu_function( '#', layout ) );
    EXPECT_TRUE( inputemu_function( '@', layout ) );
end
%% keys_success
if( false );
    layout = 'en-us';
    EXPECT_TRUE( inputemu_function( '1234567890', layout ) );
    EXPECT_TRUE( inputemu_function( '"`-=', layout ) );
    EXPECT_TRUE( inputemu_function( '?~!@#$%^&*()_+', layout ) );
    EXPECT_TRUE( inputemu_function( 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ', layout ) );
    EXPECT_TRUE( inputemu_function( ',.-;:_''@', layout ) );
    EXPECT_TRUE( inputemu_function( '<>', layout ) );
    EXPECT_TRUE( inputemu_function( '^', layout ) );
    EXPECT_TRUE( inputemu_function( '^^', layout ) );
    EXPECT_TRUE( inputemu_function( ' a^', layout ) );
    EXPECT_TRUE( inputemu_function( ' |,./<>?:"|[]{};', layout ) );
    EXPECT_TRUE( inputemu_function( '\\', layout, '\' ) );
end

%% keys_failing
if( false );
    layout = 'en-us';  %#ok<UNRCH>
    EXPECT_TRUE( function_test_input( '�', layout ) );
    EXPECT_TRUE( function_test_input( '�', layout ) );
    EXPECT_TRUE( function_test_input( '�', layout ) );
    EXPECT_TRUE( function_test_input( '�+�����', layout ) );
end
