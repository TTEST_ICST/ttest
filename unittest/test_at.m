% test TTEST at

TTEST init ttest_id_at
TTEST clear all

oldpath = path;
addpath ./at
cleanpath = onCleanup( @() path(oldpath) );

% preconditions 

%% assignat
clearat all

assignat( '-in','assign_function', '-at',4, '-assign', 'in',2 );
in = assign_function( 0 );
EXPECT_EQ( in, 2 );
clearat in assign_function

assignat( '-in','assign_function', '-at','<F>', '-assign', 'in',0, 'd',0, 'e',0 );
[in,a,~,~,d,e] = assign_function( 0 );
EXPECT_EQ( 0, in, d, e );
EXPECT_EQ( a, 30 );
clearat in assign_function

EXPECT_THROW( @() assignat( '-in','assign_function', '-at','<F>', '-assign', '$',0 ), 'ttest_at:varname' ); %may also throw something else
clearat all
EXPECT_THROW( @() assignat( '-in','assign_function', '-at','<F>', '-assign', '',0 ), 'ttest_at:varname' ); %may also throw something else
clearat all
EXPECT_THROW( @() assignat( '-in','assign_function', '-at','<F>', '-assign', 'a' ), 'ttest_at:assign' ); %may also throw something else
clearat all


%% captureat
clearat all
EXPECT_THROW( @() captureat(), 'captureat:nothing' ); %nothing captured yet, thus it must throw

captureat( '--in', 'capture_function', '--at', '<La20>', '--capture', 'a' );
capture_function();
ws = captureat();
EXPECT_EQ( ws.a, 10 );
clearat all

captureat( '--in', 'capture_function', '--at', '<Lc50>', '--capture', 'a','b','c', '--group',0 );
capture_function();
ws = captureat();
EXPECT_PRED( @(x) numel(fieldnames(x(1)))>=3, ws );
clearat all;

captureat( '--in', 'capture_function', '--at', '<La20>', '--capture', 'a' );
captureat( '--in', 'capture_function', '--at', '<La30>', '--capture', 'a' );
capture_function();
ws = captureat();
EXPECT_EQ( ws.a, 20 );
clearat in capture_function;

captureat( '--in', 'capture_function', '--at', '<La20>', '--capture', 'a', '--group','La10' );
captureat( '--in', 'capture_function', '--at', '<La30>', '--capture', 'a', '--group','La20' );
capture_function()
ws = captureat();
EXPECT_EQ( ws.La10.a, 10 );
EXPECT_EQ( ws.La20.a, 20 );
clearat in capture_function;

captureat( '--in', 'capture_function', '--at', '<La20>', '--group','La10' );
captureat( '--in', 'capture_function', '--at', '<Lc50>' );
capture_function()
ws = captureat();
EXPECT_EQ( ws.La10.a, 10 );
EXPECT_EQ( ws.a, 30 );
EXPECT_EQ( ws.b, 40 );
clearat in capture_function;

captureat( '--in', 'capture_function', '--at', '<La10>',                  '--group','0' );
captureat( '--in', 'capture_function', '--at', '<La20>',                  '--group','0' );
captureat( '--in', 'capture_function', '--at', '<La30>', '--capture','0', '--group','0' );
captureat( '--in', 'capture_function', '--at', '<Lb40>', '--capture','0', '--group',0 );
captureat( '--in', 'capture_function', '--at', '<Lc50>', '--capture',0,   '--group',0 );
captureat( '--in', 'capture_function', '--at', '<Lend>', '--capture',0,   '--group',0 );
capture_function()
ws = captureat();
EXPECT_PRED( @(x) numel(x)>=6, ws );
EXPECT_EQ( ws(end).c, 50 );
clearat all

captureat( '--in', 'capture_function', '--at', '<Lb40>', '--capture', 'b');
capture_function();
ws = captureat();
EXPECT_EQ( ws.b, 'TTEST_UNDEFINED' );
clearat in capture_function;

EXPECT_THROW( @() captureat(), 'captureat:nothing' ); %test this again, since something was captured in the meantime
clearat all


%% evalat
clearat all

evalat( '-in','assign_function', '-eval','' );
EXPECT_NTHROW( @() assign_function() );
clearat in assign_function

evalat( '-in','assign_function', '-eval','assert(true)' );
assign_function();
clearat in assign_function

evalat( '-in','assign_function', '-eval','assert(false,''ABC-MESSAGE'')' );
EXPECT_STRCONTAINS( @() assign_function(), 'ABC-MESSAGE' );
clearat in assign_function

evalat( '-in','assign_function', '-eval','fprintf(''ABC-MESSAGE'')' );
EXPECT_STRCONTAINS( @() assign_function(), 'ABC-MESSAGE' );
clearat all

evalat( '-in','assign_function', '-eval','wrongfunctioncall_asdjkals89190258' );
EXPECT_THROW( @() assign_function(), ':UndefinedFunction' );
clearat all

evalat( '-in','assign_function', '-eval','§' );
if( ismatlab );
    EXPECT_THROW( @() assign_function(), ':m_illegal_character' );
else
    EXPECT_THROW( @() assign_function(), 'invalid character' );
end;
clearat

evalat( '-in','assign_function', '-eval', 'd=2', 'e=2' );
[in,a,b,c,d,e] = assign_function(2);
EXPECT_EQ(in,d,e,2);
clearat in assign_function;

EXPECT_THROW( @() evalat( '-in','assign_function', '-eval',2 ), 'ttest_at:eval' );

evalat( '-in','assign_function', '-eval', @() fprintf('22222222222\n') );
EXPECT_STRCONTAINS( @() assign_function, '22222222222' );

%% flowat
clearat all

TTEST v 0
verboseClean = onCleanup( @() TTEST(ttest_id,'v',1) );

flowat( 'in','flowat_function', 'at','<START>', 'type','expect' );
flowat( 'in','flowat_function', 'at','<1a>', 'type','failure' );
flowat_function( 0 );
fl = flowat();
EXPECT_FLOW( fl );
clearat all

flowat( 'in','flowat_function', 'at','<1b>', 'type','expect' );
flowat_function( 1 );
assert( EXPECT_FLOW( flowat() ) );
assert( EXPECT_THROW( @() flowat(), 'flowat:nothing' ) );


flowat( 'in','flowat_function', 'at','<2b>', 'type','expect' );
assert( ~EXPECT_FLOW( 'flowat_function(1)' ) );
clearat all

flowat( 'in','flowat_function', 'at','<2b>', 'type','expect' );
assert( ~EXPECT_FLOW( @() flowat_function(1) ) );
clearat all

clearat all
flowat( 'in','flowat_function', 'at','<0a>', 'type','abort' );
flowat( 'in','flowat_function', 'at','<END>', 'type','expect' );
assert( ~EXPECT_FLOW( @() flowat_function(1) ) );  % function aborts before return value is set, thus test fails
clearat all

%% inputat
clearat all

inputat( 'in','input','ABC','type','temporal' );
[str,flag] = input( '', 's', 'debug' );
EXPECT_TRUE( flag );
EXPECT_EQ( str, 'ABC' );

inputat( 'in','input','ABC' );
[str,flag] = input( '', 's', 'debug' );
EXPECT_TRUE( flag );
EXPECT_EQ( str, 'ABC' );

inputat( 'in','input_function', 'at','<0>', 'input','ABC' );
[ret,flag] = input_function( 1 );
EXPECT_TRUE( flag );
EXPECT_EQ( ret, 'ABC' );

inputat( 'in','input_function', 'at','<0>', 'input','ABC', 'type','spatial' );
[ret,flag] = input_function( 1 );
EXPECT_TRUE( flag );
EXPECT_EQ( ret, 'ABC' );

inputat( 'in','input_function', 'at','<0>', 'input','2+2', 'type','spatial' );
[ret,flag] = input_function(2);
EXPECT_TRUE( flag );
EXPECT_EQ( ret, 4 );
clearat all

inputat( 'in','input_function', 'at','<1>', 'input','2+2', 'type','temporal' );
inputat( 'in','input_function', 'at','<2>', 'input','2+2', 'type','temporal' );
[ret,flag] = input_function( 1 );
EXPECT_TRUE( flag );
EXPECT_EQ( ret, '2+2' );
[ret,flag] = input_function( 2 );
EXPECT_TRUE( flag );
EXPECT_EQ( ret, 4 );
clearat all

inputat( 'in','input_function', 'at','<0>', 'input','ABC', 'type','temporal' );
[ret,flag] = input_function(3);
EXPECT_TRUE( flag );
EXPECT_EQ( ret, 'ABC' );
clearat all

inputat( 'in','input_function', 'at','<0>', 'input','ABC', 'type','temporal' );
inputat( 'in','input_function', 'at','<00>', 'input','DEF', 'type','spatial' );
[ret,flag] = input_function( 4 );
EXPECT_TRUE( flag );
EXPECT_EQ( ret, 'ABCDEF' );
clearat all

inputat( 'in','input_function', 'at','<0>', 'input','1', 'type','spatial' );
inputat( 'in','input_function', 'at','<00>', 'input','2', 'type','spatial' );
[ret,flag] = input_function(4);
EXPECT_FALSE( flag );
EXPECT_EQ( ret, '1' );
clearat all

EXPECT_THROW( @() inputat( 'in','input_function', 'at','<0>', 'input','ABC', 'type','temporal','temporal' ), 'inputat:type' );

%% multiat
clearat all

assignat( '-in','assign_function', '-at','<F>', '-assign', 'in',0 );
assignat( '-in','assign_function', '-at','<F>', '-assign', 'a',0 );
assignat( '-in','assign_function', '-at','<F>', '-assign', 'b',0 );
assignat( '-in','assign_function', '-at','<F>', '-assign', 'c',0 );
assignat( '-in','assign_function', '-at','<F>', '-assign', 'd',0 );
assignat( '-in','assign_function', '-at','<F>', '-assign', 'e',0 );
[in,a,b,c,d,e] = assign_function( 1 ); 
EXPECT_EQ( 0, in, a, b, c, d, e );
clearat all

assignat( '-in','assign_function', '-at','0', '-assign', 'in',0 );

%% set_breakpoint

% XX tests missing
%%% in case of unconditional breakpoint is there

%%% in case of breakpoint for anonymous function is there



