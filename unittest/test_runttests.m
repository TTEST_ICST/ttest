% test runttests

TTEST init
TTEST clear all

olddir1 = cd( 'runttests' );
cleandir1 = onCleanup( @() cd(olddir1) );
if( ismatlab );
	allfunctionhandle( 'runttests', 'assign', 'v',0 );
end;

% preconditions

%% freename
if( ismatlab );
    EXPECT_EQ( freename( 'freename', 'free', 1 ), 'free10' );
    EXPECT_EQ( freename( 'freename', 'free', 2 ), 'free10' );
    EXPECT_EQ( freename( 'freename', 'newfile', 2 ), 'newfile01' );
end;

%% makefilelist

%% maketestlist

%% no_options
olderrflag = TTEST( 'errflag' );
TTEST clear all
TTEST( 'errflag',olderrflag );
runttests('v',0)
EXPECT_EQ( TTEST('ttest_id_test2', 'var', 'test2', nan), 'test2' );
EXPECT_EQ( TTEST('ttest_id_doctest_unittest','var','doctest_unittest',nan), 'doctest_unittest' );
EXPECT_EQ( TTEST('ttest_id_doctest_doctest','var','doctest_doctest',nan), [] );

%% doctest
olderrflag = TTEST( 'errflag' );
TTEST clear all
TTEST( 'errflag',olderrflag );
runttests('doctest','v',0)
EXPECT_EQ( TTEST('ttest_id_test2_doctest', 'var', 'test2_doctest', nan), 'test2_doctest' ); 
EXPECT_EQ( TTEST('ttest_id_func', 'var', 'func', nan), 'func' );
EXPECT_EQ( TTEST('ttest_id_doctest_unittest','var','doctest_unittest',nan), [] );
EXPECT_EQ( TTEST('ttest_id_doctest_doctest','var','doctest_doctest',nan), 'doctest_doctest' );

%% only_one_doctest
olderrflag = TTEST( 'errflag' );
TTEST clear all
TTEST( 'errflag',olderrflag );
runttests( 'doctest', 'func','v',0 );
EXPECT_EQ( TTEST('ttest_id_func', 'var', 'func', nan), 'func' );
EXPECT_EQ( TTEST('ttest_id_doctest_unittest','var','doctest_unittest',nan), [] );
EXPECT_EQ( TTEST('ttest_id_doctest_doctest','var','doctest_doctest',nan), [] );

%% only_one_unittest
olderrflag = TTEST( 'errflag' );
TTEST clear all
TTEST( 'errflag',olderrflag );
runttests( 'unittest', 'func2', 'v',0 );
EXPECT_EQ( TTEST('ttest_id_func2', 'var', 'func2', nan), 'func2' );
EXPECT_EQ( TTEST('ttest_id_test2', 'var', 'test2', nan), [] );

%% recursive
olderrflag = TTEST( 'errflag' );
TTEST clear all
TTEST( 'errflag',olderrflag ); 
runttests( 'recursive', 'v',1 );
EXPECT_EQ( TTEST('ttest_id_test2', 'var', 'test2', nan), 'test2' );
EXPECT_EQ( TTEST('ttest_id_doctest_unittest','var','doctest_unittest',nan), 'doctest_unittest' );
EXPECT_EQ( TTEST('ttest_id_doctest_doctest','var','doctest_doctest',nan), [] );
EXPECT_EQ( TTEST('ttest_id_recursive_unittest','var','recursive_unittest',nan), 'recursive_unittest' );
EXPECT_EQ( TTEST('ttest_id_recursivetest_doctest','var','recursivetest_doctest',nan), [] );
EXPECT_EQ( TTEST('ttest_id_func_subfolder_unittest','var','func_subfolder_unittest',nan), [] );
EXPECT_EQ( TTEST('ttest_id_func_subfolder_doctest','var','func_subfolder_doctest',nan), [] );

%% recursive_doctest
olderrflag = TTEST( 'errflag' );
TTEST clear all
TTEST( 'errflag',olderrflag );
runttests( 'recursive', 'doctest', 'v',0 );
EXPECT_EQ( TTEST('ttest_id_test2_doctest', 'var', 'test2_doctest', nan), 'test2_doctest' );
EXPECT_EQ( TTEST('ttest_id_func', 'var', 'func', nan), 'func' );
EXPECT_EQ( TTEST('ttest_id_doctest_unittest','var','doctest_unittest',nan), [] );
EXPECT_EQ( TTEST('ttest_id_doctest_doctest','var','doctest_doctest',nan), 'doctest_doctest' );
EXPECT_EQ( TTEST('ttest_id_recursivetest_unittest','var','recursivetest_unittest',nan), [] );
EXPECT_EQ( TTEST('ttest_id_recursive_doctest','var','recursive_doctest',nan), 'recursive_doctest' );
EXPECT_EQ( TTEST('ttest_id_func_subfolder_unittest','var','func_subfolder_unittest',nan), [] );
EXPECT_EQ( TTEST('ttest_id_func_subfolder_doctest','var','func_subfolder_doctest',nan), 'func_subfolder_doctest' );

%%
EXPECT_THAT( pwd, ttest.HasSubstr('runttests') );

%% empty folder
olddir2 = cd('runttests_empty');
cleandir2 = onCleanup( @() cd(olddir2) );
EXPECT_THROW( @() runttests('v',0), 'runttests:notests' );
clear

%% function based test
olderrflag = TTEST( 'errflag' );
TTEST clear all
TTEST( 'errflag',olderrflag ); 
runttests( 'functionbasedtest' )
EXPECT_EQ( TTEST('ttest_id_functionbasedtest_test1', 'var', 'functionbasedtest_test1', nan), 'functionbasedtest_test1' );
EXPECT_EQ( TTEST('ttest_id_functionbasedtest_test2', 'var', 'functionbasedtest_test2', nan), 'functionbasedtest_test2' );

%%
clear cleandir1 cleandir2