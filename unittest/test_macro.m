% test macro

TTEST init
TTEST v 0

%check if we are in the right directiory
a = which( 'test_macro' );
assert( numel(a)>=14 && strcmpi(a(1:end-13),pwd), 'test_macro', 'This function must be called from the ./unittest folder in the TTEST folder.' );
% preconditions 

%% FAIL
assert( ~EXPECT_FAIL );
assert( ~EXPECT_FAIL, 'text' );

%% SUCCEED
assert( EXPECT_SUCCEED );
assert( ASSERT_SUCCEED );

%% TRUE
assert( EXPECT_TRUE(1) );
assert( EXPECT_TRUE(2) );
assert( ~EXPECT_TRUE(0) );
assert( ~EXPECT_TRUE(1,0,1) );
assert( ASSERT_TRUE(1) );
assert( ASSERT_TRUE(2) );
assert( EXPECT_THROW( @() ~ASSERT_TRUE(0), 'TTEST:NotTrue' ) );
assert( EXPECT_THROW( 'EXPECT_TRUE(@sum);' , 'Unable to prove' ) );
assert( TODO_THROW( 'ASSERT_TRUE(@sum);', 'Unable to prove', 'ASSERT_TRUE.m' ) );
%assert( EXPECT_TRUE('a_',PARAM,type(1,'numeric','symvpa')) );
%assert( ~EXPECT_TRUE('a_',PARAM,type(0,'numeric')) );
assert( ~EXPECT_TRUE([0 1]) );
assert( EXPECT_THROW( @() ASSERT_TRUE([0 1]), 'TTEST:noScalar' ) );



%% FALSE
assert( EXPECT_FALSE(0) );
assert( ~EXPECT_FALSE(0,1,0) );
assert( ~EXPECT_FALSE(1) );
assert( ASSERT_FALSE(0) );
assert( EXPECT_THROW( @() ~ASSERT_FALSE(1), 'TTEST:NotFalse' ) );

%% TRUTHY
assert( EXPECT_TRUTHY( [1 2 3] ) );
assert( ~EXPECT_TRUTHY( [] ) );
assert( ~EXPECT_TRUTHY( [0 2 3] ) );
if( ismatlab() )
    assert( EXPECT_THROW( @() EXPECT_TRUTHY(nan), 'MATLAB:nologicalnan' ) );
    assert( EXPECT_THROW( @() EXPECT_TRUTHY({}), 'MATLAB:invalidConversion' ) );
else;

end;
    
assert( EXPECT_TRUTHY( inf ) );

%% FALSY
assert( EXPECT_FALSY( [0 0 0] ) );
assert( EXPECT_FALSY( [] ) );
assert( EXPECT_FALSY( [1 0 0] ) );
assert( ~EXPECT_FALSY( [1 2] ) );
EXPECT_THROW( @() EXPECT_FALSY(@sum), 'sym:isAlways' );

%% PRED
assert( EXPECT_PRED( @isempty, [] ) );
assert( EXPECT_PRED( @isempty, {} ) );
assert( ~EXPECT_PRED( @isempty, 1 ) );
assert( EXPECT_PRED( @(a,b) a<b, 1, 2) );
assert( ~EXPECT_PRED( @(a,b) a<b, 2, 1) );
assert( EXPECT_PRED( @(a,b,c) a+b+c==0, 1, 2, -3 ) );
assert( ~EXPECT_PRED( @(x) x>=0 && x<=2, 3 ) );

%% NPRED
assert( ~EXPECT_NPRED( @isempty, [] ) );
assert( ~EXPECT_NPRED( @isempty, {} ) );
assert( EXPECT_NPRED( @isempty, 1 ) );
assert( ~EXPECT_NPRED( @(a,b) a<b, 1,2 ) );
assert( ~EXPECT_NPRED( @(a,b,c) a+b+c==0, 1,2,-3 ) );

%% EQ
assert( ~EXPECT_EQ( 97, 'a' ) );
assert( EXPECT_EQ( [2 3], [2 3] ) );
assert( ~EXPECT_EQ( 2, 3 ) );
assert( ~EXPECT_EQ([],{}) );
assert( EXPECT_EQ( [], [] ) );
assert( EXPECT_EQ( [], [], [] ) );
assert( EXPECT_EQ( 2 ) );
assert( EXPECT_EQ( 2, 2 ) );
assert( EXPECT_EQ( @sum, @sum ) );
assert( ~EXPECT_EQ( @sum,@prod) );
assert( ~EXPECT_EQ(2,3) );
assert( ~EXPECT_EQ( @sum, struct ) );
assert( EXPECT_NTHROW( @() EXPECT_EQ() ) );
assert( ~EXPECT_EQ( nan, nan ) );
assert( EXPECT_EQ( nan ) );
%x = [1:100];
%assert( EXPECT_EQ( @(f) f(x), @(g) g(x), PARAM,value(@(x)2*x),value(@(y)y+y)) );
%a = 0;
%assert( EXPECT_EQ( @(y35) y35, @(x) x, @(x,y35) y35+a, PARAM,'sequence',{2,3},{2,3}) );
%if( exist('assign.m','file')==2 );
%    assert( EXPECT_EQ( 'a','assign(''a'',a_)', PARAM,'sequence',{2,3,4} ) ); end;
%assert( EXPECT_EQ( 'a_','@sum',PARAM,value(@sum)) );
%assert( EXPECT_EQ( @(x)x,@()@sum,PARAM,value(@sum)) );
assert( ~EXPECT_EQ( 1, [] ) );

%% NE
assert( ~EXPECT_NE(@sum,@sum) );
assert( EXPECT_NE(@sum,@prod) );
assert( EXPECT_NE(2) );
assert( ~EXPECT_NE(2,2) );
assert( EXPECT_NE(2,3) );
assert( ~EXPECT_NE(2,3,2) );
assert( EXPECT_NE( nan, nan ) );
assert( EXPECT_NE( nan ) );
assert( EXPECT_NE( 1, [] ) );

%% LE
assert( EXPECT_LE(2) );
assert( EXPECT_LE(2,2) );
assert( EXPECT_LE(2,2,2,2) );
assert( EXPECT_LE(2,3,4,5) );
assert( EXPECT_LE(2,2,3,4,10) );
assert( ~EXPECT_LE(2,2,3,4,10,9) );
assert( EXPECT_LE(2,3) );
assert( ~EXPECT_LE(3,2) );
assert( ~EXPECT_LE(3,2,3) );
if( ismatlab() );
    assert( EXPECT_THROW( @() EXPECT_LE( 1,2,3,@sum ), 'ttest:property', ':UndefinedFunction' ) );
    assert( EXPECT_THROW( @() EXPECT_LE( @sum, @abs ), 'ttest:property', ':UndefinedFunction' ) );
    assert( EXPECT_THROW( @() ASSERT_LE( @sum, @abs ), 'ttest:property', ':UndefinedFunction' ) );
end;

%% LT
assert( EXPECT_LT(2) );
assert( ~EXPECT_LT(2,2) );
assert( EXPECT_LT(2,3) );
assert( ~EXPECT_LT(3,2) );
assert( ~EXPECT_LT(2,2) );
assert( EXPECT_LT(2,3) );
assert( EXPECT_LT(2,3,4,5) );
assert( ~EXPECT_LT(3,2) );
assert( ~EXPECT_LT(3,2,3) );

%% GE
assert( EXPECT_GE(2,2) );
assert( EXPECT_GE(2,2,1,0) );							 
assert( ~EXPECT_GE(2,3) );
assert( EXPECT_GE(3,2) );
assert( ~EXPECT_GE(2,2,1,2) );

%% GT
assert( ~EXPECT_GT(2,2) );
assert( ~EXPECT_GT(2,3) );
assert( EXPECT_GT(3,2) );
assert( ~EXPECT_GT(2,2,3) );
assert( ~EXPECT_GT(2,3,2) );
assert( EXPECT_GT(3,2,1) );	

%% ALMOST_EQ
assert( EXPECT_ALMOST_EQ( single(1), 1+eps(single(1)) ) );
assert( EXPECT_ALMOST_EQ( single(1), 1+eps(single(1)) ) );
assert( EXPECT_ALMOST_EQ( single(1), 1+eps(single(1)) ) );
assert( ~EXPECT_ALMOST_EQ( single(1), 1+5*eps(single(1)) ) );
assert( ~EXPECT_ALMOST_EQ( @sum ) );
assert( EXPECT_ALMOST_EQ( 1, 1+eps ) );
assert( EXPECT_ALMOST_EQ( 1, 1+4*eps ) );
assert( ~EXPECT_ALMOST_EQ( 1, 1+5*eps ) );
assert( ~EXPECT_ALMOST_EQ( 1, 1+2*eps, 1+5*eps ) );
assert( EXPECT_ALMOST_EQ( 1, 1+eps, 1+2*eps ) );
assert( ~EXPECT_ALMOST_EQ( 1, [] ) );

%% ALMOST_NE
assert( ~EXPECT_ALMOST_NE( single(1), 1+eps(single(1)) ) );
assert( ~EXPECT_ALMOST_NE( single(1), 1+eps(single(1)) ) );
assert( ~EXPECT_ALMOST_NE( single(1), 1+eps(single(1)) ) );
assert( EXPECT_ALMOST_NE( single(1), 1+5*eps(single(1)) ) );
assert( ~EXPECT_ALMOST_NE( @sum ) );
assert( ~EXPECT_ALMOST_NE( 1, 1+eps ) );
assert( ~EXPECT_ALMOST_NE( 1, 1+4*eps ) );
assert( EXPECT_ALMOST_NE( 1, 1+5*eps ) );
assert( ~EXPECT_ALMOST_NE( 1, 1+2*eps, 1+5*eps ) );
assert( ~EXPECT_ALMOST_NE( 1, 1+eps, 1+2*eps ) );
assert( ~EXPECT_ALMOST_NE( 1, [] ) );

%% DOUBLE_EQ
assert( ~EXPECT_DOUBLE_EQ( @sum ) );
assert( EXPECT_DOUBLE_EQ( 1, 1+eps ) );
assert( EXPECT_DOUBLE_EQ( 1, 1+4*eps ) );
assert( ~EXPECT_DOUBLE_EQ( 1, 1+5*eps ) );
assert( ~EXPECT_DOUBLE_EQ( 1, 1+2*eps, 1+5*eps ) );
assert( EXPECT_DOUBLE_EQ( 1, 1+eps, 1+2*eps ) );

%% SINGLE_EQ
assert( ~EXPECT_SINGLE_EQ( @sum ) );
assert( EXPECT_SINGLE_EQ( single(1), 1+eps(single(1)) ) );
assert( EXPECT_SINGLE_EQ( single(1), 1+eps(single(1)) ) );
assert( EXPECT_FLOAT_EQ( single(1), 1+eps(single(1)) ) );
assert( ~EXPECT_FLOAT_EQ( single(1), 1+5*eps(single(1)) ) );

%% NEAR
assert( EXPECT_NEAR( 100, 1.1 ) );
assert( EXPECT_NEAR( 1, 2, 1.1 ) );
assert( ~EXPECT_NEAR( 1, 2, 0.9 ) );
assert( ~EXPECT_NEAR( 1, 2, 3, 1.5 ) );
assert( EXPECT_NEAR( inf, inf, 0 ) );

%% NNEAR
assert( EXPECT_NNEAR( 100, 1.1 ) );
assert( ~EXPECT_NNEAR( 1, 2, 1.1 ) );
assert( EXPECT_NNEAR( 1, 2, 0.9 ) );
assert( ~EXPECT_NNEAR( 1, 2, 3, 1.5 ) );
assert( ~EXPECT_NNEAR( 1, 1.1, 2, 1.5 ) );


%% RANGE
assert( EXPECT_RANGE( [0 3] ) );
assert( EXPECT_RANGE( 2, [0 3] ) );
assert( EXPECT_RANGE( 3, [0 3] ) );
assert( ~EXPECT_RANGE( 2, [3 5] ) );
assert( ~EXPECT_RANGE( 4, 6, [3 5] ) );

%% STREQ
EXPECT_THROW( @() EXPECT_STREQ('a5',@sum), 'MATLAB:minrhs' );
assert( ~EXPECT_STREQ(1,1) );
assert( ~EXPECT_STREQ(1) );
assert( EXPECT_STREQ('23') );
assert( EXPECT_STREQ('23','23','23') );
assert( EXPECT_STREQ('ab','ab') );
assert( ~EXPECT_STREQ('ab','aB') );
assert( ~EXPECT_STREQ('ab','ac') );
assert( ~EXPECT_STREQ('23','') );
assert( ~EXPECT_STREQ([],'') );
assert( ~EXPECT_STREQ(97,'a') );
assert( EXPECT_STREQ('','') );

%% STRNE
assert( ~EXPECT_STRNE(1) );
assert( ~EXPECT_STRNE(1,2) );
assert( EXPECT_STRNE('23') );
assert( ~EXPECT_STRNE('23','23') );
assert( ~EXPECT_STRNE('ab','ab') );
assert( EXPECT_STRNE('ab','aB') );
assert( ~EXPECT_STRNE('AAA','BBB','AAA') );
assert( EXPECT_STRNE('AAA','BBB','aaa') );
assert( EXPECT_STRNE('ab','ac') );
assert( EXPECT_STRNE('23','') );
assert( ~EXPECT_STRNE('23','23','23') );
assert( ~EXPECT_STRNE('','') );

%% STRCASEEQ
assert( ~EXPECT_STRCASEEQ(1) );
assert( ~EXPECT_STRCASEEQ(1,2) );
assert( EXPECT_STRCASEEQ('23') );
assert( EXPECT_STRCASEEQ('23','23') );
assert( EXPECT_STRCASEEQ('23','23','23') );
assert( EXPECT_STRCASEEQ('ab','ab') );
assert( EXPECT_STRCASEEQ('ab','aB') );
assert( ~EXPECT_STRCASEEQ('ab','ac') );
assert( ~EXPECT_STRCASEEQ('23','') );
assert( EXPECT_STRCASEEQ('','') );

%% STRCASENE
assert( ~EXPECT_STRCASENE(1) );
assert( ~EXPECT_STRCASENE(1,2) );
assert( EXPECT_STRCASENE('23') );
assert( ~EXPECT_STRCASENE('23','23') );
assert( ~EXPECT_STRCASENE('23','23','23') );
assert( ~EXPECT_STRCASENE('ab','ab') );
assert( ~EXPECT_STRCASENE('ab','aB') );
assert( ~EXPECT_STRCASENE('AAA','BBB','AAA') );
assert( ~EXPECT_STRCASENE('AAA','BBB','aaa') );
assert( EXPECT_STRCASENE('ab','ac') );
assert( EXPECT_STRCASENE('23','') );
assert( ~EXPECT_STRCASENE('','') );

%% STRLEEQ
str_n = sprintf( '\n' );  %#ok<SPRINTFN>
str_rn = sprintf( '\r\n' );
assert( EXPECT_STRLEEQ( str_n, str_rn ) );
assert( ~EXPECT_STRLEEQ( str_n, [ str_rn ' '] ) );

%% STRLENE
str_n = sprintf( '\n' );  %#ok<SPRINTFN>
str_rn = sprintf( '\r\n' );
assert( ~EXPECT_STRLENE( str_n, str_rn ) );
assert( EXPECT_STRLENE( str_n, [ str_rn ' '] ) );


%% FILE_EQ
assert( EXPECT_FILE_EQ( 'EXPECT_EQ.m' ) );
assert( EXPECT_FILE_EQ( 'test_macro.m' ) );
assert( EXPECT_FILE_EQ( 'test_macro.m', 'test_macro.m' ) );
assert( ~EXPECT_FILE_EQ( 'filamskamgkag.m' ) );
assert( ~EXPECT_FILE_EQ( 'EXPECT_EQ.m', 'ASSERT_EQ.m' ) );

%% SUBSET
assert( EXPECT_SUBSET( 1 ) );
assert( EXPECT_SUBSET( 1, 1 ) );
assert( ~EXPECT_SUBSET( [1 2], [1 2 3], [0 1 2 3 4], [2 3 4 5] ) );
assert( EXPECT_SUBSET( [1 2], [1 2 3], [0 1 2 3 4] ) );
assert( ~EXPECT_SUBSET( {1 2}, {1 2 3}, {0 1 2 3 4}, {2 3 4 5} ) );
assert( EXPECT_SUBSET( {1 2}, {1 2 3}, {0 1 2 3 4} ) );

%% SUPERSET
assert( ~EXPECT_SUPERSET( [1 2 3 4], [1 2 3 5] ) );
assert( EXPECT_SUPERSET( [1 2], [1 ], [1] ) );
assert( EXPECT_SUPERSET( {[1] [2] [1 2] [1;2]}, {[1],[2]}) );
assert( ~EXPECT_SUPERSET( {[1;2]},{[1 2]} ) );

%% NUMEL
assert( EXPECT_NUMEL( 4 ) );
assert( EXPECT_PRODOFSIZE( 4 ) );
assert( ~EXPECT_NUMEL( [1 2 3], [3 4 5], 4 ) );
assert( EXPECT_NUMEL( [1 2 3], [3 4 5], 3 ) );

%% ISEMPTY
assert( EXPECT_ISEMPTY( [] ) );
assert( EXPECT_ISEMPTY( ) );
assert( ~EXPECT_ISEMPTY( [1] ) );

%% LENGTH
assert( EXPECT_LENGTH( randn(10,20), 20 ) );
assert( ~EXPECT_LENGTH( randn(10,20), 10 ) );

%% SIZE
assert( EXPECT_SIZE( randn(10,1), 10 ) );
assert( EXPECT_SIZE( randn(10,20), [10 20] ) );
assert( ~EXPECT_SIZE( randn(10,20), [10 10] ) );
assert( ~EXPECT_SIZE( randn(10,20), 10 ) );

%% FIELD
s.f1 = 'a';
assert( EXPECT_FIELD( s ) );
assert( EXPECT_FIELD( s, 'f1' ) );
assert( ~EXPECT_FIELD( s, 'f2' ) );
assert( ~EXPECT_FIELD( 10, 'f2' ) );

%% SCALAR
assert( EXPECT_SCALAR( 10, 20 ) );
assert( EXPECT_SCALAR( @sum) );
assert( ~EXPECT_SCALAR( 2, [10 10] ) );
assert( EXPECT_SCALAR(  ) );

%% EMPTY
assert( EXPECT_EMPTY( [], '', {} ) );
assert( ~EXPECT_EMPTY( 2 ) );
assert( ~EXPECT_EMPTY( @sum ) );

%% ISA
assert( EXPECT_ISA( cell(0), cell(1), 'cell' ) );
assert( EXPECT_ISA( 2, 'double' ) );
assert( EXPECT_CLASS( 2, 'double' ) );

%% THROW_NTHROW

oldpath = path;
cleanPath = onCleanup( @() oldpath(oldpath) );
for i = 1:2;
    if( i==1 ); 
        % first run with TTEST-warning
        % use evalc to surpress output
        evalc( 'addpath( ''../overload/warning/'' )' );  % relative location of folder where TTEST-warning is located
    else; 
        %second run with Matlab-warning
        evalc( 'rmpath( ''../overload/warning/'' )' ); end; 
    MESSAGE( 'i = %i (i.e. %s warning is used)\n', i, ttest.tifh(i==1,@()'TTEST',@()'Matlab') );
    assert( EXPECT_NTHROW( '0;' ) );
    assert( ~EXPECT_THROW( '0;', 'abc' ) );
    assert( EXPECT_THROW( 'a = inv( 0 );', ':singularMatrix' ) );  % this sets a=inf
    assert( EXPECT_NTHROW( 'isequal( a, inf );' ) );
    assert( ~EXPECT_THROW( 'inv( 0 );', 'asd:asd' ) );
    assert( TODO_THROW( 'inv( 0 );', 'asd:asd', ':singularMatrix' ) );
    assert( EXPECT_THROW( 'inv( 0 )', ':singularMatrix' ) );
    assert( EXPECT_THROW( @() inv(0), ':singularMatrix' ) );

    if( ismatlab() )
        a = 0;
        assert( ~EXPECT_THROW( 'a = inv( 0 );', 'XX' ) );
        assert( a==inf );
    end;
        
    a = 0;
    assert( ~EXPECT_THROW( @() inv(a) ) );

    assert( ~EXPECT_THROW( 'thisisnotavalidcomand', 'wrong_id' ) );
    assert( ~EXPECT_THROW( '0;', 'XX' ) );
    assert( EXPECT_THROW( 'adsasfasdasda ajkdwaklsd;', ':UndefinedFunction' ) );

    assert( EXPECT_THROW( 'adsasfasdasda ajkdwaklsd;', 'asd:asd', ':UndefinedFunction') );
    assert( EXPECT_THROW( @() error('throw'), 'ttest_msgthrow','Too many' ) );
%    assert( EXPECT_THROW( @(x) [x '(terror(''id:err''))'], @() 'id:err', PARAM, values('sum','plus') ) );
%    assert( EXPECT_THROW( '@() x(terror(''id:err''))', '''id:err''', PARAM, values(@sum,@plus) ) );
%    assert( EXPECT_THROW( '''a_(terror(''''id:err''''))''', '''id:err''', PARAM, values('sum','plus') ) );

%    assert( EXPECT_THROW( @(x) @() x(terror('id:err')), @() 'id:err', PARAM, values(@sum,@plus) ) );
%    assert( EXPECT_THROW( @(x) [x '(terror(''id:err''))'], @() 'id:err', PARAM, values('sum','plus') ) );
%    assert( EXPECT_THROW( '@() x(terror(''id:err''))', '''id:err''', PARAM, values(@sum,@plus) ) );
%    assert( EXPECT_THROW( '''a_(terror(''''id:err''''))''', '''id:err''', PARAM, values('sum','plus') ) );

    assert( EXPECT_NTHROW( '' ) );
    assert( EXPECT_NTHROW( ';' ) );
    assert( EXPECT_NTHROW( 'a = 1; a = 2; ' ) );
    
    assert( EXPECT_EQ( a, 2 ) );
    assert( EXPECT_NTHROW( '2;' ) );
    
    
    assert( ~EXPECT_NTHROW( 'adsasfasf a asdkasd;' ) );
    assert( EXPECT_NTHROW( 'adsasfasf a asdkasd;', 'asd:asd', 'MATLAB:UndefinedFunction', 'Octave:undefined-function' ) );

    assert( EXPECT_NTHROW( @() error('test:err','some msg'), 'test:err', 'test:err2' ) );
    assert( EXPECT_NTHROW( @() error('test:err','some msg'), 'test:err', 'test:err2', [0 1 410:450] ) );  % reported line number here may differ from Matlab version to Matlab version
    assert( ~EXPECT_NTHROW( @() error('test:err','some msg'), 'test:err', -1 ) );  % line number -1 is never contained
    assert( EXPECT_NTHROW( @() error('test:err','some msg'), 'test_macro.m' ) );
    assert( EXPECT_NTHROW( DISABLED('onoctave'), @() error('some msg'), '@()error(''some msg'')', 'test_macro' ) );
    assert( ~EXPECT_NTHROW( @() error('test:err','some msg'), 'test:err2', 'testTTEST' ) );
    assert( ~EXPECT_NTHROW( @() error('test:err','some msg') ) );
    assert( ~EXPECT_NTHROW( @() error('test:err','some msg') ) );

    evalc( 'warning( ''test:wrn1'', ''test:wrn1'' );' );  % surpress output
    assert( ~EXPECT_THROW( @() {}, 'test:wrn1' ) );
    assert( EXPECT_NTHROW( @() {1 2}, 'test:wrn1' ) );
    if( i==1 );
        % TTEST-warning
        assert( ~EXPECT_NTHROW( @() {warning('test:id1','some msg'), warning('test:id2','some msg')}, 'test:id1' ) );
        assert( ~EXPECT_NTHROW( @() {warning('test:id1','some msg'), warning('test:id2','some msg')}, 'test:id1', 'EXPECT_NTHROW' ) );
        assert( ~EXPECT_NTHROW( @() {warning('test:id1','some msg'), warning('test:id2','some msg')}, 'test:id1', 'FALSE_FILE' ) );
        assert( ~EXPECT_NTHROW( @() {warning('test:id1','some msg'), warning('test:id2','some msg')}, 'test:id2' ) );
        assert( ~EXPECT_NTHROW( @() {warning('test:id1','some msg'), warning('test:id2','some msg')}, 'test:id2', 'EXPECT_NTHROW' ) );
        assert( ~EXPECT_NTHROW( @() {warning('test:id1','some msg'), warning('test:id2','some msg')}, 'test:id2','FALSE_FILE' ) );        
        assert( EXPECT_NTHROW( @() {warning('test:id1','some msg'), warning('test:id2','some msg')}, 'test:id1', 'test:id2' ) );
        assert( EXPECT_NTHROW( @() {warning('test:id1','some msg'), warning('test:id2','some msg')}, 'test:id1', 'test:id2', 'test_macro.m' ) );
        assert( ~EXPECT_NTHROW( @() {warning('test:id1','some msg'), warning('test:id2','some msg')}, 'test:id1', 'test:id2', 'FALSE_FILE' ) );                
        
        assert( ~EXPECT_THROW( @() {warning('test:id1','some msg'), warning('test:id2','some msg')}, 'test:id1' ) );
        assert( ~EXPECT_THROW( @() {warning('test:id1','some msg'), warning('test:id2','some msg')}, 'test:id1', 'FALSE_FILE' ) );
        assert( ~EXPECT_THROW( @() {warning('test:id1','some msg'), warning('test:id2','some msg')}, 'test:id2' ) );
        assert( ~EXPECT_THROW( @() {warning('test:id1','some msg'), warning('test:id2','some msg')}, 'test:id2', 'FALSE_FILE' ) );        
        assert( EXPECT_THROW( @() {warning('test:id1','some msg'), warning('test:id2','some msg')}, 'test:id1', 'test:id2' ) );
        assert( ~EXPECT_THROW( @() {warning('test:id1','some msg'), warning('test:id2','some msg')}, 'test:id1', 'test:id2', 'FALSE_FILE' ) );          
        assert( ~EXPECT_THROW( @() {warning('test:id1','some msg'), warning('test:id2','some msg'), warning('test:id3','some msg')}, 'test:id1', 'test:id2' ) );
        
        assert( ~EXPECT_THROW( @() {warning('test:id1','some msg'), warning('test:id2','some msg'), warning('test:id3','some msg')}, 'test:id1', 'test:id2', 'FALSE_FILE' ) );                  
    elseif( i==2 );
        % Matlab-warning
        assert( ~EXPECT_NTHROW( @() {warning('test:id1','some msg'), warning('test:id2','some msg')}, 'test:id1' ) );
        assert( ~EXPECT_NTHROW( @() {warning('test:id1','some msg'), warning('test:id2','some msg')}, 'test:id1', 'EXPECT_NTHROW' ) );
        assert( EXPECT_NTHROW( @() {warning('test:id1','some msg'), warning('test:id2','some msg')}, 'test:id2' ) );
        assert( EXPECT_NTHROW( @() {warning('test:id1','some msg'), warning('test:id2','some msg')}, 'test:id2', 'EXPECT_NTHROW' ) );
        assert( EXPECT_NTHROW( @() {warning('test:id1','some msg'), warning('test:id2','some msg')}, 'test:id2',-1, 'FALSE_FILE' ) );  % this succeeds, since the line number cannot be checked
        assert( EXPECT_NTHROW( @() {warning('test:id1','some msg'), warning('test:id2','some msg')}, 'test:id2', 'FALSE_FILE' ) );  % this succeeds, since the filename cannot be checked
        
        assert( ~EXPECT_THROW( @() {warning('test:id1','some msg'), warning('test:id2','some msg')}, 'test:id1' ) );
        assert( ~EXPECT_THROW( @() {warning('test:id1','some msg'), warning('test:id2','some msg')}, 'test:id1', 'EXPECT_NTHROW' ) );
        assert( ~EXPECT_THROW( @() {warning('test:id1','some msg'), warning('test:id2','some msg')}, 'test:id1', 'FALSE_FILE' ) );
        assert( EXPECT_THROW( @() {warning('test:id1','some msg'), warning('test:id2','some msg')}, 'test:id2' ) );
        assert( EXPECT_THROW( @() {warning('test:id1','some msg'), warning('test:id2','some msg')}, 'test:id2', 'FALSE_FILE' ) );  % filename cannot be checked
        assert( EXPECT_THROW( @() {warning('test:id1','some msg'), warning('test:id2','some msg')}, 'test:id1', 'test:id2' ) );
        assert( EXPECT_THROW( @() {warning('test:id1','some msg'), warning('test:id2','some msg')}, 'test:id1', 'test:id2', 'FALSE_FILE' ) );          
        assert( ~EXPECT_THROW( @() {warning('test:id1','some msg'), warning('test:id2','some msg'), warning('test:id3','some msg')}, 'test:id1', 'test:id2' ) );
        assert( ~EXPECT_THROW( @() {warning('test:id1','some msg'), warning('test:id2','some msg'), warning('test:id3','some msg')}, 'test:id1', 'test:id2', 'FALSE_FILE' ) );
    else;
        error( 'test:macro', 'fatal error' ); end;
    
    assert( EXPECT_NTHROW( @() {warning('test:id1','some msg'), warning('test:id1','some msg')}, 'test:id1' ) );
    assert( EXPECT_THROW( @() {warning('test:id1','some msg'), warning('test:id1','some msg')}, 'test:id1' ) );
    
    assert( ~EXPECT_NTHROW( @() {warning('test:id1','some msg'), terror('test:id2','some msg')}, 'test:id1' ) );
    assert( ~EXPECT_NTHROW( @() {warning('test:id1','some msg'), terror('test:id2','some msg')}, 'test:id1', 'EXPECT_NTHROW' ) );
    assert( EXPECT_NTHROW( @() {warning('test:id1','some msg'), terror('test:id2','some msg')}, 'test:id1', 'test:id2' ) );
    assert( EXPECT_NTHROW( @() {warning('test:id1','some msg'), terror('test:id2','some msg')}, 'test:id1', 'test:id2' ) );
    assert( EXPECT_NTHROW( @() {warning('test:id1','some msg'), terror('test:id2','some msg')}, 'o', 'test:id1', 'o', 'test:id2', 'terror.m' ) );
    assert( EXPECT_NTHROW( @() {warning('test:id1','some msg'), terror('test:id2','some msg')}, 'test:id1', 'o', 'test:id2', 'terror.m' ) );
    assert( EXPECT_NTHROW( @() {warning('test:id1','some msg'), terror('test:id1','some msg')}, 'test:id1' ) );
    assert( EXPECT_NTHROW( @() {warning('test:id1','some msg'), terror('test:id1','some msg')}, 'test:id1', 'o', 'terror.m' ) );
    assert( ~EXPECT_THROW( @() {warning('test:id1','some msg'), terror('test:id2','some msg')}, 'test:id1' ) );
    assert( EXPECT_THROW( @() {warning('test:id1','some msg'), terror('test:id2','some msg')}, 'test:' ) );
    assert( EXPECT_THROW( @() {warning('test:id1','some msg'), terror('test:id2','some msg')}, 'm','test:id1', 'm','test:id2' ) );
    assert( EXPECT_THROW( @() {warning('test:id1','some msg'), terror('test:id2','some msg')}, 'test:id1', 'test:id2' ) );
    assert( EXPECT_THROW( @() {warning('test:id1','some msg'), terror('test:id2','some msg')}, 'm', 'test:id1', 'm', 'test:id2', 'terror' ) );
    assert( ~EXPECT_THROW( @() {warning('test:id1','some msg'), terror('test:id2','some msg')}, 'm', 'test:id1', 'm', 'test:id2', 0, 'terror' ) );
    assert( EXPECT_THROW( @() {warning('test:id1','some msg'), terror('test:id1','some msg')}, 'test:id1' ) );
    
    if( i==1 );
        assert( ~EXPECT_THROW( @() {warning('test:id1','some msg'), terror('test:id1','some msg')}, 'test:id1', 'terror' ) );      
    elseif( i==2 );
         assert( EXPECT_THROW( @() {warning('test:id1','some msg'), terror('test:id1','some msg')}, 'test:id1', 'terror' ) ); end %function name not available in warning
    
    assert( EXPECT_NTHROW( @() {terror('test:id1','some msg'), terror('test:id2','some msg')}, 'test:id1' ) );
    assert( EXPECT_NTHROW( @() {terror('test:id1','some msg'), terror('test:id2','some msg')}, 'test:id1', 'terror' ) );
    assert( ~EXPECT_NTHROW( @() {terror('test:id1','some msg'), terror('test:id2','some msg')}, 'test:id1', 'FALSE_FILE' ) );
    assert( ~EXPECT_NTHROW( @() {terror('test:id1','some msg'), terror('test:id2','some msg')}, 'test:id2' ) );
    assert( ~EXPECT_NTHROW( @() {terror('test:id1','some msg'), terror('test:id2','some msg')}, 'test:id2', 'terror' ) );    
    assert( EXPECT_THROW( @() {terror('test:id1','some msg'), terror('test:id2','some msg')}, 'test:id1' ) );
    assert( EXPECT_THROW( @() {terror('test:id1','some msg'), terror('test:id2','some msg')}, 'test:id1', 'terror' ) );
    assert( ~EXPECT_THROW( @() {terror('test:id1','some msg'), terror('test:id2','some msg')}, 'test:id1', 'FALSE_FILE' ) );
    assert( ~EXPECT_THROW( @() {terror('test:id1','some msg'), terror('test:id2','some msg')}, 'test:id2' ) );
    assert( ~EXPECT_THROW( @() {terror('test:id1','some msg'), terror('test:id2','some msg')}, 'test:id2', 'terror' ) );     
end

assert( EXPECT_NTHROW( @() nullary ) );
assert( EXPECT_NTHROW( 'nullary' ) );

evalc( 'addpath( ''../overload/warning/'' )' );  % relative location of folder where TTEST-warning is located

%% postprocessing
% Set errorflag to false
TTEST errflag false
