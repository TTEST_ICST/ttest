function ret = function_throw

    %This function tests macro calls which throw
    ret = true;
    h = localfunctions;
    for i = 1:numel( h );
        try;
            evalc( 'h{i}();' );
            ret = false;
            fprintf( 'ERROR: Macro call %i did not throw.\n==================\n', i );
        catch me; %#ok<NASGU>
            end; end;%everything ok
end

%#ok<*ASGLU>
%function f;  EXPECT_SUCCEED(); end  %For Debugging purposes. This function does not throw
function f1;    [ret,a,b] = EXPECT_TRUE(1); end 
function f2;    [ret,a] = EXPECT_FAIL(); end
function f3;    [ret,a] = EXPECT_FAIL("Text"); end
function f4;    [ret,a,b,c] = EXPECT_PRED( @sum, [2 3] ); end
function f5;    [ret,a,b,c] = EXPECT_ALMOST_NE( 1 ); end
function f6;    [ret,x1,x2] = EXPECT_EQ( 2 ); end
function dummy; assert(false); end %Generates an error, if the 'end' of a function is missing.