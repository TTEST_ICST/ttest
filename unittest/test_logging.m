% test TTEST logging

TTEST clear all
TTEST hardinit ttest_id_logging

% preconditions 

%% MESSAGE
TESTCASE( 'MESSAGE' );
clear ttest_id
a = 2; 
for i = 1:3
    switch i
        case 1; tt = {};
        case 2; TTEST hardinit ttest_id_logging
        case 3; tt = {ttest_id_c(0)}; end;
    MESSAGE( a, i );
    EXPECT_THAT( evalc('EXPECT_FAIL()'), ttest.AllOf(ttest.HasSubstr('a'),ttest.HasSubstr('2')) );
    
    MESSAGE( 'ttest_capture', a );
    EXPECT_THAT( evalc('EXPECT_FAIL()'), ttest.AllOf(ttest.HasSubstr('a'),ttest.HasSubstr('2')) );
    
    MESSAGE( 'ttest_capture', 'TEXT', a );
    EXPECT_THAT( evalc('EXPECT_FAIL()'), ttest.AllOf(ttest.HasSubstr('a'),ttest.HasSubstr('2'),ttest.HasSubstr('TEXT')) );
    
    MESSAGE( 'ttest_capture', 'TEXT' );
    EXPECT_THAT( evalc('EXPECT_FAIL()'), ttest.HasSubstr('TEXT') );
    
    MESSAGE( 'ttest_fprintf', 'TEXT' );
    EXPECT_THAT( evalc('EXPECT_FAIL()'), ttest.HasSubstr('TEXT') );
    
    MESSAGE( 'TEXT' );
    EXPECT_THAT( evalc('EXPECT_FAIL()'), ttest.HasSubstr('TEXT') );
    
    MESSAGE( 'TEXT%i', 1 );
    EXPECT_THAT( evalc('EXPECT_FAIL()'), ttest.HasSubstr('TEXT1') );
    
    CLEAR_MESSAGE();
    EXPECT_THAT( evalc('EXPECT_FAIL()'), ttest.Not(ttest.HasSubstr('TEXT1')) );
    
    EXPECT_THROW( DISABLED('onoctave'), @() MESSAGE('TEXT',a), ' format', 'undefined near line');
end


%% TRACE
TTEST hardinit ttest_id_logging
TESTCASE( 'TRACE' );
if( ismatlab )
    
    name = 'experimental';
    MESSAGE( 'message\n' );
    str = evalc( 'TRACE( @() EXPECT_EQ(2,3), name );' );
    EXPECT_THAT( str, ttest.AllOf(ttest.HasSubstr('name'),ttest.HasSubstr('experimental'),ttest.Not(ttest.HasSubstr('message')) ) );

    str = evalc( 'TRACE( @() EXPECT_EQ(2,3) );' );
    EXPECT_THAT( str, ttest.AllOf(ttest.HasSubstr('message')) );
    MESSAGE();
    str = evalc( 'EXPECT_FAIL();' );
    EXPECT_THAT( str, ttest.Not(ttest.HasSubstr('message')) );
else;
    TODO_FAIL( '''TRACE'' not working yet on Octave.' );
end;

if( ismatlab );
    ENDTESTCASE();
else( iswindows && isoctave );
    TTEST hardinit ttest_id_logging
    TODO_FAIL( 'Due to a bug, finishing the test case here crashes Octave.' ); end;

%%
TTEST errflag false  % reset to false

%%

