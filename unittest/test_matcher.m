% test matcher

TTEST init
TTEST v 0

% AllOf, Compose, etc... are tested at the end of the file

%% BeginsWith
assert( EXPECT_THAT( 'Thomas', ttest.BeginsWith('') ) );
assert( EXPECT_THAT( 'Thomas', ttest.BeginsWith('Th') ) );
assert( EXPECT_THAT( 'Thomas', ttest.BeginsWith('Thomas') ) );
assert( ~EXPECT_THAT( 'Thomas', ttest.BeginsWith('t') ) );

%% Contains
assert( EXPECT_THAT( [1 2 3], ttest.Contains(2) ) );
assert( EXPECT_THAT( [1 2 3], ttest.Contains(2) ) );

%% DoubleEq
assert( EXPECT_THAT( 1+eps, ttest.DoubleEq(1) ) );
assert( ~EXPECT_THAT( 1+5*eps, ttest.DoubleEq(1) ) );
assert( ~EXPECT_THAT( nan, ttest.DoubleEq(nan) ) );
assert( EXPECT_THAT( 1, ttest.DoubleEq(1+eps) ) );

%% EndsWith
assert( EXPECT_THAT( 'Thomas', ttest.EndsWith('') ) );
assert( EXPECT_THAT( 'Thomas', ttest.EndsWith('mas') ) );
assert( EXPECT_THAT( 'Thomas', ttest.EndsWith('Thomas') ) );
assert( ~EXPECT_THAT( 'Thomas', ttest.EndsWith('S') ) );

%% Each
assert( EXPECT_THAT( [1 2 3], ttest.Each([1 2 3]) ) );

%% Eq
assert( EXPECT_THAT( 1, ttest.Eq(1) ) );
assert( ~EXPECT_THAT( 2, ttest.Eq(1) ) );
assert( ~EXPECT_THAT( nan, ttest.Eq(nan) ) );
assert( EXPECT_THAT( [2 3], ttest.Each(ttest.Eq([2 3])) ) );
assert( ~EXPECT_THAT( [2 3], ttest.Eq([2 3]) ) );
assert( EXPECT_THAT( [2 3], ttest.Each(ttest.Eq([2 3])) ) );

%% FloatEq
assert( EXPECT_THAT( 1+eps, ttest.FloatEq(1) ) );
assert( EXPECT_THAT( 1+5*eps, ttest.FloatEq(1) ) );
assert( ~EXPECT_THAT( 1+5*eps(single(1)), ttest.FloatEq(1) ) );
assert( ~EXPECT_THAT( nan, ttest.FloatEq(nan) ) );

%% Ge
assert( EXPECT_THAT( 2, ttest.Ge(0) ) );
assert( EXPECT_THAT( 1, ttest.Ge(0) ) );
assert( EXPECT_THAT( 0, ttest.Ge(0) ) );
assert( ~EXPECT_THAT( -1, ttest.Ge(0) ) );

%% Gt
assert( EXPECT_THAT( 1, ttest.Gt(0) ) );
assert( ~EXPECT_THAT( 0, ttest.Gt(0) ) );
assert( ~EXPECT_THAT( -1, ttest.Gt(0) ) );

%% HasSubstr
assert( EXPECT_THAT( 'Thomas', ttest.HasSubstr('') ) );
assert( EXPECT_THAT( 'Thomas', ttest.HasSubstr('hom') ) );
assert( EXPECT_THAT( 'Thomas', ttest.HasSubstr('Thomas') ) );
assert( ~EXPECT_THAT( 'Thomas', ttest.HasSubstr('H') ) );

%% Id
assert( EXPECT_THAT( 6, ttest.Ge(5,ttest.Id) ) );
assert( ~EXPECT_THAT( 0, ttest.IsTrue(ttest.Id) ) );
assert( EXPECT_THAT( 10, ttest.Ge(5,ttest.Id) ) );

%% IsEmpty
assert( EXPECT_THAT( [], ttest.IsEmpty() ) );
assert( ~EXPECT_THAT( 0, ttest.IsEmpty() ) );
assert( EXPECT_THAT( [], ttest.IsEmpty() ) );

%% IsEq
assert( EXPECT_THAT( {0}, ttest.IsEq({0}) ) );
assert( EXPECT_THAT( [2 3], ttest.IsEq([2 3]) ) );

%% IsFalse
assert( EXPECT_THAT( 0, ttest.IsFalse() ) );
assert( ~EXPECT_THAT( 1, ttest.IsFalse() ) );

%% IsNan
assert( EXPECT_THAT( nan, ttest.IsNan() ) );
assert( ~EXPECT_THAT( 1, ttest.IsNan() ) );
assert( EXPECT_THAT( [1 2], ttest.Not(ttest.Each(ttest.IsNan)) ) );

%% IsSubsetOf
assert( EXPECT_THAT( [1 2 3], ttest.IsSubsetOf([1 2 3 4]) ) );

%% SubsetOf
assert( EXPECT_THAT( [1 2 3], ttest.SubsetOf([1 2 3 4]) ) );

%% IsSupersetOf
assert( EXPECT_THAT( [1 2 3], ttest.IsSupersetOf([1 2]) ) );

%% SupersetOf
assert( EXPECT_THAT( [1 2 3], ttest.IsSupersetOf([1 2]) ) );

%% IsTrue
assert( EXPECT_THAT( 1, ttest.IsTrue() ) );
assert( ~EXPECT_THAT( 0, ttest.IsTrue() ) );
assert( EXPECT_THAT( ~false, ttest.IsTrue ) );

%% Le
assert( EXPECT_THAT( 1, ttest.Le(2) ) );
assert( EXPECT_THAT( 0, ttest.Le(0) ) );
assert( ~EXPECT_THAT( -1, ttest.Le(-2) ) );

%% Lt
assert( EXPECT_THAT( 1, ttest.Lt(2) ) );
assert( ~EXPECT_THAT( 0, ttest.Lt(0) ) );
assert( ~EXPECT_THAT( -1, ttest.Lt(-2) ) );

%% MatcherRegEx

%% NanSensitiveDoubleEq
assert( EXPECT_THAT( 1+eps, ttest.NanSensitiveDoubleEq(1) ) );
assert( ~EXPECT_THAT( 1+5*eps, ttest.NanSensitiveDoubleEq(1) ) );
assert( EXPECT_THAT( nan, ttest.NanSensitiveDoubleEq(nan) ) );
assert( EXPECT_THAT( [3 nan], ttest.Each(ttest.NanSensitiveDoubleEq([3 nan])) ) );

%% NanSensitiveFloatEq
assert( EXPECT_THAT( 1+eps, ttest.NanSensitiveFloatEq(1) ) );
assert( EXPECT_THAT( 1+5*eps, ttest.NanSensitiveFloatEq(1) ) );
assert( ~EXPECT_THAT( 1+5*eps(single(1)), ttest.FloatEq(1) ) );
assert( EXPECT_THAT( nan, ttest.NanSensitiveFloatEq(nan) ) );

%% NanSensitiveNear
assert( EXPECT_THAT( 1.2, ttest.NanSensitiveNear(1,0.5) ) );
assert( ~EXPECT_THAT( 1.6, ttest.NanSensitiveNear(1,0.5) ) );
assert( ~EXPECT_THAT( nan, ttest.NanSensitiveNear(1,0.5) ) );
assert( ~EXPECT_THAT( nan, ttest.NanSensitiveNear(1,nan) ) );
assert( EXPECT_THAT( nan, ttest.NanSensitiveNear(nan,0.5) ) );
assert( EXPECT_THAT( nan, ttest.NanSensitiveNear(nan,nan) ) );
assert( EXPECT_THAT( nan, ttest.NanSensitiveNear(nan,3) ) );

%% NanSensitiveSingleEq
assert( EXPECT_THAT( 1+eps, ttest.NanSensitiveSingleEq(1) ) );
assert( EXPECT_THAT( 1+5*eps, ttest.NanSensitiveSingleEq(1) ) );
assert( ~EXPECT_THAT( 1+5*eps(single(1)), ttest.FloatEq(1) ) );
assert( EXPECT_THAT( nan, ttest.NanSensitiveSingleEq(nan) ) );

%% Ne
assert( EXPECT_THAT( false, ttest.Ne(true) ) );
assert( EXPECT_THAT( 1, ttest.Ne(2) ) );
assert( EXPECT_THAT( 1, ttest.Ne(0) ) );
assert( ~EXPECT_THAT( 0, ttest.Ne(0) ) );
assert( EXPECT_THAT( nan, ttest.Ne(nan) ) );

%% Near
assert( EXPECT_THAT( 1.2, ttest.Near(1,0.5) ) );
assert( ~EXPECT_THAT( 1.6, ttest.Near(1,0.5) ) );
assert( ~EXPECT_THAT( nan, ttest.Near(1,0.5) ) );
assert( ~EXPECT_THAT( nan, ttest.Near(1,nan) ) );
assert( ~EXPECT_THAT( nan, ttest.Near(nan,0.5) ) );
assert( ~EXPECT_THAT( nan, ttest.Near(nan,nan) ) );
assert( EXPECT_THAT( 8, ttest.Near(10,3) ) );

%% Not
assert( EXPECT_THAT( false, ttest.IsFalse() ) );
assert( ~EXPECT_THAT( 1, ttest.Not(ttest.Ne(2)) ) );
assert( ~EXPECT_THAT( 1, ttest.Not(ttest.Ne(0)) ) );
assert( EXPECT_THAT( 0, ttest.Not(ttest.Ne(0)) ) );
assert( ~EXPECT_THAT( nan, ttest.Not(ttest.Ne(nan)) ) );

%% NumelIs
assert( ~EXPECT_THAT( [1 2 3], ttest.NumelIs(4) ) );
assert( EXPECT_THAT( [1 2 3], ttest.NumelIs(3) ) );
assert( EXPECT_THAT( [1 2 3], ttest.Not(ttest.NumelIs(2)) ) );

%% NotEmpty
assert( EXPECT_THAT( 0, ttest.NotEmpty() ) );
assert( ~EXPECT_THAT( [], ttest.NotEmpty() ) );
assert( EXPECT_THAT( [1 2], ttest.NotEmpty ) );

%% SingleEq
assert( EXPECT_THAT( 1+eps, ttest.SingleEq(1) ) );
assert( EXPECT_THAT( 1+5*eps, ttest.SingleEq(1) ) );
assert( ~EXPECT_THAT( 1+5*eps(single(1)), ttest.SingleEq(1) ) );
assert( ~EXPECT_THAT( nan, ttest.SingleEq(nan) ) );

%% StartsWith
assert( EXPECT_THAT( 'Thomas', ttest.StartsWith('') ) );
assert( EXPECT_THAT( 'Thomas', ttest.StartsWith('Th') ) );
assert( EXPECT_THAT( 'Thomas', ttest.StartsWith('Thomas') ) );
assert( ~EXPECT_THAT( 'Thomas', ttest.StartsWith('t') ) );

%% StrCaseEq
assert( EXPECT_THAT( 'Thomas', ttest.StrCaseEq('thOMAS') ) );
assert( EXPECT_THAT( 'Thomas', ttest.StrCaseEq('Thomas') ) );
assert( ~EXPECT_THAT( 'Thomas', ttest.StrCaseEq('Thoma') ) );

%% StrCaseNe
assert( ~EXPECT_THAT( 'Thomas', ttest.StrCaseNe('thOMAS') ) );
assert( ~EXPECT_THAT( 'Thomas', ttest.StrCaseNe('Thomas') ) );
assert( EXPECT_THAT( 'Thomas', ttest.StrCaseNe('Thoma') ) );

%% StrEq
assert( ~EXPECT_THAT( 'Thomas', ttest.StrEq('thOMAS') ) );
assert( EXPECT_THAT( 'Thomas', ttest.StrEq('Thomas') ) );
assert( ~EXPECT_THAT( 'Thomas', ttest.StrEq('Thoma') ) );

%% StrNe
assert( EXPECT_THAT( 'Thomas', ttest.StrNe('thOMAS') ) );
assert( ~EXPECT_THAT( 'Thomas', ttest.StrNe('Thomas') ) );
assert( EXPECT_THAT( 'Thomas', ttest.StrNe('Thoma') ) );

%% Special Functions
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% AllOf
try;
    import ttest.*
    assert( EXPECT_THAT( [2 3], Each(AllOf(Ge(2),Le(3))) ) );
    assert( ~EXPECT_THAT( 3, AllOf(Ge(0),Le(2)) ) );
    assert( EXPECT_THAT( [], AllOf() ) );
    assert( EXPECT_THAT( [5], AllOf() ) );
    assert( EXPECT_THAT( [], Each(AllOf(Ge(2))) ) );
    assert( EXPECT_THAT( 5, AllOf(Ge(2)) ) );
    assert( EXPECT_THAT( 5, AllOf(Ge(2),Le(6)) ) );
    assert( EXPECT_THAT( 5, AllOf(Ge(2),Le(6),Le(6),Le(6),Le(6),Le(6),Le(6),Le(6),Le(6),Le(6),Le(6),Le(6),Le(6),Le(6),Le(6),Le(6),Le(6),Le(6),Le(6),Le(6),Le(6),Le(6),Le(6)) ) );
    assert( ~EXPECT_THAT( 5, AllOf(Ge(2),Le(6),Le(6),Le(6),Le(6),Le(6),Le(6),Le(6),Le(6),Le(6),Le(6),Le(6),Le(6),Le(6),Le(6),Le(6),Le(6),Le(6),Le(6),Le(6),Le(6),Le(6),Le(6),Le(0)) ) );
    assert( EXPECT_THAT( 5, AllOf(Ne(2), Ne(6)) ) );
catch me
    if( ismatlab() ); %in oc
        error( me ); end; end;

%% AnyOf
try;
    import ttest.*
    assert( EXPECT_THAT( [2 3], Each(AnyOf(Ge(3),Le(2))) ) );
    assert( ~EXPECT_THAT( [], AnyOf() ) );
    assert( ~EXPECT_THAT( [5], AnyOf() ) );
    assert( EXPECT_THAT( [], Not(AnyOf()) ) );
    assert( EXPECT_THAT( 5, AllOf(Ge(2)) ) );
    assert( EXPECT_THAT( 5, AllOf(Ge(2),Le(6)) ) );
    assert( EXPECT_THAT( 5, AllOf(Ge(2),Le(6),Le(6),Le(6),Le(6),Le(6),Le(6),Le(6),Le(6),Le(6),Le(6),Le(6),Le(6),Le(6),Le(6),Le(6),Le(6),Le(6),Le(6),Le(6),Le(6),Le(6),Le(6)) ) );
    assert( ~EXPECT_THAT( 5, AllOf(Ge(2),Le(6),Le(6),Le(6),Le(6),Le(6),Le(6),Le(6),Le(6),Le(6),Le(6),Le(6),Le(6),Le(6),Le(6),Le(6),Le(6),Le(6),Le(6),Le(6),Le(6),Le(6),Le(6),Le(0)) ) );
    assert( EXPECT_THAT( 5, AllOf(Ne(2), Ne(6)) ) );
catch me
    if( ismatlab() ); %in oc
        error( me ); end; end;

%% Compose
assert( EXPECT_THAT( [2 3], ttest.Each(ttest.AnyOf(ttest.Ge(3),ttest.Le(2))) ) );
assert( ~EXPECT_THAT( 2, ttest.Compose(ttest.Not(),ttest.Ge(1)) ) );
assert( EXPECT_THAT( 2, ttest.Compose(ttest.Not(),ttest.Not(),ttest.Ge(2)) ) );
assert( EXPECT_THAT( 3, ttest.Compose( ttest.AllOf(ttest.Ge(4),ttest.Le(4)), ttest.Unary(@(x)x+3), ttest.IsTrue() ) ) );
assert( EXPECT_THAT( 0, ttest.Compose(ttest.Eq(3),ttest.Compose(@(x)x+1,@(x)x+1),@(x)x+1) ) );

%% Matcher
ttest.Matcher( 'Odd', @(x) mod(x,2) );
assert( EXPECT_THAT( 3, Odd ) );
Even = ttest.Matcher( @(x) ~mod(x,2) );
assert( EXPECT_THAT( 2, Even ) );

%% Thread
assert( EXPECT_THAT( [1 2], [], ttest.Contains(ttest.Thread(ttest.IsEmpty())) ) );
assert( ~EXPECT_THAT( [1 2], [1], ttest.Contains(ttest.Thread(ttest.IsEmpty())) ) );
assert( EXPECT_THAT( [2 3 4], [1 2 3], ttest.Each(ttest.Thread(ttest.NumelIs(3))) ) );
assert( ~EXPECT_THAT( [2 3 4], [1 3], ttest.Each(ttest.Thread(ttest.NumelIs(3))) ) );

%% Unary
assert( EXPECT_THAT( [1 2 3], ttest.Unary(@(y) numel(y)==3) ) );
assert( EXPECT_THAT( 1, ttest.DoubleEq(sin(1),ttest.Unary(@sin)) ) );

%% WhenSorted
assert( EXPECT_THAT( [3 1 2], ttest.Each(ttest.WhenSorted([1 2 3])) ) );


%% postprocessing
% Set errorflag to true
TTEST errflag false