function [ varargout ] = TTEST( varargin );
% Frontend of TTESTs
% Used to set options, query values and install the TTESTs
% ret = TTEST( [id], name, value )
% ret = TTEST( [id], option, [value1], ..., [valuen] )
% ret = TTEST( [id], 'var', othername,othervalue )
%
% Input:
% =========
%  [id]             integer/of class ttest_id_c/the string 'global', a ttest-id, used to distinguish between different testing session
%  ----             An id of 0 is the default session
%                   Various ways to pass this variable:
%           - passed in the usual way as function argument
%           - if not passed, the variable ttest_id in the callers workspace is used
%           - if this variable is not present the id is set to 0
%
%  name, value      string-value pairs, where string is one of the following
%  -----------      if value is not given, the saved value is not altered
%                   the function always returns the old saved value
%           - 'errorassert'/'ass',val
%           - 'errorexpect'/'exp',val
%           - 'errortodo'/'todo',val    These three options are used define what shall happen when a TODO_/EXPECT_/ASSERT_ test fails
%                                                  val can be - a string 'ass','exp','todo'
%                                                  - a function handle
%                                                  - any other string evaluatable with eval
%           - 'maxdisp',val             integer, sets/queries the maximum number of characters printed using TTEST_DISP
%           - 'verbose'/'v',val             integer, default = 1, sets the verbose level
% %         - 'inject',val              (currently removed) logical, default= false, enables/disables TTEST injection
%           - 'savepbt',val             string, default = false, determines whether to save failed tests in GIVEN tests (proberty-based-tests)
%                                                   - 'no','none',false     do not store
%                                                   - 'ui','gui','figure'   store in figure (experimental)
%                                                   - 'file','mat'          store in mat-file
%                                                   - 'base'                store in variable in base workspace
%                                                   - 'ttest',true          store in TTEST function
%           - 'errorflag',val           logical, sets/queries whether an TODO_/EXPECT_/ASSERT_ test failed
%
%                               
%                                       
%  options      
%  -------
%           - 'init',[val]                  assigns a variable ttest_id in the caller workspace with a unique id
%                                           This variable is used to identify a test suite
%                                           If val is given, then the id has value val. This id then may not be unique
%           - 'clear',[val]                 integer, clears all data
%                                           if val == 'all', then data of all test sessions is cleared
%           - 'p'/'print',[val]             prints out the values of all currently saved variables
%                                           variables with default values may not be printed
%                                           if val == 'all', then data of all test sessions is printed
%           - 'install'/'inst'              installs the TTESTs into the folder where TTEST resides
%           - 'id'                          returns a unique id
%           - 'fail',[str]                  manages saved tests with PARAMs
%                                               str is not given: returns all failed tests
%                                               'last': returns only the last failed test, can also be done using the command 'tfail_last'
%                                               'delete': deletes all files with failed tests
%                                               'rerun': reruns all failed tests
%                                               'rerun','delete': reruns all failed tests and deletes those which are successful
%
%
%  'var', othername, othervalue     string,value, where string is any string which is not mentioned above 
%  ----------------------------     and which does not start with ttest and does not end with and '_'
%                                   creates a new persistant variable with name 'othername' and value othervalue (default=[])
%  'clearvar', othername            string, clears the user defined variable othername
%
%   
% Output:
% ============
%   Old value of the last queried or set variable OR
%   the thing which is described above

% check if we need to install
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if( numel(varargin)>=1 && strcmp(varargin{1},'install') );
    oldwarning = warning;
    cleanwarning = onCleanup( @() warning(oldwarning) );
    warning( 'off', 'all' );
    handle_install( varargin{:} );
    clear clearnwarning
    return; end;

persistent cache;  % struct containing all data
tempcache = cache;  % we do all operations on tempcache. If something goes wrong, the original cache is not modified
outputflag = nargout==0;  % if set, then output is produced even when nargout==0

% get test_id
%%%%%%%%%%%%%%%
if( nargin>=1 && isa(varargin{1},'ttest_id_c') );
    id = varargin{1};
    varargin(1) = [];
elseif( nargin>=1 && (startswithi(varargin{1},'ttest_id') || isa(varargin{1},'ttest_id_c')) );  % id given in arguments
    id = ttest_id_c( varargin{1} );
    varargin(1) = [];
elseif( nargin>=1 && (isnumeric(varargin{1}) || ~isnan(str2double(varargin{1}))) );
    id = ttest_id_c( varargin{1} );
    varargin(1) = [];
elseif( nargin>=1 && isequal(varargin{1},'global') );
    id = ttest_id_c( 0 );
    varargin(1) = [];
elseif( evalin( 'caller', 'exist(''ttest_id'',''var'');' ) );  % id not given in arguments
    id = evalin( 'caller', 'ttest_id;' );
    assert( isa(id,'ttest_id_c'), 'TTEST:ID', 'Variable ''ttest_id'' has wrong type.' );
else;  % id nowhere given
    id = ttest_id_c( 0 ); end;

id = id();  % make ttest_id_c to char

% preprocess
%%%%%%%%%%%%%%%%
tempcache = make_cache( id, tempcache );
cache = tempcache;

% disable INJECTION
%%%%%%%%%%%%%%%%%%%%%%%

% parse
%%%%%%%%%%%%%%

if( numel(varargin)==0 && ~exist('EXPECT_EQ.m','file') );
    fprintf( 'Type ''TTEST install'' to install the TTESTs.\n' );
    return;
elseif( numel(varargin)==0 );
    varargin{1} = 'print'; end;

%lun = erase( lower(char(varargin{1})), {'_','ttest'} ); %lookupname
lun = char( varargin{1} );
varargin(1) = [];
   
    %translate abbreviations into long names
    if(     strcmp(lun,'id') );                             [ret,cache] = handle_id( tempcache, varargin{:} );
    elseif( strcmp(lun,'pid') );                   ret = tempcache.(id);
    elseif( strcmp(lun,'savepbt') );                        [ret,cache,outputflag] = handle_ttestvar( id, tempcache, false, 'TTEST_SAVE_FAIL', varargin{:} );
    elseif( any(strcmp(lun,{'errflg','errflag','errorflag'})) );     [ret,cache,outputflag] = handle_ttestvar( id, tempcache, true, 'TTEST_ERRORFLAG', varargin{:} );
    elseif( any(strcmp(lun,{'mutationcounter','mc'})) );    [ret,cache,outputflag] = handle_ttestvar( id, tempcache, true,  'TTEST_MUTATION_COUNTER', varargin{:} );
    elseif( any(strcmp(lun,{'faultcounter','fc'})) );       [ret,cache,outputflag] = handle_ttestvar( id, tempcache, true,  'TTEST_FAULT_COUNTER', varargin{:} );
    elseif( any(strcmp(lun,{'verbose','v'})) );             [ret,cache,outputflag] = handle_ttestvar( id, tempcache, true,  'TTEST_VERBOSE', varargin{:} );
    elseif( strcmp(lun,'maxdisp') );                        [ret,cache,outputflag] = handle_ttestvar( id, tempcache, true,  'TTEST_MAXDISP', varargin{:} );
    elseif( any(strcmp(lun,{'errortodo','todoerror','todoerr','errtodo'})) );                         [ret,cache,outputflag] = handle_todoexpass( id, tempcache, 'TTEST_ERROR_TODO', varargin{:} );
    elseif( any(strcmp(lun,{'successtodo','todosuccess','todosucc','succtodo'})) );                   [ret,cache,outputflag] = handle_todoexpass( id, tempcache, 'TTEST_SUCCESS_TODO', varargin{:} );
    elseif( any(strcmp(lun,{'errorexpect','expecterror','errorexp','experror','experr','errexp'})) ); [ret,cache,outputflag] = handle_todoexpass( id, tempcache, 'TTEST_ERROR_EXPECT', varargin{:} );
    elseif( any(strcmp(lun,{'successexpect','expectsuccess','successexp','expsucc','succexp'})) );    [ret,cache,outputflag] = handle_todoexpass( id, tempcache, 'TTEST_SUCCESS_EXPECT', varargin{:} );
    elseif( any(strcmp(lun,{'errorassert','asserterror','errorass','asserr','errass'})) );            [ret,cache,outputflag] = handle_todoexpass( id, tempcache, 'TTEST_ERROR_ASSERT', varargin{:} );
    elseif( any(strcmp(lun,{'successassert','assertsuccess','successass','asssucc','succass'})) );    [ret,cache,outputflag] = handle_todoexpass( id, tempcache, 'TTEST_SUCCESS_ASSERT', varargin{:} );
    elseif( any(strcmp(lun,{'todo','exp','ass'})) );                                                  [ret,cache,outputflag] = handle_todoexpass( id, tempcache, lun, varargin{:} );
        
    elseif( numel(lun)>=4 && (strcmp(lun(1:4),'init') || strcmp(lun(end-3:end),'init')) );
        % we cannot put this into a subfunction, since we need to use evalin/assignin
        if( numel(lun)>=4 && strcmp(lun(1:4),'init') );
            lun(1:4) = []; end;
        if( numel(lun)>=4 && strcmp(lun(end-3:end),'init') );
            lun(end-3:end) = []; end;
        if( numel(lun)>=1 );
            if( lun(1)=='-' ); lun(1) = []; end;
            if( lun(end)=='-' ); lun(end) = []; end; end;
        outputflag = false;
        newid = [];
        
        if( isempty(lun) );
            lun = 'hard'; end;
        if( numel(varargin)==0 );
            newid = ttest_id_c;
        elseif( isnumeric(varargin{1}) || isstring(varargin{1}) || ischar(varargin{1}) );
            newid = ttest_id_c( varargin{1} );
        elseif( isa(varargin{1},'ttest_id_c') );
            newid = varargin{1}; end;
        
        switch lun;
            case {'h','hard'};
                tempcache = make_cache( newid.id, tempcache, true );
            case {'l','last'}; 
                newid = tempcache.last_ttest_id;
            case {'s','soft','save'};
                if( evalin('caller','exist(''ttest_id'',''var'');') );
                    newid = evalin( 'caller', 'ttest_id;' ); end;
            otherwise;
                error( 'TTEST:arg', 'Wrong option used for init' ); end;
            
        if( ~isempty(newid) );
            ret = newid; end;
        if( ~isempty(newid) && nargout==0 );
            tempcache.last_ttest_id = newid;
            try;
                assignin( 'caller', 'ttest_id', newid );
                cache = tempcache;
            catch me;
                error( 'TTEST:static', 'In static workspaces, use the syntax\n    ttest_id = TTEST( ''init'' )\nto initialize a test suite. Note that the output variable must have this name.\n' ); end;
        else;
           cache = tempcache;end;

    elseif( strcmp(lun,'capture') );
        group = '';
        if( numel(varargin)>=1 && (ischar(varargin{1}) || isstring(varargin{1})) && strcmp(char(varargin{1}),'group') );
            assert( numel(varargin)>=2 && ischar(varargin{2}), 'TTEST:falat', 'Programming error' );
            group = varargin{2}; 
            varargin(1:2) = []; end;
        if( numel(varargin)==1 && strcmp(varargin{1},'ttest_getcapture') );
            ret = tempcache.(id).capture;
            tempcache.(id) = rmfield( tempcache.(id), 'capture' );
            cache = tempcache;
        else;
            ret = false;
            %capture variables
            if( ~isfield(tempcache.(id),'capture') );
                tempcache.(id).capture = struct; end;
            if( isequal(group,'0') );
                tempcache.(id).capture(end+1).ttest_rng = evalin( 'caller', 'rng;' ); end;
            if( isempty(varargin) ); %capture workspace
                ws = evalin( 'caller', 'whos()' );
                for i = 1:numel( ws );
                    wscache = evalin( 'caller', ws(i).name );
                    if( isempty(group) );
                        tempcache.(id).capture.(ws(i).name) = wscache; 
                    elseif( isequal(group,'0') );
                        tempcache.(id).capture(end).(ws(i).name) = wscache; 
                    else;
                        tempcache.(id).capture.(group).(ws(i).name) = wscache; end; end;
            else; %capture given variables
                for i = 1:numel( varargin );
                    if( evalin( 'caller', ['exist( ''' varargin{i} ''', ''var'' );'] ) );
                        if( isempty(group) );
                            tempcache.(id).capture.(varargin{i}) = evalin( 'caller', varargin{i} );
                        elseif( isequal(group,'0') );
                            tempcache.(id).capture(end).(varargin{i}) = evalin( 'caller', varargin{i} );
                        else;
                            tempcache.(id).capture.(group).(varargin{i}) = evalin( 'caller', varargin{i} ); end;
                    else;
                        if( isempty(group) );
                            tempcache.(id).capture.(varargin{i}) = 'TTEST_UNDEFINED';
                        elseif( isequal(group,'0') );
                            tempcache.(id).capture(end).(varargin{i}) = 'TTEST_UNDEFINED';
                        else;
                            tempcache.(id).capture.(group).(varargin{i}) = 'TTEST_UNDEFINED'; end; end; end; end;
            cache = tempcache; end;
        
    elseif( strcmp(lun,'flow') );
        assert( numel(varargin)>=1, 'TTEST:flow', 'Programming Error. No arguments given.' );
        assert( ~isstring(varargin{1}), 'TTEST:flow', 'Programming Error. String instead of char given.' );
        assert( ischar(varargin{1}), 'TTEST:flow', 'Programming Error. NO char given.' );
        if( numel(varargin)==0 );
            error( 'TTEST:fatal', 'Programming error' );
        elseif( numel(varargin)==1 && strcmp(varargin{1},'ttest_getflow') );
            ret = tempcache.(id).flow;
            tempcache.(id) = rmfield( tempcache.(id), 'flow' );
            cache = tempcache;
        else;
            % initialize cache
            FLOWCACHE_NUMCOLS = 5;
            ret = false;
            group = 'null';
            if( strcmp(char(varargin{1}),'group') );
                assert( numel(varargin)>=2 && ischar(varargin{2}), 'TTEST:fatal', 'Programming error' );
                group = varargin{2}; 
                varargin(1:2) = []; end;
            if( ~isfield(tempcache.(id),'flow') );
                tempcache.(id).flow = struct; end;
            if( ~isfield(tempcache.(id).flow, group) );
                tempcache.(id).flow.(group) = cell(0,FLOWCACHE_NUMCOLS); end;
            
            % check if we can return early
            if( ~isempty(tempcache.(id).flow.(group)) && ...
                (strcmp(tempcache.(id).flow.(group){end,1},'abort') || strcmp(tempcache.(id).flow.(group){end,1},'failure')) );
            elseif( strcmp(varargin{1},'expected') );
                tempcache.(id).flow.(group){end+1,1} = varargin{1};
                tempcache.(id).flow.(group){end,5} = varargin{2};
                cache = tempcache;
            else;
                % save flow
                db = dbstack;
                tempcache.(id).flow.(group){end+1,1} = varargin{1};
                tempcache.(id).flow.(group){end,2} = db(2).file;
                tempcache.(id).flow.(group){end,3} = db(2).name;
                tempcache.(id).flow.(group){end,4} = db(2).line;
                tempcache.(id).flow.(group){end,5} = varargin{2};
                switch varargin{1};
                    case 'abort';
                        evalin( 'caller', 'clear' );
                    case 'failure';
                        evalin( 'caller', 'clear' );
                    case 'expect';
                        % do nothing
                    otherwise;
                        error( 'TTEST:fatal', 'programming error' ); end;
             cache = tempcache; end; end;
    elseif( strcmp(lun,'input') );
        ret = varargin{1};
        flag = evalin( 'caller', 'exist(''ttest_input'',''var'');' );
        if( flag );
            ttest_input = evalin( 'caller', 'ttest_input;' );
        else;
            ttest_input = {}; end;
        ttest_input{end+1} = varargin{1};
        assignin( 'caller', 'ttest_input', ttest_input );
    % elseif( strcmp(lun,'inject') );                        [ret,cache,outputflag] = handle_ttestvar( id, tempcache, true,  'TTEST_ENABLE_INJECTION', varargin{:} );
    elseif( strcmp(lun,'rerun') );                         error( 'TTEST:rerun', 'Option ''rerun'' not yet implemented.' );
    elseif( strcmp(lun,'clear') );                         [cache] = handle_clear( id, tempcache, varargin{:} );
    elseif( any(strcmp(lun,{'clearvar','clear-var'})) );   [cache] = handle_clearvar( id, tempcache, varargin{:} );
    elseif( strcmp(lun,'var') );                           [ret,cache,outputflag] = handle_var( id, tempcache, outputflag, varargin{:} );            
    elseif( any(strcmp(lun,{'tfaillast','lastfail'})) );   [ret] = handle_lastfail( id, tempcache );
    elseif( strcmp(lun,'fail') );                          [ret] = handle_fail( id, tempcache, varargin{:} );
    elseif( strcmp(lun,'message') );                       [ret,cache] = handle_message( id, tempcache, varargin{:} );
    elseif( strcmp(lun,'section') );                       [ret,cache] = handle_section( id, tempcache, varargin{:} );    
    elseif( strcmp(lun,'assign') );                        [ret,cache] = handle_assign( id, tempcache, varargin{:} );
    elseif( strcmp(lun,'pbt') );                           [ret,cache] = handle_pbt( id, tempcache, varargin{:} );
    elseif( any(strcmp(lun,{'h','help'})) );               help TTEST;
    elseif( any(strcmp(lun,{'p','print'})) );              handle_print( id, tempcache, varargin{:} );    
    elseif( strcmp(lun,'tellme') );                        [ret,outputflag] = handle_shakespeare( outputflag );        
    elseif( strcmp(lun,'persistent') );                    ret = tempcache;
    else;
        error( 'TTEST:argin', 'Wrong input arguments' ); end;

if( exist('ret','var') && (nargout>=1 || outputflag) );
	varargout{1} = ret; 
elseif( nargout>=1 );
    varargout{1} = 'Great'; end;

end



% Helper functions
%%%%%%%%%%%%%%%%%%%%

function tempcache = make_cache( id, tempcache, resetflag );
    
    zeroconstructed = ~isempty( tempcache );

    if( nargin>=3 && resetflag && isfield(tempcache,id) );
        tempcache = rmfield( tempcache, id ); 
        if( isequal(id,'ttest_id_0') );
            tempcache.ttest_id_0_constructed = false; end; end;
    
    if( ~zeroconstructed );
        tempcache.ttest_id_0 = make_cache_worker(); end;
    
    if( ~isfield(tempcache,'last_ttest_id') );
        tempcache.last_ttest_id = ttest_id_c( 0 ); end;
        
    if( ~isfield(tempcache,id) );
        tempcache.(id) = make_cache_worker(); end;

end

function [ st ] = make_cache_worker();
        st = struct;
        st.var = struct;  % for user defined data
        st.section = struct;  % for test sections
        st.message = '';  % for traced messages
        st.assign = struct;  % for assignat
        st.fail = struct;  % for failed examples from PBT
        st.fail.last = [];
        st.pbt = [];  % for registered strategies

        st.section = struct;
        st.section.testcase.on = 0;
        st.section.section.on = 0;
        st.section.subsection.on = 0;
        
        st.TTEST_VERBOSE            = TTEST_VERBOSE;
%       st.TTEST_ENABLE_INJECTION   = TTEST_ENABLE_INJECTION;
        st.TTEST_ERROR_TODO         = TTEST_ERROR_TODO;
        st.TTEST_SUCCESS_TODO       = TTEST_SUCCESS_TODO;
        st.TTEST_ERROR_EXPECT       = TTEST_ERROR_EXPECT;
        st.TTEST_SUCCESS_EXPECT     = TTEST_SUCCESS_EXPECT;
        st.TTEST_ERROR_ASSERT       = TTEST_ERROR_ASSERT;
        st.TTEST_SUCCESS_ASSERT     = TTEST_SUCCESS_ASSERT;
        st.TTEST_ERRORFLAG          = TTEST_ERRORFLAG;
        st.TTEST_FAULT_COUNTER      = TTEST_FAULT_COUNTER;
        st.TTEST_MAXDISP            = TTEST_MAXDISP;
%       st.TTEST_MUTATION_COUNTER   = TTEST_MUTATION_COUNTER;
        st.TTEST_SAVE_FAIL          = TTEST_SAVE_FAIL;
        st.TTEST_VERBOSE            = TTEST_VERBOSE;
end

% Workers
%%%%%%%%%%%%%

function [ ret, tempcache ] = handle_id( tempcache, varargin );
    if( numel(varargin)>=2 );
        ret = varargin{2};
    else;
        ret = ttest_id_c; end;
    
end

function handle_install( varargin );
    if( ~exist('TTEST_INSTALL.m','file') );
        olddir = cd( 'misc' ); 
        cleandir = onCleanup( @() cd(olddir) ); end;
    TTEST_INSTALL( varargin{2:end} );
end

function [ ret, tempcache ] = handle_message( id, tempcache, varargin );
    if( numel(varargin)==1 );
        ret = tempcache.(id).message;
        tempcache.(id).message = varargin{1};
    elseif( numel(varargin)==0 );
        ret = tempcache.(id).message; end;
end

function [ret, tempcache] = handle_section( id, tempcache, varargin );
    assert( ~mod(numel(varargin),2), 'Only an even number of extra arguments can be passed.' );
    ret = tempcache.(id).section; 
    while( ~isempty(varargin) );
        % str = lower( varargin{end-1} );  % unnecessary since this function is not meant to be called by a user
        str = varargin{end-1};
        switch str(1:2);
            case 'ba'; tempcache.(id).section.base =       handle_section_worker( tempcache.(id).section.base,       str( 5:end), varargin{end} );
            case 'te'; tempcache.(id).section.testcase =   handle_section_worker( tempcache.(id).section.testcase,   str( 9:end), varargin{end} );
            case 'se'; tempcache.(id).section.section =    handle_section_worker( tempcache.(id).section.section,    str( 8:end), varargin{end} );
            case 'su'; tempcache.(id).section.subsection = handle_section_worker( tempcache.(id).section.subsection, str(11:end), varargin{end} );
            otherwise; 
                error( 'TTEST:arg', 'fatal error. Wrong string given: %s (%s)', str(1:2), str ); end;
        varargin(end-1:end) = []; end;

end

function [ st ] = handle_section_worker( st, str, val );
    switch str;
        case 'on';        st.on       = val;
        case 'base';      st.base     = val;
        case 'debug';     st.debug    = val;
        case 'global';    st.global   = val;
        case 'name';      st.name     = val;
        case 'path';      st.path     = val;
        case 'pwd';       st.pwd      = val;
        case 'rng';       st.rng      = val;
        case 'setup';     st.setup    = val;
        case 'teardown';  st.teardown = val;
        case 'vars';      st.vars     = val;
        case 'warning';   st.warning  = val;
        otherwise;
            error( 'TTEST:arg', 'fatal error. Wrong string given: %s', str ); end;
        
end

function [ ret, tempcache ] = handle_assign( id, tempcache, varargin );
    assert( numel(varargin)>=2, 'TTEST:assign', 'Argument missing.' );
    as_id = varargin{1};
    if( ~isfield( tempcache.(id).assign, as_id ) );
        tempcache.(id).assign.as_id = struct; end;
    try;
        ret = tempcache.(id).assign.(as_id).(varargin{2});
    catch;
        tempcache.(id).assign.(as_id).(varargin{2}) = [];
        ret = []; end;
    if( numel(varargin)==3 );
        tempcache.(id).assign.(as_id).(varargin{2}) = varargin{3}; end;
end

function [ ret, tempcache ] = handle_pbt( id, tempcache, varargin );

    % register strategies if not happend yet
    if( isempty(tempcache.(id).pbt) );
        tempcache.(id).pbt = struct;
        tempcache.(id).pbt.strategy = {};
        tempcache.(id).pbt.name = {};
        tempcache.(id).pbt.longname = {};
        pathstr = fileparts( mfilename( 'fullpath' ) );
        strategypath = fullfile( pathstr, 'utility', 'pbt', 'strategy', '+ttest', '*.m' );
        ls = dir( strategypath );
        for i = 1:numel( ls );
            if( numel(ls(i).name)>=8 && strcmp(ls(i).name(end-7:end),'drawer.m') );
                continue; end;
            val = feval( ['ttest.' ls(i).name(1:end-2)], 'register' );
            if( ~isempty(val) );
                assert( numel(val)==2, 'TTEST:GIVEN', '''Register'' callback must return a 2 element cell array.' );
                assert( isa(val{1},'ttest_strategy_c'), 'TTEST:GIVEN', '''Register'' callback must return a ttest_strategy_c object as first cell element.' );
                assert( isa(val{2},'char'), 'TTEST:GIVEN', '''Register'' callback must return a char array as second cell element.' );
                assert( numel(val{2})==3 || numel(val{2})==4, 'TTEST:GIVEN', '''Register'' callback must return a char array with three or four characters..' );
                tempcache.(id).pbt.strategy{end+1} = val{1};
                tempcache.(id).pbt.name{end+1} = val{2};
                tempcache.(id).pbt.longname{end+1} = ls(i).name(1:end-2); end; end;
        val = unique( tempcache.(id).pbt.name );
        assert( numel(val)==numel(tempcache.(id).pbt.name), 'TTEST:GIVEN', 'Multiple strategies got registered under the same name. This is an error.' );

        % check if adaptors return a function handle if called without strategy
        adaptorpath = fullfile( pathstr, 'experimental', 'pbt', 'adaptor', '*.m' );  % XX
        ls = dir( adaptorpath );
        for i = 1:numel( ls );
            val = feval( ls(i).name(1:end-2) );
            assert( isa(val,'function_handle') ); end;

    end;

    if( numel(varargin)==1 );
        % return queried strategies
        idx = strcmp( varargin{1}, tempcache.(id).pbt.name );
        if( nnz(idx)==0 );
            idx_longname = find( strcmp( varargin{1}, tempcache.(id).pbt.longname ) );
            if( nnz(idx_longname)>=1 );
                error( 'TTEST:GIVEN', ...
                    'Found no registered strategy with name: ''%s''.\n  You probably meant: ''%s''.\n  For implicitly defining the strategy the shorthand name must be used.', ...
                    varargin{1}, tempcache.(id).pbt.name{idx_longname(1)} );
            else;
                error( 'TTEST:GIVEN', 'Found no registered strategy with name %s (but there should be one).', varargin{1} ); end; end;
        ret = deepcopy( tempcache.(id).pbt.strategy{idx} );
    elseif( numel(varargin)==2 );
        % register new strategy
        if( varargin{1}(end)=='_' || varargin{1}(end)>=0 && varargin{1}(end)<='9' );
        	error( 'TTEST:GIVEN', 'Shorthands for strategies must not end with a digit or with and underscore.' ); end;
        idx = strcmp( varargin{1}, tempcache.(id).pbt.name );

        if( any(idx) );
            tempcache.(id).pbt.name(idx) = [];
            tempcache.(id).pbt.strategy(idx) = []; end;
        tempcache.(id).pbt.name{end+1} = varargin{1};
        tempcache.(id).pbt.strategy{end+1} = deepcopy( varargin{2} );
        ret = deepcopy( varargin{2} ); end;

end


function [ ret ] = handle_fail( id, tempcache, varargin );
    if( numel(varargin)>=3 && (isa(varargin{2},'string')||isa(varargin{2},'char')) && isa(varargin{3},'function_handle') );
        tempcache.(id).fail.(varargin{2}) = varargin{3};
        tempcache.(id).fail.last = tempcache.(id).fail.(varargin{2});
    elseif( numel(varargin)>=2 && strcmp(varargin{2},'rerun') );
        if( numel(varargin)>=3 && strcmp(varargin{3},'delete') );
            deleteflag = true;
        else;
            deleteflag = false; end;
        TTEST_RERUN( id, deleteflag );
    elseif( numel(varargin)==2 && strcmp(varargin{2},'last') );
        ret = tempcache.(id).fail.last;
    else;
        ret = tempcache.(id).fail; end;
end

function [ ret ] = handle_lastfail( id, tempcache )
    ret = tempcache.(id).fail.last;
end

function [ ret, outputflag ] = handle_shakespeare( outputflag );        
    ret = ttest.shakespeare;
    if( outputflag );
        disp( ret ); end;
    outputflag = false;
end

function [ ret, tempcache ] = handle_todoexpass_helper1( id, tempcache, name )
    if( ~isfield(tempcache.(id),name) );
        tempcache.(id).(name) = feval( name ); end;
    ret = tempcache.(id).(name);
end

function [ ret, tempcache, outputflag ] = handle_todoexpass( id, tempcache, varargin );
    if( numel(varargin)==2 );
        outputflag = 0;
    else;
        outputflag = 1; end;
    assert( numel(varargin)<=2, 'TTEST:todoexpass', 'At most two arguments are necessary.'  );
    
    switch varargin{1};
        case 'todo';
            [ret{1},tempcache] = handle_todoexpass_helper1( id, tempcache, 'TTEST_ERROR_TODO' );
            [ret{2},tempcache] = handle_todoexpass_helper1( id, tempcache, 'TTEST_SUCCESS_TODO' );
        case 'exp';
            [ret{1},tempcache] = handle_todoexpass_helper1( id, tempcache, 'TTEST_ERROR_EXPECT' );
            [ret{2},tempcache] = handle_todoexpass_helper1( id, tempcache, 'TTEST_SUCCESS_EXPECT' );
        case 'ass';
            [ret{1},tempcache] = handle_todoexpass_helper1( id, tempcache, 'TTEST_ERROR_ASSERT' );
            [ret{2},tempcache] = handle_todoexpass_helper1( id, tempcache, 'TTEST_SUCCESS_ASSERT' );
        otherwise;
            if( ~isfield(tempcache.(id),varargin{1}) );
                tempcache.(id).(varargin{1}) = feval( varargin{1} ); end;
            ret = tempcache.(id).(varargin{1}); end;    
	if( numel(varargin)==2 );
        if( isstring(varargin{2}) || ischar(varargin{2}) );
            switch varargin{2};
                case 'TTEST_ERROR_TODO';     errorhandler   = TTEST_ERROR_TODO;
                case 'TTEST_SUCCESS_TODO';   successhandler = TTEST_SUCCESS_TODO;
                case 'TTEST_ERROR_EXPECT';   errorhandler   = TTEST_ERROR_EXPECT;
                case 'TTEST_SUCCESS_EXPECT'; successhandler = TTEST_SUCCESS_EXPECT;
                case 'TTEST_ERROR_ASSERT';   errorhandler   = TTEST_ERROR_ASSERT;
                case 'TTEST_SUCCESS_ASSERT'; successhandler = TTEST_SUCCESS_ASSERT;
                case 'todo'; 
                    errorhandler   = TTEST_ERROR_TODO;
                    successhandler = TTEST_SUCCESS_TODO;
                case 'exp'; 
                    errorhandler   = TTEST_ERROR_EXPECT;
                    successhandler = TTEST_SUCCESS_EXPECT;
                case 'ass'; 
                    errorhandler   = TTEST_ERROR_ASSERT;
                    successhandler = TTEST_SUCCESS_ASSERT; 
                otherwise;
                    varargin{2} = str2func( varargin{2} );
                    assert( isa(varargin{2},'function_handle'), 'TTEST:todoexpass', 'Second argument must be either a function_handle or one of : ''errortodo'', ''successtodo'', ''errorexpect'', ''successexpect'', ''errorassert'', ''successassert'', ''todo'', ''exp'', ''ass''.' ); 
                    assert( nargin(varargin{2})==4, 'TTEST:todoexpass', 'Function handle must accept exactly 4 input arguments.' ); 
                    errorhandler = varargin{2};
                    successhandler = varargin{2}; end; 
        else;
            assert( isa(varargin{2},'function_handle'), 'TTEST:todoexpass', 'Second argument must be either a function_handle or one of : ''errortodo'', ''successtodo'', ''errorexpect'', ''successexpect'', ''errorassert'', ''successassert'', ''todo'', ''exp'', ''ass''.' ); 
            assert( nargin(varargin{2})==4, 'TTEST:todoexpass', 'Function handle must accept exactly 4 input arguments.' ); 
            errorhandler = varargin{2};
            successhandler = varargin{2}; end;

        switch varargin{1};
            case 'TTEST_ERROR_TODO';     tempcache.(id).TTEST_ERROR_TODO     = errorhandler;
            case 'TTEST_SUCCESS_TODO';   tempcache.(id).TTEST_SUCCESS_TODO   = successhandler;
            case 'TTEST_ERROR_EXPECT';   tempcache.(id).TTEST_ERROR_EXPECT   = errorhandler;
            case 'TTEST_SUCCESS_EXPECT'; tempcache.(id).TTEST_SUCCESS_EXPECT = successhandler;
            case 'TTEST_ERROR_ASSERT';   tempcache.(id).TTEST_ERROR_ASSERT   = errorhandler;
            case 'TTEST_SUCCESS_ASSERT'; tempcache.(id).TTEST_SUCCESS_ASSERT = successhandler;
            case 'todo'; 
                assert( numel(varargin)==2, 'TTEST:todoexpass', 'Option ''todo'' needs another second option one of: ''todo'', ''exp'', ''ass''.' );
                tempcache.(id).TTEST_ERROR_TODO = errorhandler;
                tempcache.(id).TTEST_SUCCESS_TODO = successhandler;           
            case {'exp'};
                assert( numel(varargin)==2, 'TTEST:todoexpass', 'Option ''exp'' needs another second option one of: ''todo'', ''exp'', ''ass''.' );
                tempcache.(id).TTEST_ERROR_EXPECT = errorhandler;
                tempcache.(id).TTEST_SUCCESS_EXPECT = successhandler;            
            case {'ass'};
                assert( numel(varargin)==2, 'TTEST:todoexpass', 'Option ''ass'' needs another second option one of: ''todo'', ''exp'', ''ass''.' );
                tempcache.(id).TTEST_ERROR_ASSERT = errorhandler;
                tempcache.(id).TTEST_SUCCESS_ASSERT = successhandler;
            otherwise;
                error( 'TTEST:todoexpass', 'Wrong first argument. Must be one of: ''errortodo'', ''successtodo'', ''errorexpect'', ''successexpect'', ''errorassert'', ''successassert'', ''todo'', ''exp'', ''ass''.' ); end; end;
end

function [ tempcache ] = handle_clear( id, tempcache, varargin );
    if( numel(varargin)>=1 );
        if( strcmp(varargin{1},'all') );
            tempcache = struct;
        elseif( strcmp(varargin{1},'var') );
            tempcache = handle_clearvar( id, tempcache, varargin{2:end} );
        else;
            error( 'TTEST:clear', 'wrong option given to ''clear''.' ); end;
    else;
        tempcache = rmfield( tempcache , id ); end;
end

function [ tempcache ] = handle_clearvar( id, tempcache, varargin )
    for i = 1:numel( varargin );
        if( ~isfield(tempcache.(id).var,varargin{i}) );
            continue; end;
        tempcache.(id).var = rmfield( tempcache.(id).var, varargin{i} ); end;
end

function [ ret, tempcache, outputflag ] = handle_var( id, tempcache, outputflag, varargin );
    if( numel(varargin)==0 );
        ret = tempcache.(id).var;
        outputflag = true;
    else;
        if( ~isfield(tempcache.(id).var,varargin{1}) );
            if( ~strcmp(id,'ttest_id_0') && numel(varargin)==1 );
                error( 'TTEST:var', 'To create a variable it must be given a value at initialization' );
            else;
                tempcache.(id).var.(varargin{1}) = []; end; end;
        ret = tempcache.(id).var.(varargin{1});
        if( numel(varargin)>=2 );
            outputflag = false;
            tempcache.(id).var.(varargin{1}) = varargin{2}; end; end;
end

function [ outputflag ] = handle_print( id, tempcache, varargin );
    outputflag = false;
    if( numel(varargin)>=1 && strcmp(varargin{1},'all') );
        %print all data
        ttest.structdisp( tempcache );
    else;
        %print current ttest_ids data
        fprintf( 'last id: %s\n', strtrim(evalc('disp(tempcache.last_ttest_id())')) );
        fprintf( '\nTTEST local variables: \n' );
        disp( tempcache.(id) )
        if( ~isempty(fieldnames(tempcache.(id).var)) );
            fprintf( 'User defined variables in .var: \n' );
            disp( tempcache.(id).var ); end;
        if( ~isempty(fieldnames(tempcache.(id).fail)) && ~isempty(tempcache.(id).fail.last) );
            fprintf( 'Saved failed tests in .fail: \n' );
            disp( tempcache.(id).fail ); end; 
        %print global data
        id = ttest_id_c( 0 );
        id = id();
        fprintf( '\nTTEST global variables: \n' );
        disp( tempcache.(id) );
        if( ~isempty(fieldnames(tempcache.(id).var)) );
            fprintf( 'User defined variables in .var: \n' );
            disp( tempcache.(id).var ); end;
        if( ~isempty(fieldnames(tempcache.(id).fail)) && ~isempty(tempcache.(id).fail.last) );
            fprintf( 'Saved failed tests in .fail: \n' );
            disp( tempcache.(id).fail ); end; end;
end

function [ ret, tempcache, outputflag ] = handle_ttestvar( id, tempcache, numflag, varargin );
    if( ~isfield(tempcache.(id),varargin{1}) || isempty(tempcache.(id).(varargin{1})) );
        tempcache.(id).(varargin{1}) = feval( varargin{1} ); end; %hack, use variable name as function name inside of this file
    ret = tempcache.(id).(varargin{1});
    if( numel(varargin)>=2 );
        if( numflag && (isstring(varargin{2}) || ischar(varargin{2})));
            varargin{2} = str2num( varargin{2} ); end; %#ok<ST2NM>
        outputflag = false;
        tempcache.(id).(varargin{1}) = varargin{2}; 
    else;
        outputflag = true; end;
end



% Helper functions
%%%%%%%%%%%%%%%%%%%%%%
function [ ret ] = startswithi( str, pat );
    cl = class( str );

    ret = false;
    if( ~isequal(cl,'string') && ~isequal(cl,'char') );
        return; end;

    idx = strfind( lower(str), lower(pat) );
    if( isempty(idx) );
        return; end;
    if( idx(1)~=1 );
        return; end;

    ret = true;    
end

% Default values
%%%%%%%%%%%%%%%%

% function [ val ] = TTEST_ENABLE_INJECTION;  val = false;   end %#ok<DEFNU>
function [ val ] = TTEST_ERROR_TODO;        val = @(id,ret,errid,errmsg) TTEST_ERROR_TODO_WORKER( id, ret, errid, errmsg );  end
function [ val ] = TTEST_SUCCESS_TODO;      val = @(id,ret,errid,errmsg) TTEST_SUCCESS_TODO_WORKER( id, ret, errid, errmsg );  end
function [ val ] = TTEST_ERROR_EXPECT;      val = @(id,ret,errid,errmsg) TTEST_ERROR_EXPECT_WORKER( id, ret, errid, errmsg );  end
function [ val ] = TTEST_SUCCESS_EXPECT;    val = @(id,ret,errid,errmsg) TTEST_SUCCESS_EXPECT_WORKER( id, ret, errid, errmsg );  end
function [ val ] = TTEST_ERROR_ASSERT;      val = @(id,ret,errid,errmsg) TTEST_ERROR_ASSERT_WORKER( id, ret, errid, errmsg );  end
function [ val ] = TTEST_SUCCESS_ASSERT;    val = @(id,ret,errid,errmsg) TTEST_SUCCESS_ASSERT_WORKER( id, ret, errid, errmsg );  end
function [ val ] = TTEST_ERRORFLAG;         val = false;   end 
function [ val ] = TTEST_FAULT_COUNTER;     val = 0;       end 
function [ val ] = TTEST_MAXDISP;           val = 6000;    end 
% function [ val ] = TTEST_MUTATION_COUNTER;  val = 0;       end %#ok<DEFNU>
function [ val ] = TTEST_SAVE_FAIL;         val = 'file';  end 
function [ val ] = TTEST_VERBOSE;           val = 1;       end 


function dummy; end %#ok<DEFNU> %Generates an error, if the 'end' of a function is missing. 

% This Source Code Form is subject to the terms of the Mozilla Public
% License, v. 2.0. If a copy of the MPL was not distributed with this
% file, You can obtain one at https://mozilla.org/MPL/2.0/.
