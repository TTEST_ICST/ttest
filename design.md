# Contents

This file contains
- information how to contribute to *TTESTs*
- Styleguides
- Design decisions
- Issue Tracker

## Table of Contents
[[_TOC_]]


# Design Decisions

## Easy to use

### Stuff which makes it easy to use
- no verbosity
- everything case-insensitive
- consistent names, everything in singular


## Interesting things to reaad

- [Shrink anonymous function in size](https://de.mathworks.com/matlabcentral/fileexchange/45749-memory-efficient-anonymous-functions?s_tid=srchtitle)
- [Parsing mlint (Code Analyzer) output](https://undocumentedmatlab.com/articles/parsing-mlint-code-analyzer-output)
- [Find references 1](https://www.mathworks.com/matlabcentral/fileexchange/69156-find_references?s_tid=prof_contriblnk), [Find references 2]( https://blogs.mathworks.com/pick/2015/03/06/plot-subfunctions/)
- [TextInject](https://www.mathworks.com/matlabcentral/fileexchange/29594-textinject)
- [mockobject](https://www.mathworks.com/matlabcentral/fileexchange/42959-mockobject)
- [modules and namespaces in Matlab](https://www.mathworks.com/matlabcentral/fileexchange/38014)
- [getkey](https://www.mathworks.com/matlabcentral/fileexchange/7465-getkey?s_tid=prof_contriblnk), [getkeywait](https://www.mathworks.com/matlabcentral/fileexchange/8297-getkeywait?s_tid=prof_contriblnk)
- [robot automation](https://undocumentedmatlab.com/articles/gui-automation-robot)
- [Interesting List of Test Frameworks](https://proxy.c2.com/cgi/wiki?TestingFramework)
    - [Prolog Unit Tests](https://www.swi-prolog.org/pldoc/doc_for?object=section(%27packages/plunit.html%27))
    - [Scheme Unit](http://wiki.c2.com/?SchemeUnit)
    - [List of Assertations - pytruth](https://github.com/google/pytruth)
    - [Matlab Unit Test Framework Constraints](https://www.mathworks.com/help/matlab/ref/matlab.unittest.constraints-package.html)
- The syntax `regexp '' '(?@a=1)'` is equivalent to `eval 'a=1'`.
- [Clipboard](https://www.mathworks.com/help/matlab/ref/clipboard.html)
- [Serializing objects](https://undocumentedmatlab.com/articles/serializing-deserializing-matlab-data)




# Bugs Tracker
- `[ret,r] = EXPECT_MAXTIME( @() tjsr({A,B}), 20 );` returns `true`, but does not set `r` - `MAXTIME`seems to have problems when the called function itself uses the parpool
- `_MAXTIME` and `_MINTIME` do not work
- `freename` does not increase number of digits by itself
- generators not bugfree yet, especially on octave
-  warning function does not report anymore when all warnings are disabled
- `TRACE` currently does not accept evalable strings
- `TRACE` change `h = ['@() { ''' id() ''', handle{' num2str(i) '}() }']; `
to `h = ['@(' id() ') handle{' num2str(i) '}()']; `
- Rename `ttest_id` to `ttest_id_jaksldu893z5kab34giqyalmqpg8oahg780h`.

# Issue Tracker
In order from important to unimportant to impossible


## testrunner
- Multi line comments for doctests
-  search whole matlab path (when filename is given) so tests can be started without being in the correct folder
- `runttests` shall understand Matlab function based unit tests
- run test files in random order
- open `base` section before each function is executed, so each testcase gets a fresh state.

## tests
- add aliasas to string tests: `STR_EQ`, `STR_NE`, etc.. to be consistent with `EQ` tests.
- add global variable to disable lowercase macros
- add tests for symbolic and numeric computations
- make test macros compatible to *MOxUnit* and *Matlab unit test framework*
- Add help text to test macros
- `_FILE_EQ` test: Shall the additional outputs have the processed line endings and caps: Yes.
- add input checks in test-macros before the try-catch block of the test-part, in order to correctly fail if the input is wrong
- installation unit tests are missing: use `restoredefaultpath` and sections
- make exception-ids consistent
- comparison tests accept a `ttest_matcher_c` and a binary function (if at least two elements are not `function_handles`) for comparison. 
- `opentoline` missing in Octave
- `allfunctionhandle` not working in Octave
- nice `repr` output for failed examples (`class2str` and `uneval` from FEX both do not work)
- turning assertions into break points
- allow defining new test macros at runtime, similar to the machinery for matchers

## Severity
If a condition is passed to `disabled`, no warning is issued.

## Test Options
A certain set of options can be passed to test macros, wrappend inside the `OPTIONS(__)` function. The option can be given at any position. Note though that most of the options 
|       Option         | Description                                |
| -------------------- | ------------------------------------------ | 
|  `'flakey'`          | Test is repeated five times and must succeed four times (or something similar) |
|  `'exception',me`    | specifies exception to be thrown when test fails |
|  `'msg',str`        | specifies message to be printed when test fails |
|  `'not_pointwise'`   | *(experimental)* Negates the result of each check in a test | 
|  `'not'`             | Negates the outcome of a test              |
|  `'verbose',val`     | Overrides verbosity level |
**`'not_pointwise'`** This option negates each check in a test. In general it does not negate the result of a test. E.g. the test `EXPECT_EQ( OPTION('not_pointwise'), 2, 3, 3 );` fails.
**`'not'`** This option negates the outcome of a test.  E.g. the test `EXPECT_EQ( OPTION('not'), 2, 3, 3 );` succeeds.


## CI/CD
- Travis CI
- Jenkins. See: [Link](https://github.com/sbesson/matlab-xunit-doctest)
- See how MOxUnit does it

## Property based testing

#### Description of the important functions

#### Shrinking
Works as follows:
1. takes cell array of generated parameters for which the test failed
2. chooses one element of the cell array, stores which cell was chosen
3. shrink chosen parameter using the `shrinker` of this parameter
5. try whether the test still fails. If it fails with the same error/warning/failure (check stack trace, line number, error-id, error-msg) we assume it is the same bug. Otherwise we found a new bug, and do shrinking for that new bug separately. This is for not converging to always the same bug too. Furthermore, we save all bad examples. Hypothesis´ way: [1](https://hypothesis.works/articles/multi-bug-discovery/), [2](https://github.com/HypothesisWorks/hypothesis/pull/836)
    - If so, shrink it again. If the `shrinker` returns the same output as input, or some already returned output, the next parameters is tried.  To compare the returned parameters we `hash` it and use `isequal` (better: `builtin( 'isequal', ...)`.
    - If not, try the next parameter
6. Repeat this until no further shrinking is possible, but at most ~200 times.

#### Sequential keyword
```matlab
strategy( value(4,5), 'name','fof' );
GIVEN( 'sequential', @(fof1,fof2) EXPECT_EQ(fof1,fof2) );
GIVEN( 'sequential', 'EXPECT_EQ(fof1_,fof2_)' );
```

#### Alternative form
For later: The test macros can call the `GIVEN` function. Then both of the following may be possible
```matlab
GIVEN( @(int) EXPECT_TRUE( isfinite(int) ) );
EXPECT_TRUE( @(int) isfinite(int), integer );
EXPECT_TRUE( 'isfinite(int_)', integer );
EXPECT_THROW( @(sqm,int) sqm==int, squarematrix, integer );
EXPECT_THROW( 'sqm_==int_', squarematrix, integer );
```

### Options for strategies
- `'hasher',h` replaces the current hasher with `h`
- `'shrinker',h` replaces the current shrinker with `h`. Can be set to `[]` to disable shrinking.
- `'maxbyte',n` restricts generates examples to be smaller than `n` bytes
- `'map',h` constructs the strategy `map( _, h )`
- `'filter',h` constructs the strategy `filter( _, h )`


### Options for `GIVEN`
- `'sequential'`, `'pairwise'`, `'exhaustive'`/`'cartesian'`, `'inline'`
- `'example',val` explicit examples which shall be tried
- `'database'` controls from where previous failed examples will be tried. Can be set to `[]`
- pass severity: e.g. `GIVEN( EXPECTED, @(flt) isnumeric(flt) )`.
- Options can be passend also using `OPTIONS` function.

### Predefined strategies
|       Strategy         |          Description               |  Additional options | Shorthand |
| ---------------------- | ---------------------------------- | ---------- | --- |
| `midi` | Midi Data | | `'mid'` |
| `image` |       Pictures     | `format`, `resolution`, `bitdepth` | `'img'` |
| `graph` | Graphs | `directed` | `'grph'` | 
| `mrt` | MRT scans | `format` |  `'mrt`' |
| `audio`    | Audiosignals | `format`, `samplingrate`, `bitdepth` | `'aud'`   | 
| `vector`   | Vectors      |                                      | `'vec'`   |
| `function` | Unary function handles |                            | `'fun'``  |
| `byte`     | Vectors of bytes |                                  | `'byte'`  |
| `integer`  | Vectors of integers |                               | `'int'`   |
| `fraction` | Vectors of fractions |                              | `'frac'`  |
| `complex`  | Vectors of complex numbers |                        | `'cplx'` |
- prime numbers
- audio signals(`'wav'`), pictures(`'img'`), graphs, MRT scans (ask Anna)(`'mrt'`)
- matrices (use function `rjr()` for randomizeation), sets of matrices, sparse matrices (Alappatet et al, ECM modelling and perforamcen tuning of SpMV and Lattice QCD an A64FX), University of Florida collection,
(`'sqm'`,`'mat'`,`'vec'`,`'arr'`)
- functions(`'fun'`), functions for optimization:  See: [Robust multi-objective test problems](https://www.mathworks.com/matlabcentral/fileexchange/49776-robust-multi-objective-test-problems?s_tid=srchtitle), ODEs
- [Terrain](https://www.mathworks.com/matlabcentral/fileexchange/39559)
- Midi data(`'mid'`)
- [polytopes](https://www.mathworks.com/matlabcentral/fileexchange/18523-polytopes?s_tid=srchtitle)
- times, dates, Matlab datetimes, timedeltas
- bools(`'bool'`), bytes(`'byte'`), integers(`'int'`), fractions(`'frac'`), floats(`'flt'`,`'dbl'`), complex(`'cplx'`) numbers
- sampled_from
- characters(`'char'`), text, dictionaries(`'txt'`)
- lists, tuples, permutations
- uuids(`'uuid'`)
- email (`'eml'`) addresses, ip addresses (`'ip4'`.`'ip6'`), urls (`'url'`), domains (`'dmn'`)
- fprintf strings
- stuff from [hypothesis](https://hypothesis.readthedocs.io/en/latest/data.html#core-strategies)
- files (on disk)

|       Generator           | Description                           | Example |
| ------------------------- | ------------------------------------- | ------- |
|  `combine( a1, ..., an )`   | where `a1` to `an` are parameter lists, yields all combinations of `a1` to `an`, i.e. construct the Cartesian product of `a1` to `an` | `combine( value(1, 2), value('a','b') )` yields {{`1`,`'a'`},{`2`,`'a'`},{`1`,`'b'`},{`2`,`'b'`}} |
| `limit( 'class' )` | yields a list of interesting values of class `class` | `limit( 'int8' )` yields  {`0`,`1`,`-1`,`2`,`-128`,`-127`,`127`,`126`} |

**Combination of parameter generators** 
Parameter generators can be combined, e.g. `symvpa( values(1,2) )` yields {`1`,`2`,`sym(1)`,`sym(2)`,`vpa(1)`,`vpa(2)`}.


### Adaptors

**compose**
 - `shrink( x )` calls `composite( f ).shrink();` which sets some flag `shrinkflag=true` such that the strategies `st1` and `st2` inside `f` know they shall shrink and set the state of the `rng` such that the strategies inside hopefully draw the same example again.

**flatmap** 
`flatmap` takes a function and a strategy , and (to say it in the words of MacIver) *draws an example, then turns that example into a strategy, then draws an example from **that** strategy*.  If this explanation is hard for the reader to grasp, we show how `flatmap`  would be implemented using `compose`. `flatmap( @(x) st2(x) )` is `compose( f )` where 
  ```matlab
  ex = function f(); 
      ex = st2(x).draw();
  ```

- [**deferred**](https://hypothesis.readthedocs.io/en/latest/data.html#hypothesis.strategies.deferred) A deferred strategy allows you to write a strategy that references other strategies that have not yet been defined. This allows for the easy definition of recursive and mutually recursive strategies. For implementation see: [Functional programing in Matlab](https://www.mathworks.com/matlabcentral/fileexchange/39735-functional-programming-constructs). Example usages:

  ```python
  x = st.deferred(lambda: st.booleans() | st.tuples(x, x))
  a = st.deferred(lambda: st.booleans() | b)
  b = st.deferred(lambda: st.tuples(a, a))
  ```


### Exhaustive tests
- If we want also Octave compatibilty, we probably need to get rid of Mathworks pairwise generator
- [Latin squares](https://www.mathworks.com/matlabcentral/fileexchange/9996-ballatsq?s_tid=prof_contriblnk)
- API:
    ```
    GIVEN( value(1,2,3), value(4,5,6), 'cartesian', @(x,y) EXPECT_NE(x,y) );
    GIVEN( value(1,2,3), value(4,5,6), 'pairwise', 'EXPECT_NE(a_,b_)' );
    GIVEN( value(1,2,3), value(4,5,6), 'inline', @(varargin) EXPECT_NE(varargin{:}) );
    GIVEN( value(1,2,3), value(4,5,6), 'inline', 'EXPECT_NE(a_)' );
    ```



#### Assigning of parameters to variable
By default, all possible combinations of parameters are generated. To change this behaviour, add an option right after `TTEST_PARAM`/`PARAM`.

|     Option                    |  Explanation  |
| ----------------------------- | ------------- |
| `'cartesian'`/`'exhaustive'`  | All possible combinations are generated |
| `'sequential'`/`'sequence'`   | Tests are only made for corresponding test. In particular, all parameter lists must have the same number of elements |
| `'pairwise'` | Generates a set of combinations where each pair of parameters is at least included once. **[5]** |
**[5]** Note that this option uses code from *TheMathworks*. Thus, you must have a Matlab version newer R2016b, since in older releases this function is not included. You must not and cannot use this function in Octave.


#### `'inline'` option
One can give additionally the option `'inline'`after `PARAM` which causes that only one test is made with all possible combinations "concatenated". For example, the following test are equivalent
```matlab
EXPECT_LE( 'a_', PARAM,'inline',values(1,2,3) );
EXPECT_LE( 1, 2, 3 );
        
EXPECT_EQ( @(x)x, double(1), PARAM,'inline', ttest.type(1,'int8','int16') );
EXPECT_EQ( int8(1), double(1), int16(1), double(1) );
```
By default, only parameters which are marked with an `x` in the *List of Tests* table are repeated. For example, the following tests are equivalent
```matlab
EXPECT_PRED( '@and', 'a_', ...
    PARAM, 'inline', ...
    values(1,2) );
EXPECT_PRED( @and, 1, 2 );
```
To change this behaviour, one can add an option right after `'inline'`, which must be a scalar or a 2-element vector. The 2-element vector determines which arguments are to be repeated. The vector is 1-index based, the first argument of the interval is included, the second is excluded. A negative number can be given, which then is counted from the back. For example, the following test are equivalent
```matlab
EXPECT_EQ( '2', 'a_', 'b_', int32(2), ...
    PARAM, 'inline', [2 -1], ...
    value(single(2)), value(int8(2),int16(2)) );
EXPECT_EQ( 2, single(2), int8(2), single(2), int16(2), int32(2) );

EXPECT_WARNING( '@()inv(0)', 'a_', ...
    PARAM, 'inline', [2 0], ...
    value('MATLAB:singularMatrix') );
EXPECT_WARNING( 'inv(0)', 'MATLAB:singularMatrix' );
```



### Minor stuff
- Make try catch blocks around generating of parameters and testing of examples so that all errors are caught at the right place and produce an informative error message
 - Feature for later: The function `CONDITION` can be used in the functions-under-test to signalize to the generators when a constructed example is non-suitable for the test (same as `assume` in hypothesis). If a `condition` fails, it throws an exception `TTEST:CONDITION` error, which is caught by the generator.

### Function property tests
These are tests for general properties of functions
The test macros try to find out by themselves the domain and range of the function
```matlab
EXPECT_SELFINVERSE( @f );
GIVEN( 'metric', @f );
GIVEN( int, @f, 'selfinverse' );
```
- Possible properties to be checked
  - idempotent
  - nilpotent
  - inner product, norm, metric
  - monotone
  - constant
  - commutative
  - assoziative
  - equal (tests *for all `x`: `f(x)==g(x)`*)
  - total order, partial order
  - smooth, C0, C1, etc...

## benchmark tests
- testing cyclotomatic complexity: [code metrics](https://www.mathworks.com/matlabcentral/fileexchange/10514-code-metrics), [McCabeComplexity](https://github.com/mathworks/TestingMcCabeComplexity)
- testing execution time
- testing complexity O(1), O(log n), O(n), O(n log n), O(n^2^), O(n^3^), O(2^n^), O(n!)
- memory usage (not possible probably)
- check shadowing of functions
- https://github.com/woodrush/MATLABvargraph
- [paralellel tic toc](https://blogs.mathworks.com/pick/2014/08/01/time-parallel-for-parfor-loops/)
- See [google/benchmark](https://github.com/google/benchmark/blob/master/docs/tools.md)
- See [nanobench](https://nanobench.ankerl.com/tutorial.html#asymptotic-complexity)

## Sections
- We do not make ~~`SCENARIO()`, `GIVEN`, `WHEN`, `THEN`~~, since we do not want `BDD`.
- `TESTCASE`/`SECTION`/`SUBSECTION( 'Name' )` start a section *Testcase*/*Section*/*Subsection* with name *Name*. 
- If a function handle is passed, the id is put into the stack trace, and the handle is evaluated. A test macros error handler can then find the id in the stack trace. That way, I would not need to pass the stack trace into subfunctions

### Options for `SECTIONS`
From important to unimportant
- conditional execution of sections.  A `SECTION` returns `true`/`false`, and thus, can  be used in an `if` statement. For the beginning, a section always returns `true`
```matlab
if( TESTCASE( 't1', DISABLED('onoctave') ) );
    EXPECT_EQ( 2, 3 );
end;
```
**If a section returns `false` and the return value is captured, then no new section is started.**
- `'setup'`/`'tearup'`,`val` and `'setdown'`/`'teardown'`,`val` where `val` is a `eval`able string or a function handle. Defines setup and teardown functions.
- tags, [see Catch](https://github.com/catchorg/Catch2/blob/devel/docs/test-cases-and-sections.md#tags), `SECTION( name, ['[tag1]', ..., '[tagn]'] )`  
- `'reportsideeffect'` keeps track of as many things possible, and tells the user aftwards which side effect happend
- parsing of pre/suffixes `'DISABLED'`, `ENABLED`, `TODOED`, `EXPECTED`, `ASSERTED` in the section name and arguments of class `ttest_severity_c` and appropriate handling of this stuff (To be done by TM).
- If a section is disabled one of two things happen 
  - (1) they throw an error (default for `TESTCASE`). Furthermore they assignin a variable which on destruction calls `ENDTESTCASE` and checks whether the error thrown is from `TESTCASE` or from an error of the tests.
  - (2) they return false (default for `SECTION` and `SUBSECTION`)
  They than can be used as
    ```matlab
    try; TESTCASE( false );
        if( SECTION( false );
        end;
    end;
    ```

| What is changed back and when    |     Option    | Section | After | Section | After   |
| ---------------------------------|---------------|:-------:|:-----:|:-------:|:-------:|
|                                  |               | **Mat** |**lab**| **TT**  | **est** |
| Variables                        |               |   Yes   |  Yes  |   Yes   |   Yes   |
| Matlab path                      |               |   No    |  Yes  |   Yes   |   Yes   |
| global variables                 | `'global'`    |   No    |  No   | opt out | opt out |
| persistent variables?            | `'persistent'`|   No    |  No   | not possible  | ... |
| state of user-defined/global RNG | `'rng'`/`'prng'`|   No    |  No   | opt in  | opt in  |
| figures                          | `'fig'`/`'figure'`|   No    |  No   | opt in  | opt in  |
| base workspace                   | `'base'`        |   No    |  No   | opt in  | opt in  |
| working directory                |               |   No    |  Yes  |   Yes   |   Yes   |
| `import`s                        |               |   Yes   |  Yes  |   Yes   |   Yes   |
| debugger breakpoints             |               |         |       |    ?    |    ?    |

### Notes
- Persistent variables are not possible most likely. 
  -   But we can set the state of functions back using `clear functions` and/or `clear <functionName>`.   
  - try saving a function handle to a file and reload it
- *rng*: Saving the state of user-defined RNG is straightforward. API: `'rng',false` and `'rng',[]` disables this. `'rng','global',r1, ..., rn` saves the global and the streams `r1` to `rn`. Global rng-state is always saved if option is not given. If `'rng'` option is given, global rng-state is only saved if option 'global' is given. `r1`, ..., `rn` are the rng objects. 
- *figures*: API: `'fig',???`, Save figures using `savefig(H,filename,'compact')`. Maybe use the `'compact'` option which can reduce the size of the `.fig` file and the time required to create the file.
See [.fig is .mat](https://undocumentedmatlab.com/articles/fig-files-format)
- `import` behaves differently in Matlab and Octave.
- [Matlab documentation about `clear`](https://de.mathworks.com/help/matlab/ref/clear.html#btdyzbv-1-ItemType)
  - `clearvars` clears only variables
  - `clear` does not clear `import`s
  - `clear all` does clear `import`s

## General purpose tests

These three tests are special, since 
1. they are *experimental*, in particular they may get renamed,
2. they do not have a suffix, thus their names are `TODO`, `EXPECT` and `ASSERT` and
3. they try to figure out what to test from their given arguments.

In the following we only discuss the `EXPECT` test.
| Name                          | Short explanatation |
| ----------------------------- | --------------------|
| `EXPECT( value )`             | tests whether `all(value)` is `true`                   |
| `EXPECT( function_handle )`   | tests whether `function_handle()` does not throw       |
| `EXPECT( 'cmd' )`             | tests whether `evalin( 'caller', cmd )` does not throw |

## `TTEST` function
- save already used id´s (in particular from users) such that no duplicates are generated
- documentation missing
- Easy syntax to copy global variables to local variables - Easier than `TTEST('var','all',TTEST('global','var','all'));`
  Maybe do
    ```matlab
    TTEST( id_from, id_to, 'var','name' );
    TTEST( id_from, id_to, 'err' );

    TTEST( id_from, id_to, 'copy', 'err' );    
    
    TTEST( id_from, 'copy_to', id_to, 'err' );
    TTEST( id_from, 'copy', id_to, 'err' );
    ```

### changing error handlers
If the settings value is `'todo'`/`'exp'`/`'ass'` then the default `TODO_`, `EXPECT_` `ASSERT_`  error handler is called. Their behaviour is
 - `'todo'` If a test fails, then `TTEST_ERROR_TODO_WORKER()` is called, which prints out a summary of the failed test. `true` is returned. If a test succeeds, a message is printed that a `TODO_` test succeeded.
- `'exp'`If a test fails, then `TTEST_ERROR_EXPECT_WORKER()` is called, which prints out a summary of the failed test and the `TTEST` variable `errflg` is set to true. If a test succeeds, nothing happens.
 - `'ass'` If a test fails, then `TTEST_ERROR_ASSERT()` is called, which sets the global variable `TTEST_ERRORFLAG` to `true` and afterwards calls `assert(false)`, which is catched and rethrown (to have nicer Matlab output).  If a test succeeds, nothing happens.
    - If the queried variables value is a `function_handle`, it is evaluated, if the queried variables value is a `string` or `char`-array, it is passed to `feval`. This allows to define custom error handlers.
```matlab
TTEST init
TTEST todo exp  % makes all TODO_ tests to EXPECT_ tests
TTEST errass fprintf('Failure');
```

## Test macros / Matchers
- Make error output text consistent: `', where'` vs `'. Where'`
- Shall matchers have the `Is` prefix, e.g. `IsFalse( )` instead of `False( )`? - No, but it seems it must be some cases
- See: https://hypothesis.works/articles/threshold-problem/
- `_MINTIME` macro needs some option to specify number of output arguments of function
- `_...TIME` macros accept a function handle as second input, whose execution duration is then used
- `ALMOST_EQ` function needs to be partly rewritten  
- `_DEFINED( cmd )`, tests whether `cmd` makes syntactically sense, i.e. Matlab can parse the command |


## Legacy Code
### `CACHE`
   - `shared` integer, default = 0 (means not-shared), if greater than 0 this indicates that also other processes may write to the file, and thus Goldstandard has to check whether the content has changed before accessing. Furthermore,  multiple attempts to read write data are performed each time. Value of `shared` is the interval in seconds how often 
  
Function should throw when
  - file `name` exists, but could not open it
  - file `name` does not exist, and could not create it
  - given `'name'` of file is a bad name
 
### `at` functions
- `messageat`: convenience function
- `assignat`: when only one struct is given, all variables in the struct are assigned


### misc stuff
- Run program with some lines commented out (See: Seams in WEWLC)

### Disabling test files

- *(experimental)* To disable sections of tests, put the section in an 'if' block and append `'DISABLED'` to the name of a section. E.g. change `TEST('test_1')` to 
```matlab
if( TEST('DISABLEDtest_1') )`
    EXPECT_EQ( 2, 3 );
end;
```
- *(experimental)* Another possibility is to add the function `DISABLED` as an argument to the section. E.g. change `TESTCASE('test_1')` to `TESTCASE( 'test_1', DISABLED )`.

 
## Logging
### functions
- `INFO_ONCE()` *(for later)* : Only the next called test would print the message (do not implement this yet).
- `messageat()` *(for later)* : Convenience function
### Report Hooks
- See  [Criterion](https://criterion.readthedocs.io/en/master/hooks.html)



## File documentation
- change order from: "signature/short-explanation" to "short-explanation/signature", because than the output from Matlabs help ( e.g.: `help TTEST` ) is nicer.


## Inject
- how does the injected functions get their `ttest_id`?
- May use moduls: [module](https://www.mathworks.com/matlabcentral/fileexchange/38014-module-encapsulate-matlab-package-into-a-name-space-module)
- add persistent variable `numcall`
- add option `'mutationconditions'`
- add variable `callline`
- add variable `callfunction`
- add variable `delay`
- add option `'dir'` to change behaviour from builtin-function to .m functions and to explicitly set the directory where one shall change
- call `rehash` after generating function


 
## Test macro  matchers
- Comparison macros `AnyOf`/`AllOf`/`NoneOf` should be extended to allow things like
    ```matlab
    _NEAR( 10, 11, RelativeError(0.1) );
    _NEAR( 10, AnyOf(11,15), RelativeError(0.1) );
    ```
  - different norms (1, 2, p, inf, 'fro')
  - matrix norms/vector norms
  - metrics
  - double/single precision
  - absolute/relative difference
  - difference with respect to smaller/larger floating point entry
  - weight functions for norms
  - 
## Dependency tests
 - check all dependencies (of functions/toolboxes/auxiliary files/...) and compare with a list of allowed dependencies


## testing of figures
- save all figures to files
- add test comparing result of plots/figures

## Tab completions
- See [User-defined tab completions](https://undocumentedmatlab.com/articles/user-defined-tab-completions-take-2), [Matlab documentation](https://de.mathworks.com/help/matlab/matlab_prog/customize-code-suggestions-and-completions.html)
- Tab completion for `CACHE`, `POSTCONDITION`, `TTEST`


## Compatibility
- Test which checks for function calls which may make problems

## code coverage
- See [MOcov](https://github.com/MOcov/MOcov)

## Stateful testing
E.g. For a database `d` which has the methods `add`, `remove`, we want to test sequence of function calls like `d.add(23)`,`d.add(23)`,`d.remove(23)`,`d.remove(23)`.
Approaches found so far:
- compare two implementations
- run test-invariant function after every method call
- call methods and use asserts in the production source-code to check for errors


## add spell checker
This can extract all strings
```matlab
TREE = mtree( 'plotm.m', '-file' );
STRINGS = A.strings;
idx = cellfun( @isempty, STRINGS );
STRINGS = STRINGS(~idx);
idx = cellfun( @(x) isequal(x(1),'''') && isequal(x(end),''''), STRINGS );
STRINGS = STRINGS(idx);
```
`mtree` function is not available in Octave

Function `correctSpelling` is introduced in R2020a


## testing private/nested/local functions
### recursive retrieval
- get local functions in private functions
- get nested functions in local functions
- get nested functions in nested functions
- etc...
### nested functions
  - via handles `allfunctionhandle()` - does not return nested functions. One can use:
```matlab
fileData = mlintmex('-calls',which('static'));
fileData = regexp(fileData,'[NS](\d+) (\d+) \d+ (\w+)\n','tokens');
```
  - what to do with dependencies of variables in the mother function of a nested function
    - put them into a another function, define there the missing variables
  
### classes
  - one can define classes which are allowed to access private members [(Link)](https://www.mathworks.com/help/matlab/matlab_oop/selective-access-to-class-methods.html) :
```matlab
classdef READ
    methods(Static)
        function s = get( obj, name );
            s = obj.(name);
        end;
    end
end
```
In the class in question one has to do
```matlab
classdef PRIVATE
   properties ( Access = {?READ} )
        priv; % the private member,  is allowed to access
   end
end
```
  Now, `READ.get( p, 'priv' )` can read the member `p.priv`, where `p` is of class `PRIVATE`.
- `struct(class)` converts the members of a class to a struct, *including* the private data members.
  - [find-and-replace-in-files](https://www.mathworks.com/matlabcentral/fileexchange/42877)

## Mutation Tests / Fault injection *(experimental)*
*This part of the *TTESTs* is experimental, thus these features may be removed (unlikely) and/or the API is subject to be changed in future releases (very likely).*

Although mutation testing and fault injections serve different purposes in unit testing, their implementation and use is quite similar. Thus, we discuss them together.
*Mutation testing* tries to assess the unit tests by identifying tests which have not been added but possibly are necessary. It does this, by modifying (mutating) the source code on purpose and then runs the test suit which should clearly fail now. If all tests still pass, this is an indication that either, 
- the part of software which was mutated is not reached by the test suite, or
- the wrong output of the program is not assessed.

*Fault injection* on the other hand helps to understand how a function behaves when used in unusual ways, by introducing faults to test code paths, in particular error handling code paths, that are mostly rarely followed.

The *TTESTs* support two ways for mutation testing and fault injection. One can either 
1. overload Matlab functions, or
2. modify the source code of the function to be tested.

### Overloading Matlab functions - `INJECT` (currently broken)
This technique creates overloads for a given function which is then called instead of the original function. The overloaded function can alter the call and/or throw an error. Due to some Matlab implementation detail, the overhead for this technique is small for *builtins*, but quite large for *not-builtins*. To create overloads one must call the function `INJECT`. `INJECT` expects 0, 1 or more arguments.
```matlab
INJECT( name, [faultchance], [faultconditions], [mutationchance], [mutations], [classes] );  % creates overloads
INJECT( name );  % deletes all overloads of function 'name'
INJECT();  % deletes all overloads
```
`faultchance` is the chance that the function will throw an error, `faultconditions` are conditions when the function shall throw an error, `mutationchance` is the chance that the function call is altered as dfined by `mutations`. `mutations` and `faultconditions` are *cell array of strings* of Matlab evaluatable commands. The input variables are numbered from `i1` to `i10`, the function to be called is a string named `fun`. To keep the overhead small for builtins, it is not possible to replace the function `fun` with a non-builtin function. For example, this is not allowed: `fun=''1'';`. If one wants to overload functions with more than 10 input arguments, one has to change the source-code of `INJECT.m`. `classes`  is a cell array of strings. If given, then only overloads for these classes are generated.
If no mutations are given, but `classes` are given, one should set `faultconditions` to `[]` in order to prevent confusion of `faultconditions` with `classes`.
If `faultchance` and `mutationchance` are zero and `faultconditions` is empty, then all overloads of this function are deleted. The overloads are disabled until the global variable `TTEST_ENABLE_INJECTION` is set to true. This can be done with the call `INIT( 'inject',true )`.  Note that, every call to `INJECT` sets the value of `TTEST_ENABLE_INJECTION` to false. 

We present the overloading of functions on some examples.
``` matlab
INJECT( 'zeros', ...
        .05, {'i1*i2>=10'}, ...
        .2, {'fun = ''ones'';', 'i1 = i1.*0;', 'x = i1; i1 = i2; i2 = x;', ...
        {'double',''} );
TTEST('inject',true );
```          
This call generates overloads for the function `zeros` for the types `double` and for non-specialized input. With probability *0.05* a call to `zeros` results in an error, also whenever `i1*i2>=10` an error is trown. With probability *0.2* the function call gets mutated using on of the following mutations (as defined by the given cell array):

- Replace `zeros` with `ones`
- Multiple first argument with 0
- Swap first and second argument

To delete all overloads of `zeros` one can call `INJECT( 'zeros' )`. To delete *all* overloads generated by `INJECT`, one can call `INJECT()`.

We give another example how to asses the quality of a test suite. The following test is repeated until a mutant is found which survived. This will happen quite fast, as should be obvious from the code. 
```matlab
INJECT( 'zeros', 0, .1, {'fun=''ones'''} );  %s hadow zeros operator

while( true );  % loop until we found a surviving mutant
    TTEST( 'clear' );  % reset everything
        
    % test - suite
    TTEST( 'inject',true );  % enable overloads
        x = zeros( 1,100 );
        EXPECT_THAT( x, ttest.NumelIs(100) );
        % EXPECT_THAT( x, ttest.Each(ttest.Eq(0)) );  % disabled important test
    TTEST( 'inject',false );  % disable overloads
        
    if( ~TTEST('flag') && TTEST('mc') );  % check if mutant survived
        break; end; end;  % stop if mutant survived

TTEST( 'p' );  % print out statistics
```    

#### Very important notes

- Some functions must not be overloaded, or overloading them does not have the expected behaviour
  - In particular: `builtin`, `lt`, `ge`, `switch`, `function`, `end`, `switch`, `for`, `while`, ... . Shadowing those most likely will break Matlab.
  - Functions which have no side effects originally - Matlab may cache the result of such calls.
  - *private* functions - Private functions have higher precedence than any other functions on the path, and thus, shadowing them does not work.
  - Overloads of user-defined classes are only generated if the class is loaded or the function to be overloaded is defined in a separate file in an @-folder or in a package.
- All *TTEST* functions set `TTEST_ENABLE_INJECTION`temporarily to false. Thus, inside their body, all overloads are disabled. *[5]*
- The current working directory should be the directory where the overloads are stored. Otherwise, Matlab may still use the original functions.
- The calls `INJECT()`, `INJECT( 'xxx' )` should only delete files which start with `'%TTEST_AUTOGENERATE_INJECT`. Nevertheless:

  *THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.*

*[5]* In fact they are still active, but keep the function call unchanged.

### Source Code modifications

4. **Mutation Tests** Copy function to folder, add that folder to Matlab Path, change working directory to that folder, mutate this function,



## JSON output
- [JSON output](https://undocumentedmatlab.com/articles/json-matlab-integration)
- https://blogs.mathworks.com/pick/2015/04/24/parsing-json-files/

## System information
- [System information](https://www.mathworks.com/matlabcentral/fileexchange/18510-matlab-performance-measurement?s_tid=srchtitle)
  
## threads
- Add test for using variable number of threads

## Things which are likely not possible to do

### Matchers:
- make matchers to class which support `||` and `&&` (most likely not possible)

### Test parsing
- parsing of test macro input, so that `EXPECT( 2 > 3 )` is possible

### Rounding tests
- Change rounding mode to find numerical instabilities
- [See](https://stackoverflow.com/questions/55682034/how-to-change-the-rounding-mode-for-floating-point-operations-in-matlab/55768421)



# Matlab/Octave compatibility
- [Octave-wiki](https://wiki.octave.org/Differences_between_Octave_and_Matlab)
- Missing functions:
  - `import`
  - `gcp`
  - `MException`, `getReport`, `throwAsCaller`
- Problematic functions:
  - `subsref`,
- Octave does not understand '-v7.3' file format
- no use of nested functions due to different scoping rules
- do not use `"strings"`, just use `'char-arrays'`. In Octave both are the same, whereas in Matlab they are not. Thus, convert `"strings"` to `'chars'`. 
- Operators `&` and `|` behave differently
- Matlab has maximum length of variable name, `_` not at beginning of variable
- `dbstop` does not return value in Matlab
- nearly no JIT in Octave
- Octave uses less copy-on-write than Matlab (according to some places in the documentation)
- Use `any(strfind())` instead of `contains()`
- Exception ids are different. Octave allows the use of `'-'` in an id, and even uses it in its ids.



