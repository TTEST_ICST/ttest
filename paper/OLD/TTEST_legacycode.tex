\documentclass[conference]{IEEEtran}
\IEEEoverridecommandlockouts
% The preceding line is only needed to identify funding in the first footnote. If that is unneeded, please comment it out.
\usepackage{cite}
\usepackage{amsmath,amssymb,amsfonts}
\usepackage{algorithmic}
\usepackage{enumitem}
\usepackage{graphicx}
\usepackage{textcomp}
\usepackage{multicol}
\usepackage{multirow}
\usepackage[dvipsnames]{xcolor}
\usepackage{matlab-prettifier-ttest}
\lstset{basicstyle=\ttfamily, keywordstyle=\bfseries, style=Matlab-TTEST}
\usepackage[bookmarks=true, breaklinks=true, colorlinks=false, pdfborder={0 0 0}, pdfa, draft=false]{hyperref}

\usepackage{selnolig}  % disable illegal ligatures

\definecolor{remark}{RGB}{034,139,034}
\definecolor{keyword}{RGB}{0,0,255}

\begin{document}

\title{Handling legacy code with the TTEST framework}

\author{\IEEEauthorblockN{1\textsuperscript{st} Thomas Mejstrik}
\IEEEauthorblockA{\textit{NuHAG} \\
\textit{University of Vienna}\\
Vienna, Austria \\
tommsch@gmx.at}
\and
\IEEEauthorblockN{2\textsuperscript{nd} Clara Hollomey}
\IEEEauthorblockA{\textit{ARI} \\
\textit{Austrian Academy of Sciences}\\
Vienna, Austria}
}

\maketitle

\begin{abstract}
Code instrumentation, 
i.e. changing the behaviour of a program without changing its source code,
is an already used tool in unit testing. 
It is used for 
  mutation testing (e.g.\ in \emph{Javalanche}),
  fault injection,
  measuring code coverage, 
  injection of assert statements or logging capabilities (often in C), etc..

\end{abstract}

\begin{IEEEkeywords}
unit testing, code instrumentation, seams, gold standard testing, Matlab hacks
\end{IEEEkeywords}

\section{Introduction}

What legacy code handling comprises:
\begin{itemize}
    \item debugging
    \item refactoring
    \item keeping periods of adjustment short
\end{itemize}

It needs to be automated to save time and because noone would do it otherwise.\\

Typical tools are:
\begin{itemize}
    \item ensuring that certain preconditions/postconditions are met
    \item Code instrumentation
    \item Snapshot/Goldstandard/Master tests
\end{itemize}

Why we need it for Matlab:
\begin{itemize}
    \item Matlab itself provides insufficient tools (even though it introduced its Project suite with version 2020)
    \item many research departments have an ever growing code base on which they rely as tools for their research
    \item even with the ever-increasing popularity of Python, these code bases are unlikely to go away
    \item rather, there are projects like e.g. the Auditory Modeling Toolbox \cite{amt} that collect such code from various authors, further increasing the need for refactoring
    \item researchers are not necessarily software engineers
    \item Matlab is still a very popular programming language
    \item Matlab itself has undergone several changes during the last years (multithreading pool, object orientation, ...) that make even performance analysis necessary (further side topic: adopting Matlab code for running on a GPU for Machine Learning applications)
\end{itemize}

Why we need it particularly in scientific environments:
\begin{itemize}
    \item high fluctuation of responsible personnel: within a 2 years PostDoc, there is not much time for getting acquainted to code, particularly if your actual field of research is something else
    \item the expectations on the accuracy/correctness/reproducibility of the code are high
    \item no specialized knowledge should be required for ensuring that the code works (because people are specialists in other fields)
    \item people often add bits to existing code (e.g. an if clause), potentially changing code coverage, and noone will ever know
    \item people work often on their own and not in teams, there usually is no code review process etc.
    \item it is often important that one can ensure that one replicates the precise setup that has been used by researcherXY 5 years ago
    \item code has often not been written with maintainability and extensibility in mind (or reusability, in the first place)
\end{itemize}

Ultimately, and in summary, no such thing exists.\\

Here, we describe a library for handling legacy code, specifically targeting scientific code bases in Matlab. This paper is organized as follows: in section II, we briefly introduce the TTEST framework, in section III,
we discuss code instrumentation and its usage in software engineering, in section IV, we describe our implementation, along with Matlab-specific challenges and design considerations, in section V, we give results/examples, in section VI, we give an outlook.

\section{The TTEST framework}
TTEST is a a unit test framework written in Matlab/GNU Octave specifically targeted at scientists.

TTEST features:
\begin{itemize}
    \item Test macros
    \item Matchers
    \item Test runners with sections
    \item Property based tests
\end{itemize}
In addition, it provides users with 
\begin{itemize}
    \item the ability to adjust test severity
    \item logging functionality
    \item several utility functions
    \item ...
\end{itemize}

It is largely compatible with Octave, comprehensive, and is the fastest unit test framework out there.

\section{Code instrumentation}
BACKGROUND ON CODE INSTRUMENTATION, PRECONDITIONS, GOLD STANDARD\\
With \emph{code instrumentation} we mean 
changing the behaviour of a program without changing its source code.\footnote{%
The term \emph{code instrumentation} also refers to many other things, 
including changing the source code in place to add profiling or logging capabilites. 
Thus, the term \emph{code injection} would be better suited.
But, \emph{code injection} on the other hand could be mistaken for \emph{dependency injection}, 
a technique which has nothing to do with injection\dots.
}

The technique of runtime code injection is widely in use. 
For example, add-ons for the internet browser \emph{Firefox} were a long time implemented in this way. Also the \emph{Nvidia} driver for Linux uses patches.\\

Code injection is a common prerequisite for several debugging and performance analysis techniques, e.g. to validate the parameters given to a function call, determining the signal flow, or timing measurements. 
For unit tests the technique is already in use for 
mutation testing~\cite{Schu2009}\footnote{%
Mutation testing is used to assess the quality of a unit test suite.
In the SUT errors are injected on purpose (the source code is mutated) 
and it is checked whether the unit test suite fails with this wrong functions.
If the unit test suite not fails, it may indicate the some parts of the SUT 
are note tested yet.},
coverage testing~\cite{fw_moxunit},
or profiling (e.g.\ \emph{gprof}).
In our \emph{TTEST} framework we use code instrumentation for tools 
which help to refactor and unit test functions as well as for evaluating \emph{TTEST} itself.\\

\paragraph{Mutation Testing}
We present how code instrumentation may be used for mutation testing. 
Mutation testing is usually done in the following way:
\begin{itemize}
\item An external program generates a mutated program given the original source code.
\item The new program is compiled (if necessary).
\item The mutated program is executed.
\item Test test suite is run and checked whether it fails
\end{itemize}
The need of external software has severe drawbacks. 
Often the external software is written in another language or has special dependencies. 
Both can increase the effort to maintain the test suite enormously. 
Also, for compiled languages mutating the source code implies 
that the code has to be recompiled for each mutant, 
which is very time consuming. 
Using code instrumentation the mutation part can be incorporated into the unit test suite, 
written in the same language. 
An example interface in Matlab would look like the following
\begin{lstlisting}
MUTATE( 'func' );
\end{lstlisting}
The function \verb|MUTATE| injects code at some lines, for example
\begin{lstlisting}
if( randi(10)==1 ); x = x + 1; end;  x = x + 1  
    % injected code plus original code afterwards
\end{lstlisting}
and afterwards repeats the test suite until a surviving mutant has found.
Thus, a minimal implementation of \verb|MUTATE| looks like
\begin{lstlisting}
function r = MUTATE( name );
inject_mutations( name );
for i = 1:300;
  r = rng();  % store state of RNG
  if( runttests );
    fprintf( 'Surviving mutant found' );  
    break; end; end;
    
\end{lstlisting}
Via code injection, some classical operations of mutation testing may not be possible, 
due to restrictions in the language. 
For example, skipping code lines is not possible in Matlab 
due to the lack of a \verb|goto| function.

\paragraph{Fault Injection}
Obviously, the technique of fault injection is also possible to implement with runtime code injection and the same advantages as above apply.

\paragraph{Optional: Goldstandard testing}
Mutation testing is often complemented with Goldstandard testing...\\

\section{Code injection implementation in \emph{TTEST}}

\emph{TTEST} comes along with some test macros and helper functions 
especially targeted at scientific software. 
\emph{TTEST} is not the only framework which has those, 
but their usage is the most easiest of all frameworks we know.\\

\textit{Wahrscheinlich müsste man hier noch irgendwo function handles in Matlab/Octave erklären, und wie sie in TTEST benutzt werden. Evtl. macht es auch Sinn, diese Section hier in 2 zu teilen, 1x Matlab-spezifische challenges/Rahmenbedingungen, einmal tatsächliche Implementation.}\\

For a correct unit test suite it is of absolute importance, 
that the injected code does not alter the behaviour of the injected program, 
or at least, 
does not alter the behaviour in an unintentional way. 
Thus, the \emph{TTEST} framework contains a lot of convenience functions 
which should cover the most use cases in unit testing, 
and thus spares the user to resort to write the code instrumentation code by herself.
The convenience functions are
\begin{itemize}
\item \texttt{\color{keyword}assignat} for assigning values to variables
\item \texttt{\color{keyword}captureat} for capturing the value of variables
\item \texttt{\color{keyword}flowat} for testing the control flow
\end{itemize}

Apart from the convenience functions, there is also the generic function \texttt{evalat} 
which executes user defined code. 
If this function is used, 
then it is the users responsibility that the injected code 
does not alter the behaviour of the injected program in an unintended way.\\

The \emph{TTESTs} come with a rudimentary implementation of fault injection/mutation testing via the function \verb|INJECT|, but currently it is fully undocumented due to its very experimental status.

\subsection{Features relying on code instrumentation}
\paragraph{\texttt{at} functions}
These are functions which will be barely necessary in well structured code,
but they can prove very beneficial for refactoring code.
The \verb|at| functions consists of the set
\texttt{\color{keyword}assignat}, \texttt{\color{keyword}captureat}, 
\texttt{\color{keyword}flowat} and \texttt{\color{keyword}inputat} which
assign variables, read variables, test the control flow
of a program or make the function \verb|input| non-blocking 
in any function which resides in an \verb|.m|-file, respectively.\footnote{%
We call these functions \texttt{at} functions, 
since their usage is similar to Matlabs \texttt{in} functions, 
\texttt{evalin} and \texttt{assignin}.}

Additionally there is the function \texttt{\color{keyword}evalat} which injects (nearly) arbitrary code into a function.
Clearly, using \verb|evalat| it is easy to alter the behaviour of the program through a unit test
in a non-intended way, and thus this function must be used very carefully.\footnote{%
We try to follow the spirit of C++:
\emph{``C++ makes it harder [to shoot yourself in the foot], 
but when you do, it blows your whole leg of'', (Bjarne Stroustrup).}}

\paragraph{Early Return}
The \emph{experimental} function \texttt{\color{keyword}errorat} can be used to programmatically return early from a function.
We do not count it as part of the \verb|at| functions since it is quite unreliable. It works as follows,
it injects an error at a user defined position in a function. 
This error just has to be catched and one achieved an early return.

\paragraph{Tracing}
Using \texttt{\color{keyword}TRACE} one can trace
from where a function was called. With some more work one can also
manipulate the workspace of the caller, even when it is farther away
than 1 in the stack trace. Something which is not possible with
\texttt{assignin} and \texttt{evalin}.

\paragraph{Executing subfunctions}
In Matlab it is not possible to call subfunctions. 
Only the very first function in an \verb|.m| file can be called by the user.
But this is only half the truth. 
If one is able to get a function handle to such a function, 
it is possible. Even more, such handles can be stored to a \verb|.mat| file. 
It is unclear, why there is no way in the Matlab language to generate such function handles programmatically. 
One way would be to reverse-engineer the format of \verb|.mat| files 
of stored function handles.\footnote{This would probably be a task for Yair Altman~\cite{AltUndoc}}
We chose the path using code injection to get function handles to subfunctions.




\paragraph{Pre and postconditions}
\emph{TTEST} uses lazy evaluation for post conditions. 
A postcondition is a condition which has to be true at the time when the function returns.
With \emph{TTEST} it can defined with the function \texttt{\color{keyword}POSTCONDITION}.
It accepts one function handle, the variables used for defining the postcondition
are those which are checked when the function returns. We give an example: 
The function \verb|sortbackwards| shall sort a vector in backwards order.
We add a postcondition which checks at return time whether the input 
and output vectors have the same number of elements. 
\begin{lstlisting}
function [ ret ] = sortbackwards( v );
  POSTCONDITION( @(ret) numel(ret)==numel(v) );
  v = sort( v );
  ret = v(end:-1:1);
\end{lstlisting}
It works as follows, at time of creation the variable \verb|v| is captured.
At the time of return the variable \verb|ret| is passed to the function handle 
and the test \verb|numel(ret)==numel(v)| is executed.

Checking of preconditions is easy with the plethora of test macros shipped with \emph{TTEST}.
Furthermore each test macro comes in two versions, one tightly integrated into \emph{TTEST},
another faster one with less functionality as a stand alone function. 
This makes it possible to use more expressive assertions than \verb|assert| in her owns code.




%\section{Explanation of hacks}
\subsection{Explanation of code instrumentation hacks}
Since the Matlab language does not support runtime code injection directly, 
we implement it using the conditional breakpoints.
Conditional breakpoints evaluate a string in run-time. If the result is truthy\footnote{%
A \emph{truthy} value is a value which implicitly evaluates to \texttt{true}, for example in an \texttt{if} condition.
Examples are \texttt{true}, \texttt{1} or an array with only non-zero values.
Contrary, a \emph{falsy} value implicitly evaluates to \texttt{false}, 
for example \texttt{false}, \texttt{0}, an array with at least on zero.
There is a Matlab bug (feature?) making an empty array \texttt{[]} being a falsy value, 
although it should be truthy.
}, then the code run is stopped at that location.
But if its false then the code run continues normally.

The basic idea of code instrumentation should be clear now:
On the line where we want to inject code, we add a conditional breakpoint, 
whose corresponding string is the code we want to evaluate.
This method has some restrictions, and Matlab adds even more. 

\begin{enumerate}[wide]

\item\label{inject:falsy}
  Injected code must always return a falsy value.
  Therefore we wrap the to-be-injected code in a function which returns always \texttt{false}
  and evaluates the to-be-injected code via \texttt{evalin}.
\item\label{inject:handle}
  The debugger only accepts evalable\footnote{An \texttt{evalable} string is a string which can be passed
  to Matlabs \texttt{eval} command.} strings, no function handles.
  If we want to execute a function handle, we store it in a persistent variable in some function, 
  and generate a string which than executes that function handle. The downside of this approach is, 
  that the function handle must itself return a falsy value.  
\item\label{inject:error}
  If the injected code throws an error, then the exception is caught by
  Matlab and the program stops, and thus, error handling is hard to
  impossible via code injection\footnote{ We think that error handling via injected code is a slight 
  code stench, and thus, do not deem it as a problem.}
  Thus, we catch all exceptions inside the function which we use to evaluate the string or function handle,
  and rethrow the exception as a warning.
\item\label{inject:before}
  Injected code is always executed before the rest of the code on the
  same line, in particular, code cannot be injected between statements
  or after statements. 
  Currently, we have no idea how to bypass this small problem. 
\item\label{inject:octave}
  The debugger in Matlab and Octave behaves slightly different, 
  in particular conditional breakpoints sometimes crash Octave fully.  
\end{enumerate}



Our implementation only uses documented features of the Matlab language. 
Of course, it would be better if the language itself would support runtime code injection,
but, to our knowledge no current language has support for this at all.

The idea of abusing the debugger is not new but already used in~\cite{Isa2016} 
and should work in most scripted languages, like Matlab, Python and R. 
For compiled languages, which often follow the \emph{as-if} rule, 
more language support would be needed to accomplish code injection. 
The problem for the latter is that code lines may get reordered or optimized away,
and thus, the place where one wants to inject code in the source code 
can differ from the place where the code should be injected at runtime. 
%Nevertheless, this framework is targeted as scientists, and these often use script based languages.

This approach does not alter the source code. 
The only exception is, when it is necessary to define some markers in the source code 
where the code shall be injected. 
This can be done safely with comments, e.g. like

\begin{lstlisting}
function x = plus1( x );
    x = x + 1;  %<TTEST_1>
\end{lstlisting}

Here one may inject code at the line marked with \texttt{\color{remark}\%<TTEST\_1>}. 
The special format of the comment clearly indicates that it contains some meta information, 
and thus, must not be removed.

%The technique of augmenting the source code for unit testing is already accepted.
%For example in so-called \emph{doctests}, 
%commented out code lines together with their expected output are added to the source code. 
%A special test runner then executes those tests. 
%An example in Python is given below.\footnote{%
%Note that the \emph{TTESTs} also come with a doctest enabled test runner, 
%but the \emph{TTEST} doctests differ in one part significantly from other doctests. 
%See the section of the test runner.}
%
%\begin{figure}
%\begin{lstlisting}[language=Python]
%def add( a, b ):
%    """
%    >>> add 2, 3 )
%    5
%    """
%    return a + b
%\end{lstlisting}
%\caption{Example of a doctest in Python.}
%\end{figure}

\paragraph{\color{keyword}\texttt{inputat}} 
This function renders the \verb|input| function, 
which asks for user input, non-blocking. 
This we achieve by $(1)$ overloading the original function and $(2)$ code instrumentation.
\verb|input| provides two ways to make \texttt{\color{keyword}inputat} non blocking.
The first stores the to be inputted string as a persistent variable 
in the overloaded function \verb|input|,
in particular does not use code instrumentation.
The second uses code instrumentation to assign a variable in the functions workspace
for which the overloaded function \verb|input| is looking for when it is invoked.
These two types allow for a very fine grained definition of where to inject which text,
something which is necessary whenever multiple \verb|input| commands 
are present in a function or script.

\paragraph{Executing subfunctions}
 
The main idea is the following:
\begin{itemize}
\item inject a \verb|localfunctions| call at the beginning of the function
\item Pass the result of this call to another function
\item use early return afterwards.
\end{itemize}

In theory this works, problems only arise due to the many natures of subfunctions: There are local functions, nested functions and private functions.\footnote{%
~\textbullet~
Local functions are defined in the same file, 
after the main function ends. 
They are only callable from other functions in the same file. 

\textbullet~
Nested functions are defined in the body of the main function. 
In Matlab a nested function shares its workspace with the parent function.
In Octave, incomprehensibly, nested functions and local functions are the same 
(This is even more strange under the view that script files called from functions 
behave the same in Matlab and Octave). 
They are only callable from their parent function.

\textbullet~
Private functions are defined in a subfolder \texttt{/private/}.
They are only callable from any function in the parent folder.
}

Furthermore, we cannot guarantee that a call to \texttt{\color{keyword}allfunctionhandle}
has no side effects.\footnote{%
A function is said to have a \emph{side effects} when it has an observable effect besides returning a value.}
And there are script-files, function-files and class-files. 
Furthermore subfunctions can have other subfunctions. 
Nearly each combination needs special handling, 
something which we did not implement fully yet. 
Currently \emph{TTEST} only implements retrieving subfunctions from files 
and does not retrieve subfunctions from subfunctions. 
The details are rather boring, so we omit it here. 
We just add a note for nested functions. 
Since these functions share the workspace with their parent function, 
a function handle to a nested function is usually useless, 
since most variables needed for the subfunctions are not initialized. 
Thus, calling a function handle to a nested function usually results in throwing an exception.



\subsection{Explanation of other hacks}

\paragraph{\color{keyword}\texttt{TRACE}} This is a rather hacky hack and it works only on
Matlab. Furthermore, it uses undocumented ``features''.\footnote{%
Although we use undocumented features, we deem it as as stable as 
relying on documented features, for two reasons:
1) This hack uses the string representation as returned by \texttt{disp} and \texttt{dbstack} 
which is sensible and unlikely to be changed,
2) Even documented features of the Matlab language get frequently changed by \emph{The MathWorks},
resulting in Matlab code usually only working for some releases of Matlab,
at the most.
}
 We do it the following way:
\texttt{\color{keyword}TRACE} takes a function handle \texttt{@f} and stores some data
from the callers site. The function handle is wrapped in an anonymous
function which has a unique string in its code, something like
\texttt{@(unique\_name)\ f()}. This anonymous function gets executed.

Later, with \texttt{dbstack} one can check the stack trace and will find
the string \texttt{unique\_name} somewhere in it. This string can be
passed to \texttt{TRACE} which happily returns the data originally stored at the
callers site. With some more work, one can also command \texttt{\color{keyword}TRACE}
to manipulate the callers site whenever the program flow goes back to
\texttt{\color{keyword}TRACE}.

On Octave, anonymous functions are represented differently with
\texttt{dbstack} (namely, not at all), and thus, this does not work.



\paragraph{Type inference for property based tests} With \emph{TTESTs} it
is possible to write properties based tests for functions and in most
cases does not need to specify the possible inputs to the functions. For
example, the following test checks whether the function \texttt{transp}
really transposes a matrix

\begin{lstlisting}
GIVEN( @(mat) isequaln(mat.', transp(mat)) );
\end{lstlisting}

\texttt{transp} takes a matrix and returns the transpose. The
\texttt{\color{keyword}GIVEN} function makes 30 tests with random input and checks
whether the condition
\texttt{isequaln( mat.', transp(mat) )} is
fulfilled. Note that we only implicitly gave the type of the input data
(matrices) by using the variable name \texttt{mat}. This we achieve by
parsing the begin of the string representation of the function handle
\texttt{@(mat)}. The same technique is used in the postcondition tests.

\section{Timing/Examples}
SOME EMPIRICAL RESULTS IF POSSIBLE (aber mir fällt nichts wirklich gutes ein, ausser speed)
ich könnte überlegen, ob ich ein Beispiel aus einem Modellcode finde, das sich anbietet

\section{Summary and Outlook}
%\newpage
Besides its main purpose as a unit test framework, \emph{TTEST} also provides functions specifically dedicated to handling scientific legacy code. 
We presented the implementation of these functions in Matlab, outlined the associated challenges. \\

TTEST can be downloaded from \url{https://gitlab.com/tommsch/ttest/-/blob/master/readme.md#ttests-scientific-part}, its documentation can be found there too. \\

Future work...

\section{IEEE}
\begin{itemize}
\item In American English, commas, semicolons, periods, question and exclamation marks are located within quotation marks only when a complete thought or name is cited, such as a title or full quotation. When quotation marks are used, instead of a bold or italic typeface, to highlight a word or phrase, punctuation should appear outside of the quotation marks.
\item The prefix ``non'' is not a word; it should be joined to the word it modifies, usually without a hyphen.


\item  Place figures and tables at the top and 
bottom of columns. Avoid placing them in the middle of columns. Large 
figures and tables may span across both columns. Figure captions should be 
below the figures; table heads should appear above the tables. Insert 
figures and tables after they are cited in the text. Use the abbreviation 
``Fig.~\ref{fig_IEEE}'', even at the beginning of a sentence.


\item The preferred spelling of the word ``acknowledgment'' in America is without 
an ``e'' after the ``g''. Instead, try ``R. B. G. thanks$\ldots$''. Put sponsor 
acknowledgments in the unnumbered footnote on the first page.

\item Please number citations consecutively within brackets. 
Papers that have been accepted for publication should be cited as ``in press''. 
\end{itemize}






\newcommand{\doi}[1]{\href{https://doi.org/#1}{doi: #1}}
\newcommand{\arxiv}[1]{\href{https://arxiv.org/abs/#1}{arXiv: #1}}

\begin{thebibliography}{00}

\bibitem{Fow2006}
Martin Fowler,
\emph{Xunit}, \href{https://martinfowler.com/bliki/Xunit.html}{martinfowler.com/bliki/Xunit.html}, 2006.

\bibitem{WikiRefactor}
Wikipedia, \emph{Code refactoring}, 2021-09-08,
\href{https://en.wikipedia.org/wiki/Code_refactoring}{en.wikipedia.org/\-wiki/\-Code\_\-refactoring}.

\bibitem{Khor2020}
Vladimir Khorikov, \emph{Unit Testing Principles, Practices, and Patterns}, O'Reilly, 2020.


\bibitem{Fea2004}
Michael Feathers, \emph{Working Effectively with Legacy Code}, Prentice Hall PTR, USA, 2004.

\bibitem{Isa2016}
Per Isakson, \emph{tracer4m}, 2016,
\href{https://www.mathworks.com/matlabcentral/fileexchange/28929-tracer4m}%
{mathworks.com/matlabcentral/\-file\-ex\-change/\-28929}.
 
\bibitem{Schu2009}
David Schuler, Andreas Zeller,
\emph{Javalanche: Efficient mutation testing for Java},
ACM SIGSOFT (2009),
\doi{10.1145/1595696.1595750}.

\bibitem{AltUndoc}
Yair Altman,
\emph{Undocumented Matlab}, 2021-09-08,
\href{https://undocumentedmatlab.com/}{undocumentedmatlab.com}.

% Test frameworks

\bibitem{fw_lombardi}
Gabriele Lombardi,
\emph{MUnit: a unit testing framework in Matlab}, 2006,
\href{https://de.mathworks.com/matlabcentral/fileexchange/11306}{mathworks.com/matlabcentral/file\-exchange/\-11306}.

\bibitem{fw_christoper}
Christopher,
\emph{mlunit\_2008a}, 2009,
\href{https://de.mathworks.com/matlabcentral/fileexchange/21888}{mathworks.com/matlabcentral/\-file\-exchange/\-21888}.

\bibitem{fw_eddins}
Steve Eddins, 
\emph{xunit4}, 2010,
\href{https://www.mathworks.com/matlabcentral/fileexchange/47302}{mathworks.com/matlabcentral/\-fileexchange/\-47302}.

\bibitem{fw_arka}
arka, \emph{munit}, 2013,
\href{https://github.com/arka/munit}{github.com/arka/munit}.

\bibitem{fw_hetu}
\emph{mlUnit}, 2015,
\href{https://sourceforge.net/projects/mlunit/}{sourceforge.net/projects/mlunit/}.

\bibitem{fw_legland}
David Legland, 2016,
\href{https://de.mathworks.com/matlabcentral/fileexchange/7845}{mathworks.com/matlabcentral/fileexchange/7845}.

\bibitem{fw_schiessl}
Stefan Schiessl, 2016,
\href{https://www.mathworks.com/matlabcentral/fileexchange/52179}{mathworks.com/matlabcentral/fileexchange/52179}.

\bibitem{fw_mwgeurts}
Mark Geurts, 2016,
\href{https://github.com/mwgeurts/unit_harness}{github.com/mwgeurts/unit\_harness}.

\bibitem{fw_sexton}
Paul Sexton, Thomas Smith, 2016,
\href{https://github.com/psexton/matlab-xunit}{github.com/psexton/matlab-xunit}.

\bibitem{fw_moxunit}
Nikolaas N.\ Oosterhof et al., 2017,
\href{https://github.com/MOxUnit/MOxUnit}{github.com/MOxUnit/MOxUnit}

\bibitem{fw_zimmermann}
Ray Zimmermann, 2021,
\href{https://github.com/MATPOWER/mptest}{github.com/MATPOWER/mptest}.

\bibitem{fw_mathworks}
The MathWorks, \emph{Testing Frameworks}, 2021-09-08, \href{https://de.mathworks.com/help/matlab/matlab-unit-test-framework.html}{mathworks.com/\-help/\-matlab/\-matlab-unit-test-framework.html}

\bibitem{fw_ttest}
Thomas Mejstrik, Clara Hollomey, 2021,
\href{https://gitlab.com/tommsch/TTEST}{gitlab.com/tommsch/TTEST}.
 
\end{thebibliography}


\end{document}
