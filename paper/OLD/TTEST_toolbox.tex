\documentclass[conference]{IEEEtran}
\IEEEoverridecommandlockouts
% The preceding line is only needed to identify funding in the first footnote. If that is unneeded, please comment it out.
\usepackage{cite}
\usepackage{amsmath,amssymb,amsfonts}
\usepackage{algorithmic}
\usepackage{enumitem}
\usepackage{graphicx}
\usepackage{textcomp}
\usepackage{multicol}
\usepackage{multirow}
\usepackage[dvipsnames]{xcolor}
\usepackage{matlab-prettifier-ttest}
\lstset{basicstyle=\ttfamily, keywordstyle=\bfseries, style=Matlab-TTEST}
\usepackage[bookmarks=true, breaklinks=true, colorlinks=false, pdfborder={0 0 0}, pdfa, draft=false]{hyperref}

\usepackage{selnolig}  % disable illegal ligatures

\definecolor{remark}{RGB}{034,139,034}
\definecolor{keyword}{RGB}{0,0,255}

\begin{document}

\title{TTEST: a framework for unit testing and managing legacy code}

\author{\IEEEauthorblockN{1\textsuperscript{st} Thomas Mejstrik}
\IEEEauthorblockA{\textit{NuHAG} \\
\textit{University of Vienna}\\
Vienna, Austria \\
tommsch@gmx.at}
\and
\IEEEauthorblockN{2\textsuperscript{nd} Clara Hollomey}
\IEEEauthorblockA{\textit{ARI} \\
\textit{Austrian Academy of Sciences}\\
Vienna, Austria}
}

\maketitle

\begin{abstract}
With our new Matlab/Octave unit testing framework \emph{TTEST} we add to this list some new points:
  testing local functions, 
  programmatically early returning, 
  testing control flow.
  
Additionally we present some Matlab Hacks which allow to
  programmatically manipulate the workspace,
  reliable test exceptions
  making blocking commands non-blocking,
  test pre and postconditions in functions,
  make non-blocking timing hacks.  

The \emph{TTEST} framework is targeted for scientific software written and at scientific people.
and  \emph{TTEST} has strong capabilities for helping refactoring code, 
writing self explaining, concise, unit tests and testing of scientific software.
\end{abstract}

\section{Unit tests}
Unit tests are used to check the correct behaviour of small functional units of a program. 
Furthermore, they can assure the programmer that the results of her program are correct. 
Something which is of crucial importance for scientific software.
In this chapter we only introduce some basic nomenclature which is used in this paper.
For a more thorough introduction into unit tests see~\cite{Khor2020,fw_mathworks,Fea2004}.

Unit tests can be split up in one category: Automated unit tests and manual unit tests. 
The latter usually are never executed, and thus do not test anything.
The current design of automated unit tests (and the only one we can think of and which is used) 
was invented for the language \emph{Smalltalk} 
and the first public implementation for the language \emph{Java} in 1997, 
named \emph{JUnit}, 
later backported to \emph{Smalltalk} under the name \emph{SUnit}.
Since then, \emph{JUnit} got ported to nearly all programming languages, 
and so its abstract idea got called \emph{xUnit}~\cite{Fow2006}.

An \emph{xUnit} framework consists of the following parts:
\begin{itemize}
\item \emph{Test runner}, runs the tests and reports the result
\item \emph{Test case}, represents one unit test
\item \emph{Test suite}, collection of all test cases
\item \emph{Test fixture}, code which is run before each test case and prepares the environment needed for the test case
\item \emph{Assertions}, helper functions to write bug-free test cases
\end{itemize}

The program, function or system which is tested is usually called 
\emph{system under test} \emph({SUT}).

Although the importance of writing tests is undoubted because unit testing
\begin{itemize}
\item saves time on the long term
\item provides documentation
\item helps to develop sensible interfaces
\item increases re-usability 
\item helps to find bugs
\item prevents introducing new bugs
\item prevents from reintroducing old bugs, i.e. regression
\item simplifies the debugging process,
\end{itemize}
it was not common to write unit tests before 1990,
and still today many developers do not write unit tests.
Code written without the requirement to be testable, 
usually is 
$(1)$ hard to understand 
$(2)$ hard to \emph{refactor}\footnote{%
\emph{Refactoring} is restructuring existing code without changing its functionality 
with the intent to improve the design, structure or implementation~\cite{WikiRefactor}.},
and 
$(3)$ hard to extend.

\subsection{State of the art of unit testing in Matlab}
A quick survey of papers published on \emph{JORS}, \emph{JOSS} and \emph{ACM TOMS}\footnote{%
\emph{JORS}: Journal of Open Research Software, \emph{JOSS}: Journal of Open Source Software, \emph{ACM TOMS}: ACM Transactions on Mathematical Software}
 between 2015 and 2020 for the Matlab language yields 
that only a small amount of submissions include automatic unit tests, see Figure~\ref{fig:survey}.

\begin{figure}
\centering
\caption{Small survey about how many software submissions using Matlab to journals include unit tests and automatic unit tests.\newline}
\label{fig:survey}
\begin{tabular}{|c|c|c|c|}
\hline
Journal & survey & unit  & automatic  \\
        &  size  & tests & unit tests \\
\hline
JORS &  19 &  7 & 2 \\
JOSS & 18 & 7 & 2 \\
ACM TOMS & 18 & 8 & 2 \\
\hline
\end{tabular}
\end{figure}

We suspect that this is mainly due to the following two reasons
\begin{enumerate}
\item \label{reason:matlabprog} People who use Matlab are, to a large extent, not developers but mathematicians or engineers.%
\footnote{Conversely, most developers do not use Matlab.}
Therefore, most Matlab users do not know about the importance of writing unit tests for a program.
\item \label{reason:boilerplate} Currently it needs a lot of boilerplate code and/or 
knowledge of advanced features of Matlab to write useful tests in Matlab.
\end{enumerate}

While we do not know of a way to fix issue~\eqref{reason:matlabprog}, 
we can work on issue~\eqref{reason:boilerplate} by making unit testing as easy as possible.


\subsection{Existing Matlab unit test frameworks}
To our knowledge there are currently (2021) three test frameworks still in use: 
\emph{MOxUnit}, \emph{xunit4} and \emph{Matlabs built in test framework}. 
Because \emph{xunit4} can be seen as a predecessor of Matlabs built in test framework we merely discuss it.
A list of all frameworks for Matlab known to use can be found in the appendix, Figure~\ref{fig:framework}.
Matlabs framework is very powerful, but also very verbose. 
For large projects Matlabs framework is well suited.
But, given that most Matlab projects often consist only of a few \verb|.m| files, 
it is quite an overkill when the accompanying test suite needs more code,
and more complex language constructs, than the project. 
On the other hand, \emph{MOxUnit} and \emph{xunit} need much less code to write sensible unit tests, 
but lack a lot of important features. 
% Therefore, the \emph{TTESTs} are a perfect supplement to the current situation.

\subsection{Optional: Other unit test frameworks}

\section{Design considerations}

\subsection{Usability}
In this section we compare the usability of \emph{TTEST}, \emph{MOxUnit} and \emph{Matlabs unit test framework}, the latter divides further into \emph{script based}, \emph{function based} and \emph{class based} unit tests. Also, \emph{TTESTs} divide up into \emph{script based} and \emph{function based} unit tests.\footnote{\emph{TTESTs} do not support class based tests, since the OO design of Matlab is slow and questionable.}
Also, unit tests written for \emph{TTESTs} can be executed with Matlabs test runner \texttt{runtests} or with \emph{TTESTs} test runner \texttt{\color{keyword}runttests}.


\paragraph{Boilerplate code}
To demotivate users to write unit test, its best to burden them with writing lots of boiler plate code. 
Thus, we compare the number of characters needed to write a single unit test 
similar to \verb|assert( 2==2 )|.
The code stubs used (or parts of it) are printed in the appendix, Figure~\ref{fig:codestub}.
Matlab script based unit tests needs the least number of characters, followed by the \emph{TTEST} framework.
But, we clearly have to note that Matlabs script based unit tests 
lack nearly all necessities of a reasonable unit test framework, 
and thus, they are quite useless.

For comparison reasons we also asses test frameworks for different languages, 
namely \emph{GoogleTest} and \emph{Catch2} for C++, \emph{PyTest} for Python, \emph{testthat} for R 
and the unit test frameworks of the languages \emph{D} and \emph{Julia}
which are directly integrated into the language.
Except for the latter two, which are clearly a special case, 
the numbers of characters needed to write a test is comparable to \emph{TTEST}.


\paragraph{Performance}
Although not the most critical part of a unit test framework, 
performance is still important for two reasons: 
\begin{enumerate}[wide]
\item If the unit tests take too long to run, then the user will not do unit tests, or 
\item she will get bored while waiting for the unit tests to finish and start doing something else. 
\end{enumerate}

Thus, from the frameworks design point of view, 
the longest time in a test run must be spent in the software under test, 
not in setting up the unit test framework environment.
The exact test setup is stated in the appendix~\ref{fig:performance}.

\emph{TTEST} is the fastest unit test frameworks, closely followed by MOxUnit.
Matlabs unit test framework needs a tremendous amount of time,
in the worst case $210\,s$ whereas \emph{TTEST} needed $5.5\,s$.\footnote{
If no \emph{TTEST}-sections, an equivalent to test fixtures,
and user defined options are used, then \emph{TTEST} is even faster.
}

Of course, we have to admit that Matlabs built in unit test framework 
has for more features than \emph{TTEST}, and so it is to be expected that it may be slower.
Nevertheless a relative difference of 100\% to 2500\% is not neglectable.

Still, we want to present which part of the testing environment is restored 
to its original state for each test framework. 
Since for unit tests we want independent tests, the more state is restored -- the better it is.
\begin{itemize}
\item By default, \emph{MOxUnit} only restores the workspace (i.e. the variables) back for each unit test.

\item \emph{Matlabs built in unit test framework} additionally restores namespace imports. 

\item \emph{TTEST} restores all global variables, the working directory, the warning state, 
debug break points, the Matlab path and 
(optionally and experimentally) the base workspace and 
the random number generator (\emph{RNG}).\footnote{%
Future works includes restoring the state of figures and 
user defined random number generators.}
\end{itemize}

In order for \emph{TTEST} to restore this plethora in a fast way, 
\emph{TTEST} transparently overloads the functions 
\verb|addpath|, \verb|dbclear|, \verb|dbstop|, \verb|path|, 
\verb|restoredefaultpath|, \verb|rmpath| and \verb|userpath|.

\subsection{Octave compatibility}

\section{Structure of \emph{TTEST}}
\emph{TTEST} comprises 3 modules/parts/whatever:
\begin{itemize}
    \item the unittest framework
    \item property based testing
    \item functionality for handling legacy code
\end{itemize}

\paragraph{Property based tests}

\paragraph{Gold standard tests} These are unit tests were the result of a test is compared with a some stored data in a file. Usually, they are used in the following situations:
- The output of the algorithm is not known exactly and one tests whether new versions of the algorithm still produce reasonable results. Such tests are usually called \emph{regression tests}.
- The output of the function is highly complex (e.g. a picture) and cannot be stored directly in the source code.

Gold standard tests are by no means an invention by \emph{TTEST}, but,
our implementation of gold standard test is by far the easiest possible, for example
\begin{lstlisting}
EXPECT_EQ( CACHE('name'), 2 );
\end{lstlisting}
is a full featured gold standard test which takes care of all the work needed, 
in particular generating the gold standard.
\paragraph{Test macros and matchers}
\emph{TTEST} supports two types of assertions: Old fashioned assertions and 
\emph{(experimentally)}newfangled matchers. 
The latter are a popular way of writing unit tests 
where the code somewhat reads like English language.\footnote{%
Our implementation of matchers does not read that fluent 
due to restrictions of the Matlab language: 
A returned object may not directly used further, 
e.g.\ the following is impossible in Matlab: \texttt{[0 1 2 3](2)}, 
one has instead to write \texttt{x = [0 1 2 3]; x = x(2);}.
In languages where such constructs are possible, 
fluent interfaces are much easier to write. 
See for example \emph{Fluent Assertions} for \emph{Java} 
where the matcher test above can be written as as 
\texttt{Number.Should().BeGreater\-OrEqualTo(0).\-And().\-BeLessOrEqualTo(2);}.
}
One example, together with its output, is:
\begin{lstlisting}
>> EXPECT_THAT( 3, AllOf(Ge(0),Le(2)) );
Argument does not fulfil matcher, but it should, where
  matcher = greater equal 0 and less equal 2 
  Argument 1 = 3
\end{lstlisting}
Written as a conservative test macro this would be
\begin{lstlisting}
>> EXPECT_PRED( @(x) x>=0 && x<=2, 3 )
Value does not fulfil predicate, but it should, where
  Predicate =  @(x)x>=0&&x<=2
  Value 1 = 3
\end{lstlisting}
or
\begin{lstlisting}
>> EXPECT_GE( 3, 0 );
>> EXPECT_LE( 3, 2 );
lhs > rhs ( but they should be <= ). Where
  lhs = 3
  rhs = 2
\end{lstlisting}

\paragraph{Timing tests}
\emph{(experimental)} Sometimes mathematical algorithms are known to take forever (or at least very long) for some specific inputs. Thus, to unit test such behaviour we need a test which checks that an algorithm does not terminate within reasonable time. Here, the \texttt{\color{keyword}\_MINTIME} macro comes into play, which checks the condition in a non-blocking way.\footnote{ Unfortunately, we currently face a dilemma with this macro. 1) Algorithms which handle difficult tasks are often multithreaded 2) To achieve a non-blocking test macro \texttt{\color{keyword}\_MINTIME} we  want to execute this test in parallel. Now, Matlab seems to have problems with timer callbacks when a multithreaded function (the function under test) is started from another multithreaded function (the test macro).

For Octave things are easy here. Octave does not support parallel execution of tasks at all and so we cannot implement this macro at all in Octave in a non blocking way.}.
Clearly, \emph{TTEST} also has a  \texttt{\color{keyword}\_MAXTIME} macro.

\section{Features of \emph{TTEST}}
\emph{TTEST} comes along with some test macros and helper functions 
especially targeted at scientific software. 
\emph{TTEST} is not the only framework which has those, 
but their usage is the most easiest of all frameworks we know.

%\subsection{Features relying on other hacks or no hacks}

\paragraph{Reliable testing of exceptions} 
Testing exceptions in Matlab is not
easy. There are warning and errors, but warnings can be promoted to
errors with a call to warning with the undocumented option
\texttt{'error'}, e.g. like in

\begin{lstlisting}
warning( 'error', 'MATLAB:singularMatrix' );
inv( 0 );
\end{lstlisting}

Another problem is a severe bug in Matlab which has the effect that
sometimes warnings are disabled totally.

\emph{TTEST} solves these problems (at least partially - 
for builtin functions our approach does not work reliable).
Firstly, \emph{TTEST} does not distinguish between warnings and errors,
\emph{TTEST} warns whenever all warnings are disabled,
\emph{TTEST} allows to catch more than one warning, 
not only the last one via~\verb|lastwarn|.

Another design decision of \emph{TTEST} for unit testing exceptions 
seems to be contrary to one design principles of \emph{TTEST}(easy writing):
We force the user to specify the to be expected exception.\footnote{%
Also, this design decision is contrary to most unit test frameworks in use. 
Even \emph{GoogleTest} which influenced \emph{TTESTs} greatly is not that strict in this regard.}
The reason is simple. Not specifying
the expected exception usually results in false positives\footnote{%
We give an example where we can easily encounter a false-positive exception. 
The following unit test should check whether a call of 
\texttt{\{error('test:err')\}} throws
an exception with id
\texttt{'test:err'}:
\texttt{{\color{keyword}EXPECT\_THROW}( @() error('test:err') );}
And indeed, an error is thrown, but not the one we expect: 
\emph{TTEST} reports correctly:
\texttt{
Error was thrown which was not expected.
Thrown error:
 id:       MATLAB:maxlhs
 msg:      Too many output arguments.
}
The reason is simple, \texttt{error} does not
return arguments and so one cannot store a return value of
\texttt{error} in a cell array.
%If \emph{TTESTs} would not mandate
%that the exception must be specified, this test would indeed succeed.
%But, due to \emph{TTESTs} policy this test fails which the message.
%If one is forced to specify the exception, such frequent false positives cannot arise.
}


\paragraph{Independence of unit tests}
\paragraph{Severity of unit tests}
\paragraph{Variadic test macros} Most (actually all test frameworks we know) accept exactly two arguments in a test 
\verb|assert_equal|. This is quite user-unfriendly. 
Because, one often has a bunch of values which needs to be compared. \emph{TTEST} test macros accept 
as many arguments as one likes wherever it makes sense. For example, the following tests all succeed
\begin{lstlisting}
EXPECT_EQ( 2, 2, 2 );
EXPECT_NEAR( 10, 11, 9, 10.5. 2 );  % last argument is maximum difference
EXPECT_LE( 1, 2, 3, 4 );
EXPECT_STRCONTAINS( 'abc', 'bc', 'b' );
\end{lstlisting}

\paragraph{Lazy evaluation of function handles} 
For novices of Matlab,
anonymous functions behave strangely 
in that they capture the whole workspace at their time of creation. 
For novices of programming they behave even more strange, 
in that the name of the variables as arguments used have 
nothing to do with the variables in the workspace.

For more experienced programmers the former becomes a great tool, 
whereas the latter is sometimes annoying. It is quite cumbersome to 
evaluate an anonymous function with the variables in the current workspace.
Indeed, this is merely needed, 
and when it is needed it is usually a sign of bad code design.

\paragraph{Pluszero, Minuszero} IEEE\,745 defines two representations of zero: +0 and -0. 
\emph{TTEST} supplies test macros for both.

\subsection{Other features}
\paragraph{Test runner}
\paragraph{Script based tests}
\paragraph{Function based tests}
\paragraph{Doctests}

\section{Explanation of hacks/Implementation}

\paragraph{The \emph{TTEST} function}


\paragraph{Gold standard tests} 
The function \texttt{\color{keyword}CACHE} for gold standard tests is just a
wrapper for a \texttt{.mat} file. In order to synchronize the content
of the file and the corresponding variable we use getter and setter
methods which are called whenever the variable is read or written.

\paragraph{Fast test runner} 
We suspect, one reason why \emph{Matlabs} test
runner is slow is that it needs to restore the testing environment after
each call to a clean state\footnote{1. Although, Matlabs test runner only restores
the most basic things, 2. The more severe reasons seems to be the abundant use of \texttt{eval} in a lot of Matlab functions.}. Due to the same reason we suspect the
\emph{MOxUnit} decided to not restore the testing environment at all
(The workspace is restored implicitly since \emph{MOxUnit} uses
functions to isolate test cases).

To restore the testing environment in a fast way, \emph{TTEST} uses the
following approach. \emph{TTEST} keeps track of whether the state of the
Matlab path or break points of the debugger changed, and only restores
the state when this is needed. In order to monitor the state,
\emph{TTEST} overloads the functions \texttt{addpath}, \texttt{dbclear},
\texttt{dbstop}, \texttt{path}, \texttt{rmpath} and \texttt{userpath}.

\paragraph{Reliable testing of exceptions} 
This we achieve simply by overloading the \verb|warning| function.
For builtin functions this does not work, since builtin functions 
seem to be able to throw exceptions without calling

\paragraph{Type inference for property based tests} With \emph{TTESTs} it
is possible to write properties based tests for functions and in most
cases does not need to specify the possible inputs to the functions. For
example, the following test checks whether the function \texttt{transp}
really transposes a matrix

\begin{lstlisting}
GIVEN( @(mat) isequaln(mat.', transp(mat)) );
\end{lstlisting}

\texttt{transp} takes a matrix and returns the transpose. The
\texttt{\color{keyword}GIVEN} function makes 30 tests with random input and checks
whether the condition
\texttt{isequaln( mat.', transp(mat) )} is
fulfilled. Note that we only implicitly gave the type of the input data
(matrices) by using the variable name \texttt{mat}. This we achieve by
parsing the begin of the string representation of the function handle
\texttt{@(mat)}. The same technique is used in the postcondition tests.

\section{Full documentation of \emph{TTEST}}
The full documentation of~\emph{TTEST} can be found at
\href{https://gitlab.com/tommsch/ttest/}{gitlab.com/tommsch/ttest}.

Other resources ? Mailing list?\\

\section{Summary and Outlook}

\newpage
\paragraph{The testrunner}

\appendix



\begin{figure*}
\centering
\caption[List of Matlab unit test frameworks]{}
List of Matlab unit test frameworks\\[2ex]
\label{fig:framework}
\begin{tabular}{l|c|l}
\hfil \multirow{2}{*}{Name} \hfil & Last & \hfil \multirow{2}{*}{Remark} \hfil \\
& Update & \\[1ex] \hline\hline
& & \\[-.5ex]
\href{https://de.mathworks.com/matlabcentral/fileexchange/11306}{Lombardi MUnit}\cite{fw_lombardi}
    & 2006 & Simple, Good, Outdated   \\

\href{https://de.mathworks.com/matlabcentral/fileexchange/21888}{Christoper mlunit 2008a}\cite{fw_christoper}
    & 2009 & Very verbose, Fork of Lombardi MUnit \\

\href{https://www.mathworks.com/matlabcentral/fileexchange/47302}{Eddins xUnit}\cite{fw_eddins}
    & 2010 & Predecessor of Saxton xunit4 \\

\href{https://github.com/arka/munit}{arka mUnit}\cite{fw_arka}
    & 2013 & Too basic\\

\href{https://sourceforge.net/projects/mlunit/}{Hetu mlUnit}\cite{fw_hetu}
    & 2015 & Very verbose  \\

\href{https://de.mathworks.com/matlabcentral/fileexchange/7845}{Legland munit}\cite{fw_legland}
    & 2016 & Too basic, Only two assertions available\\

\href{https://www.mathworks.com/matlabcentral/fileexchange/52179}{Schiessl unitTAPsci}\cite{fw_schiessl}
    & 2016 &  Plugin for Matlabs framework, adds Tap output and Jenkins support\\

\href{https://github.com/mwgeurts/unit_harness}{mwgeurts unit harness}\cite{fw_mwgeurts}
    & 2016 & Merely a test runner \\

\href{https://de.mathworks.com/matlabcentral/fileexchange/47302}{Sexton xunit4}\cite{fw_sexton}
    & 2016 &  Predecessor of Matlabs built in framework, \href{https://github.com/psexton/matlab-xunit-doctests}{Doctests}  \\

\href{https://github.com/MOxUnit/MOxUnit}{MOxUnit}\cite{fw_moxunit}
    & 2021 & Only some tests available, Octave compatible, \href{https://github.com/MOdox/MOdox}{Doctests} \href{https://github.com/MOcov/MOcov}{Coverage} \\
    
\href{https://github.com/MATPOWER/mptest}{Zimmermann mptest}\cite{fw_zimmermann}
    & 2021 & Only used for one project\\
    
\href{https://de.mathworks.com/help/matlab/matlab-unit-test-framework.html}{Matlabs built in framework}\cite{fw_mathworks} &  &  \\
    $\qquad$\href{https://de.mathworks.com/help/matlab/script-based-unit-tests.html}{Script based tests} & 2021 & Too basic, Only one assertion available \\
    $\qquad$\href{https://de.mathworks.com/help/matlab/function-based-unit-tests.html}{Function based tests} & 2021 & Verbose, Very slow\\
    $\qquad$\href{https://de.mathworks.com/help/matlab/class-based-unit-tests.html}{Class based tests} & 2021 & Very verbose\\

\href{https://gitlab.com/tommsch/TTEST}{\emph{TTEST}}\cite{fw_ttest}
    & 2021 & Precise and mighty  \\

\end{tabular}
\end{figure*}

\begin{figure*}
\centering
\caption[Performance of various Matlab unit test frameworks.]{}
\label{fig:performance}
Wall clock time needed to finish three types of test suites:
\begin{itemize}
\item[\emph{(E)}] a folder with an empty test file
\item[\emph{(1024)}] a folder consisting of 1024 files each with one simple test, similar to \verb|assert( 2==2 );|
\item[\emph{(16)}] a folder consisting of 16 files each with 1024 simple tests, similar to \verb|assert( 2==2 )|
\end{itemize}
measured on an AMD Ryzen 3600, 64 GB RAM, Matlab R2020a, 7200 rpm HD. The code stubs (or parts of it) are printed in Figure~\ref{fig:codestub}.\\[2ex]

\begin{tabular}{c||c|c|c|c|c|c}
               & \emph{TTEST} & \emph{TTEST} & \multirow{2}{*}{MOxUnit} & \emph{Matlab} & \emph{Matlab} & \emph{Matlab}   \\
               &    Script    &   Function   &                          &    Script     &   Function    &     Class       \\[1ex] \hline\hline
               &              &              &                          &               &               &                 \\[-.5ex]
  \emph{(E)}   &   $0.7\,s$   &   $0.8\,s$   &        $0.002\,s$        &   $0.8\,s$    &   $0.13\,s$   &   $0.14\,s$     \\
 \emph{(1024)} &   $9.1\,s$   &   $5.5\,s$   &         $9.4\,s$         &    $40\,s$    &    $31\,s$    &   $210\,s$      \\
  \emph{(16)}  &  $16.2\,s$   &   $4.9\,s$   &         $23\,s$          &   $160\,s$    &   $105\,s$    &    $94\,s$     \\
\end{tabular}

\end{figure*}

\begin{figure*}
\centering
\caption[List of things which are restored for each unit test]{}
\label{fig:restor}
List of things which are restored for each unit test.

$^a$ The Matlab path and the working directory is only set back after the test suite is finished, it is not set back between tests.\newline
$^b$ \emph{(experimentally)} Currently, this only works when it is enabled by the option 
\texttt{'rng',true} / \texttt{'base',true} at the first occurrence of the opening of a section. \newline
$^c$ \emph{TTEST} does not set back namespace imports, since (1) Octave does not support \texttt{import} yet and (2) the exact way how Matlab handles imports is undocumented. In particular, \texttt{import} statements are parsed before execution of a script/function begins.\\[2ex]

\begin{tabular}{l|c|c|c}
    What is restored           &         \emph{Matlab}          &          \emph{TTEST}          & \emph{MOxUnit} \\ \hline
    workspace / variables      &              Yes               &              Yes               &      Yes       \\
    Matlab path                & $\times$\makebox[0pt][l]{$^a$} &              Yes               &    $\times$    \\
    global variables           &            $\times$            &              Yes               &    $\times$    \\
    persistent variables       &            $\times$            &            $\times$            &    $\times$    \\
    state of global RNG        &            $\times$            &   Opt.\makebox[0pt][l]{$^b$}   &    $\times$    \\
    state of user-defined RNGs &            $\times$            &            $\times$            &    $\times$    \\
    figures                    &            $\times$            &            $\times$            &    $\times$    \\
    base workspace             &            $\times$            &   Opt.\makebox[0pt][l]{$^b$}   &    $\times$    \\
    working directory          & $\times$\makebox[0pt][l]{$^a$} &              Yes               &    $\times$    \\
    namespace \texttt{import}  &              Yes               & $\times$\makebox[0pt][l]{$^c$} &    $\times$    \\
    debugger breakpoints       &            $\times$            &              Yes               &    $\times$    \\
    warning state              &            $\times$            &              Yes               &    $\times$
\end{tabular}
\end{figure*}

\begin{figure*}
\centering
\caption[Boilerplate code needed to write unit tests]{}
\label{fig:boilerplate}
Number of characters, without whitespace, needed to write a basic unit test for frameworks for Matlab and for frameworks in other languages. 
One can see, writing tests in most languages needs comparable number of characters than \emph{TTESTs}.
A special case are \emph{Julia} and \emph{D} which have unit testing features already incorporated into the language itself.
The corresponding code stubs are printed in Figure~\ref{fig:codestub}.\\[2ex]



\begin{tabular}{l||c|c|c|c|c|c}
    Number of            &   \emph{TTESTs}   & \emph{TTESTs} & \multirow{2}{*}{\emph{MOxUnit}} & \emph{Matlab} &  \emph{Matlab}  & \emph{Matlab} \\
    characters needed    &      Script       &   Function    &                                 &    Script     &    Function     &     Class     \\ \hline
    for boilerplate code &     $10$/$0$      &      $0$      &              $74$               &      $0$      &      $55$       &     $57$      \\
    for one unit test    &      $35/25$      &     $32$      &              $32$               &     $23$      &      $52$       &     $56$      \\
                         &                   &               &                                 &               &                 &               \\ \hline
                         &                   &               &                                 &               &                 &               \\
                         & \emph{GoogleTest} & \emph{Catch2} &          \emph{PyTest}          & \emph{Julia}  & \emph{testthat} &   \emph{D}    \\
                         &       (C++)       &     (C++)     &            (Python)             &    (Julia)    &       (R)       &      (D)      \\ \hline
    for boilerplate code &       $17$        &     $17$      &              $44$               &     $22$      &       $0$       &      $0$      \\
    for one unit test    &       $35$        &     $37$      &              $38$               &      $9$      &      $41$       &    $40$  \\ &&&&&%
\end{tabular}
\end{figure*}

\begin{figure*}
\centering
\caption[Code stubs used for usability tests]{}
\label{fig:codestub}
Code stubs (or parts of it) used to count the number of characters needed to write unit tests and to asses the speed of various Matlab unit test frameworks and frameworks for other languages.\\[2ex]
\begin{multicols}{2}
\begin{itemize}[nosep,noitemsep,wide]
\item \emph{TTEST Script} with test runner \texttt{\color{keyword}runttests}
\begin{lstlisting}[aboveskip=0pt,belowskip=4pt]
TESTCASE( 'test_2_2' );
  EXPECT_EQ( 2, 2 );
\end{lstlisting}

\item 
\emph{TTEST Script} with test runner \verb|runtests|
\begin{lstlisting}[aboveskip=0pt,belowskip=4pt]
%% test_2_2
  EXPECT_EQ( 2, 2 );
\end{lstlisting}    

\item \emph{TTEST Function} with test runner \texttt{\color{keyword}runttests}
\begin{lstlisting}[aboveskip=0pt,belowskip=4pt]
function test_2_2
  EXPECT_EQ( 2, 2 );
\end{lstlisting}    

\item \emph{MOxUnit} with test runner \verb|moxunit_run_tests|
\begin{lstlisting}[aboveskip=0pt,belowskip=4pt]
function test_suite = test1Test
  test_functions = localfunctions();
    initTestSuite;

function test_2_2
  assertEqual( 2, 2 );
\end{lstlisting}

\item \emph{Matlab Script} with test runner \verb|runtests|
\begin{lstlisting}[aboveskip=0pt,belowskip=4pt]
%% test_2_2
  assert( 2 == 2 );
\end{lstlisting}  

\item \emph{Matlab Function} with test runner \verb|runtests|
\begin{lstlisting}[aboveskip=0pt,belowskip=4pt]
function tests = test2
  tests = functiontests( localfunctions );

function test_2_2( testCase )
  verifyEqual( testCase, 2, 2 );
\end{lstlisting}  

\item \emph{Matlab Class} with test runner \verb|runtests|
\begin{lstlisting}[aboveskip=0pt,belowskip=4pt]
classdef test2 < matlab.unittest.TestCase
  methods( Test )
    function test_2_2( testCase );
      testCase.verifyEqual( 2, 2 );
    end
  end
end
\end{lstlisting}
\pagebreak

\item \emph{GoogleTest} for C++   
\begin{lstlisting}[language=C++,aboveskip=0pt,belowskip=4pt]
#include <gtest.h>
TEST( Test2, Test22 ) {
  EXPECT_EQ( 2, 2 );
}
\end{lstlisting}

\item \emph{Catch2} for C++
\begin{lstlisting}[language=C++,aboveskip=0pt,belowskip=4pt]
#include <catch2/catch_test_macros.hpp>
TEST_CASE( "test_2_2" ) {
  REQUIRE( 2 == 2 );
}
\end{lstlisting}

\item \emph{PyTest} for Python
\begin{lstlisting}[language=Python,aboveskip=0pt,belowskip=4pt]
import unittest  
class test_2_2( unittest.TestCase ):
  def test_2_2( self )
    self.assertEqual( 2, 2 )
\end{lstlisting} 

\item \emph{Julia framework} in Julia
\begin{lstlisting}[language=Python,aboveskip=0pt,belowskip=4pt]
@testset "2_2" begin
  @test 2 == 2
end;
\end{lstlisting}

\item \emph{testthat} for \emph{R}
\begin{lstlisting}[language=Python,aboveskip=0pt,belowskip=4pt] 
test_that( "test_2_2", {
  expect_equal( 2, 2 )
} )
\end{lstlisting} 

\item \emph{D framework} in D
\begin{lstlisting}[language=C++,aboveskip=0pt,belowskip=4pt] 
/// test_2_2  
unittest {
  Assert.equal( 2, 2 );
}
\end{lstlisting} 

\end{itemize}
\end{multicols}
\end{figure*}


\newcommand{\doi}[1]{\href{https://doi.org/#1}{doi: #1}}
\newcommand{\arxiv}[1]{\href{https://arxiv.org/abs/#1}{arXiv: #1}}

\begin{thebibliography}{00}

\bibitem{Fow2006}
Martin Fowler,
\emph{Xunit}, \href{https://martinfowler.com/bliki/Xunit.html}{martinfowler.com/bliki/Xunit.html}, 2006.

\bibitem{WikiRefactor}
Wikipedia, \emph{Code refactoring}, 2021-09-08,
\href{https://en.wikipedia.org/wiki/Code_refactoring}{en.wikipedia.org/\-wiki/\-Code\_\-refactoring}.

\bibitem{Khor2020}
Vladimir Khorikov, \emph{Unit Testing Principles, Practices, and Patterns}, O'Reilly, 2020.


\bibitem{Fea2004}
Michael Feathers, \emph{Working Effectively with Legacy Code}, Prentice Hall PTR, USA, 2004.

\bibitem{Isa2016}
Per Isakson, \emph{tracer4m}, 2016,
\href{https://www.mathworks.com/matlabcentral/fileexchange/28929-tracer4m}%
{mathworks.com/matlabcentral/\-file\-ex\-change/\-28929}.
 
\bibitem{Schu2009}
David Schuler, Andreas Zeller,
\emph{Javalanche: Efficient mutation testing for Java},
ACM SIGSOFT (2009),
\doi{10.1145/1595696.1595750}.

\bibitem{AltUndoc}
Yair Altman,
\emph{Undocumented Matlab}, 2021-09-08,
\href{https://undocumentedmatlab.com/}{undocumentedmatlab.com}.

% Test frameworks

\bibitem{fw_lombardi}
Gabriele Lombardi,
\emph{MUnit: a unit testing framework in Matlab}, 2006,
\href{https://de.mathworks.com/matlabcentral/fileexchange/11306}{mathworks.com/matlabcentral/file\-exchange/\-11306}.

\bibitem{fw_christoper}
Christopher,
\emph{mlunit\_2008a}, 2009,
\href{https://de.mathworks.com/matlabcentral/fileexchange/21888}{mathworks.com/matlabcentral/\-file\-exchange/\-21888}.

\bibitem{fw_eddins}
Steve Eddins, 
\emph{xunit4}, 2010,
\href{https://www.mathworks.com/matlabcentral/fileexchange/47302}{mathworks.com/matlabcentral/\-fileexchange/\-47302}.

\bibitem{fw_arka}
arka, \emph{munit}, 2013,
\href{https://github.com/arka/munit}{github.com/arka/munit}.

\bibitem{fw_hetu}
\emph{mlUnit}, 2015,
\href{https://sourceforge.net/projects/mlunit/}{sourceforge.net/projects/mlunit/}.

\bibitem{fw_legland}
David Legland, 2016,
\href{https://de.mathworks.com/matlabcentral/fileexchange/7845}{mathworks.com/matlabcentral/fileexchange/7845}.

\bibitem{fw_schiessl}
Stefan Schiessl, 2016,
\href{https://www.mathworks.com/matlabcentral/fileexchange/52179}{mathworks.com/matlabcentral/fileexchange/52179}.

\bibitem{fw_mwgeurts}
Mark Geurts, 2016,
\href{https://github.com/mwgeurts/unit_harness}{github.com/mwgeurts/unit\_harness}.

\bibitem{fw_sexton}
Paul Sexton, Thomas Smith, 2016,
\href{https://github.com/psexton/matlab-xunit}{github.com/psexton/matlab-xunit}.

\bibitem{fw_moxunit}
Nikolaas N.\ Oosterhof et al., 2017,
\href{https://github.com/MOxUnit/MOxUnit}{github.com/MOxUnit/MOxUnit}

\bibitem{fw_zimmermann}
Ray Zimmermann, 2021,
\href{https://github.com/MATPOWER/mptest}{github.com/MATPOWER/mptest}.

\bibitem{fw_mathworks}
The MathWorks, \emph{Testing Frameworks}, 2021-09-08, \href{https://de.mathworks.com/help/matlab/matlab-unit-test-framework.html}{mathworks.com/\-help/\-matlab/\-matlab-unit-test-framework.html}

\bibitem{fw_ttest}
Thomas Mejstrik, Clara Hollomey, 2021,
\href{https://gitlab.com/tommsch/TTEST}{gitlab.com/tommsch/TTEST}.
 
\end{thebibliography}


\end{document}
