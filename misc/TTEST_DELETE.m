function TTEST_DELETE( filename, message );
% deletes the file with name 'filename'
% does not delete folders
% silently deletes files starting with 'message', default: '%TTEST_AUTOGENERATE'
    
	if( nargin<=1 );
        message = '%TTEST_AUTOGENERATE'; end;
    
    if( ismatlab() )
        recycle( 'on' ); end;
    try;
        val = fileread( filename );
        if( numel(val)>=numel(message) && strcmp(val(1:numel(message)),message) );
            silentdelete = true; 
        else;
            silentdelete = false; end
    catch;
        warning( 'TTEST:DELETE', 'Cannot delete file: %s', filename );
        return; end;

    if( ~silentdelete );
        fprintf( 'Do you want to delete the file: %s\n', filename );
        a = input( 'y/n: ', 's' ); 
        if( isequal(a,'y') );
            silentdelete = true; end; end;

    if( silentdelete );
        delete( filename ); end;
    
end

function dummy; end %#ok<DEFNU> %Generates an error, if the 'end' of a function is missing.

% This Source Code Form is subject to the terms of the Mozilla Public
% License, v. 2.0. If a copy of the MPL was not distributed with this
% file, You can obtain one at https://mozilla.org/MPL/2.0/.
