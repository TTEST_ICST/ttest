function str = TTEST_DISP( maxdisp, varargin );
% TTEST_DISP( [id], varargin )
% displays all inputs using disp, cuts of 
% E.g.: TTEST_DISP( [2;3;4;5], randn(200), 'abdsfasdf' )

    if( nargin<=1 );
        warning( 'TTEST:DISP', 'First argument to TTEST_DISP must be the maxmimum number of characters to be printed or empty.' ); end;
    if( isempty(maxdisp) );
        maxdisp = 500; end;
    str = '';
    for i = 1:numel( varargin );
        x = varargin{i};
        newstr = strnewlinetrim( evalc('disp(x)') );
        if( isempty(x) );
            if( iscell(x) );
                newstr = '{}';
            else;
                newstr = '[]'; end;
        elseif( numel(newstr) > maxdisp );
            newstr(maxdisp+1:end) = [];
            newstr = [newstr ' ... Output truncated' newline]; end;  %#ok<AGROW>
        str = [str newstr newline]; end; %#ok<AGROW>
    if( ~isempty(str) );
        str(end) = []; end; %remove last newline
end

function str = strnewlinetrim( str );

    while( ~isempty(str) );
        idx = [];
        if( numel(str)>=2 && str(2)==newline );
            idx = [idx 2]; end;  %#ok<AGROW>
        if( str(end)==newline );
            idx = [idx numel(str)]; end;  %#ok<AGROW>
        str(idx) = [];
        if( isempty(idx) );
            break; end; end;
     str = deblank( str );
        
end

function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.

% This Source Code Form is subject to the terms of the Mozilla Public
% License, v. 2.0. If a copy of the MPL was not distributed with this
% file, You can obtain one at https://mozilla.org/MPL/2.0/.
