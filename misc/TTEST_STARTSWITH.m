function ret = TTEST_STARTSWITH( str, pat );
    cl = class( str );

    ret = false;
    if( ~isequal(cl,'string') && ~isequal(cl,'char') );
        return; end;

    idx = strfind( lower(str), lower(pat) );
    if( isempty(idx) );
        return; end;
    if( idx(1)~=1 );
        return; end;

    ret = true;    
end

% This Source Code Form is subject to the terms of the Mozilla Public
% License, v. 2.0. If a copy of the MPL was not distributed with this
% file, You can obtain one at https://mozilla.org/MPL/2.0/.
