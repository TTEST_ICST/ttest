function ret = TTEST_ERROR_ASSERT_WORKER( id, ret, errid, errmsg )
% TTEST_ERROR_ASSERT_WORKER( [errid], [errmsg] )
% Default function called when an ASSERT_ fails
%
% See also: TTEST_ERROR_EXPECT_WORKER

TTEST( id, 'errorflag', true );
if( nargin<=0 || isempty(errid) );
    errid = 'TTEST:assertion:failed';
    errmsg = 'TTEST:assertion failed.';
elseif( nargin<=1 || isempty(errmsg) );
    errmsg = errid; end;

if( ismatlab );
    throwAsCaller( MException( errid, errmsg) );
elseif( isoctave )
    error( errid, errmsg );
else;
    error( 'TTEST:fatal', 'fatal error' ); end;

end

function dummy; end %#ok<DEFNU> %Generates an error, if the 'end' of a function is missing.

% This Source Code Form is subject to the terms of the Mozilla Public
% License, v. 2.0. If a copy of the MPL was not distributed with this
% file, You can obtain one at https://mozilla.org/MPL/2.0/.
