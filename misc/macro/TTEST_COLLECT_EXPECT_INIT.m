function [exp_exc, throw_exc] = TTEST_COLLECT_EXPECT_INIT( start, varargin );

    exp_exc = [ start varargin(2:end)];
    throw_exc = {};
    lastwarn( 'TTEST:NOWARN' );
    try; %#ok<TRYNC>
        %reset TTEST warning function
        warning( nan, nan ); end;

end

function dummy; end %#ok<DEFNU> %Generates an error, if the 'end' of a function is missing.

% This Source Code Form is subject to the terms of the Mozilla Public
% License, v. 2.0. If a copy of the MPL was not distributed with this
% file, You can obtain one at https://mozilla.org/MPL/2.0/.
