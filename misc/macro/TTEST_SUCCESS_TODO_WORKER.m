function ret_ = TTEST_SUCCESS_TODO_WORKER( id, ret, errid, errmsg, todoflag )
% TTEST_SUCCESS_TODO_WORKER()
% Default function called when a TODO test succeeds

    ds = dbstack;
    str = '';
    
    stackidx = 1;
    stackflag = true;
    while( stackflag && stackidx<numel(ds) );
        stackidx = stackidx + 1;
        stackflag = contains(ds(stackidx).file,'EXPECT') || contains(ds(stackidx).file,'ASSERT') || contains(ds(stackidx).file,'TODO') || startsWith(ds(stackidx).file,'TTEST'); end;
    if( ~stackflag );
        name =  ds(stackidx).file;
        fullname = which(name); end;
    if( ~stackflag && ~isempty(name) );
        str = [str sprintf( '\nIn: <a href="matlab: opentoline(''%s'',%i,0)"  style="font-weight:bold">%s:%i</a>\n', fullname, ds(stackidx).line, name, ds(stackidx).line )];
        [sec,subsec] = sectionname( stackidx );
        if( ~isempty(sec) );
            str = [str sprintf( 'Test name: %s\n', sec )]; end;
        if( ~isempty(subsec) );
            str = [str sprintf( 'Subtest name: %s\n', subsec )]; end; end;
    
    str = [str sprintf( '  >>TODO test succeeded<<\n' )];
    str = [str sprintf( '===========================\n' )];
    
    ret_ = {ret str};
end

function ret = contains( str, pat ) %needed for Octave compatibilty
    ret = any( strfind(str,pat) );
end




function dummy; end %#ok<DEFNU> %Generates an error, if the 'end' of a function is missing.

% This Source Code Form is subject to the terms of the Mozilla Public
% License, v. 2.0. If a copy of the MPL was not distributed with this
% file, You can obtain one at https://mozilla.org/MPL/2.0/.
