function [t_type, t_id, t_msg, t_line, t_name ] = TTEST_COLLECT_THROW_INIT( );

    t_type = '';
    t_id = {};
    t_msg = {};
    t_line = [];
    t_name = {};
    
    try; %#ok<TRYNC>
        warning( nan, nan ); end; %delete saved warnings in TTEST-warning function (if it is used)

end

function dummy; end %#ok<DEFNU> %Generates an error, if the 'end' of a function is missing.

% This Source Code Form is subject to the terms of the Mozilla Public
% License, v. 2.0. If a copy of the MPL was not distributed with this
% file, You can obtain one at https://mozilla.org/MPL/2.0/.
