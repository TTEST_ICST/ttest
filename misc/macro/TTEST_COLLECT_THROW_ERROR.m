function [ throw_exc ] = TTEST_COLLECT_THROW_ERROR( me );
    throw_exc = {'t', {me.identifier}, {me.message}, me.stack(1).line, {me.stack(1).name}, {me.stack(1).file}, 'Error'};
end

function dummy; end %#ok<DEFNU> %Generates an error, if the 'end' of a function is missing.

% This Source Code Form is subject to the terms of the Mozilla Public
% License, v. 2.0. If a copy of the MPL was not distributed with this
% file, You can obtain one at https://mozilla.org/MPL/2.0/.
