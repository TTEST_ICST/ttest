function ret_ = TTEST_ERROR_TODO_WORKER( id, ret, errid, errmsg, todoflag )
% TTEST_ERROR_TODO_WORKER()
% Default function called when a TODO_ fails
% prints out message that this is only a TODO test, except if notodoflag is set to false
%
% Tries to print out the filename, linenumber and testname
%
% See also: TTEST_ERROR_ASSERT_WORKER

    str = '';
    ds = dbstack;
    
    % get throwing function
    stackidx = 1;
    stackflag = true;
    while( stackflag && stackidx<numel(ds) );
        stackidx = stackidx + 1;
        if( isoctave );
            [~,ds(stackidx).file,~] = fileparts( ds(stackidx).file );
            ds(stackidx).file = [ds(stackidx).file '.m']; end;
        % search for first function not belonging to TTEST
        % we cannot use .name because of anonymous functions
        % octave reports whole pathname, but matlab only the filename.
        stackflag = ... 
             contains(lower(ds(stackidx).file),'expect') || ...
             contains(lower(ds(stackidx).file),'assert') || ...
             contains(lower(ds(stackidx).file),'todo') || ...
             contains(lower(ds(stackidx).file),'ttest') || ...
             strcmp(ds(stackidx).file,'GIVEN.m'); end;
    if( ~stackflag );
        name =  ds(stackidx).file;
        fullname = which(name); end;
    if( ~stackflag && ~isempty(name) );
        if( ismatlab() );
            str = [str sprintf( '\nIn: <a href="matlab: opentoline(''%s'',%i,0)"  style="font-weight:bold">%s:%i</a>\n', fullname, ds(stackidx).line, name, ds(stackidx).line )];
        else; 
            str = [str sprintf( '\nIn: %s:%i\n', name, ds(stackidx).line )]; end;
        %get %% and %%% strings
        [sec,subsec] = sectionname( stackidx ); 
        if( ~isempty(sec) );
            str = [str sprintf( 'Test name: %s\n', sec )]; end;
        if( ~isempty(subsec) );
            str = [str sprintf( 'Subtest name: %s\n', subsec )]; end; end;
    pos = TTEST( id, 'section' );
    if( pos.testcase.on && ~isempty(pos.testcase.name) );
        str = [str sprintf( 'Testcase: %s\n', pos.testcase.name )]; end;
    if( pos.section.on && ~isempty(pos.section.name) );
        str = [str sprintf( 'Section: %s\n', pos.section.name )]; end;
    if( pos.subsection.on && ~isempty(pos.subsection.name) );
        str = [str sprintf( 'Subsection: %s\n', pos.subsection.name )]; end;    
    
    % print TODO message
    if( nargin<=4 || todoflag );
        ret = true;
        str = [str sprintf( '    >>This is a TODO test<<\n' )]; end;    
    
    % print trace
    for i = 1:numel( ds );
        if( ismatlab )
            filename = ds(i).file;
        else;
            [~,filename,~] = fileparts( ds(i).file ); 
            filename = [filename '.m']; end;
        if( strcmp(filename,'TRACE.m') );
            idx1 = strfind( ds(i).name, 'ttest_id' );
            idx2 = strfind( ds(i).name(idx1:end), '''' );
            trace_id = ds(i).name(idx1:idx2+idx1-2);
            msg = TTEST( trace_id, 'message' );
            str = [str sprintf( '%s', msg )]; %#ok<AGROW>
            break; end; end;
            
    
    % print ending
    str = [str sprintf( '=====================================\n' )];
    ret_ = {ret, str};
end






function dummy; end %#ok<DEFNU> %Generates an error, if the 'end' of a function is missing. 

% This Source Code Form is subject to the terms of the Mozilla Public
% License, v. 2.0. If a copy of the MPL was not distributed with this
% file, You can obtain one at https://mozilla.org/MPL/2.0/.
