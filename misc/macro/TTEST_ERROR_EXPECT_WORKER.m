function ret = TTEST_ERROR_EXPECT_WORKER( id, ret, errid, errmsg )
% TTEST_ERROR_EXPECT_WORKER()
% Default function called when an EXPECT_ fails
%
% Tries to print out the filename, linenumber and testname
%
% See also: TTEST_ERROR_ASSERT_WORKER

    TTEST( id, 'errorflag', true );
    ret = TTEST_ERROR_TODO_WORKER( id, ret, errid, errmsg, false );
end




function dummy; end %#ok<DEFNU> %Generates an error, if the 'end' of a function is missing. 