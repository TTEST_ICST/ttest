function [ throw_exc ] = TTEST_COLLECT_THROW_WARNING( throw_exc );


   
    %collect TTEST warnings first, since They are usually a superset of the Matlab warning and more detailed
    try; %#ok<TRYNC>
        w = [];
        w = warning( nan ); end; %get thrown warnings from ttest-warning
    if( ~isempty(w) );
        for i = 1:numel( w );
            if( any(strfind(w{i}{1},':')) );
                exc = {'t' {w{i}{1}}, {{}}, {[]}, {{}}, {{}}, 'Warning'};  %#ok<CCAT1>
                %throw_exc = [throw_exc; exc]; % DEBUG
                if( ~isempty(w{i}{3}) );
                    exc{3} = {w{i}{2}}; %#ok<CCAT1> %message
                    exc{4} = w{i}{3}(1).line;
                    exc{5} = {w{i}{3}(1).name};
                    exc{6} = {w{i}{3}(1).file}; end; 
                throw_exc = [throw_exc; exc]; end; end; end; %#ok<AGROW> 
    
    %collect Matlab warning
    [lastmsg,lastid] = lastwarn;
    if( ~isequal(lastmsg,'TTEST:NOWARN') );
        throw_exc = [throw_exc; 't' {{lastid}} {{lastmsg}} {[]}, {{}}, {{}}, 'Warning']; end;    

end

function dummy; end %#ok<DEFNU> %Generates an error, if the 'end' of a function is missing.

% This Source Code Form is subject to the terms of the Mozilla Public
% License, v. 2.0. If a copy of the MPL was not distributed with this
% file, You can obtain one at https://mozilla.org/MPL/2.0/.
