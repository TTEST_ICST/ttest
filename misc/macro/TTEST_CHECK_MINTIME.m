function ret = TTEST_CHECK_MINTIME( id, cmd, minduration, F, t );
% evaluation function for _MINTIME macros
    if( isequal(F(1).State,'finished') );
        duration = seconds(F.FinishDateTime-F.StartDateTime);
        ret = false;
        msg = sprintf( 'Execution of command was too fast.\n  Minimum execution time: %i sec\n  Measured execution time: %f sec\n  Command:  %s\n', ...
            minduration, duration, TTEST_DISP(id,cmd) );
        EXPECT_FAIL( id, msg ); 
    else;
        EXPECT_SUCCEED();
        ret = true;
        end;
    cancel( F(1) ); % cancel future
    stop( t ); % stop timer
    delete( t ); % delete timer
end  

function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.

% This Source Code Form is subject to the terms of the Mozilla Public
% License, v. 2.0. If a copy of the MPL was not distributed with this
% file, You can obtain one at https://mozilla.org/MPL/2.0/.
