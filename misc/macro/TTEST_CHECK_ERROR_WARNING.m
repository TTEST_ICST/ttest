function [ ret, str ] = TTEST_CHECK_ERROR_WARNING( id, str, varargin )
% checks if thrown exceptions and expected exceptions match
% [ ret ] = TTEST_CHECK_ERROR_WARNING( id, exceptions )
% 
% Input:
% ========
%   id              ttest_id of testrun
%   exceptions      list of exceptions in the format 
%       'thrown', type, id,        msg,        line,...,    fun,...,   file,...
%       'mandatory',   [id1,...], [msg1,...], [line1,...], [fun1,...], [file1,...]
%       'optional',    [id1,...], [msg1,...], [line1,...], [fun1,...], [file1,...]
%                  
%  'thrown'/'mandatory'/'optional'    These are the thrown/mandatory/optional exceptions, where
%                       type        is the exception type specified as one of 'e','error','w' or 'warning'
%                       id          is the exception id, see below for the format of an id
%                       msg         is the exception message
%                       line        line number where exception is thrown
%                       fun         functionname where exception is thrown
%                       file        filename where exception is thrown, see below for the format of a filename
% Output:
% =========
%   ret            true if test succeeded
%   
%
% Notes:
% =========
%   A test succeeds if 
%       - all of the mandatory exceptions are thrown, and
%       - no exceptions are thrown which are either 'mandatory' or 'optional'.
%   Thus, a standard _THROW test has only 'mandatory' exceptions
%   and a standard _NTHROW test has only 'optional' exceptions
%   
%   A 'thrown' exception is matched with a 'mandatory'/'optional' one, 
%   if at least
%       - one id (or part of an id) (when given), 
%       - one thrown msg (or part of a msg) (when given),
%       - one line number (when given),
%       - one function name (when given) 
%       - one filename (or part of a filename) (when given)
%   matches
%
%   Format of arguments:
%           - Every integer or vector of integers is considered to be line numbers, the integers must be greater equal -1
%           - Every string 
%                   which only consists of parts, each of which is either empty or starts with a letter and only consisting of letters, numbers and underscore, where the parts are divided by ':' or
%                   starting with 'ttest_id' (where 'ttest_id' is stripped of from the string)
%             is considered to be an id.
%           - Every string
%                   ending on '.m', '.p', '/', '\' or 
%                   starting with '/', '~/', '\', 'A:\', ... 'Z:\', or
%                   starting with 'ttest_file' (where 'ttest_file' is stripped from the string)
%             is considered to be a file name
%           - Every string 
%                   begining with an alphabetic character, and containing only letters, numbers, or underscores, or
%                   starting with 'ttest_fun' (where 'ttest_fun' is stripped from the string)
%             is considered to be a functionname, or
%           - Every string
%                   not in a format above, or
%                   startin with 'ttest_msg'  (where 'ttest_msg' is stripped from the string)
%             is considered to be a message     
%


%#ok<*AGROW>
    
    %parse input
    %%%%%%%%%%%%%%
    tmo = cell( 0, 7 );
    idx = cellfun( 'isclass', varargin, 'cell' );
    if( any(idx) );
        tmo = vertcat( varargin{idx} ); 
        varargin(idx) = []; end;
    tmo = [tmo; TTEST_PARSE( varargin{:} )];
    
    %remove empty lines and split up
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % idx = all( cellfun( 'isempty', tmo(:,2:end-1) ), 2 );
    % tmo(idx,:) = [];
    [t,m,o] = TTEST_SPLIT_TMO( tmo );
        
    %check mandatory exceptions
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    if( isempty(t) && ~isempty(m) );
            str = [str sprintf( '\nNo exception was thrown (but one was expected).\n' )];
            ret = false;
            return; end;
    ret = false;
    for im = 1:size( m, 1 ); %loop through all mandatory exceptions
        for i = 1:5;
            switch i;
                case 1; IDX = IDX_ID;   what = 'id';        fun = @idfind;
                case 2; IDX = IDX_MSG;  what = 'message';   fun = @strfind;
                case 3; IDX = IDX_LINE; what = 'line';      fun = @isequal;
                case 4; IDX = IDX_FUN;  what = 'function';  fun = @strcmp;
                case 5; IDX = IDX_FILE; what = 'file';      fun = @strfind; end;        
                
        if( ~TTEST_CHECK_MANDATORY_EXCEPTION( t(:,IDX), m{im,IDX}, fun ) ) ;
            str = TTEST_CHECK_MANDATORY_OUTPUT( id, str, t, m, what, IDX );
            return; end; end; end;
    
    %check optional exceptions
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%
    ret = true;
    mo = [m;o];
    for it = 1:size( t, 1 ); %loop through all thrown exceptions
        ret = false;
        tit = t(it,:);
        for imo = 1:size( mo, 1 );
            moimo = mo(imo,:);
            flagid   = TTEST_CHECK_MANDATORY_EXCEPTION( tit{IDX_ID},   moimo{IDX_ID},   @idfind );
            flagmsg  = TTEST_CHECK_MANDATORY_EXCEPTION( tit{IDX_MSG},  moimo{IDX_MSG},  @strfind );
            flagline = TTEST_CHECK_MANDATORY_EXCEPTION( tit{IDX_LINE}, moimo{IDX_LINE}, @isequal );
            flagfun  = TTEST_CHECK_MANDATORY_EXCEPTION( tit{IDX_FUN},  moimo{IDX_FUN},  @strcmp );
            flagfile = TTEST_CHECK_MANDATORY_EXCEPTION( tit{IDX_FILE}, moimo{IDX_FILE}, @strfind );
            flags = [flagid flagmsg flagline flagfun flagfile];
            if( any(flags==1) && ~any(flags==0) );
                ret = true;
                break; end; end; 
        if( ret );
            continue; end;
        
        str = [str sprintf( [tit{IDX_EXC} ' was thrown which was not expected.\n'] )];
        str = [str sprintf( ['Thrown ' lower(tit{IDX_EXC}) ':\n'] )]; 
        if( ~isempty(tit{IDX_ID}) );   
            str = [str sprintf( ' id:       %s\n', tit{IDX_ID}{:} )]; end;
        if( ~isempty(tit{IDX_MSG}) );  
            str = [str sprintf( ' msg:      %s\n', tit{IDX_MSG}{:} )]; end;
        if( ~isempty(tit{IDX_LINE}) && ~isempty(tit{IDX_FUN}) && ~isempty(tit{IDX_FILE}) );
            if( ismatlab() );
                str = [str sprintf( '<a href = "matlab: opentoline(''%s'',%i,0)" style="font-weight:bold"> line:     %i</a>\n function: %s\n file:     %s\n\n', ...
                                which(tit{IDX_FILE}{:}), tit{IDX_LINE}, tit{IDX_LINE}, tit{IDX_FUN}{:}, tit{IDX_FILE}{:} )];
            else;
                str = [str sprintf( ' line:     %i\n function: %s\n file:     %s\n\n', tit{IDX_LINE}, tit{IDX_FUN}{:}, tit{IDX_FILE}{:} )]; end;
        else; 
            if( ~isempty(tit{IDX_LINE}) ); 
                str = [str sprintf( ' line:     %i\n', tit{IDX_LINE} )]; end;
            if( ~isempty(tit{IDX_FUN}) );  
                str = [str sprintf( ' function: %s\n', tit{IDX_FUN}{:} )]; end;
            if( ~isempty(tit{IDX_FILE}) ); 
                str = [str sprintf( ' file:     %s\n', tit{IDX_FILE}{:} )]; end; end;
        return; end;
    
end

function [ str ] = TTEST_CHECK_MANDATORY_OUTPUT( id, str, t, m, what, IDX );
    str = [str sprintf( 'Mandatory exception was not thrown.\nAn exception should throw one of the %s(s):\n  %s\n', what, TTEST_DISP( [], m{:,IDX} ) )];
    if( ~isempty(t(:,IDX)) );
        str = [str sprintf( 'But thrown exceptions has %s:\n  %s\n', what, TTEST_DISP([],t{:,IDX}) )]; 
    else;
        str = [str sprintf( 'But no exception was thrown.\n' )]; end;
end

function [ flag ] = TTEST_CHECK_MANDATORY_EXCEPTION( have, shouldhave, FUN );
% tests whether exception shouldhave is matched by any of the exceptions in have
% [ flag ] = TTEST_CHECK_MANDATORY_EXCEPTION( have, shouldhave, FUN );
%
% Input:
%   shouldhave  cell array, exception to be matched
%   have        cell array, exceptions used for matching
%   FUN         function handle, function used to compare elements in have with elements in shouldhave
%
% Output:  
%   flag    0       if it is not matched
%           1       if it is matched
%          -1       if all specificiations of have are empty
%
    if( isempty(have) || iscell(have)&&numel(have)==1&&isempty(have{1}) );
        checkflag = false;
    elseif( iscell(shouldhave) );
        checkflag = any( ~cellfun('isempty',shouldhave) );
    else;
        checkflag = ~isempty( shouldhave ); end;
    
    if( checkflag );
        flag = 0;
        for i = 1:size( have, 1 );
            if( iscell(have) );
                havei = have{i};
            else;
                havei = have(i); end;
            for jm = 1:numel( shouldhave );
                if( iscell(shouldhave) );
                    val = FUN( havei, shouldhave{jm} );
                else;
                    val = FUN( havei, shouldhave(jm) ); end;
                if( iscell(val) );
                    val = [val{:}]; end;
                if( any(val) );
                    flag = 1; 
                    break; end; end;
            if( flag );
                    break; end; end;
    else;
        flag = -1; end;
    
end

function [ t, m, o ] = TTEST_SPLIT_TMO( tmo );
    idxt = cellfun( @(x) isequal(x,'t'), tmo(:,1) );
    idxm = cellfun( @(x) isequal(x,'m'), tmo(:,1) );
    idxo = cellfun( @(x) isequal(x,'o'), tmo(:,1) );
    t = tmo(idxt,:);
    m = tmo(idxm,:);
    o = tmo(idxo,:);
    assert( all(idxt|idxm|idxo), 'TTEST:EXCEPTION', 'Programming error.' );
end

function tmo = TTEST_PARSE( varargin );
    tmoflag = '\0';  % thrown-mandatory-optional, state of current exception specifications to parse, uninitialized at the beginning
    tmo = cell( 0, IDX_EXC );
    
    NEWLINE = { '', {}, {}, [], {}, {}, '' };
    for i = 1:numel( varargin );
        if( isstring(varargin{i}) );
            varargin{i} = char( varargin{i} ); end;
        if( ischar(varargin{i}) );
            switch varargin{i};
                case {'t','thrown'};    
                    tmoflag = 't'; 
                    tmo = [tmo;NEWLINE]; %#ok<AGROW>
                    tmo{end,IDX_TYPE} = 't'; 
                    tmo{end,IDX_EXC} = 'Exception';
                    continue;
                case {'m','mandatory'}; 
                    tmoflag = 'm'; 
                    tmo = [tmo;NEWLINE]; %#ok<AGROW>
                    tmo{end,IDX_TYPE} = 'm';
                    tmo{end,IDX_EXC} = 'Exception';
                    continue;
                case {'o','optional'};  
                    tmoflag = 'o'; 
                    tmo = [tmo;NEWLINE]; %#ok<AGROW>
                    tmo{end,IDX_TYPE} = 'o';
                    tmo{end,IDX_EXC} = 'Exception';
                    continue; end; end;
            
        assert( ~isempty(tmo), 'TTEST:EXCEPTION', 'First argument of exception specification must be one of ''t'',''thrown'',''m'',''mandatory'',''o'',''optional''.' );
        
        if( isequal(tmoflag,'t') && ischar(varargin{i}) );
            switch varargin{i};
                case {'e','error'}; 
                    tmo{end,IDX_EXC} = 'Error';
                    continue;
                case {'w','warning'}; 
                    tmo{end,IDX_EXC} = 'Warning';
                    continue; end; end;
            
        [type,varargin{i}] = TTEST_IDENTIFY( varargin{i} );
        switch type;
            case 'id';   col = IDX_ID;
            case 'msg';  col = IDX_MSG;
            case 'line'; col = IDX_LINE;
            case 'fun';  col = IDX_FUN;
            case 'file'; col = IDX_FILE;
            case 'err'; error( 'TTEST:EXCEPTION', 'Wrong format of exception specification.' );
            otherwise; error( 'TTEST:EXCEPTION', 'Wrong format of exception specification.' ); end;
            
        if( ischar(varargin{i}) );
            if( isempty(tmo{end,col}) );
                tmo{end,col} = {}; end;
            tmo{end,col}{end+1} = varargin{i};
        else;
            tmo{end,col} = [tmo{end,col} varargin{i}]; end; end;
    
end

function [type,arg] = TTEST_IDENTIFY( arg );
    % Takes an argument and decides whether it is an
    %   - line number vector    'line'
    %   - exception id          'id'
    %   - exception message     'msg'
    %   - filename              'file'
    %   - functionname          'fun'
    %   - anything else         'err'
    %
    % The processed argument is returned as arg
    % The deduced type is returned as type

    if( isnumeric(arg) && all(isfinite(arg)) && all(~mod(arg,1)) && all(arg>=-1) );
        type = 'line';
        return; end;

    %check whether argument is a string
    if( isstring(arg) );
        arg = char( arg ); end;
    if( ~ischar(arg) );
        type = 'err';
        return; end;

    %check arguments depending on prefixes
    if( startsWith(arg,'ttest_id','IgnoreCase',true) );
        type = 'id';
        arg = arg(9:end);
        return;
    elseif( startsWith(arg,'ttestid','IgnoreCase',true) );
        type = 'id';
        arg = arg(8:end);
        return;        
    elseif( startsWith(arg,'ttest_msg','IgnoreCase',true) );
        type = 'msg';
        arg = arg(10:end);
        return;
    elseif( startsWith(arg,'ttestmsg','IgnoreCase',true) );
        type = 'msg';
        arg = arg(9:end);
        return;        
    elseif( startsWith(arg,'ttest_file','IgnoreCase',true) );
        type = 'file';
        arg = arg(11:end);
        return;
    elseif( startsWith(arg,'ttestfile','IgnoreCase',true) );
        type = 'file';
        arg = arg(10:end);
        return;
    elseif( startsWith(arg,'ttest_fun','IgnoreCase',true) );
        type = 'fun';
        arg = arg(10:end);
        if( ~TTEST_IDENTIFY_FUN(arg) );
            warning( 'TTEST:EXCEPTION', 'Given functionname ''%s'' does not have the format of a functionname.', arg ); end;
        return;        
    elseif( startsWith(arg,'ttestfun','IgnoreCase',true) );
        type = 'fun';
        arg = arg(9:end);
        if( ~TTEST_IDENTIFY_FUN(arg) );
            warning( 'TTEST:EXCEPTION', 'Given functionname ''%s'' does not have the format of a functionname.', arg ); end;
        return; end;

    if( TTEST_IDENTIFY_ID(arg) );
        type = 'id';
    elseif( TTEST_IDENTIFY_FILE(arg) );
        type = 'file';
    elseif( TTEST_IDENTIFY_FUN(arg) );
        type = 'fun';
    else;
        type = 'msg'; end

end

function flag = TTEST_IDENTIFY_ID( arg );
    flag = false;
    idx = find( arg==':' );
    if( isempty(idx) );
        return; end;
    allowedchars_beginning = 'abcdefghijklmnopqrstuvwxyz';
    allowedchars = 'abcdefghijklmnopqrstuvwxyz1234567890_:-';
    arg = lower( arg );
    if( ~all(ismember(arg, allowedchars)) );
        return; end;
    if( idx(1)~=1 );
        idx = [0 idx]; end;
    if( idx(end)==numel(arg) );
        idx(end) = []; end;
    if( ~all(ismember(arg(idx+1),allowedchars_beginning)) );
        return; end;
    flag = true;     
end

function flag = TTEST_IDENTIFY_FILE( arg );
    flag = false;
    if( startsWith( arg, '\' ) || ... 
        startsWith( arg, '/' ) || ... 
        startsWith( arg, '~/') || ...
        numel(arg)>=3 && ismember(arg(1),'ABCDEFGHIJKLMNOPQRSTUVWXYZ') && isequal(arg(2:3),':\') || ...
        endsWith( arg, '.m') || endsWith( arg, '.p') || endsWith( arg, '/') || endsWith( arg, '\') );
        %do nothing
    else;
        return; end;
    flag = true;
end

function flag = TTEST_IDENTIFY_FUN( arg );
    flag = isvarname( arg ) || startsWith( arg, '@(' );
end

function idx = idfind( str, pat );
    str = strrep( str, '-', '' );
    pat = strrep( pat, '-', '' );
    idx = strfind( lower(str), lower(pat) );
end

%% Constants
%%%%%%%%%%%%%%%%%

function ret = IDX_TYPE;    ret = 1; end
function ret = IDX_ID;      ret = 2; end
function ret = IDX_MSG;     ret = 3; end
function ret = IDX_LINE;    ret = 4; end
function ret = IDX_FUN;     ret = 5; end
function ret = IDX_FILE;    ret = 6; end
function ret = IDX_EXC;     ret = 7; end

function dummy; end %#ok<DEFNU> %Generates an error, if the 'end' of a function is missing.

% This Source Code Form is subject to the terms of the Mozilla Public
% License, v. 2.0. If a copy of the MPL was not distributed with this
% file, You can obtain one at https://mozilla.org/MPL/2.0/.
