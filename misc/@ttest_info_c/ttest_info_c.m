% Class which holds information and is identifiable via class()

classdef ttest_info_c
    
	properties ( Access = public)
        info;  % info
    end
    
    methods
        
        function obj = ttest_info_c( info_ );  % Constructor
            narginchk( 1, 1 );
            obj.info = info_;
        end
        
        function ret = isempty( obj );
            ret = strcmpi( obj.info, 'empty' );
        end
        
    end
end




function dummy; end %#ok<DEFNU> %Generates an error, if the 'end' of a function is missing.   

% This Source Code Form is subject to the terms of the Mozilla Public
% License, v. 2.0. If a copy of the MPL was not distributed with this
% file, You can obtain one at https://mozilla.org/MPL/2.0/.
