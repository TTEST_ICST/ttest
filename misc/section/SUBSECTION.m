function [ varargout ] = SUBSECTION( varargin )

    %% get ttest_id
    if( numel(varargin)>=1 && isa(varargin{1},'ttest_id_c') );
        id = varargin{1};
        varargin(1) = [];
    elseif( evalin( 'caller', 'exist(''ttest_id'',''var'');' ) );  % id not given
        id = evalin( 'caller', 'ttest_id;' );
    else;
        id = ttest_id_c( 0 ); end;

    %% parse input
    [ name, opt ] = parse_section_option( varargin{:} );

    % where are we?
    p = TTEST( id, 'pid' );
    pos = p.section;
    
    %% print output
    verbose = p.TTEST_VERBOSE;
    if( verbose>=2 );
        fprintf( 'Subsection start: %s\n', name );
    elseif( verbose>=1 );
        fprintf( '.' ); end;

    % parse the output of where we are
    if( ~pos.subsection.on );
        % if we are nowhere, then save what we have so far in 'base', 'testcase', 'section' and 'subsection'
        % if we are in a testcase, then save what we have so far in 'section' and 'subsection'
        % if we are in a section, then save what we have so far in  'subsection'
        if( ~pos.testcase.on );
            TTEST( id, 'section', 'testcaseon',1, 'testcasename','', 'sectionon',1, 'sectionname','', 'subsectionon',1, 'subsectionname',name );
        elseif( ~pos.section.on );
            TTEST( id, 'section', 'sectionon',1, 'sectionname','', 'subsectionon',1, 'subsectionname',name );
        else;
            TTEST( id, 'section', 'subsectionon',1, 'subsectionname',name ); end;
        
        if( opt.vars );
            vars = evalin( 'caller', 'who' );
            ws = [];
            if( numel(vars)>=1 );
                ws(numel( vars )).name = [];
                ws(numel( vars )).value = []; end;
            for ii = 1:numel( vars );
                ws(ii).name = vars{ii};  %#ok<AGROW>
                ws(ii).value = evalin( 'caller', vars{ii} ); end;  %#ok<AGROW>
            if( ~pos.testcase.on && ~pos.section.on );
                TTEST( id, 'section', 'testcasevars',ws, 'sectionvars', ws, 'subsectionvars', ws );
            elseif( ~pos.section.on );
                TTEST( id, 'section', 'sectionvars', ws, 'subsectionvars', ws );
            else;
                TTEST( id, 'section', 'subsectionvars', ws ); end; end;
        
        store_section_data( id, opt, 'subsection' );

    else;
        % if we are in a subsection, then we load the subsectionvars
        TTEST( id, 'section', 'subsectionon',1, 'subsectionname',name );  % add the new subsection
        
        if( opt.vars );
            evalin( 'caller', 'clear;' );  % delete the caller workspace

            % load the subsectionvars
            vars = pos.subsection.vars;
            for kk = 1:numel( vars );
                assignin( 'caller', vars(kk).name, vars(kk).value ); end; end;
        
        load_section_data( opt, pos.subsection ); end;

    if( nargout>=1 );
        varargout{1} = false; end;
    
end

function dummy; end %#ok<DEFNU> %Generates an error, if the 'end' of a function is missing.

% This Source Code Form is subject to the terms of the Mozilla Public
% License, v. 2.0. If a copy of the MPL was not distributed with this
% file, You can obtain one at https://mozilla.org/MPL/2.0/.
