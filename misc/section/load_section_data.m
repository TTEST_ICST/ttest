function load_section_data( opt, pos_type );  %#ok<INUSL>

    if( ~isfield(pos_type,'path') );
        return; end;
    
    if( ~isempty(pos_type.path) );
        if( path_debug_worker( pos_type.path{1}{2}, 'path' ) );
            path( pos_type.path{1}{1} ); end; end;


    if( ~isempty(pos_type.rng) );
        r = rng;
        if( ~isequal(r,pos_type.rng) );
            rng( pos_type.rng ); end; end;
    
    if( isempty(pos_type.global) );
        clearvars -global
        vars = pos_type.global;
        for kk = 1:numel( vars )
            if( ~isempty(vars(kk).name) );
                eval( ['global ' vars(kk).name '; ' vars(kk).name ' = ' vars(kk).value ';'] ); end; end; end;
                
    if( ~isempty(pos_type.warning) );
        w = builtin( 'warning' );
        if( ~isequal(w,pos_type.warning) );
            if( ismatlab );
                warning( pos_type.warning ); 
            else;
                for i = 1:numel( pos_type.warning );
                    warning( pos_type.warning(i).state, pos_type.warning(i).identifier ); end; end; end; end;
    
    if( ~isempty(pos_type.pwd) );
        p = pwd;
        if( ~isequal(p,pos_type.pwd) );
            cd( pos_type.pwd ); end; end;
    
    if( ~isempty(pos_type.base) );
        evalin( 'base', 'clear;' );
        vars = pos_type.base;
        for kk = 1:numel( vars )
            assignin( 'base', vars(kk).name, vars(kk).value ); end; end;
    
    if( ~isequal(pos_type.debug,[]) );
        if( ismatlab );
            if( path_debug_worker( pos_type.debug{1}{2}, 'debug' ) )
                dbclear all;
                dbstop( pos_type.debug{1}{1} ); end;
        else;
            db = dbstatus;
            if( ~isequal(db,pos_type.debug) );
                for i = 1:numel( db );
                    [~,fn] = fileparts( db(i).file );
                    dbclear( fn ); end;
                dbstop( pos_type.debug ); end; end; end;
        
end

function dummy; end   %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.

% This Source Code Form is subject to the terms of the Mozilla Public
% License, v. 2.0. If a copy of the MPL was not distributed with this
% file, You can obtain one at https://mozilla.org/MPL/2.0/.
