function [ ret ] = SECTION( varargin )

    %% get ttest_id
    if( numel(varargin)>=1 && isa(varargin{1},'ttest_id_c') );
        id = varargin{1};
        varargin(1) = [];
    elseif( evalin( 'caller', 'exist(''ttest_id'',''var'');' ) );  % id not given
        id = evalin( 'caller', 'ttest_id;' );
    else;
        id = ttest_id_c( 0 ); end;
    
    %% parse errorlevel
    idx = cellfun( 'isclass', varargin, 'ttest_errorlvl_c' );
    if( any(idx) );
        val = varargin{idx}();
        if( isequal(val{1},'dis') );
            assert( nargout>=1, 'TTEST:section', 'Section is disabled, but return value is discarded. This is an error.' );
            ret = false; 
            return; end; end;

    %% parse input
    [ name, opt ] = parse_section_option( varargin{:} );

    % where are we?
    p = TTEST( id, 'pid' );
    pos = p.section;
        
    %% print output
    verbose = p.TTEST_VERBOSE;
    if( verbose>=2 );
        fprintf( 'Section start: %s\n', name );
    elseif( verbose>=1 );
        fprintf( '.' ); end;

    %% parse the output of where we are
    if( ~pos.section.on );
        % if we are nowhere, then save what we have so far in 'base', 'testcase' and 'section'
        % if we are in a testcase, then save what we have so far in 'section'
        
        if( ~pos.testcase.on );
            TTEST( id, 'section', 'testcaseon',1, 'testcasename','', 'sectionon',1, 'sectionname',name );
        else;
            TTEST( id, 'section', 'sectionon',1, 'sectionname',name ); end;
        
        
        
        if( opt.vars );
            vars = evalin( 'caller', 'who' );
            ws = [];
            if( numel(vars)>=1 );
                ws(numel( vars )).name = [];
                ws(numel( vars )).value = []; end;
            for ii = 1:numel( vars );
                ws(ii).name = vars{ii};  %#ok<AGROW>
                ws(ii).value = evalin( 'caller', vars{ii} ); end;  %#ok<AGROW>
            if( ~pos.testcase.on );
                TTEST( id, 'section', 'testcasevars',ws, 'sectionvars',ws );
            else;
                TTEST( id, 'section', 'sectionvars',ws ); end; end;
        
        store_section_data( id, opt, 'section' );
        
    else
        % if we are in a section or subsection, then we load the sectionsvars
        TTEST( id, 'section', 'subsectionon',0, 'subsectionname','', 'sectionname',name );  % close subsection if necessary  % add the new sectionname

        if( opt.vars );
            evalin( 'caller', 'clear;' );  % delete the caller workspace

            % load the sectionvars
            vars = pos.section.vars;
            for kk = 1:numel( vars );
                assignin( 'caller', vars(kk).name, vars(kk).value ); end; end; 
    
        load_section_data( opt, pos.section ); end;

    if( nargout>=1 );
        varargout{1} = false; end;

end

function dummy; end %#ok<DEFNU> %Generates an error, if the 'end' of a function is missing.

% This Source Code Form is subject to the terms of the Mozilla Public
% License, v. 2.0. If a copy of the MPL was not distributed with this
% file, You can obtain one at https://mozilla.org/MPL/2.0/.
