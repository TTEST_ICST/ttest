function ENDTESTCASE( varargin )

    %% get ttest_id
    if( numel(varargin)>=1 && isa(varargin{1},'ttest_id_c') );
        id = varargin{1};
        varargin(1) = [];
    elseif( evalin( 'caller', 'exist(''ttest_id'',''var'');' ) );  % id not given
        id = evalin( 'caller', 'ttest_id;' );
    else
        id = ttest_id_c( 0 ); end;
    
    assert( numel(varargin)==0, 'TTEST:SECTION', 'End-section functions only accept at most one argument of ttest_id_c (which is discarded currently).' );

    %% we do not care where we are, just close testcases, sections and subsections
    pos = TTEST( id, 'section', 'testcaseon',0, 'sectionon', 0, 'subsectionon',0, 'testcasename','', 'sectionname','', 'subsectionname','' );
    assert( pos.testcase.on==true, 'TTEST:SECTION', 'No TESTCASE open. ENDTESTCASE can only be called, when a TESTCASE is open.' );

    %% clear workspace 
    evalin( 'caller', 'clear;' );

    %% load the testcase variables
    vars = pos.testcase.vars;
    for kk = 1:numel( vars );
        assignin( 'caller', vars(kk).name, vars(kk).value ); end;
        
    load_section_data( [], pos.testcase )

end        

function dummy; end %#ok<DEFNU> %Generates an error, if the 'end' of a function is missing.

% This Source Code Form is subject to the terms of the Mozilla Public
% License, v. 2.0. If a copy of the MPL was not distributed with this
% file, You can obtain one at https://mozilla.org/MPL/2.0/.
