function [ varargout ] = TESTCASE( varargin )

    %% get ttest_id
    if( numel(varargin)>=1 && isa(varargin{1},'ttest_id_c') );
        id = varargin{1};
        varargin(1) = [];
    elseif( evalin( 'caller', 'exist(''ttest_id'',''var'');' ) );  % id not given
        id = evalin( 'caller', 'ttest_id;' );
    else;
        id = ttest_id_c( 0 ); end;
    
    %% parse input
    [ name, opt ] = parse_section_option( varargin{:} );

    % where are we?
    p = TTEST( id, 'pid' );
    pos = p.section;
    
    %% print output
    verbose = p.TTEST_VERBOSE;
    if( verbose>=2 );
        fprintf( 'Testcase start: %s\n', name );
    elseif( verbose>=1 );
        fprintf( '.' ); end;

    %% parse the output of where we are
    if( ~pos.testcase.on );
        % if we are nowhere, then just save what we have so far in 'base' and 'testcase'
        
        
        if( true );
            TTEST( id, 'section', 'testcaseon',1, 'testcasename',name ); end;

        
        
        
        
        if( opt.vars );
            vars = evalin( 'caller', 'who' );
            ws = [];
            if( numel(vars)>=1 );
                ws(numel( vars )).name = [];
                ws(numel( vars )).value = []; end;
            for ii = 1:numel( vars )
                ws(ii).name = vars{ii};  %#ok<AGROW>
                ws(ii).value = evalin( 'caller', vars{ii} ); end;  %#ok<AGROW>
            TTEST( id, 'section', 'testcasevars', ws ); end;
        
        store_section_data( id, opt, 'testcase' );
        
    else;
        % if we are in a testcase, then we load the testcasevars
        TTEST( id, 'section', 'testcaseon',1, 'testcasename',name, 'sectionon',0, 'sectionname','', 'subsectionon',0, 'subsectionname','' );  % set testcase to on, add the new testname, set section to off, set subsection to off
        
        if( opt.vars && isfield(pos.testcase,'vars') );
            % delete the caller workspace
            evalin( 'caller', 'clear;' );  % clear is much faster then clearvars
         
            % load the testcasevars
            vars = pos.testcase.vars;
            for kk = 1:numel( vars )
                assignin( 'caller', vars(kk).name, vars(kk).value ); end; end; 
        
        load_section_data( opt, pos.testcase ); end;

    if( nargout>=1 );
        varargout{1} = false; end;

end

function dummy; end %#ok<DEFNU> %Generates an error, if the 'end' of a function is missing.

% This Source Code Form is subject to the terms of the Mozilla Public
% License, v. 2.0. If a copy of the MPL was not distributed with this
% file, You can obtain one at https://mozilla.org/MPL/2.0/.

