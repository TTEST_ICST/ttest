function store_section_data( id, opt, sectiontype )

    savecell = {};
    
    base = [];
    if( opt.base );
        bvars = evalin( 'base', 'who' );
        if( numel(bvars) >=1 );
            base(numel( bvars )).name = [];
            base(numel( bvars )).value = [];
            for ii = 1:numel( bvars );
                base(ii).name = bvars{ii};
                base(ii).value = evalin( 'base', bvars{ii} ); end; end; end;
    savecell(end+1:end+2) = {[sectiontype 'base'], base };    
    
    db = [];
    if( opt.debug );
        if( ismatlab)
            db = {{dbstatus,path_debug_worker(nan,'debug')}}; 
        else;
            db = dbstatus; end; end;
    savecell(end+1:end+2) = {[sectiontype 'debug'], db };    
    
    gws = [];
    if( opt.global );
        gvars = who( 'global' );
        gws = store_global( gvars ); end;
    savecell(end+1:end+2) = {[sectiontype 'global'], gws };    
     
    p = [];
    if( opt.path );
        p = {{path,path_debug_worker(nan,'path')}}; end;
    savecell(end+1:end+2) = {[sectiontype 'path'], p }; 
    
    pd = [];
    if( opt.pwd );
        pd = pwd; end;
    savecell(end+1:end+2) = {[sectiontype 'pwd'], pd };    
    
    r = [];
    if( opt.rng );
        r = rng; end;
    savecell(end+1:end+2) = {[sectiontype 'rng'], r };    
    
    w = [];    
    if( opt.warning );
        w = builtin( 'warning' ); end;
    savecell(end+1:end+2) = {[sectiontype 'warning'], w };
    
    if( ~isempty(savecell) );
        TTEST( id, 'section', savecell{:} ); end;
    
end

function [ ttest_gws_adhjkalshd37r278hjaksdf728zhdgjskhdfgasg ] = store_global( gvars );
    if( numel(gvars)>=1 );
        ttest_gws_adhjkalshd37r278hjaksdf728zhdgjskhdfgasg(numel( gvars )).name = [];
        ttest_gws_adhjkalshd37r278hjaksdf728zhdgjskhdfgasg(numel( gvars )).value = []; 
        for ii = 1:numel( gvars );
            eval( ['global ' gvars{ii} ';'] );
            ttest_gws_adhjkalshd37r278hjaksdf728zhdgjskhdfgasg(ii).name = gvars{ii};
            ttest_gws_adhjkalshd37r278hjaksdf728zhdgjskhdfgasg(ii).value = eval( gvars{ii} ); end;
    else;
        ttest_gws_adhjkalshd37r278hjaksdf728zhdgjskhdfgasg.name = [];
        ttest_gws_adhjkalshd37r278hjaksdf728zhdgjskhdfgasg.value = []; end;
end

function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.

% This Source Code Form is subject to the terms of the Mozilla Public
% License, v. 2.0. If a copy of the MPL was not distributed with this
% file, You can obtain one at https://mozilla.org/MPL/2.0/.
