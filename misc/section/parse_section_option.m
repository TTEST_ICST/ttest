function [ name, opt ] = parse_section_option( varargin );

    if( numel(varargin)==0 );
        name = '';
    else;
        name = varargin{1}; 
        varargin(1) = []; end;
    
    if( isstring(name) );
        name = char( name ); end;    

    opt.teardown = [];
    opt.setup = [];
    opt.vars = true;
    opt.path = true;
    opt.global = true;
    opt.rng = false;
    opt.pwd = true;
    opt.warning = true;
    opt.base = false;
    opt.debug = true;
    for i = 1:2:numel( varargin);
        switch varargin{i};
            case {'setdown','teardown'}; opt.teardown = varargin{i+1};
            case {'setup','tearup'}; opt.setup = varargin{i+1};
            case {'ws','var','vars'}; opt.vars = varargin{i+1};
            case {'path'}; opt.path = varargin{i+1};;
            case {'global'}; opt.global = true;
            case {'rng'}; opt.rng = true;
            case {'pwd'}; opt.pwd = true;
            case {'warning'}; opt.warning = true;
            case {'base'}; opt.base = true;
            case {'debug'}; opt.debug = true;
            otherwise; error( 'TTEST:section', 'wrong option given.' ); end; end;
    
end

function dummy; end %#ok<DEFNU> %Generates an error, if the 'end' of a function is missing.

% This Source Code Form is subject to the terms of the Mozilla Public
% License, v. 2.0. If a copy of the MPL was not distributed with this
% file, You can obtain one at https://mozilla.org/MPL/2.0/.
