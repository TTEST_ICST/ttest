classdef ttest_matcher_c
    
    properties ( Access = public)
        m;       % the matcher function
        t;       % the help text
        c;       % the comparison function
    end
    
    methods
        
        function obj = ttest_matcher_c( m_, t_, c_ ) %Constructor %t: variable concering the construction of tensor valued sequence
            assert( isa(m_,'function_handle'), 'ttest_matcher_c:argin', 'First argument to construct a matcher must be a function handle.' );
            assert( isstring(t_) || ischar(t_) , 'ttest_matcher_c:argin', 'Second argument to construct a matcher must be a string.' );
            obj.m = m_;
            obj.t = t_;
            if( nargin>=3 );
                assert( isa(c_,'function_handle'), 'ttest_matcher_c:argin', 'Optional third argument to construct a matcher must be a function handle.' );
                obj.c = c_; end;
        end
        
        function ret = subsref( obj, S );
            % Handle the first indexing on your obj itself
            switch S(1).type;
                case '()';
                    ret = obj.m( S.subs{:} );
                otherwise
                    % Enable normal "." and "{}" behavior
                    ret = builtin( 'subsref', obj, S(1) ); end;
            % Handle "chaining" (not sure this part is fully correct; it is tricky)
            %orig_B = B; % hold on to a copy for debugging purposes
            if numel(S) > 1
                ret = subsref( ret, S(2:end) ); end; % regular call, not "builtin", to support overrides
        end
        
        function x = ismatcher( ~ );
            x = true; 
        end
        
        function ret = eq( lhs, rhs ); ret = comparison( @eq, lhs, rhs ); end
        function ret = gt( lhs, rhs ); ret = comparison( @gt, lhs, rhs ); end
        function ret = ge( lhs, rhs ); ret = comparison( @ge, lhs, rhs ); end
        function ret = lt( lhs, rhs ); ret = comparison( @lt, lhs, rhs ); end
        function ret = le( lhs, rhs ); ret = comparison( @le, lhs, rhs ); end
        function ret = ne( lhs, rhs ); ret = comparison( @ne, lhs, rhs ); end
        function ret = isequal( lhs, rhs ); ret = comparison( @isequal, lhs, rhs ); end
        function ret = isequaln( lhs, rhs ); ret = comparison( @isequaln, lhs, rhs ); end
        
        function disp( obj )
            txt = obj.t;
            if( ~isempty(txt) );
                txt = lower( txt );
                txt(1) = upper( txt(1) );
                txt = strtrim( txt );
                txt(end+1) = '.';
                txt = strrep( txt, 'is not has', 'does not have' );
                txt = strrep( txt, '  ', ' ' );
                disp( txt );
            else;
                disp( 'Empty matcher.' ); end;
        end
        
        function ret = comparison( comp, lhs, rhs );
            if( ismatcher(lhs) );
                val = cellfun( @(x) comp(x,rhs), lhs.c{2} );
                ret = lhs.c{1}( val );
            else;
                val = cellfun( @(x) comp(lhs,x), rhs.c{2} );
                ret = rhs.c{1}( val ); end;
        end
        

    end
    
    methods (Access=private)
        function inf(obj)
            obj;
            fprintf( 'In function: ttest_matcher_c.inf\n' );
        end
    end
    
    methods ( Static, Access=private )
        function true
            fprintf( 'In function: ttest_matcher_c.true\n' );
        end
    end
    
    methods ( Static)
        function false
            fprintf( 'In function: ttest_matcher_c.false\n' );
        end
        function h = gethandle;
            h = {@ttest_matcher_c.true %this way it works for static methods
                 @ttest_matcher_c.false %for private and public
                 @le %this way it works for non-static methods
                 @inf %for private and public
                 };
        end
    end
    
end

function outside_matcher %#ok<DEFNU>
    fprintf( 'In function: outside_matcher\n' );
end

function dummy; end %#ok<DEFNU> %Generates an error, if the 'end' of a function is missing.   

% This Source Code Form is subject to the terms of the Mozilla Public
% License, v. 2.0. If a copy of the MPL was not distributed with this
% file, You can obtain one at https://mozilla.org/MPL/2.0/.
