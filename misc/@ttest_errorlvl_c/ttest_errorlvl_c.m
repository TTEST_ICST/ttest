% Class which represents the severity level of the test

classdef ttest_errorlvl_c;

	properties ( Access = public)
        errorhandler;
        successhandler;
        condition;
    end
    
    methods
        
        function obj = ttest_errorlvl_c( varargin );
            assert( numel(varargin)>=1, 'ttest_errorlvl_c:nargin', 'ttest_errorlvl_c needs at least one input argument' );
            condition = true;
            
            for idx = numel(varargin):-1:1;
                %cast to char
                if( ischar(varargin{idx}) || isstring(varargin{idx}) );
                    varargin{idx} = char( varargin{idx} ); 
                
                    %parse string conditions
                    switch lower( varargin{idx} );
                        case {'isunix','islinux','onunix','onlinux'}; 
                            varargin(idx) = [];
                            condition = condition && isunix;
                        case {'ispc','iswindows','onpc','onwindows'};
                            varargin(idx) = [];
                            condition = condition && ispc;
                        case {'ismac','isapple','onmac','onapple'};
                            varargin(idx) = [];
                            condition = condition && ismac;
                        case {'isx64','onx64'};
                            varargin(idx) = [];
                            condition = condition && any( strfind(computer,'64') );
                        case {'isx32','isx68','onx32','onx68'};
                            varargin(idx) = [];
                            condition = condition && ~any( strfind(computer,'64') );
                        case {'ismatlab','onmatlab'};
                            varargin(idx) = [];
                            condition = condition && ismatlab;
                        case {'isoctave','onoctave'};
                            varargin(idx) = [];
                            condition = condition && isoctave; end; end; end;
            
            % parse severity arguments
            severity_argnum = 1;
            if( numel(varargin)>=1 );
                if( strcmp(varargin{1},'dis') ); [val1{1},val1{2}] = deal( 'dis' );
                elseif( strcmp(varargin{1},'en') ); [val1{1},val1{2}] = deal( 'en' );
                elseif( strcmp(varargin{1},'todo') ); val1 = TTEST('todo');
                elseif( strcmp(varargin{1},'exp') ); val1 = TTEST('exp');
                elseif( strcmp(varargin{1},'ass') ); val1 = TTEST('ass'); 
                elseif( ischar(varargin{1}) ); val1{1} = str2func( varargin{1} );
                else;  val1{1} = varargin{1}; end; end;
            if( numel(varargin)>=2 );
                if( isempty(varargin{2}) ); val2 = val1;
                elseif( strcmp(varargin{2},'dis') ); [val2{1},val2{2}] = deal( 'dis' );
                elseif( strcmp(varargin{2},'en') ); [val2{1},val2{2}] = deal( 'en' );
                elseif( strcmp(varargin{2},'todo') ); val2 = TTEST('todo');
                elseif( strcmp(varargin{2},'exp') ); val2 = TTEST('exp');
                elseif( strcmp(varargin{2},'ass') ); val2 = TTEST('ass');
                elseif( ischar(varargin{2}) ); val2{2} = str2func( varargin{2} ); severity_argnum = 2;
                else;  val2{2} = varargin{2}; severity_argnum = 2; end; end;
            
            %parse additional conditional options
            for idx = 3:numel( varargin );
                if( isa(varargin{idx},'function_handle') );
                    condition = condition && varargin{3}(); 
                elseif( ischar(varargin{idx}) );
                    val = str2func( varargin{idx} );
                    condition = condition && val(); 
                elseif( isscalar(varargin{idx}) );
                    condition = varargin{idx}; 
                else;
                    assert( numel(varargin)>=1, 'ttest_errorlvl_c:option', 'Wrong condition given' );
                end; end;
            
            %assign variables
            if( severity_argnum==1 );
                obj.errorhandler = val1{1};
                obj.successhandler = val1{2}; 
            else;
                obj.errorhandler = val1{1};
                obj.successhandler = val2{2}; end;
            
            if( strcmp(obj.errorhandler,'en') ); 
                assert( strcmp(obj.successhandler,'en') ); end;
            if( strcmp(obj.errorhandler,'dis') ); 
                assert( strcmp(obj.successhandler,'dis') ); end;
            if( strcmp(obj.errorhandler,'en') );
                obj.errorhandler = 'dis';
                obj.successhandler = 'dis';
                condition = @() ~condition; end;            
            obj.condition = condition;
        end
                
        
        function ret = subsref( obj, S );
            % Handle the first indexing on your obj itself
            switch S(1).type;
                
                case '()';
                    if( isa(obj.condition,'function_handle') );
                        val = obj.condition();
                    else;
                        val = obj.condition; end;
                    if( val );
                        ret = {obj.errorhandler, obj.successhandler};
                    else
                        ret = {[], []}; end;
                otherwise;
                    % Enable normal "." and "{}" behavior
                    ret = builtin( 'subsref', obj, S(1) ); end;
            % Handle "chaining" (not sure this part is fully correct; it is tricky)
            if( numel(S) > 1 );
                ret = subsref( ret, S(2:end) ); end; % regular call, not "builtin", to support overrides
        end

    end
	
end


function dummy; end %#ok<DEFNU> %Generates an error, if the 'end' of a function is missing.   

% This Source Code Form is subject to the terms of the Mozilla Public
% License, v. 2.0. If a copy of the MPL was not distributed with this
% file, You can obtain one at https://mozilla.org/MPL/2.0/.

