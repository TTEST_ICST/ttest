% Class which holds information and is identifiable via class()

classdef OPTION
    
    properties ( Access = public)
        id;
        msg;
    end
    
    methods
        
        function obj = OPTION( id_, msg_ );  % Constructor
            narginchk( 1, 2 );
            obj.id = id_;
            if( nargin==2 );
                obj.msg = msg_; end;
        end
        
    end
end




function dummy; end %#ok<DEFNU> %Generates an error, if the 'end' of a function is missing.   

% This Source Code Form is subject to the terms of the Mozilla Public
% License, v. 2.0. If a copy of the MPL was not distributed with this
% file, You can obtain one at https://mozilla.org/MPL/2.0/.
