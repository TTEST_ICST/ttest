% Class which signals to input that a string has to be saved into persistent cache

classdef ttest_input_c;

	properties ( Access = public)
        str;
    end
    
    methods
        function obj = ttest_input_c( str_ );
            obj.str = str_;
        end

    end
	
end


function dummy; end %#ok<DEFNU> %Generates an error, if the 'end' of a function is missing.   

% This Source Code Form is subject to the terms of the Mozilla Public
% License, v. 2.0. If a copy of the MPL was not distributed with this
% file, You can obtain one at https://mozilla.org/MPL/2.0/.
