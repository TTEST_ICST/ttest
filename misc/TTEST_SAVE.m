function TTEST_SAVE( text, filename, message );
% saves the 'text' to the file with name 'filename'
% creates folders if necessary
% silently overwrites files starting with 'message'
% TTEST_SAVE( text, filename, [message] );
%
% Input:
%   text        string / char-array, content of the file
%   filename    string / char-array, filename of the file
%   message     string / char-array, default = '%TTEST_AUTOGENERATE', files starting with message are silently overwritten
%



    if( nargin==2 );
        message = '%TTEST_AUTOGENERATE'; end;
    try;
        val = fileread( filename );
        if( numel(val)>=numel(message) && strcmp(val(1:numel(message)),message) );
            overwrite = true; 
        else;
            overwrite = false; end
    catch;
        overwrite = true; end;

    if( ~overwrite );
        fprintf( 'Do you want to overwrite the file: %s\n', filename );
        a = input( 'y/n: ', 's' ); 
        if( isequal(a,'y') );
            overwrite = true; end; end;

    if( overwrite );
        if( ~exist(fileparts(filename),'dir') );
            status = mkdir( fileparts(filename)); 
            if( ~status );
                warning( 'TTEST:save', 'Cannot create directory %s.', filename ); 
                return; end; end;
        fid = fopen( filename, 'wt' );
        if( fid==-1 );
            warning( 'TTEST:save', 'Cannot save file %s.', filename );
            return;
        else;
            fprintf( fid, '%s', text ); end;
        if( fid>=0 );
            fclose( fid );
%             try;
%                 dbstatus( filename ); %check generated file for errors %this seems to be faster than checkcode()
%             catch me; %#ok<NASGU>
%                 fprintf( 'Generated filed %s contains errors.\n', filename ); end; 
        end; end;
end

function dummy; end %#ok<DEFNU> %Generates an error, if the 'end' of a function is missing.

% This Source Code Form is subject to the terms of the Mozilla Public
% License, v. 2.0. If a copy of the MPL was not distributed with this
% file, You can obtain one at https://mozilla.org/MPL/2.0/.
