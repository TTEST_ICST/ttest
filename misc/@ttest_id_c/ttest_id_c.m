% Class which represents the ttest_id-s

classdef ttest_id_c
    
	properties ( Access = public)
		id; % the test id
    end
    
    methods
        
        function obj = ttest_id_c( id_ ); %Constructor %t: variable concering the construction of tensor valued sequence
            if( nargin==0 );
                id_ = obj.newid; end;
            if( isnumeric(id_) );
                id_ = ['ttest_id_' num2str( id_ )];
            elseif( isequal(id_,'global') );
                id_ = 'ttest_id_0';
            elseif( ~TTEST_STARTSWITH(id_,'ttest_id_') );
                id_ = ['ttest_id_' id_]; end;
            obj.id = id_;
        end
        
        function ret = subsref( obj, S );
            % Handle the first indexing on your obj itself
            switch S(1).type;
                case '()';
                    ret = obj.id;
                otherwise;
                    % Enable normal "." and "{}" behavior
                    ret = builtin( 'subsref', obj, S(1) ); end;
            % Handle "chaining" (not sure this part is fully correct; it is tricky) 
            if( numel(S) > 1 );
                ret = subsref( ret, S(2:end) ); end; % regular call, not "builtin", to support overrides
        end
        
        function ret = newid( ~ );
        % [ id_ ] = ttest_id_c
        % gives a strictly increasing sequence of integers
        % function is mlock-ed, thus, `clear` does not reset the counter
            mlock
            persistent lastid;
            if( isempty(lastid) );
                lastid = 1; end;
            lastid = lastid + 1;
            ret = lastid;
        end
        
        function disp( obj );
            fprintf( 'ttest_id = %s\n', strtrim(TTEST_DISP([],obj.id)) );
        end
        
    end
end


function dummy; end %#ok<DEFNU> %Generates an error, if the 'end' of a function is missing.   

% This Source Code Form is subject to the terms of the Mozilla Public
% License, v. 2.0. If a copy of the MPL was not distributed with this
% file, You can obtain one at https://mozilla.org/MPL/2.0/.
