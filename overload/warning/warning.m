function [ varargout ] = warning( varargin );
% Shadowing of warning() in order to
%   1) store meta data of warning
%   2) save all (most) warnings thrown
%
%   [ msg ] = warning( ___ )                    all possible standard calls as defined for matlab-warning
%   warning( ['temp',[duration] ], ___ )        changes warning settings temporarily, duration is either a number or a string representing a number
%   warning( 'reset' )              resets the warning state prior to the last warning( 'off' ) call
%   [ w ] = warning( nan )          returns the saved warnings, in backwards order
%   [ w ] = warning( nan, nan )     returns the saved warnings, in backwards order and deletes all saved warnings
%
% Restrictions: 
%   Does not save calls from warning() or workspacefunc()
%   May not save warnings thrown from builtins
%
% Output:
%   msg     first return value of matlab-warning
%           (the undocumented second return value is not returned)
%   w       cell array, the saved warnings in the format
%

% Changelog: 2021-05-06         Added option 'temp'
%            2021-08-18         Automatically restore warnings after a `warning off`
%

    persistent allwarningoffflag;
    persistent warning_container;
    persistent warning_laststate;
    persistent timer_container;
    if( isempty(timer_container) );
        timer_container = cell( 0, 2 ); end;
    if( isempty(allwarningoffflag) );
        allwarningoffflag = true; end;
    
    MAXNUM = 500;
    if( isoctave );
        builtin( 'warning', 'off', 'Octave:shadowed-function' ); 
    else;
         builtin( 'warning', 'off', 'MATLAB:dispatcher:nameConflict' ); end;
    if( isempty(warning_laststate) );
        warning_laststate = builtin( 'warning' ); end;    
    if( isempty(warning_container) );
        warning_container = {}; end;
    
    if( numel(varargin)>=1 && ~ischar(varargin{1}) && ~isstring(varargin{1}) && ~isstruct(varargin{1}) );
        varargout{1} = warning_container;
        if( numel(varargin)==2  );
            warning_container = {};  end;
        return; end;


    ds = dbstack;
    if( numel(ds)>=2 && (isequal(ds(2).name,'workspacefunc') || isequal(ds(2).name,'warning')) );
        %do nothing
    else;
        if( numel(ds)>=2 );
            ds = ds(2:end);
        else; 
            ds = []; end;
        if( numel(varargin)==0 );
            warning_container = [ {{'SAVE', ds}} warning_container ];
        elseif( isstruct(varargin{1}) );
            warning_container = [ {{'LOAD', varargin{1}, ds}} warning_container ];
        % elseif( numel(varargin)==1 && isequal(varargin{1},'off') );
            % warning_laststate = builtin( 'warning' );
        elseif( ismatlab && numel(varargin)<=1 && isequal(varargin{1},'off') );
            warning_container = [ {{varargin{1:end} ds}} warning_container ];
            startdelay = 10;
            t = timer( 'TimerFcn', ['warning(''ttest_temp_back'',' num2str(size(timer_container,1)+1) ');'], 'StartDelay',startdelay );
            timer_container{end+1,2} = t;
            timer_container{end,1} = builtin( 'warning' );
            start( t );
            return;
        elseif( numel(varargin)<=2 && ( ...
            isequal(varargin{1},'off') || isequal(varargin{1},'on') || isequal(varargin{1},'query') || isequal(varargin{1},'error')...
              ) );
            warning_container = [ {{varargin{1:end} ds}} warning_container ];
        elseif( isequal(varargin{1},'temp') );  % temporarily change warnings
            warning_container = [ {{varargin{1:end} ds}} warning_container ];
            startdelay = 2;
            varargin(1) = [];
            if( numel(varargin)>=1 && (ischar(varargin{1}) || isstring(varargin{1})) );
                val = str2double( char(varargin{1}) );
                if( ~isnan(val) );
                    varargin(1) = [];
                    startdelay = val; end;
            elseif( numel(varargin)>=1 );
                varargin(1) = [];
                startdelay = varargin{2}; end;
            t = timer( 'TimerFcn', ['warning(''ttest_temp_back'',' num2str(size(timer_container,1)+1) ');'], 'StartDelay',startdelay );
            timer_container{end+1,2} = t;
            timer_container{end,1} = builtin( 'warning' );
            start( t );
            builtin( 'warning', varargin{:} );
            allwarningoffflag = false;
            return;
        elseif( isequal(varargin{1},'ttest_temp_back') );  % called by timer object to reenable warning, not to be used by the user
            warning_container = [ {{varargin{1:end} ds}} warning_container ];
            delete( timer_container{varargin{2},2} );
            builtin( 'warning', timer_container{varargin{2},1} );
            return;
        elseif( isequal(varargin{1},'reset') );  % this is for debugging reasons. Matlab sometimes turns warnings off
            warning_container = [ {{'RESET', ds}} warning_container ];
            varargin = {warning_laststate};
        elseif( numel(varargin)==1 || ~any(strfind(varargin{1},':')) );
            warning_container = [{{ 'TTEST:NOID', sprintf( varargin{1:end} ), ds }} warning_container];
        else;
            warning_container = [{{ varargin{1}, sprintf( varargin{2:end} ), ds }} warning_container]; 
            end; end;
    
    if( numel(warning_container)>=2*MAXNUM );
        warning_container = warning_container( 1:MAXNUM ); end;

    if( ~nargout );
        builtin( 'warning', varargin{:} );
    else;
        msg = builtin( 'warning', varargin{:} ); 
        varargout{1} = msg; end;
    
    %% Safety check against Matlab functions
	woff.identifier = 'all';
    woff.state = 'off';
    w = builtin( 'warning' );
    if( isequal(w,woff) );
        if( numel(ds)>=1 );
            name = which( ds(1).name );
            flag = ~any(strfind( name, matlabroot ));
        else;
            flag = false; end;
        if( flag && ~allwarningoffflag );
            allwarningoffflag = true;
            fprintf( 'WARNING: All warnings disabled.\n' ); end; end;

end

function dummy; end  %#ok<DEFNU>  %Generates an error, if the 'end' of a function is missing.

% This Source Code Form is subject to the terms of the Mozilla Public
% License, v. 2.0. If a copy of the MPL was not distributed with this
% file, You can obtain one at https://mozilla.org/MPL/2.0/.
