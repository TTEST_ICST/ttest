function [ varargout ] = addpath( varargin );
% wrapper function for path, which stores whenever the path is potentially changed
    if( nargout>=0 );
        varargout = cell( 1, nargout ); end;
    [ varargout{:} ] = path_debug_worker( 'addpath', 'path', varargin{:} );

end

function TTEST_dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing. 

% This Source Code Form is subject to the terms of the Mozilla Public
% License, v. 2.0. If a copy of the MPL was not distributed with this
% file, You can obtain one at https://mozilla.org/MPL/2.0/.
