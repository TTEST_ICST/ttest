function [ varargout ] = path_debug_worker( funcname, type, varargin )

% type must be one of: 'path', 'debug'
% funcname must be any function name OR nan, or the returned value from a call with nan
% varargin are the arguments which shall be passed to function funcname

    persistent lastpathchange;
    persistent handle;

    if( isequaln(funcname,nan) );
        varargout{1} = lastpathchange.(type); 
		return; 
	elseif( isnumeric(funcname) );
		varargout{1} = lastpathchange.(type)>funcname;
		return; end;
    
    assert( isstring(funcname) || ischar(funcname) );
    funcname = char( funcname );

    if( nargout>=1 );
        varargout = cell( 1, nargout ); end;
    

    if( isempty(handle) );
        handle = struct; 
        lastpathchange = struct; 
        lastpathchange.path = now;
        lastpathchange.debug = now; end;
    
    if( ~isfield(handle,funcname) );
        handle.(funcname) =  getmatlabfunction( funcname ); end;
    
    if( nargout==0 );
        handle.(funcname)( varargin{:} );
    else;
        [ varargout{:} ] = handle.(funcname)( varargin{:} ); end;
    
    lastpathchange.(type) = now;
    
end

function TTEST_dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing. 

% This Source Code Form is subject to the terms of the Mozilla Public
% License, v. 2.0. If a copy of the MPL was not distributed with this
% file, You can obtain one at https://mozilla.org/MPL/2.0/.
