function [ ret, flag ] = input( varargin )
% overload for matlabs input allowing programmatically inputing stuff
% [ ret, [flag] ] = input( prompt, ['debug'] );
% [ ret, [flag] ] = input( prompt, ['s'], ['debug'] );
%
% Input:
%   prompt      string, returns the evaluated entered text
%   's'         returns the entered text, without evaluating the input as an expression.
%
% Debug options:
%   'debug'     if given, output variable flag is set to false whenever the function would need to ask for user input
%
% Output:
%   ret         the stuff returned by Matlab-input
%
% Debug output:
%   flag        false if 'debug' is set, user input was necessary, but no user input occured. In this case, ret is set to []
%   
% Additionally input:
%   If a variable with name 'ttest_input' exists in the caller workspace, the content of this variable is evaluated/returned
%


    flag = true;
    persistent inputcache;
    if( numel(varargin)>=1 && ischar(varargin{end}) && strcmp(varargin{end},'debug') );
        noinputflag = true;
        varargin(end) = [];
    else;
        noinputflag = false; end;
    
    if( isempty(inputcache) );
        inputcache = {}; end;
    if( isa(varargin{1},'ttest_input_c') );
        inputcache{end+1} = varargin{1}.str; 
        ret = varargin{1}.str;
        return; end;
    
    if( ~isempty(inputcache) );
        %return content in inputcache
        in = inputcache{1};
        inputcache(1) = [];
        if( numel(varargin)==1 );
            try;
                ret = eval( in );
            catch me;
                throw me; end;
        else;
            ret = in; end;
    elseif( evalin( 'caller', 'exist( ''ttest_input'', ''var'' );') && ~isempty(evalin( 'caller', 'ttest_input' )) );
        %return content from caller site
        ttest_input = evalin( 'caller', 'ttest_input' );
        in = ttest_input{1};
        ttest_input(1) = [];
        assignin( 'caller', 'ttest_input', ttest_input );
        if( numel(varargin)==1 );
            try;
                ret = eval( in );
            catch me;
                throw me; end;
        else;
            ret = in; end;
    else;
        %get input from keyboard
        if( noinputflag );
            ret = [];
            flag = false; 
        else;
            ret = builtin( 'input', varargin{:} ); end; end;
        
    
end

function dummy; end %#ok<DEFNU> %Generates an error, if the 'end' of a function is missing. 

% This Source Code Form is subject to the terms of the Mozilla Public
% License, v. 2.0. If a copy of the MPL was not distributed with this
% file, You can obtain one at https://mozilla.org/MPL/2.0/.
