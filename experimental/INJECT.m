function INJECT( name, varargin );
% INJECT( name, [faultchance], [faultconditions], [mutationchance], [mutations], [type] );
%
% Input:
%   faultchance         double between 0 and 1, default=0, chance that the call throws an error 
%   faultconditions     optional, default=empty, cell array of cell array of Matlab-evaluatable-strings, conditions when a fault occurs unconditionally
%   mutationchance      double between 0 and 1, default=0, chance that the call gets mutated
%   mutations           mandatory if mutationchance~=0, cell array of cell arrays of Matlab-evaluateable-strings, list of mutations
%   type                string of cell array of strings, default=empty, if given, only generates overloads for these types
% 
% Output:
%   Overloaded 

%#ok<*AGROW>

% XX currently broken since it uses old global variables

error( 'currently broken' );

TTEST_DIR_INJECT = 'inject';

if( nargin==0 );
    deleteinjects(); 
    return; end;
 
% Pre processing
%%%%%%%%%%%%%%%%%%%%%%%%%%%%
olddir = cd( fileparts(which('INJECT')) );
clean_dir = onCleanup(@(~)cd(olddir)); % Make sure we always end up where we started

oldwarning = warning;
clean_warning = onCleanup( @(~)warning(oldwarning) ); % Make sure we always restore the warnings
warning( 'off', 'MATLAB:mpath:packageDirectoriesNotAllowedOnPath' );

switch name; case {'lt', 'builtin', 'function', 'switch' };
    error( 'TTEST:INJECT', ['The function ''' name ''' cannot be injected since it would break Matlab.'] ); end;

val = regexp( path, pathsep, 'split' );
val = cellfun( @lower, val, 'UniformOutput',false );
onPath = any( contains(val,fullfile('ttest','inject')) );
if( ~onPath )
    fprintf( 'Add INJECTs to matlab-path:\n' );
    addpath( fullfile( pwd ) );
    addpath( fullfile( pwd, 'inject' ) );
    addpath( fullfile( pwd, TTEST_DIR_INJECT ) ); end; %namespaces are ignored by Matlab, thus, this is safe

% parse input
%%%%%%%%%%%%%%%
if( numel(varargin)==0 || isempty(varargin{1}) || ~isscalar(varargin{1}) );
    faultchance = 0; 
else;
    faultchance = varargin{1}; 
    varargin(1) = []; end;
if( numel(varargin)>=1 && iscell(varargin{1}) && iscell(varargin{1}{1}) )
    faultcondition = varargin{1}; 
    varargin(1) = []; 
else;
    faultcondition = []; end;
if( numel(varargin)<=1 || isempty(varargin{1}) );
    mutationchance = 0;
    mutation = [];
else;
    mutationchance = varargin{1}; 
    mutation = varargin{2}; 
    varargin(1:2) = []; end;
if( numel(varargin)==1 && (isstring(varargin{1}) || ischar(varargin{1}) || iscell(varargin{1})) );
    type = varargin{1};
    varargin(1) = [];
    if( ~iscell(type) );
        type = {type}; end;
else;
    type = []; end;

assert( isempty(varargin), 'TTEST:INJECT', 'Wrong input parameters.' );

if( ischar(mutation) || isstring(mutation) );
    switch mutation;
        case {'comparison','comp'};
            mutation = {
                'fun = ''lt'';', 'fun = ''le'';', 'fun = ''gt'';', 'fun = ''ge'';', 'fun = ''ne'';', ...
                'i1 = i1.*0;', 'i1 = i1+1;', 'i1 = 2.*i1;', 'i1 = 1.1*i1;', 'i1 = 0.9*i1;', ...
                'x = i1; i1 = i2; i2 = x;','i2 = i2.*0;', 'i2 = i2 + 1;', 'i2 = 2.*i2;', 'i2 = 1.1*i2;', 'i2 = 0.9*i2;' ...
                };
        otherwise;
            error( 'wrong name for ''mutation''.' ); end; end;
    
assert( mutationchance==0 || ~isempty(mutation), 'TTEST:INJECT', 'If mutationchance>0, one must also give mutations.' );
    
if( faultchance>1 );
    faultchance = faultchance/100; end;
if( mutationchance>1 );
    mutationchance = mutationchance/100; end;


% get information about function
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
[info1,~] = eval( ['which( ''' name ''', ''-all'' );'] );
info1 = splitlines( info1 );
classname = {};
builtinflag = [];
matlabflag = [];
ttestflag = [];
pathname = {};
extname = {};
filename = {};
fullfilename = {};
for i = 1:numel( info1 );
    if( ispc );
         val = regexp( info1{i}, '(?<=\@)(.*?)(?=\\)', 'match' );
    else;
        val = regexp( info1{i}, '(?<=\@)(.*?)(?=\/)', 'match' ); end; 
    if( isempty(val) );
        val = {''}; end;
    classname{end+1} = val{1};
    builtinflag(end+1) = contains( info1{i}, 'built-in' ); 
    matlabflag(end+1) = contains( lower(info1{i}), lower(matlabroot) );
    ttestflag(end+1) = contains( lower(info1{i}), 'ttest' );
    matlabflag(end) = matlabflag(end) | builtinflag(end); %builtins are always Matlab stuff
    if( ~builtinflag(end) );
        idx = strfind( info1{i}, '(' );
        val = info1{i};
        val(1:idx) = []; 
        fullfilename{end+1} = val;
        [pathname{end+1},filename{end+1},extname{end+1}] = fileparts( val ); 
    else;
        fullfilename{end+1} = '';
        [pathname{end+1},filename{end+1},extname{end+1}] = deal( '' ); end; 
%     if( ~strcmp(extname{end},'.m') );
%         warning( 'TTEST:INJECT', 'To be injected function has not the file extensions ''.m''.' ); end;
%     if( ~strcmp(filename{end},name) );
%         warning( 'TTEST:INJECT', 'Probably a Matlab bug. Function found to be injected has different name then ''name''.' ); end;
    
end; 

idx = find( ~cellfun( 'isempty', pathname ) & ~ttestflag, 1 );
if( ~isempty(idx) );
    here = cd( pathname{idx} );
else;
    error( 'TTEST:INJECT', 'Could not find function folder. Inject not possible.' ); end;

try;
    numargin = nargin( name );
    numargout = nargout( name );
catch;
    warning( 'TTEST:INJECT', 'Could not find function from which one can deduce nargin and nargout.' );
    numargin = -1;
    numargout = -1; end;


cd( here );
% Generate function
%%%%%%%%%%%%%%%%%%%%%%%%%%
    if( faultchance==0 && mutationchance==0 && isempty(faultcondition) ); %delete injected files
        for i = 1:numel( pathname );
            if( ~ttestflag(i) );
                continue; end;
            TTEST_DELETE( fullfilename{i}, '%TTEST_AUTOGENERATE_INJECT' ); end;
        
    else; %create injected files
        for i = 1:numel( pathname );
            if( ttestflag(i) );
                continue; end;
            if( ~isempty(type) && ~any(cellfun(@(x)strcmp(x,classname{i}),type)) );
                continue; end;
            GENERATE_INJECT( name, numargin, numargout, faultchance, faultcondition, mutationchance, mutation, ...
                             classname{i}, builtinflag(i), matlabflag(i), ttestflag(i), pathname{i} ); end; end;

end

function [var,varmax] = VARARGIN_VARARGOUT( n, nmax, varname, vararginout )
    % generates the string of the variable names in the function definition
    varmax = '';
    if( n == 0 );
        var = ' ';
    elseif( n<0 );
        var = arrayfun( @(x) [varname num2str(x) ', '], 1:-n-1, 'UniformOutput',false );
        var = [var{:}];
        var = [var vararginout];
        varmax = arrayfun( @(x) [varname num2str(x) ', '], -n:abs(nmax), 'UniformOutput',false );
        varmax = [varmax{:}];
        varmax(end-1:end) = [];
    else;
        var = arrayfun( @(x) [varname num2str(x) ', '], 1:n, 'UniformOutput',false );
        var = [var{:}];
        var(end-1:end) = []; end;
end

function GENERATE_INJECT( name, numargin, numargout, faultchance, faultcondition, mutationchance, mutation, ...
                          classname, builtinflag, matlabflag, ttestflag, path )
                      
    TTEST_DIR_INJECT = 'inject';
    INPUTVAR = 'i';
    OUTPUTVAR = 'o';
    MAXNUMVAR = 10;
    MESSAGE = '%TTEST_AUTOGENERATE_INJECT'; 
    
    if( numargin>MAXNUMVAR );
        warning( 'TTEST:INJECT', 'Function expects more arguments than current maximum. In order to inject this function, you must alter the sourcecode.' ); end;
    
    message = [ MESSAGE newline '%This file is auto-generated. Do not modify it. Changes may be overwritten.' newline];
    
    [varin,varinmax] = VARARGIN_VARARGOUT( numargin, MAXNUMVAR, INPUTVAR, 'varargin' );
    varout = VARARGIN_VARARGOUT( numargout, MAXNUMVAR, OUTPUTVAR, 'varargout' );
    header = [
        'function [ ' varout ' ] = ' name '( ' varin ' )' newline ...
        '% This is a mutated and/or fault injected version of ' name '().' newline newline ...
        ];
    
% pre-stuff
pretxt = [
    'global TTEST_ENABLE_INJECTION;' newline ...
    '%temporarily disable injections' newline ...
    'OLDVAL_TTEST_ENABLE_INJECTION = TTEST_ENABLE_INJECTION;' newline ...
    'if( isempty(TTEST_ENABLE_INJECTION) || ~TTEST_ENABLE_INJECTION );' newline ...
    '     TTEST_ENABLE_INJECTION = false;' newline ...
    '     mutationchance = 0;' newline ...
    '     faultchance = 0;' newline ...
    'else;' newline ...
    '    TTEST_ENABLE_INJECTION = false;' newline ...
    '    mutationchance = ' num2str(mutationchance) ';' newline ...
    '    faultchance = ' num2str(faultchance) '; end;' newline ...
    'r = rng; %save state of random number generator' newline ...
    ];
    
    
% make input variables
    infostuff = ['fun = '''';' newline];
    if( numargin<0 );
        inputvar = [
            '[' varinmax '] = TTEST_DEAL( varargin{1:min(' num2str( MAXNUMVAR ) ',numel(varargin))} );' newline ...
            'varargin = varargin(11:end);' newline ];
    else;
        inputvar = ['varargin = cell( 0, 0 );' newline]; end;
        
    outputvar = '';
    if( numargout<0 );
        outputvar = ['varargout = cell( 1, ' num2str(nargout+numargout+1) ' );' newline newline]; end;
        
    faulttxt = '';
    if( faultchance>0 || ~isempty(faultcondition) );
        faulttxt = [
            '% Fault injection' newline ...
            '%%%%%%%%%%%%%%%%%%%%' newline ...
            'global TTEST_FAULT_COUNTER;' newline ]; end;
    faultcondtxt = '';
    if( ~isempty(faultcondition) );
        for i = 1:numel( faultcondition );
            for j = 1:numel( faultcondition{i} );
                faultcondtxt = [ faultcondtxt ...
            'try; if( ' faultcondition{i}{j} ' ); %#ok<TRYNC>' newline ...
            '    faultchance = 1; end; end;' newline ...
            ]; end; end; end;    
    if( faultchance>0 );
        faulttxt = [ faulttxt faultcondtxt ...
            'if( faultchance && rand<faultchance );' newline ...
            '    TTEST_ENABLE_INJECTION = OLDVAL_TTEST_ENABLE_INJECTION;' newline ...
            '    TTEST_FAULT_COUNTER = TTEST_FAULT_COUNTER + 1;' newline ...
            '    error( ''TTEST:FAULT:all'', ''TTEST:FAULT:all - Injected error.'' ); end;' newline newline]; end;

        
    mutationtxt = '';
    if( mutationchance>0 );
        mutationtxt = [ % XX This is not finished yet
            '% Mutations' newline ...
            '%%%%%%%%%%%%%%%%%%%%%%' newline ...
            'global TTEST_MUTATION_COUNTER;' newline ...
            'mutated = false;' newline ...
            'if( mutationchance && rand<mutationchance );' newline ...
            '    TTEST_MUTATION_COUNTER = TTEST_MUTATION_COUNTER + 1;' newline ...
            '    while( ~mutated );' newline ...
            '        try; %#ok<TRYNC>' newline ...
            '            x = randi( ' num2str(2*numel(mutation)+1) ' );' newline ... )-1;' newline ...
            '            switch x;' newline]; 
        mutcase = '';
        for i = 1:numel( mutation );
            mutcase = [mutcase '                case {' num2str(i) ', ' num2str(i+numel(mutation)) '}; ' mutation{i} newline ]; end;
        mutcase = [mutcase '                otherwise; %do nothing - to avoid infinite loops' newline ];
        mutationtxt = [mutationtxt mutcase ...
            '                end;' newline ...
            '            mutated = true; end; %this line is only reached when no error occured' newline ...
            '    end; end;' newline newline ...
            ]; end;
    
    [precall,postcall] = deal( '' );
    if( ~builtinflag );
        precall = ['here = cd( ''' path ''' );' newline 'try;' newline newline]; 
        postcall = ['catch me;' newline 'cd( here ); rethrow( me ); end;' newline 'cd( here );' newline]; end;
    
    if( ~builtinflag );
        callingfunction = 'feval';
    else;
        callingfunction = 'builtin'; end;
    
    varout2 = varout;
    if( numargout<0 );
        varout2 = [varout2 '{:}']; end;
    calltxt = [
            'if( isempty(fun) );' newline ...
            '    fun = ''' name '''; end;' newline ...
            'switch nargin;' newline ...
            '    case 0;    [' varout2 '] = ' callingfunction '( fun );' newline ...
            '    case 1;    [' varout2 '] = ' callingfunction '( fun, i1 );' newline ...
            '    case 2;    [' varout2 '] = ' callingfunction '( fun, i1, i2 );' newline ...
            '    case 3;    [' varout2 '] = ' callingfunction '( fun, i1, i2, i3 );' newline ...
            '    case 4;    [' varout2 '] = ' callingfunction '( fun, i1, i2, i3, i4 );' newline ...
            '    case 5;    [' varout2 '] = ' callingfunction '( fun, i1, i2, i3, i4, i5 );' newline ...
            '    case 6;    [' varout2 '] = ' callingfunction '( fun, i1, i2, i3, i4, i5, i6 );' newline ...
            '    case 7;    [' varout2 '] = ' callingfunction '( fun, i1, i2, i3, i4, i5, i6, i7 );' newline ...
            '    case 8;    [' varout2 '] = ' callingfunction '( fun, i1, i2, i3, i4, i5, i6, i7, i8 );' newline ...
            '    case 9;    [' varout2 '] = ' callingfunction '( fun, i1, i2, i3, i4, i5, i6, i7, i8, i9 );' newline ...
            '    case 10;   [' varout2 '] = ' callingfunction '( fun, i1, i2, i3, i4, i5, i6, i7, i8, i10 );' newline ...
            '    otherwise; [' varout2 '] = ' callingfunction '( fun, i1, i2, i3, i4, i5, i6, i7, i8, i10, varargin{:} ); end;' newline newline ...
        ];
    
    endtxt = [
        'TTEST_ENABLE_INJECTION = OLDVAL_TTEST_ENABLE_INJECTION;' newline ...
        'rng( r ); %restore random number generator state' newline ...
        'end' newline newline ...
        'function dummy; end %#ok<DEFNU> %Generates an error, if the ''end'' of a function is missing.' newline ];
    
    text = [message header pretxt infostuff inputvar outputvar faulttxt mutationtxt precall calltxt postcall endtxt];
    
    if( isempty(classname) );
        filename = [fullfile( pwd, TTEST_DIR_INJECT, name ) '.m'];
    else;
        filename = [fullfile( pwd, TTEST_DIR_INJECT, ['@' classname], name ) '.m']; end;
    TTEST_SAVE( text, filename, MESSAGE );
        
end

function deleteinjects
    MESSAGE = '%TTEST_AUTOGENERATE_INJECT'; 
    global TTEST_DIR_INJECT;
    
    olddir = cd( fileparts(which('INJECT')) );
    
    %remove files
    d = dir( [ TTEST_DIR_INJECT '/**'] );
    for i = 1:numel( d );
        if( d(i).isdir );
            continue; end;
        f = fullfile( d(i).folder, d(i).name );
        TTEST_DELETE( f, MESSAGE ); end;
    
    %remove folders
    d = dir( [ TTEST_DIR_INJECT '/**'] );    
    for i = 1:numel( d );
        if( strcmp(d(i).folder,fullfile( pwd, TTEST_DIR_INJECT )) );
            continue; end;
        [~] = rmdir( d(i).folder ); end; %this suceeds only if the folder is empty
    
    dir( olddir );
end

function dummy; end %#ok<DEFNU> %Generates an error, if the 'end' of a function is missing.

% This Source Code Form is subject to the terms of the Mozilla Public
% License, v. 2.0. If a copy of the MPL was not distributed with this
% file, You can obtain one at https://mozilla.org/MPL/2.0/.
