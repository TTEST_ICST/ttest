function varargout = TTEST_DEAL( varargin );
% [ varargout ] = TTEST_DEAL( varargin );
% assigns the nth input to the nth output
% surplus inputs are discarded
% surplus outputs are set to []
%


varargout = cell( 1, nargout );
if( nargout>nargin);
    [varargout{1:nargin}] = varargin{1:nargin};
    [varargout{nargin+1:nargout}] = deal( [] );
else;
    [varargout{1:nargout}] = varargin{1:nargout}; end;

end

function dummy; end %#ok<DEFNU> %Generates an error, if the 'end' of a function is missing.

% This Source Code Form is subject to the terms of the Mozilla Public
% License, v. 2.0. If a copy of the MPL was not distributed with this
% file, You can obtain one at https://mozilla.org/MPL/2.0/.
