%TTEST_AUTOGENERATE
%This file is auto-generated. Do not modify it. Changes may be overwritten.
function [ret,varargout] = EXPECT_DEFINED( varargin )

%% %%%%%%%%%%%
% get test_id
%%%%%%%%%%%%%%
if( numel(varargin)>=1 && isa(varargin{1},'ttest_id_c') );
    id = varargin{1};
    varargin(1) = [];
elseif( evalin( 'caller', 'exist(''ttest_id'',''var'');' ) ); %id not given
    id = evalin( 'caller', 'ttest_id;' );
else
    id = ttest_id_c( 0 ); end;
% enableinjection = TTEST( id, 'inject', false ); %disable injection and get current state
% cleanup_injection = onCleanup( @() TTEST( id, 'inject', enableinjection ) );

errorhandler = TTEST( id, 'errorexpect' );
successhandler = TTEST( id, 'successexpect' );
%% %%%%%%%%%%%%%%%%%%%%%%%%%
% parse ttest_errorlvl_c
%%%%%%%%%%%%%%%%%%%%%%%%%%%%
idx = cellfun( 'isclass', varargin, 'ttest_errorlvl_c' );
if( any(idx) );
    errorhandler = varargin{idx}(); 
    varargin(idx) = []; end;
if( isempty(errorhandler) );
   errorhandler = 'exp'; end;
if( isequal(errorhandler,'nil') );
    TTEST_FPRINTF( id, 'Test skipped.\n' );
    ret = true;
    return; end;

%% %%%%%%%%%%%%%%%%%%%%%%
% set some variables
%%%%%%%%%%%%%%%%%%%%%%%%%
testn = 2; % THIS NUMBER IS ALSO FUNCTION SPECIFIC
ret = true;
errid = '';
errmsg = '';

%% %%%%%%%%%%%%%%%%%%%%%%%%%
% parse PARAM begins
%%%%%%%%%%%%%%%%%%%%%%%%%%%
idx = cellfun( 'isclass', varargin, 'TTEST_PARAM' );
try;
    if( any(idx) );
        skipsingletest = true;
        fun = mfilename; %test to be executed 
        idx = find( idx, 1 ); %find PARAM options
        cmd = varargin(1:idx-1);
        [testn,arg] = TTEST_PARSE_PARAM( testn, numel(cmd), varargin{idx(1)+1:end} ); %generate parameters

        %make functions calls
        stringflag = all( cellfun('isclass',cmd,'string') | cellfun('isclass',cmd,'char') ) && ~any( cellfun('isclass',cmd,'function_handle') );

        if( stringflag ); %everything is given as a string
            varname = arrayfun( @(i) char(['a'+i-1 '_']), 1:size(arg,2), 'UniformOutput',false ); %strings of occuring variables ( a_, ... )

            if( isequal(testn,0) ); % not-'inline' case
                cmd_ = cellfun( @(x) [x ','], cmd, 'UniformOutput',false ); %the arguments
                cmd_  = [cmd_{:}];
                cmd_(end) = [];
                for j = 1:size( arg, 1 );
                    for k = 1:size( arg, 2 );
                        assignin( 'caller', varname{k}, arg{j,k} ); end; %store a_,... variables in caller workspace
                    rngstate = rng();
                    ret = ret & evalin( 'caller', [fun '( ' cmd_ ' );' ] );
                    if( ~ret );
                        h = evalin( 'caller', ['@() ' fun '( ' cmd_ ' );' ] );
                        TTEST_SAVEEXAMPLE( id, h, rngstate );
                        break; end; end; %execute command
                if( ~ret );
                    TTEST_FPRINTF( id, '\nTest case failed with params:\n===============================\n' );
                    for i = 1:numel( varname );
                        TTEST_FPRINTF( id, '%s : %s\n', varname{i}, TTEST_DISP(id,arg{j,i}) ); end; end;

            else; % 'inline' case
                assignin( 'caller', 'expandedarg_', {} ); %storage for expanded arguments
                %store variables in caller workspace
                for k = 1:testn(1)-1;%arguments before repetition
                    evalin( 'caller', ['expandedarg_{end+1} = ' cmd{k} ';'] ); end;
                for j = 1:size( arg, 1 ); %loop through all argument combinations
                    for k = 1:size( arg, 2 );
                        assignin( 'caller', varname{k}, arg{j,k} ); end; %repeated arguments
                    for k = testn(1):testn(2)-1;
                        evalin( 'caller', ['expandedarg_{end+1} = ' cmd{k} ';'] ); end; end;
                for k = testn(2):numel( cmd ); %arguments after repetition
                    evalin( 'caller', ['expandedarg_{end+1} = ' cmd{k} ';'] ); end;
                rngstate = rng();
                ret = evalin( 'caller', [fun '( expandedarg_{:} );'] );
                if( ~ret );
                    h = evalin( 'caller', ['@() ' fun '( expandedarg_{:} );'] ); %get function handle including whole workspace
                    TTEST_SAVEEXAMPLE( id, h, rngstate ); end; end;

            %clean up
            val = cellfun( @(x) [x ' '], varname, 'UniformOutput',false ); %delete a_,... variables in caller workspace
            evalin( 'caller', ['clear ' [val{:}] 'expandedarg_'] );
            clear cmd_;

        else; %everything is given as a function handle, or should be made to one
            varname = cell( 1, numel(cmd) );
            [varname{:}] = deal( {} );
            for i = 1:numel( cmd );
                if( ~isa(cmd{i},'function_handle') ); %make everything to function handles
                    cmd{i} = @() cmd{i}; end;
                %get variable names
                var = func2str( cmd{i} );
                var = [',' var(3:find(var==')',1) - 1) ',']; %remove '@('  and ')', add ',' so that it is easier to parse
                idx = find( var == ',' );  %find ','
                for j = 1:numel( idx ) - 1;
                    val = var(idx(j)+1:idx(j+1)-1);
                    if( ~isempty(val) );
                        varname{i}{end+1} = val; end; end; end; %store variable names
            [allvarname,~,varidx] = unique( [varname{:}] ); %sort variable names and give them integer values
            varidx = mat2cell( varidx.', 1, cellfun('prodofsize',varname) );

            if( isequal(testn,0) ); % not-'inline' case
                expandedarg_ = cell( size(cmd) );
                for j = 1:size( arg, 1 );
                    for i = 1:numel( cmd );
                        expandedarg_{i} = cmd{i}(arg{j,varidx{i}}); end;
                    rngstate = rng();
                    %we need to pass the id!
                    ret = ret & feval( fun, id, expandedarg_{:} );
                    if( ~ret );
                        TTEST_SAVEEXAMPLE( id, @() feval( fun, expandedarg_{:} ), rngstate );
                        break; end; end;
                if( ~ret );
                    TTEST_FPRINTF( id, '\nTest case failed with params:\n===============================\n' );
                    for i = 1:numel( allvarname );
                        TTEST_FPRINTF( id, '%s : %s', allvarname{i}, TTEST_DISP(id,arg{j,i}) ); end; end;

            else; % 'inline' case
                expandedarg_ = cell( 1, 0 );
                for k = 1:testn(1)-1;
                    expandedarg_{end+1} = cmd{k}(); end; %#ok<AGROW>
                for j = 1:size( arg, 1 ); %loop through all argument combinations
                    for k = testn(1):testn(2)-1;
                        expandedarg_{end+1} = cmd{k}( arg{j,varidx{k}} ); end; end; %#ok<AGROW>
                for k = testn(2):numel( cmd ); %arguments after repetition
                    expandedarg_{end+1} = cmd{k}(); end; %#ok<AGROW>
                rngstate = rng();
                %we need to pass the id!
                ret = ret & feval( fun, id, expandedarg_{:} );
                if( ~ret );
                    TTEST_SAVEEXAMPLE( id, @() feval( fun, expandedarg_{:} ), rngstate ); end; end; end;

    else;
        skipsingletest = false; end;
catch me;
    ret = false;
    TTEST_FPRINTF( id, '\n TTEST: Generation of parameters failed with error:\n%s\n', getReport(me) );
    errid = 'TTEST:ParamGeneration';
    errmsg = 'Generation of parameters failed.'; end;
    
%% %%%%%%%%%%%%%%%%%%%%%%%%%
% parse PARAM ends
%%%%%%%%%%%%%%%%%%%%%%%%%%%    
%% 
if( ~skipsingletest );
    try;
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% function specific part begins
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        assert( nargin==1, 'TTEST:nargin', 'wrong number of input arguments.' );
        varargout = cell( 1, nargout-1 );
        ttest_p = gcp(); 
        cmd = varargin{1};
        if( isstring(cmd) || ischar(cmd) );
            assignin( 'caller', 'ttest_p', ttest_p );
            F = evalin( 'caller', ['parfeval( ttest_p, @() ' cmd ', ' num2str(max(0,nargout-1)) ' )'] );
        else;
            F = parfeval( ttest_p, cmd, max(0,nargout-1) ); end;
        ret = wait( F, 'running', 5 );
        assert( ret, 'TTEST:Defined',  'Error while trying to execute the function.' );
        pause( 0.2 );
        if( ~isempty( F.Error ) && numel(F.Error.stack)<=1 );
            ret = false;
            TTEST_FPRINTF( id, 'Command not defined (but it should), where\n  Command:    %s\n  Error id:   %s\n  Error msg:  %s\n', TTEST_DISP(id,cmd), F.Error.identifier, F.Error.message );
        else;
            ret = true; end;
        cancel( F(1) );

%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% function specific part ends
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

ret = isAlways( ret, 'Unknown','false' );
catch me;
    ret = false;
    errid = me.identifier;
    warning( me.identifier, '%s', me.message ); end; end;

if( ret );
    if( isa(successhandler,'function_handle') );
        ret = successhandler( id, ret, errid, errmsg );
    else;  %string
        ret = eval( successhandler ); end;
else;
    if( isempty(errid) );
        errid = 'TTEST:NotDefined'; end;
    if( isa(errorhandler,'function_handle') );
        ret = errorhandler( id, ret, errid, errmsg );
    else;  %string
        ret = eval( errorhandler ); end; end;
end

function dummy; end %#ok<DEFNU> %Generates an error, if the 'end' of a function is missing.

% This Source Code Form is subject to the terms of the Mozilla Public
% License, v. 2.0. If a copy of the MPL was not distributed with this
% file, You can obtain one at https://mozilla.org/MPL/2.0/.
