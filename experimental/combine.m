function A = combine( varargin );
% This is actually allcomb from Jos van der Geest
% (c) Jos van der Geest

    n = nargin;
    for i = 1:nargin;
        if( ~iscell(varargin{i}) );
            varargin{i} = varargin(i); end; end;
    if( any(cellfun('isempty', varargin)) );  % check for empty inputs
        A = {cell( 0, n )};
    elseif( n == 0 );  % no inputs
        A = {cell( 0, 0 )}; 
    elseif( n == 1 );  % a single input, nothing to combine
        A = varargin{1}(:); 
    else;
        ix = cellfun( @(c) 1:numel(c), varargin, 'UniformOutput',false );  % use to indices to get all combinations
        [ix{n:-1:1}] = ndgrid( ix{n:-1:1} );  % flip using ii if last column is changing fastest
        A = cell( numel(ix{1}), n ) ;  % pre-allocate the output
        for k = 1:n;
            A(:,k) = reshape( varargin{k}(ix{k}), [], 1 ); end; end;  % combine

    A = arrayfun( @(i) A(i,:), 1:size(A,1), 'UniformOutput',false );  % make to cell array
end

function dummy; end %#ok<DEFNU> %Generates an error, if the 'end' of a function is missing.

% This Source Code Form is subject to the terms of the Mozilla Public
% License, v. 2.0. If a copy of the MPL was not distributed with this
% file, You can obtain one at https://mozilla.org/MPL/2.0/.
